#!/usr/bin/python

import dev_appserver
dev_appserver.fix_sys_path()

import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../../', 'src'),)
os.environ['SERVER_PORT'] = "80"
os.environ['SERVER_NAME'] = "space"

import appengine_config

from google.appengine.ext.remote_api import remote_api_stub
import getpass

def auth_func():
    return (raw_input('Username: '), getpass.getpass('Password: '))

remote_api_stub.ConfigureRemoteApi(None, '/_ah/remote_api', auth_func, 'qburst-activities.appspot.com')


class Models(object):
    PROJECT = 'Project'
    EMPLOYEE = 'Employee'

def convert_key(key, kind):
    from google.appengine.ext.db import Key

    if not key:
        return u''

    key = Key(key)
    to_app = 's~qburst-space'
    if key.app() == to_app:
        return unicode(key)

    return unicode(Key.from_path(kind, key.id(), _app=to_app))


batch_size = 100


class EmployeeActivitiesLogKeyFixer(object):
    def run(self, is_debug, count=None):
        from google.appengine.ext.db import put
        from activity.log.models import EmployeeActivityLog

        counter = 0
        query = EmployeeActivityLog.query()
        emp_logs = query.fetch(batch_size)
        pls = []
        while emp_logs:
            for emp_log in emp_logs:
                if counter == count:
                    break
                newEmployeeKey = convert_key(emp_log.employee, Models.EMPLOYEE)
                newProjectList = []
                for project in emp_log.projects:
                    newProjectList.append(convert_key(project, Models.PROJECT))
                newGroupedDic = {}
                for project, hours in emp_log.groupedByProject.iteritems():
                    newGroupedDic[convert_key(project, Models.PROJECT)] = hours
                if is_debug:
                    print 'Current Employee: %s New Employee: %s' % (emp_log.employee, newEmployeeKey)
                    print 'Current Project: %s New Project: %s' % (emp_log.projects, newProjectList)
                    print 'Current groupedByProject: %s New groupedByProject: %s' % (emp_log.groupedByProject, newGroupedDic)
                else:
                    emp_log.employee = newEmployeeKey
                    emp_log.projects = newProjectList
                    emp_log.groupedByProject = newGroupedDic
                    pls.append(emp_log)
                counter += 1

            put(pls)
            print 'Processed: ', counter
            pls = []
            if counter == count:
                break
            query.with_cursor(query.cursor())
            emp_logs = query.fetch(batch_size)


if __name__ == '__main__':
    EmployeeActivitiesLogKeyFixer().run(is_debug=True, count=1)

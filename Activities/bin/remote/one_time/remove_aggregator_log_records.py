# setup environment
#### Common stuff to get remote API to work
import imp
import os
environment = imp.load_source('environment', os.path.join(os.path.dirname(os.path.realpath(__file__)), './', 'environment.py'))
environment.setup(remote_api_uri='/_ah/remote_api')

#### End remote API stuff
# django libraries
from django.conf import settings

print settings.EMAIL_USERNAME

from datetime import date

from google.appengine.ext import ndb

# models and enums
from activity.log.models import ProjectLog, EmployeeActivityLog


BATCH = 400


class RemoveAggregatorLogs(object):
    def run(self, is_debug, month, year):
        to_delete = []
        e_c = 0
        p_c = 0
        logged_for = date(day=1, month=month, year=year)
        emp_log_query = EmployeeActivityLog.query().filter(EmployeeActivityLog.loggedFor == logged_for)
        emp_log_query = emp_log_query.iter()
        for e_c, i in enumerate(emp_log_query):
            to_delete.append(i.key)
            # Delete log records
            if not is_debug and (e_c+1) % BATCH == 0:
                ndb.delete_multi(to_delete)
                to_delete = []
        proj_log_query = ProjectLog.query().filter(ProjectLog.loggedFor == logged_for)
        proj_log_query = proj_log_query.iter()
        for p_c, i in enumerate(proj_log_query):
            to_delete.append(i.key)
            # Delete log records
            if not is_debug and (p_c + 1) % BATCH == 0:
                ndb.delete_multi(to_delete)
                to_delete = []
        print 'Found %s employee log records and %s proj log records....' % (e_c + 1, p_c + 1)
        # final Delete log records
        if not is_debug:
            ndb.delete_multi(to_delete)
            print 'Completed deleting %s records....' % (e_c + p_c + 2)

if __name__ == '__main__':
    """
    Execute remote scripts
    """
    print 'Going to run `RemoveAggregatorLogs`'
    month = raw_input('Enter the month:')
    year = raw_input('Enter the year:')
    RemoveAggregatorLogs().run(is_debug=True, month=int(month), year=int(year))
    print 'Scripts completed successfully'

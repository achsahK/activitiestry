#!/usr/bin/python

import dev_appserver
dev_appserver.fix_sys_path()

import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../../', 'src'),)
os.environ['SERVER_PORT'] = "80"
os.environ['SERVER_NAME'] = "space"

import appengine_config

from google.appengine.ext.remote_api import remote_api_stub
import getpass


def auth_func():
    return (raw_input('Email: '), getpass.getpass('Password: '))

remote_api_stub.ConfigureRemoteApi(None, '/_ah/remote_api', auth_func, 'qburst-activities.appspot.com')


class Models(object):
    PROJECT = 'Project'
    EMPLOYEE = 'Employee'
    GRADE = 'Grade'
    DESIGNATION = 'Designation'
    DEPARTMENT = 'Department'


def convert_key(key, kind):
    from google.appengine.ext.db import Key

    if not key:
        return u''

    key = Key(key)
    to_app = 's~qburst-space'
    if key.app() == to_app:
        return unicode(key)

    return unicode(Key.from_path(kind, key.id(), _app=to_app))


batch_size = 100


class ProjectLogKeyFixer(object):
    def run(self, is_debug, count=None):
        from google.appengine.ext.db import put
        from activity.log.models import ProjectLog

        counter = 0
        query = ProjectLog.query()
        project_logs = query.fetch(batch_size)
        pls = []
        while project_logs:
            for project_log in project_logs:
                if counter == count:
                    break

                # Process project data
                project = convert_key(project_log.project, Models.PROJECT)
                # Process employees data
                employees = []
                for employee in project_log.employees:
                    employees.append(convert_key(employee, Models.EMPLOYEE))
                # Process groupedByDepartment data
                groupedByDepartment = {}
                for department, hours in project_log.groupedByDepartment.iteritems():
                    groupedByDepartment[convert_key(department, Models.DEPARTMENT)] = hours
                # Process groupedByEmployee data
                groupedByEmployee = {}
                for employee, hours in project_log.groupedByEmployee.iteritems():
                    groupedByEmployee[convert_key(employee, Models.EMPLOYEE)] = hours
                # Process groupedByGrade data
                groupedByGrade = {}
                for grade, hours in project_log.groupedByGrade.iteritems():
                    groupedByGrade[convert_key(grade, Models.GRADE)] = hours

                if is_debug:
                    print ('''
\033[1;33mProject\033[0m
Current: %s    New: %s
\033[1;33mEmployees\033[0m
Current: %s    New: %s
\033[1;33mGroupedByDepartment\033[0m
Current: %s    New: %s
\033[1;33mGroupedByEmployee\033[0m
Current: %s    New: %s
\033[1;33mGroupedByGrade\033[0m
Current: %s    New: %s
                    ''' % (
                        project_log.project, project,
                        project_log.employees, employees,
                        project_log.groupedByDepartment, groupedByDepartment,
                        project_log.groupedByEmployee, groupedByEmployee,
                        project_log.groupedByGrade, groupedByGrade
                    )).strip()
                else:
                    project_log.project = project
                    project_log.employees = employees
                    project_log.groupedByDepartment = groupedByDepartment
                    project_log.groupedByEmployee = groupedByEmployee
                    project_log.groupedByGrade = groupedByGrade
                    pls.append(project_log)

                counter += 1

            if counter == count:
                break
            put(pls)
            print 'Processed: ', counter
            pls = []
            query.with_cursor(query.cursor())
            project_logs = query.fetch(batch_size)


if __name__ == '__main__':
    ProjectLogKeyFixer().run(is_debug=True, count=1)
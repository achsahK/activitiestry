# setup environment
#### Common stuff to get remote API to work
import imp
import os
environment = imp.load_source('environment', os.path.join(os.path.dirname(os.path.realpath(__file__)), './', 'environment.py'))
environment.setup(remote_api_uri='/_ah/remote_api')

#### End remote API stuff

# python libraries
import csv
from datetime import datetime

# django libraries
from django.conf import settings

# app engine libraries
from google.appengine.ext import db

# models and enums
from activity.log.models import Message

# constants
BATCH_SIZE = 400


class BulkInsertDailyStatus(object):
    """
    Utility script to bulk insert daily status messages.
    This is useful for anyone who wants to post daily status backlog.
    IMPORTANT NOTE: Use this with care, it should only be used with approval.
                    Remember to update the date range in activity.log.crons.ActivitiyDataAggregatorCron so that the
                    messages are picked up.
                    self.start_date = get_future_datetime(self.end_date, days=-90)
    """
    @staticmethod
    def run(is_debug, fle, user):
        to_put = []
        count = 0

        print "Going to bulk insert from %s for employee: %s" % (fle, user)

        with open(fle, 'r') as csv_file:
            reader = csv.DictReader(csv_file)
            for row in reader:
                count += 1

                to_put.append(Message(
                    activityType=row['activityType'],
                    department=row['department'],
                    designation=row['designation'],
                    grade=row['grade'],
                    isProcessed=False,  # Is always going to be False since we need to process
                    message=row['message'],
                    postedBy=user,
                    postedFor=datetime.strptime((row['postedFor']), '%d/%m/%Y'),
                    # postedOn=row['postedOn'],  # Defaults to noe "auto_now_add"
                    project=row['project'],
                    status="PENDING",  # Currently we only have "PENDING"
                    timeSpent=float(row['timeSpent']),
                    type="end-of-day",  # Currently we only have "end-of-day"
                    user=user
                ))

                # Save messages when count reaches BATCH_SIZE
                if count % BATCH_SIZE == 0 and not is_debug:
                    print "Going to insert batch, current count %s" % count
                    db.put(to_put)
                    to_put = []

        if to_put and not is_debug:
            print "Going to insert batch, current count %s" % count
            db.put(to_put)

        print "All Done. Inserted %s messages." % count


if __name__ == '__main__':
    print "Bulk insert daily status for employee"
    BulkInsertDailyStatus.run(is_debug=True, fle=raw_input("File: "), user=raw_input("Employee Key: "))

#! /usr/bin/python

"""
This file performs the initial steps for using remote api

Usage:
import initial_configuration
initial_configuration.setup('qburst-activities-staging.appspot.com')
"""

# python libraries
import os

# app engine libraries
from google.appengine.ext.remote_api import remote_api_stub


# function to configure remote api
def setup(app_id=None, remote_api_uri='/remote_api'):

    # validate app id
    if not app_id:
        app_id = raw_input('Application ID (eg: qburst-activities-staging): ')
    else:
        pass

    app_url = app_id + ".appspot.com"

    # set the environment variable for OAuth login, we will use the app url as filename
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = os.path.expanduser('~/Dev/Space/private-keys/' + app_id + '.json')

    # validate remote api handler
    if not remote_api_uri:
        remote_api_uri = raw_input('Remote API handler URI (eg: /remote_api): ')
    else:
        pass

    # configure remote api
    remote_api_stub.ConfigureRemoteApiForOAuth(app_url, remote_api_uri)

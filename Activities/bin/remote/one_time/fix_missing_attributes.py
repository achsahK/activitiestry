# setup environment
import environment
environment.setup()

# python libraries
from urllib2 import URLError

# django libraries
from django.conf import settings

# app engine libraries
from google.appengine.api.urlfetch_errors import ConnectionClosedError, DeadlineExceededError, DownloadError

# models and enums
from activity.log.models import Message, ActivityType, MessageStatus

# others
from space.utils.decorators import retry

# constants
BATCH_SIZE = 400
COUNTER_LIMIT = 400


class FixMessageAttributes(object):
    """
    Remote script to fix missing attributes in message. Need to run this script after adding new fields to the message
    expando model in future.
    """
    @staticmethod
    def run(is_debug=True):
        count = 0
        total_count = 0
        objects_to_save = []

        # debug mode alert
        if is_debug:
            print 'Note: Script is running in debug mode. Changes wont be persisted.'

        # fetch all messages
        query = Message.all()
        messages = query.fetch(BATCH_SIZE)
        while messages:
        
            # process message
            for message in messages:
                total_count += 1

                # set flag
                message_updated = False

                # check status
                if not hasattr(message, 'status'):
                    print "Missing status! Key: %s, Updating with pending..." % str(message.key())
                    message.status = MessageStatus.PENDING
                    message_updated = True

                # check activityType
                if not hasattr(message, 'activityType'):
                    print "Missing activityType! Key: %s, Updating with other..." % str(message.key())
                    message.activityType = ActivityType.OTHER
                    message_updated = True

                # check project
                if not hasattr(message, 'project'):
                    print "Missing project! Key: %s, Updating with empty string..." % str(message.key())
                    message.project = ''
                    message_updated = True

                # check department
                if not hasattr(message, 'department'):
                    print "Missing department! Key: %s, Updating with None..." % str(message.key())
                    message.department = None
                    message_updated = True

                # check designation
                if not hasattr(message, 'designation'):
                    print "Missing designation! Key: %s, Updating with None..." % str(message.key())
                    message.designation = None
                    message_updated = True

                # check grade
                if not hasattr(message, 'grade'):
                    print "Missing grade! Key: %s, Updating with None..." % str(message.key())
                    message.grade = None
                    message_updated = True

                # mark message for saving
                if message_updated:
                    count += 1
                    objects_to_save.append(message)

                # save messages when count reaches BATCH_SIZE
                if count >= BATCH_SIZE:
                    FixMessageAttributes._save_messages(objects_to_save, is_debug)
                    objects_to_save = []
                    count = 0

                # print total_count after every 100 messages
                if total_count % COUNTER_LIMIT == 0:
                    print 'Processed %s messages' % total_count

            # fetch next set of data
            query.with_cursor(query.cursor())
            messages = query.fetch(BATCH_SIZE)

        # save pending messages if any
        if objects_to_save:
            FixMessageAttributes._save_messages(objects_to_save, is_debug)

        print 'Script completed successfully. Processed %s messages' % total_count

    @staticmethod
    @retry(tries=4, exceptions=(ConnectionClosedError, DeadlineExceededError, DownloadError, URLError))
    def _save_messages(message_list, is_debug=True):
        """
        Save updated message instances
        """
        if not is_debug:
            print "Saving updated message instances..."
            Message.save_multi(message_list)

if __name__ == '__main__':
    print "Fixing corrupted messages..."
    FixMessageAttributes.run(is_debug=True)

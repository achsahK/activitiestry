# setup environment
import environment
environment.setup(remote_api_uri='/_ah/remote_api')

# python libraries
import time
from urllib2 import URLError
from datetime import date

# django libraries
from django.conf import settings

# app engine libraries
from google.appengine.api.urlfetch_errors import ConnectionClosedError, DeadlineExceededError, DownloadError
from google.appengine.ext import db

# models and enums
from activity.log.models import Message
from utils import convert_minute_to_calculatable_value

# others
from space.utils.decorators import retry

# constants
BATCH_SIZE = 400


class FixMessageTimeSpent(object):
    """
    Remote script to fix timeSpent, in previous releases timeSpent saved in wrong formats, and in wrong types.
    This script is to fix the corrupted data in messages project log and activity log.
    """
    @staticmethod
    def run(is_debug=True):
        count = 0
        updated_count = 0
        total_count = 0
        objects_to_save = []

        # debug mode alert
        if is_debug:
            print 'Note: Script is running in debug mode. Changes wont be persisted.'

        wrong_time_spent = ['0.15', '0.30', '0.45']
        # fetch all messages
        query = Message.all().filter('postedOn >=', date(2015, 7, 29)).filter('postedOn <=', date.today())
        messages = FixMessageTimeSpent._fetch(query) # query.fetch(BATCH_SIZE)
        while messages:
            # process message
            for message in messages:
                total_count += 1
                update_message = False

                print "Time Spent: " + str(message.timeSpent)
                print "Type: " + str(type(message.timeSpent))
                print "Key: %s\nPosted On: %s\nPosted For: %s" % (message.key(), message.postedOn, message.postedFor)

                time_spent = float(message.timeSpent)
                if time_spent > 24:
                    time_spent = 24
                ts = time_spent % 1
                if '%.2f' % ts in wrong_time_spent:
                    t = time.strptime(str(time_spent), "%H.%M")
                    minutes = convert_minute_to_calculatable_value(t.tm_min)
                    message.timeSpent = float('%s.%s' % (int(time_spent), minutes))
                    update_message = True
                elif isinstance(message.timeSpent, str) or isinstance(message.timeSpent, unicode):
                    message.timeSpent = float(message.timeSpent)
                    update_message = True

                if update_message:
                    print "Updated time spent: %s" % message.timeSpent
                    print "Update time spent type: %s" % str(type(message.timeSpent))
                    # Append message to list for saving
                    count += 1
                    updated_count += 1
                    objects_to_save.append(message)

                # save messages when count reaches BATCH_SIZE
                if count >= BATCH_SIZE:
                    FixMessageTimeSpent._save_messages(objects_to_save, is_debug)
                    objects_to_save = []
                    count = 0

            # print total_count after every 400 messages
            print 'Processed %s messages' % total_count

            # fetch next set of data
            query.with_cursor(query.cursor())
            messages = FixMessageTimeSpent._fetch(query) #query.fetch(BATCH_SIZE)

        # save pending messages if any
        if objects_to_save:
            FixMessageTimeSpent._save_messages(objects_to_save, is_debug)

        print 'Script completed successfully. Processed %s messages' % total_count
        print 'Updated %s messages' % updated_count

    @staticmethod
    @retry(tries=4, exceptions=(ConnectionClosedError, DeadlineExceededError, DownloadError, URLError))
    def _fetch(query):
        return query.fetch(BATCH_SIZE)

    @staticmethod
#    @retry(tries=4, exceptions=(ConnectionClosedError, DeadlineExceededError, DownloadError, URLError))
    def _save_messages(message_list, is_debug=True):
        """
        Save updated message instances
        """
        if not is_debug:
            print "Saving updated message instances..."
            db.put(message_list)
            #Message.save_multi(message_list)

if __name__ == '__main__':
    print "Fixing corrupted time spent in messages..."
    FixMessageTimeSpent.run(is_debug=True)

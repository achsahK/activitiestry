# setup environment
import environment
environment.setup()

# python libraries
from datetime import date

# models
from activity.log.models import ProjectLog, EmployeeActivityLog

# constants
START_DATE = date(day=1, month=8, year=2015)
END_DATE = date(day=1, month=10, year=2015)


class MergeDuplicateProjectLogs(object):
    """
    Remote script to merge duplicate project logs
    """

    def __init__(self, is_debug=True):
        self.is_debug = is_debug

        # show debug mode notification
        if is_debug:
            print 'Note: `MergeDuplicateProjectLogs` running in debug mode'

    def run(self):

        project_logs_dict = {}

        # fetch corrupted project logs
        print 'Fetching project logs between %s and %s' % (START_DATE.strftime('%d/%m/%Y'), END_DATE.strftime('%d/%m/%Y'))
        # TODO: Need-to-fix will not work because of make_query issues
        project_logs = ProjectLog.make_query(
            filters=[
                {'property': 'loggedFor', 'operator': '>=', 'value': START_DATE},
                {'property': 'loggedFor', 'operator': '<=', 'value': END_DATE}
            ],
            iterator_only=True
        )

        # prepare project log dict
        print 'Preparing project logs dict'
        for project_log in project_logs:

            if (project_log.project, project_log.loggedFor) not in project_logs_dict.keys():
                project_logs_dict[(project_log.project, project_log.loggedFor)] = [project_log]
            else:
                project_logs_dict[(project_log.project, project_log.loggedFor)].extend([project_log])

        # project_logs_dict = {
        #     (project, loggedFor): [project-log],
        #     (project, loggedFor): [project-log, project-log],
        #     ................................................
        #     ...............................................
        # }

        for key, value in project_logs_dict.iteritems():

            # check whether the loggedFor date has multiple records
            if len(value) > 1:

                print 'Duplicate record found'
                master_object = None
                objects_to_delete = []

                # iterate through multiple records
                for item in value:

                    if not master_object:
                        master_object = item
                        continue

                    # update employees
                    master_object.employees.extend(item.employees)
                    master_object.employees = list(set(master_object.employees))

                    # update groupedByGrade
                    for k, v in item.groupedByGrade.iteritems():
                        if k in master_object.groupedByGrade.keys():
                            master_object.groupedByGrade[k] += v
                        else:
                            master_object.groupedByGrade[k] = v

                    # update groupedByEmployee
                    for k, v in item.groupedByEmployee.iteritems():
                        if k in master_object.groupedByEmployee.keys():
                            master_object.groupedByEmployee[k] += v
                        else:
                            master_object.groupedByEmployee[k] = v

                    # update groupedByDepartment
                    for k, v in item.groupedByDepartment.iteritems():
                        if k in master_object.groupedByDepartment.keys():
                            master_object.groupedByDepartment[k] += v
                        else:
                            master_object.groupedByDepartment[k] = v

                    # update actualHoursBurned
                    master_object.actualHoursBurned += item.actualHoursBurned

                    # update hoursBurned
                    master_object.hoursBurned += item.hoursBurned

                    # update activities
                    master_object.activities.extend(item.activities)
                    master_object.activities = list(set(master_object.activities))

                    # update groupedByActivityType
                    for k, v in item.groupedByActivityType.iteritems():
                        if k in master_object.groupedByActivityType.keys():
                            master_object.groupedByActivityType[k] += v
                        else:
                            master_object.groupedByActivityType[k] = v

                    # mark project_log for deletion
                    objects_to_delete.append(item)

                # save updated project log
                if not self.is_debug:
                    print 'Saving corrected record'
                    master_object.save()

                # remove duplicate project logs
                if not self.is_debug:
                    print 'Deleting duplicates'
                    # TODO: Need-to-fix will not work because of make_query issues
                    ProjectLog.delete_multi([x.key() for x in objects_to_delete])


class MergeDuplicateEmployeeActivityLogs(object):
    """
    Remote script to merge duplicate employee activity logs
    """

    def __init__(self, is_debug=True):
        self.is_debug = is_debug

        # show debug mode notification
        if is_debug:
            print 'Note: `MergeDuplicateEmployeeActivityLogs` running in debug mode'

    def run(self):

        employee_activity_logs_dict = {}

        # fetch corrupted employee activity logs
        print 'Fetching project logs between %s and %s' % (START_DATE.strftime('%d/%m/%Y'), END_DATE.strftime('%d/%m/%Y'))
        # TODO: Need-to-fix will not work because of make_query issues
        employee_activity_logs = EmployeeActivityLog.make_query(
            filters=[
                {'property': 'loggedFor', 'operator': '>=', 'value': START_DATE},
                {'property': 'loggedFor', 'operator': '<=', 'value': END_DATE}
            ],
            iterator_only=True
        )

        # prepare employee activity logs dict
        for employee_activity_log in employee_activity_logs:

            if (employee_activity_log.employee, employee_activity_log.loggedFor) not in employee_activity_logs_dict.keys():
                employee_activity_logs_dict[(employee_activity_log.employee, employee_activity_log.loggedFor)] = [employee_activity_log]
            else:
                employee_activity_logs_dict[(employee_activity_log.employee, employee_activity_log.loggedFor)].extend([employee_activity_log])

        # employee_activity_logs_dict = {
        #     (employee, loggedFor): [employee-activity-log],
        #     (employee, loggedFor): [employee-activity-log, employee-activity-log],
        #     ....................................................................
        #     ....................................................................
        # }

        for key, value in employee_activity_logs_dict.iteritems():

            # check whether the loggedFor date has multiple records
            if len(value) > 1:

                print 'Duplicate record found'
                master_object = None
                objects_to_delete = []

                for item in value:

                    # mark master object
                    if not master_object:
                        master_object = item
                        continue

                    # update projects
                    master_object.projects.extend(item.projects)
                    master_object.projects = list(set(master_object.projects))

                    # update hoursBurnedAgainstProjects
                    master_object.hoursBurnedAgainstProjects += item.hoursBurnedAgainstProjects

                    # update groupedByProject
                    for k, v in item.groupedByProject.iteritems():
                        if key in master_object.groupedByProject.keys():
                            master_object.groupedByProject[k] += v
                        else:
                            master_object.groupedByProject[k] = v

                    # update hoursBurned
                    master_object.hoursBurned += item.hoursBurned

                    # update activities
                    master_object.activities.extend(item.activities)
                    master_object.activities = list(set(master_object.activities))

                    # update groupedByActivityType
                    for k, v in item.groupedByActivityType.iteritems():
                        if k in master_object.groupedByActivityType.keys():
                            master_object.groupedByActivityType[k] += v
                        else:
                            master_object.groupedByActivityType[k] = v

                    # mark employee_activity_log for deletion
                    objects_to_delete.append(item)

                # save updated employee activity log
                if not self.is_debug:
                    print 'Saving corrected record'
                    master_object.save()

                # remove duplicate employee activity log
                if not self.is_debug:
                    print 'Deleting duplicates'
                    # TODO: Need-to-fix will not work because of make_query issues
                    EmployeeActivityLog.delete_multi([x.key() for x in objects_to_delete])


if __name__ == '__main__':
    """
    Execute remote scripts
    """
    print 'Going to run `MergeDuplicateProjectLogs`'
    MergeDuplicateProjectLogs(is_debug=True).run()
    print 'Going to run `MergeDuplicateEmployeeActivityLogs`'
    MergeDuplicateEmployeeActivityLogs(is_debug=True).run()
    print 'Scripts completed successfully'

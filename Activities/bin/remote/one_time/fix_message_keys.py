#!/usr/bin/python

import dev_appserver
dev_appserver.fix_sys_path()

import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../../', 'src'),)
os.environ['SERVER_PORT'] = "80"
os.environ['SERVER_NAME'] = "space"

import appengine_config

from google.appengine.ext.remote_api import remote_api_stub
import getpass


def auth_func():
    return (raw_input('Email: '), getpass.getpass('Password: '))

remote_api_stub.ConfigureRemoteApi(None, '/_ah/remote_api', auth_func, 'qburst-activities.appspot.com')


from google.appengine.ext.db import Key


class Models(object):
    PROJECT = 'Project'
    EMPLOYEE = 'Employee'
    GRADE = 'Grade'
    DESIGNATION = 'Designation'
    DEPARTMENT = 'Department'


to_app = 's~qburst-space'

def is_processed(key):
    if not key or key == 'None':
        return False
    key = Key(key)
    is_processed = key.app() == to_app

    if is_processed:
        print "Key (%s) is processed app is (%s)" % (key, key.app())
    
    return is_processed

def convert_key(key, kind):
    if not key or key == 'None':
        return u''

    key = Key(key)
    if key.app() == to_app:
        return unicode(key)

    return unicode(Key.from_path(kind, key.id(), _app=to_app))


batch_size = 100


class MessageKeyFixer(object):
    def run(self, is_debug, count=None):
        from activity.log.models import Message
        from google.appengine.ext.db import put

        counter = -1
        query = Message.all()
        messages = query.fetch(batch_size)
        while messages:
            listToPut = []
            for message in messages:
                counter += 1
                if counter == count:
                    break
                if is_processed(message.user):
                    continue
                # Get new key for user
                currentUserKey = message.user
                newUserKey = convert_key(message.user, Models.EMPLOYEE)
                # Get new key for project
                currentPerojectKey = message.project
                newProjectKey = convert_key(message.project, Models.PROJECT)
                # Get new key for department
                currentDepartmentKey = currentGradeKey = currentDesignationKey = newDepartmentKey = newDesignationKey = newGradeKey = None
                try:
                    currentDepartmentKey = message.department
                    newDepartmentKey = convert_key(message.department, Models.DEPARTMENT)
                except:
                    pass
                try:
                    # Get new key for designation
                    currentDesignationKey = message.designation
                    newDesignationKey = convert_key(message.designation, Models.DESIGNATION)
                except:
                    pass
                try:
                    # Get new key for grade
                    currentGradeKey = message.grade
                    newGradeKey = convert_key(message.grade, Models.GRADE)
                except:
                    pass
                if is_debug:
                    print 'Current User: %s    New User: %s' % (currentUserKey, newUserKey)
                    print 'Current Project: %s    New Project: %s' % (currentPerojectKey, newProjectKey)
                    print 'Current Department: %s    New Department: %s' % (currentDepartmentKey, newDepartmentKey)
                    print 'Current Designation: %s    New Designation: %s' % (currentDesignationKey, newDesignationKey)
                    print 'Current Grade: %s    New Grade: %s' % (currentGradeKey, newGradeKey)
                else:
                    message.user = newUserKey
                    message.postedBy = newUserKey
                    message.project = newProjectKey
                    if newDepartmentKey:
                        message.department = newDepartmentKey
                    if newDesignationKey:
                        message.designation = newDesignationKey
                    if newGradeKey:
                        message.grade = newGradeKey
                    listToPut.append(message)

            if listToPut and not is_debug:
                put(listToPut)
            print 'Processed: ', counter
            if counter == count:
                break
            query.with_cursor(query.cursor())
            messages = query.fetch(batch_size)


if __name__ == '__main__':
    MessageKeyFixer().run(is_debug=True, count=1)

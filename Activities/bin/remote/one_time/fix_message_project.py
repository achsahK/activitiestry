# setup environment
import environment
environment.setup()

from datetime import datetime
from urllib2 import URLError

from django.conf import settings

from google.appengine.api.urlfetch_errors import ConnectionClosedError, DeadlineExceededError, DownloadError

from activity.log.models import Message, ActivityType, MessageStatus
from space.utils.decorators import retry

class Fix_Message_Project(object):
    @staticmethod
    def run(is_debug=True):
    
        # define constants
        count = 0
        limit = 100
        objects_to_save = []

        # define filters
        posted_on_gt_filter = {'property': 'postedOn', 'operator': '>', 'value': datetime(day=6, month=9, year=2015)}
        posted_on_lt_filter = {'property': 'postedOn', 'operator': '<', 'value': datetime(day=8, month=9, year=2015)}

        # get records from datastore
        print "Getting filtered messages..."
        messages = Message.make_query(filters=[posted_on_gt_filter, posted_on_lt_filter], iterator_only=True)

        # process records one by one
        print "Processing Message..."
        for message in messages:
            message_updated = False
            
            if not hasattr(message, 'status'):
                print "Missing status! Updating with PENDING..."
                message.activityType = MessageStatus.PENDING
                message_updated = True

            if not hasattr(message, 'activityType'):
                print "Missing activityType! Updating with Other..."
                message.activityType = ActivityType.OTHER
                message_updated = True

            if not hasattr(message, 'project'):
                print "Missing project! Updating with empty string..."
                message.project = ''
                message_updated = True

            if message_updated:
                print "Key: %s \n User: %s \n Posted For: %s \n Posted On: %s \n Project: %s \n Status: %s \n Activity type: %s" % (
                    str(message.key()),
                    str(message.user),
                    message.postedFor.strftime(settings.DEFAULT_DATE_FORMAT),
                    message.postedOn.strftime(settings.DEFAULT_DATE_FORMAT),
                    str(message.project),
                    str(message.status),
                    str(message.activityType)
                )
                objects_to_save.append(message)
                count += 1

            # interim save
            if count >= limit and not is_debug:
                Fix_Message_Project._put_messages(objects_to_save)
                count = 0
                objects_to_save = []

        # save remaining items if any
        if objects_to_save and not is_debug:
            Fix_Message_Project._put_messages(objects_to_save)

    @staticmethod
    @retry(tries=4, exceptions=(ConnectionClosedError, DeadlineExceededError, DownloadError, URLError))
    def _put_messages(message_list):
        """Put the updated message instances"""
        print "Saving updated message instances..."
        Message.save_multi(message_list)

if __name__ == '__main__':
    print "Fixing corrupted posted for date for messages posted between 06/09/15 and 08/09/15..."
    Fix_Message_Project.run(is_debug=True)

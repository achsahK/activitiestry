# setup environment
import environment
environment.setup()

# python libraries
from datetime import date

# django libraries
from django.conf import settings

# models
from activity.log.models import Message, ProjectLog, EmployeeActivityLog

# others
from gaeqblib_broken.utilities.functions import chunks


def get_from_url(url=None, method=2, data={}, headers={}, retry=5, user=None):
    """
    function used to fetch external resources
    @param method: HTTP method to be used. Possible values 1(GET), 2(POST), 3(HEAD), 4(PUT), 5(DELETE), 6(PATCH)
    """
    # python libraries
    import json
    import urllib
    import logging

    # app engine libraries
    from google.appengine.api import users, urlfetch
    from google.appengine.api.urlfetch_errors import InternalTransientError, DeadlineExceededError, DownloadError, \
        ConnectionClosedError

    # models
    from gaeqblib_broken.oauth2.models import Oauth2Credentials

    # validate url
    if not url:
        raise Exception('Invalid URL')

    # validate user
    if not user:
        raise Exception('Invalid user')

    # insert content type header
    headers.update({'Content-Type': 'application/x-www-form-urlencoded'})

    # insert authorization header
    try:
        user = users.User(email=user)
        user_filter = {'property': 'user', 'operator': '=', 'value': user}
        credentials_object = Oauth2Credentials.make_query(filters=[user_filter])['results'][0]
        headers.update({'Authorization': 'Bearer %s' % credentials_object.get_valid_access_token()})
    except Exception as ex:
        logging.exception(ex)
        raise ex

    # url encode data
    if data:
        data = urllib.urlencode(data)
    else:
        pass

    # perform request and get response
    count = 0
    deadline = 60
    while count <= retry:
        try:
            response = urlfetch.fetch(url=url, method=method, payload=data, headers=headers, deadline=deadline)
            return json.loads(response.content)
        except (InternalTransientError, DeadlineExceededError, DownloadError, ConnectionClosedError) as ex:
            if count >= retry:
                raise ex
            else:
                count += 1  # ignore error and retry
                deadline += 5  # increase deadline
        except ValueError as ex:
            logging.info('Response for API call to %s \n\n %s' % (url, response.content))
            raise ex


class FixEmployeeProjectLogs(object):
    """
    Fix employee logs and project logs
    """
    logged_for = None
    is_debug = True
    chunk_size = 50
    project_logs = {}
    employee_activity_logs = {}
    grade_wise_ratios_dict = {}

    @staticmethod
    def update_project_log_with_message(grade_wise_ratios_dict, project_log, message):

        # calculate time spent
        time_spent = float(message.timeSpent) * grade_wise_ratios_dict.get(message.grade, 1.0)

        # update employees
        if message.user not in project_log.employees:
            project_log.employees.append(message.user)

        # update groupedByGrade
        if not project_log.groupedByGrade:
            project_log.groupedByGrade = {message.grade: 0.0}
        elif not project_log.groupedByGrade.get(message.grade, False):
            project_log.groupedByGrade[message.grade] = 0.0
        else:
            pass
        project_log.groupedByGrade[message.grade] += time_spent

        # update groupedByActivityType
        if not project_log.groupedByActivityType:
            project_log.groupedByActivityType = {message.activityType: 0.0}
        elif not project_log.groupedByActivityType.get(message.activityType, False):
            project_log.groupedByActivityType[message.activityType] = 0.0
        else:
            pass
        project_log.groupedByActivityType[message.activityType] += time_spent

        # update groupedByEmployee
        if not project_log.groupedByEmployee:
            project_log.groupedByEmployee = {message.user: 0.0}
        elif not project_log.groupedByEmployee.get(message.user, False):
            project_log.groupedByEmployee[message.user] = 0.0
        else:
            pass
        project_log.groupedByEmployee[message.user] += time_spent

        # update groupedByDepartment
        if not project_log.groupedByDepartment:
            project_log.groupedByDepartment = {message.department: 0.0}
        elif not project_log.groupedByDepartment.get(message.department, False):
            project_log.groupedByDepartment[message.department] = 0.0
        else:
            pass
        project_log.groupedByDepartment[message.department] += time_spent

        # update hoursBurned
        project_log.hoursBurned += time_spent

        # update actualHoursBurned
        project_log.actualHoursBurned += float(message.timeSpent)

        # update activities
        project_log.activities.append(message.get_key_instance())
        project_log.activities = list(set(project_log.activities))

        return project_log

    @staticmethod
    def update_employee_log_with_message(employee_log, message):

        # calculate time spent
        time_spent = float(message.timeSpent)

        # update hoursBurnedAgainstProjects
        if message.project:
            employee_log.hoursBurnedAgainstProjects += time_spent

        # update projects
        if message.project not in employee_log.projects:
            employee_log.projects.append(message.project)

        # update groupedByProject
        if not employee_log.groupedByProject:
            employee_log.groupedByProject = {message.project: 0.0}
        elif not employee_log.groupedByProject.get(message.project, False):
            employee_log.groupedByProject[message.project] = 0.0
        else:
            pass
        employee_log.groupedByProject[message.project] += time_spent

        # update groupedByActivityType
        if not employee_log.groupedByActivityType:
            employee_log.groupedByActivityType = {message.activityType: 0.0}
        elif not employee_log.groupedByActivityType.get(message.activityType, False):
            employee_log.groupedByActivityType[message.activityType] = 0.0
        else:
            pass
        employee_log.groupedByActivityType[message.activityType] += time_spent

        # update hoursBurned
        employee_log.hoursBurned += time_spent

        # update activities
        employee_log.activities.append(message.get_key_instance())
        employee_log.activities = list(set(employee_log.activities))

        return employee_log

    def __init__(self, day=1, month=None, year=None, is_debug=True, *args, **kwargs):

        # assign logged_for and debug flag
        self.logged_for = date(day=day, month=month, year=year)
        self.is_debug = is_debug

    def pre_run(self):

        # identify project logs for the date being processed
        filters = [{'property': 'loggedFor', 'operator': '=', 'value': self.logged_for}]
        # TODO: Need-to-fix will not work because of make_query issues
        project_logs = ProjectLog.make_query(filters=filters, iterator_only=True)

        # prepare project logs dict
        for project_log in project_logs:
            self.project_logs[project_log.project] = project_log

        # identify employee activity logs for the date being processed
        filters = [{'property': 'loggedFor', 'operator': '=', 'value': self.logged_for}]
        # TODO: Need-to-fix will not work because of make_query issues
        employee_logs = EmployeeActivityLog.make_query(filters=filters, iterator_only=True)

        # prepare employee logs dict
        for employee_log in employee_logs:
            self.employee_activity_logs[employee_log.employee] = employee_log

        # prepare grade wise ratios dict from space
        self.grade_wise_ratios_dict = get_from_url(
            url=settings.SPACE_APIS['grade_wise_ratio'],
            user=settings.SCRIPT_USER
        )

    def run(self, messages):

        # process messages one by one
        for message in messages:

            # identify project log
            if message.project:
                project_log = self.project_logs.get(message.project)

                # create project log, if project log for project do not exists
                if not project_log:
                    project_log = ProjectLog(
                        hoursBurned=0.0,
                        project=message.project,
                        employees=[],
                        activities=[],
                        loggedFor=self.logged_for
                    )

                # check whether the message is there in project log
                if message.key() not in project_log.activities:

                    # update project log in project logs dict
                    print 'Identified missing message in project log - %s' % str(message.key())
                    self.project_logs[project_log.project] = self.update_project_log_with_message(self.grade_wise_ratios_dict, project_log, message)

            # identify employee activity log
            if message.user:
                employee_activity_log = self.employee_activity_logs.get(message.user)

                # create employee log, if employee log for employee do not exists
                if not employee_activity_log:
                    employee_activity_log = EmployeeActivityLog(
                        hoursBurned=0.0,
                        employee=message.user,
                        projects=[],
                        activities=[],
                        loggedFor=self.logged_for
                    )

                # check whether the message is there in employee activity log
                if message.key() not in employee_activity_log.activities:

                    # update employee activity log in employee activity logs dict
                    print 'Identified missing message in employee activity log - %s' % str(message.key())
                    self.employee_activity_logs[employee_activity_log.employee] = self.update_employee_log_with_message(employee_activity_log, message)

    def post_run(self):

        # save updated project logs
        for project_log_chunks in chunks(self.project_logs.values(), self.chunk_size):
            if not self.is_debug:
                print 'Saving project log chunk'
                # TODO: Need-to-fix will not work because of make_query issues
                ProjectLog.save_multi(project_log_chunks)

        # save updated employee activity logs
        for emplyoee_log_chunks in chunks(self.employee_activity_logs.values(), self.chunk_size):
            if not self.is_debug:
                print 'Saving employee activity log chunk'
                EmployeeActivityLog.save_multi(emplyoee_log_chunks)


if __name__ == '__main__':

    # configure date being processed
    month = 9
    year = 2015
    print 'Preparing to run for %s-%s' % (month, year)
    remote_script = FixEmployeeProjectLogs(month=month, year=year, is_debug=True)

    # execute pre run statements
    remote_script.pre_run()

    # get messages posted during the period being processed
    filters = [
        {'property': 'postedFor', 'operator': '>=', 'value': date(day=1, month=month, year=year)},
        {'property': 'postedFor', 'operator': '<', 'value': date(day=1, month=month + 1, year=year)},
    ]
    print 'Fetching messages posted for the month %s' % month
    messages = Message.make_query(filters=filters, iterator_only=True)

    # execute run statements
    remote_script.run(messages)

    # execute post run statements
    remote_script.post_run()
    print 'Script completed successfully'

# setup environment
import environment
environment.setup()

# python libraries
from urllib2 import URLError
from datetime import datetime

# app engine libraries
from google.appengine.api.urlfetch_errors import ConnectionClosedError, DeadlineExceededError, DownloadError

# django libraries
from django.conf import settings

# models
from activity.log.models import Message, MessageStatus, ActivityType

# others
from space.utils.decorators import retry


class FixMessageStatus(object):
    @staticmethod
    def run(is_debug=True):
    
        # define constants
        count = 0
        limit = 100
        objects_to_save = []

        # define filters
        posted_on_gt_filter = {'property': 'postedOn', 'operator': '>', 'value': datetime(day=1, month=9, year=2015)}
        posted_on_lt_filter = {'property': 'postedOn', 'operator': '<', 'value': datetime(day=11, month=9, year=2015)}

        # get records from datastore
        print "Getting filtered messages..."
        messages = Message.make_query(filters=[posted_on_gt_filter, posted_on_lt_filter], iterator_only=True)

        # process records one by one
        print "Processing Messages one by one..."
        for message in messages:
            message_updated = False
            print "Processing message - %s" % str(message.key())

            if not hasattr(message, 'status'):
                print "Missing status! Updating with PENDING..."
                message.status = MessageStatus.PENDING
                message_updated = True

            if not hasattr(message, 'activityType'):
                print "Missing activityType! Updating with Other..."
                message.activityType = ActivityType.OTHER
                message_updated = True

            if message_updated:
                print "Key: %s \n User: %s \n Posted For: %s \n Posted On: %s \n Project: %s \n Status: %s \n Activity Type: %s" % (
                    str(message.key()),
                    str(message.user),
                    message.postedFor.strftime(settings.DEFAULT_DATE_FORMAT),
                    message.postedOn.strftime(settings.DEFAULT_DATE_FORMAT),
                    str(message.project),
                    str(message.status),
                    str(message.activityType)
                )
                objects_to_save.append(message)
                count += 1

            # interim save
            if count >= limit and not is_debug:
                FixMessageStatus._put_messages(objects_to_save)
                count = 0
                objects_to_save = []

        # save remaining items if any
        if objects_to_save and not is_debug:
            FixMessageStatus._put_messages(objects_to_save)

    @staticmethod
    @retry(tries=4, exceptions=(ConnectionClosedError, DeadlineExceededError, DownloadError, URLError))
    def _put_messages(message_list):
        """Put the updated message instances"""
        print "Saving updated message instances..."
        Message.save_multi(message_list)

if __name__ == '__main__':
    print "Fixing messages without status between 01/09/15 and 11/09/15..."
    FixMessageStatus.run(is_debug=True)

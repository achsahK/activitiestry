#! /usr/bin/python

# setup environment
import environment
environment.setup()

# app engine libraries
from google.appengine.api import users

# oauth2 libraries
from oauth2client.contrib.appengine import CredentialsModel

# gaeqblib_broken
from gaeqblib_broken.oauth2.models import Oauth2Credentials

# define constants
COUNT = 0
LIMIT = 100
FAILED = []
TO_PORT = []

# get records from credentials model
query = CredentialsModel.all()
records = query.fetch(LIMIT)

while records:

    # process records one by one
    for record in records:
        try:

            # prepare new credentials object
            credentials_object = record.credentials
            employee =users.User(credentials_object.id_token['email'])
            TO_PORT.append(Oauth2Credentials(user=employee, access_token=credentials_object.access_token,
                                             refresh_token=credentials_object.refresh_token,
                                             expiry_in=credentials_object.token_expiry,
                                             credentials=credentials_object))
            COUNT += 1

            # interim save
            if COUNT >= LIMIT:
                Oauth2Credentials.save_multi(TO_PORT)
                COUNT = 0
                TO_PORT = []
        except Exception as e:

            # track failed users
            FAILED.append(credentials_object.id_token['email'])

    # fetch next set of records from credentials model
    query.with_cursor(query.cursor())
    records = query.fetch(LIMIT)

# save pending users
if COUNT > 0:
    Oauth2Credentials.save_multi(TO_PORT)

# print status of script
if FAILED:
    print 'Failed to port credentials of the following users. Please ask then to perform Oauth2 manually.'
    print ', '.join(FAILED)
else:
    print 'All credentials has been ported successfully.'

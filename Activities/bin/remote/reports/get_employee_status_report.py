#!/usr/bin/python

import dev_appserver
dev_appserver.fix_sys_path()

import os
import sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../../', 'src'),)
os.environ['SERVER_PORT'] = "80"
os.environ['SERVER_NAME'] = "space"

import appengine_config

from google.appengine.ext.remote_api import remote_api_stub
import getpass


def auth_func():
    return (raw_input('Email: '), getpass.getpass('Password: '))

remote_api_stub.ConfigureRemoteApi(None, '/_ah/remote_api', auth_func, 'qburst-activities.appspot.com')

#### End remote API stuff

from datetime import date
from google.appengine.api.urlfetch_errors import ConnectionClosedError, DeadlineExceededError, DownloadError
from urllib2 import URLError

from django.conf import settings
from openpyxl import Workbook
from openpyxl.cell import get_column_letter

from space.utils.decorators import retry
from activity.log.models import Message
from space.utils.utils import safestringfy, getDateFromString
from activity.utils.manager import Manager

FETCH_BATCH_SIZE = 200


class GetEmployeeDailyLogForDateRange(object):
    """
    Script to pull out report of daily logs by a user, for a given time range;
    Excel is provided with following fields
    Project Name (as hyperlink if present else 'N/A'), Message, Time Spent, Activity Type, Posted For and Posted On
    """
    projects = {}

    @staticmethod
    def run(start_date, end_date, emp_key):
        print 'Generate daily log report'

        count = 0
        msg_count = 0

        range_start = getDateFromString(start_date, '%d-%m-%Y')
        range_end = getDateFromString(end_date, '%d-%m-%Y')

        # Creating excel workbook
        wb = Workbook()
        ws = wb.create_sheet()
        ws.title = 'Daily Status - Report'
        # Report Headers
        headers = [
            'Project',
            'Message',
            'Time Spent',
            'Activity Type',
            'Posted For',
            'Posted On'
        ]

        # Get projects from space
        GetEmployeeDailyLogForDateRange.projects = GetEmployeeDailyLogForDateRange._get_projects(emp_key)

        # Add header
        GetEmployeeDailyLogForDateRange._build_row(
            ws,
            count + 1,
            headers
        )

        # Get the list of Messages
        query = Message.all().filter("user =", emp_key)\
            .filter("postedFor >=", range_start)\
            .filter("postedFor <=", range_end)\
            .filter("type =", "end-of-day")
        entities = GetEmployeeDailyLogForDateRange._fetch_it(query)

        while entities:
            for message in entities:
                msg_count += 1

                count += 1
                # Build Excel rows
                GetEmployeeDailyLogForDateRange._build_row(
                    ws,
                    count + 1,
                    [
                        GetEmployeeDailyLogForDateRange._build_link(message.project),
                        safestringfy(message.message),
                        message.displayTimeSpent,
                        message.activityType,
                        message.postedFor.strftime(settings.DEFAULT_DATE_FORMAT),
                        message.postedOn.strftime(settings.DEFAULT_DATE_FORMAT)
                    ]
                )

            print 'Processed %s Messages' % msg_count
            query.with_cursor(query.cursor())
            entities = GetEmployeeDailyLogForDateRange._fetch_it(query)

        print 'Total message count for employee (%s) from %s to %s: %s' % (emp_key, start_date, end_date, msg_count)
        wb.save(filename='Daily Log Report from %s to %s.xlsx' % (start_date, end_date))

    @staticmethod
    def _build_link(project_key):
        """
        Build the project hyperlink. (Hyperlink is hard coded)
        If project is not present returns N/A else project hyperlink
        """
        if not project_key:
            return 'N/A'
        if project_key not in GetEmployeeDailyLogForDateRange.projects.keys():
            # Get project name from space, if key is not present in the active projects list.
            # Updating the projects variables also, to avoid further space hit for the same project key
            values = Manager().makeAPICall('HR', ['getProjAndEmpNameForActivity', project_key], useScriptUserCreds=True)
            GetEmployeeDailyLogForDateRange.projects.update({
                project_key: values['projectName']
            })
        return safestringfy('=HYPERLINK("http://space.qburst.com/project/%s/view", "%s")' % (project_key, GetEmployeeDailyLogForDateRange.projects[project_key]))

    @staticmethod
    def _build_row(ws, row_idx, row):
        """
        Build worksheet rows
        """
        for col_idx, val in enumerate(row, start=1):
            ws.cell('%s%s' % (get_column_letter(col_idx), row_idx)).value = val

    @staticmethod
    @retry(tries=4, exceptions=(ConnectionClosedError, DeadlineExceededError, DownloadError, URLError))
    def _fetch_it(query):
        """
        Fetching records using the query passed
        """
        return query.fetch(FETCH_BATCH_SIZE)

    @staticmethod
    @retry(tries=4, exceptions=(ConnectionClosedError, DeadlineExceededError, DownloadError, URLError))
    def _get_projects(emp_key):
        """
        Getting active projects from Space
        """
        return dict(
            map(
                lambda project: (str(project[0]), project[1]),
                Manager().makeAPICall('HR', ['getProjectChoicesForActivity', emp_key], useScriptUserCreds=True).get('choices')
            )
        )


if __name__ == '__main__':
    start_date = raw_input('Start Date(dd-mm-yyyy): ')
    end_date = raw_input('End Date(dd-mm-yyyy): ')
    emp_key = raw_input('Employee Key: ')

    GetEmployeeDailyLogForDateRange.run(start_date=start_date, end_date=end_date, emp_key=emp_key)

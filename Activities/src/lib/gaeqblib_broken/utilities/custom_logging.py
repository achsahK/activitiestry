# Normal logging available wont persist message for future use
# This helps to persist the messages internally in a dictionary
#
# usage:
#
# from gaeqblib_broken.utilities import custom_logging as logging
# .......
# logging.debug('log message with level debug')
# logging.info('log message with level info')
# logging.warning('log message with level warning')
# logging.error('log message with level error')
# logging.exception('log message with level exception')
# .......
# print logging.get_messages() # print all the above messages

# python libraries
import logging

# dict to save log messages
messages = []


# debug
def debug(msg, *args, **kwargs):
    messages.append('debug: %s' % msg)
    logging.debug(msg, *args, **kwargs)


# info
def info(msg, *args, **kwargs):
    messages.append('info: %s' % msg)
    logging.info(msg, *args, **kwargs)


# warning
def warning(msg, *args, **kwargs):
    messages.append('warning: %s' % msg)
    logging.warning(msg, *args, **kwargs)


# error
def error(msg, *args, **kwargs):
    messages.append('error: %s' % msg)
    logging.error(msg, *args, **kwargs)


# exception
def exception(msg, *args, **kwargs):
    messages.append('exception: %s' % msg)
    logging.exception(msg, *args, **kwargs)


# return message dict as string
def get_messages():
    return '\n'.join(messages)

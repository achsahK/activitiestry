# python libraries
import json
import urllib
import logging
import calendar


from pytz import timezone
from datetime import date, datetime, timedelta

# django libraries
from django.conf import settings
from django.utils import timezone as django_timezone

# app engine libraries
from google.appengine.api import users, urlfetch, memcache, app_identity
from google.appengine.api.urlfetch_errors import InternalTransientError, DeadlineExceededError, DownloadError, \
    ConnectionClosedError


def update(model_object, data={}):
    for attr in data:
        setattr(model_object, attr, data[attr])
    return model_object


def chunks(l, n):
    """
    Yield successive n-sized chunks from l
    @param l: iterable to be sliced to chunks
    @param n: chunk size
    @return: generator which generates chunks of iterable passed
    """
    for i in xrange(0, len(l), n):
        yield l[i: i +n]


def get_date_from_string(date_string, date_format=settings.DEFAULT_DATE_FORMAT):
    """
    Construct date object from string
    """
    return datetime.strptime(date_string, date_format)


def get_datetime_from_string(datetime_string, date_time_format=settings.DEFAULT_DATETIME_FORMAT):
    """
    Construct datetime object from string
    """
    return datetime.strptime(datetime_string, date_time_format)


def format_datetime(datetime_object, date_format=settings.DEFAULT_DATE_FORMAT,
                    date_time_format=settings.DEFAULT_DATETIME_FORMAT):
    """
    Get date/datetime object formatted
    """
    if isinstance(datetime_object, date):
        return datetime_object.strftime(date_format)
    elif isinstance(datetime_object, datetime):
        return datetime_object.strftime(date_time_format)


def convert_datetime(datetime_object=None, convert_to_timezone=settings.TIME_ZONE):
    """
    This function returns datetime in the specified time zone
    """
    if not datetime_object:
        datetime_object = datetime.now(timezone('UTC'))
    if not django_timezone.is_aware(datetime_object):
        datetime_object = django_timezone.make_aware(datetime_object, timezone('UTC'))  # update naive datetime with UTC
    return datetime_object.astimezone(timezone(convert_to_timezone))


def get_current_datetime():
    """
    This function returns current datetime of time zone defined in settings
    """
    return convert_datetime()


def get_future_datetime(current_datetime=None, days=0, seconds=0, microseconds=0, milliseconds=0,
                        minutes=0, hours=0, weeks=0):
    """
    This function returns future datetime
    """
    if not current_datetime:
        current_datetime = get_current_datetime()
    datetime_difference_to_future_datetime = timedelta(days=days, seconds=seconds, microseconds=microseconds,
                                                       milliseconds=milliseconds, minutes=minutes, hours=hours,
                                                       weeks=weeks)
    return current_datetime + datetime_difference_to_future_datetime


def get_date_range_tuple(start_date, end_date):
    """
    Function to generate tuple of dates
    """
    date_choices = ()
    while start_date <= end_date:
        date_choices += (format_datetime(start_date),)
        start_date = get_future_datetime(start_date, days=1)
    return date_choices


def clear_time(date_time_obj):
    """
    Removes the time part of a date time object
    """
    return datetime.datetime(date_time_obj.year, date_time_obj.month, date_time_obj.day, 0, 0, 0, 0)


def get_month_start_and_end(date_obj=None):
    """
    Get first and last date of a month in a given year
    """
    if not date_obj:
        date_obj = datetime.today().date()
    month_range = calendar.monthrange(date_obj.year, date_obj.month)
    start_date = date_obj.replace(day=1)
    end_date = date_obj.replace(day=month_range[1])
    return {'start': start_date, 'end': end_date}


def add_months(date_obj=None, months=1):
    """
    Add given no of months to a date object
    """
    if not date_obj:
        date_obj = datetime.today().date()
    month = date_obj.month - 1 + months
    year = date_obj.year + month / 12
    month = month % 12 + 1
    day = min(date_obj.day, calendar.monthrange(year, month)[1])
    return datetime.date(year, month, day)


def get_value_from_request_by_key(request_obj, key):
    """
    Get value from request based on key
    """
    return request_obj.GET.get(key) or request_obj.POST.get(key)


def is_safe_to_redirect(path, redirect_to, additional_unsafe_uris=[]):
    """
    Function to check whether we can redirect to the given path
    """

    # get unsafe URIs
    unsafe_uris = settings.UNSAFE_URI_PATTERNS
    if additional_unsafe_uris:
        unsafe_uris.extend(additional_unsafe_uris)

    # block redirecting to same uri (infinite loop)
    if path == redirect_to:
        return False

    # block redirecting to unsafe uris
    for uri in unsafe_uris:
        if path.find(uri) > -1:
            return False

    # allow all other uris
    return True


def get_from_url(url=None, method=2, data={}, headers={}, retry=5, user=None):
    """
    function used to fetch external resources
    @param method: HTTP method to be used. Possible values 1(GET), 2(POST), 3(HEAD), 4(PUT), 5(DELETE), 6(PATCH)
    """
    # imports
    from gaeqblib_broken.oauth2.models import Oauth2Credentials

    # validate url
    if not url:
        raise Exception('Invalid URL')

    # validate user
    if not user:
        raise Exception('Invalid user')

    # insert content type header
    headers.update({'Content-Type': 'application/x-www-form-urlencoded'})

    # insert authorization header
    try:
        user = users.User(email=user)
        key = 'oauth2_credentials_%s' % user.email()
        credentials_object = memcache.get(key)
        if credentials_object is None:
            user_filter = {'property': 'user', 'operator': '=', 'value': user}
            credentials_object = Oauth2Credentials.make_query(filters=[user_filter])['results'][0]
            if memcache.add(key, credentials_object, credentials_object.life_in_seconds()):
                logging.info('Memcache set success [key - %s]' % key)
            else:
                logging.error('Memcache set fail [key - %s]' % key)
        else:
            pass
        headers.update({'Authorization': 'Bearer %s' % credentials_object.get_valid_access_token()})
    except Exception as ex:
        logging.exception(ex)
        logging.error('Authorization code for %s not found' % user.email())

    # url encode data
    if data:
        data = urllib.urlencode(data)
    else:
        pass

    # perform request and get response
    count = 0
    deadline = 60
    while count <= retry:
        try:
            response = urlfetch.fetch(url=url, method=method, payload=data, headers=headers, deadline=deadline)
            return json.loads(response.content)
        except (InternalTransientError, DeadlineExceededError, DownloadError, ConnectionClosedError) as ex:
            if count >= retry:
                raise ex
            else:
                count += 1  # ignore error and retry
                deadline += 5  # increase deadline
        except ValueError as ex:
            logging.info('Response for API call to %s \n\n %s' % (url, response.content))
            raise ex

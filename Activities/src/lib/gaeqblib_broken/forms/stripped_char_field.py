# django libraries
from django import forms


class StrippedCharField(forms.CharField):
    def clean(self, value):
        if value is not None:
            value = value.strip()
        else:
            pass
        return super(StrippedCharField, self).clean(value)

# app engine libraries
from google.appengine.ext.remote_api import remote_api_stub


class RemoteScriptMixin(object):
    """
    Base class for writing remote scripts as django management commands
    """
    app_url = None
    remote_api_uri = '/_ah/remote_api'

    def configure_remote_api(self):
        """
        Function used to configure remote api
        """

        # validate app id
        if not self.app_url:
            self.app_url = raw_input('Application URL (eg: qburst-space-staging.appspot.com): ')

        # validate remote api handler
        if not self.remote_api_uri:
            self.remote_api_uri = raw_input('Remote API handler URI (eg: /_ah/remote_api): ')

        # configure remote api
        remote_api_stub.ConfigureRemoteApiForOAuth(self.app_url, self.remote_api_uri)

    def __init__(self, *args, **kwargs):

        # initialize super()
        super(RemoteScriptMixin, self).__init__(*args, **kwargs)

        # configure remote api
        self.configure_remote_api()

# django rest libraries
from rest_framework import serializers
from collections import OrderedDict

# app engine libraries
from google.appengine.ext import db, ndb

# models
from gaeqblib_broken.models import DbModel, NdbModel


class ReferenceField(serializers.RelatedField):
    """
    Field representing ReferenceProperty of app engine
    """

    def __init__(self, model=None, label_field='name', serializer=None, hide_choices=False, options=(), *args, **kwargs):
        """
        Constructor
        """
        kwargs['queryset'] = ''  # hack
        self.model = model
        self.label_field = label_field
        self.serializer = serializer
        self.hide_choices = hide_choices
        self.options = options
        self.attribute_is_safe_key = False
        super(ReferenceField, self).__init__(*args, **kwargs)

    @property
    def choices(self):
        """
        Choices to be used in front end
        """
        # hide choices
        if self.hide_choices:
            return OrderedDict()

        # from kwargs
        if self.options:
            return OrderedDict(self.options)

        # from model
        if self.model:
            return OrderedDict(self.model.get_choices(label=self.label_field))

        return OrderedDict()

    def get_attribute(self, instance):
        """
        Tries to get source, if it fails try to get the url safe key.
        If everything fails return None
        """
        try:
            return super(ReferenceField, self).get_attribute(instance)
        except:
            attribute = str(self.source_attrs[0])
            if '_obj' in attribute:
                attribute = attribute.replace('_obj', '')
                safe_key = instance.replace_ref_obj_with_key(instance, attribute)
                self.attribute_is_safe_key = True
                return safe_key
            else:
                return None

    def to_representation(self, value):
        """
        Convert data to HTML representation
        """
        # return same if the attribute value is safe key
        if self.attribute_is_safe_key:
            return value

        # serializer is available
        if self.serializer:
            return self.serializer(value).data

        # serializer is not available
        else:

            # db instance
            if isinstance(value, db.Model):
                try:
                    return str(value.key())
                except Exception as e:
                    return None

            # ndb instance
            elif isinstance(value, ndb.Model):
                try:
                    return value.key.urlsafe()
                except Exception as e:
                    return None

        return ''

    def to_internal_value(self, data):
        """
        Convert key to reference object
        """
        if data:
            # db
            try:
                return DbModel.get_key(data)
            except:

                # ndb
                try:
                    return NdbModel.get_key(data)
                except:
                    return None
        else:
            return None

from rest_framework import serializers


class JSONField(serializers.Field):
    """
    Field representing json property of app engine `gaeqblib_broken.properties.db.JSONProperty`
    """

    def to_internal_value(self, data):
        """
        convert to format supported by datastore
        """
        return data

    def to_representation(self, value):
        """
        convert to html form
        """
        return value

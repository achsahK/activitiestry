# django rest libraries
from rest_framework import serializers

# user defined libraries
from gaeqblib_broken.utilities.functions import convert_datetime


class DateTimeField(serializers.DateTimeField):
    """
    Field representing date time property of app engine
    """

    def to_representation(self, value):
        """
        convert date time object to timezone defined in settings and then convert to html representation
        """
        value = convert_datetime(value)  # convert datetime to timezone configured in settings
        return super(DateTimeField, self).to_representation(value)

# python libraries
from datetime import datetime

# django rest libraries
from rest_framework import serializers

# others
from gaeqblib_broken.utilities.functions import get_date_from_string


class DateChoiceField(serializers.ChoiceField):
    """
    Field representing date time property of app engine
    """
    def to_internal_value(self, data):
        """
        convert date string to date time object (app engine saves date as datetime object)
        """
        date_obj = get_date_from_string(super(DateChoiceField, self).to_internal_value(data))
        return datetime(day=date_obj.day, month=date_obj.month, year=date_obj.year)

    def to_representation(self, value):
        """
        convert date object to string representation
        """
        return serializers.DateField().to_representation(value.date())

# django rest libraries
from rest_framework import serializers
from collections import OrderedDict


# models
from gaeqblib_broken.models import DbModel, NdbModel


class ListField(serializers.Field):
    """
    Field representing key list of app engine
    """

    def __init__(self, model=None, serializer=None, label_field='name', hide_choices=False, *args, **kwargs):
        """
        Constructor
        """
        self.model = model
        self.serializer = serializer
        self.label_field = label_field
        self.hide_choices = hide_choices
        super(ListField, self).__init__(*args, **kwargs)

    @property
    def choices(self):
        """
        Choices to be used in front end
        """
        # model is available
        if self.hide_choices or not self.model:
            return OrderedDict()

        # model is not available
        else:
            return OrderedDict(self.model.get_choices(label=self.label_field))

    def to_representation(self, data):
        """
        Convert data to HTML representation
        """
        # valid list
        if len(data) > 0:

            # model is available
            if self.model:
                data = self.model.get_multi(data)

            # model is not available
            else:
                pass
                # # handle db
                # try:
                #     data = DbModel.get_multi(data)
                # except:
                #
                #     # handle ndb
                #     try:
                #         data = NdbModel.get_multi(data)
                #     except:
                #         data = None

            # serializer is available
            if data and self.serializer:
                return self.serializer(data, many=True).data

            # serializer is not available
            elif data:
                return_list = []
                for value in data:
                    return_list.append(str(value))
                    # try:
                    #     data = value.to_dict()
                    #     for k, v in data.iteritems():
                    #         data[k] = value.replace_ref_obj_with_key(value, k)
                    #     return_list.append(data)
                    # except:
                    #     pass
                return return_list

            # default
            else:
                return []

        # empty list
        else:
            return []

    def to_internal_value(self, data):
        """
        Convert key to reference object
        """
        # make sure value is a list
        if type(data) is not list:
            data = [data]
        else:
            pass

        # valid list
        if len(data) > 0:

            # model is available
            if self.model:
                return [self.model.get_key(x) for x in data]

            # model is not available
            else:
                key_list = []
                for x in data:
                    try:
                        key_list.append(DbModel.get_key(x))
                    except:
                        try:
                            key_list.append(NdbModel.get_key(x))
                        except:
                            pass
                return key_list

        # empty list
        else:
            return []

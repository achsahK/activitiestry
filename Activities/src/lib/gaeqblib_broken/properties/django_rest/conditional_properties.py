# django rest libraries
from rest_framework import serializers


class ConditionalPropertiedSerializerMeta(serializers.SerializerMetaclass):
    """
    Meta class for adding dynamic/conditional attributes to a class definition.
    """
    def __new__(cls, name, bases, attrs):
        properties = attrs.pop('_conditional_properties', None)  # get generator function
        if properties:
            attrs.update(properties())  # update class definition
        return super(ConditionalPropertiedSerializerMeta, cls).__new__(cls, name, bases, attrs)

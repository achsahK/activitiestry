# app engine libraries
from google.appengine.ext import db, ndb


# django rest libraries
from rest_framework import serializers


# django rest - app engine fields mapping
SERIALIZER_FIELD_MAPPING = {
    # db properties
    db.IntegerProperty: serializers.IntegerField,
    db.BooleanProperty: serializers.BooleanField,
    db.StringProperty: serializers.CharField,
    db.TextProperty: serializers.CharField,
    db.ByteStringProperty: serializers.CharField,
    db.BlobProperty: serializers.CharField,
    db.DateProperty: serializers.DateField,
    db.TimeProperty: serializers.TimeField,
    db.DateTimeProperty: serializers.DateTimeField,
    db.GeoPtProperty: serializers.CharField,
    db.PostalAddressProperty: serializers.CharField,
    db.PhoneNumberProperty: serializers.CharField,
    db.EmailProperty: serializers.EmailField,
    db.IMProperty: serializers.CharField,
    db.LinkProperty: serializers.URLField,
    db.CategoryProperty: serializers.CharField,
    db.RatingProperty: serializers.IntegerField,
    db.ReferenceProperty: serializers.CharField,
    db.SelfReferenceProperty: serializers.CharField,
    db.ListProperty: serializers.ListField,
    db.StringListProperty: serializers.ListField,
    # ndb properties
    ndb.IntegerProperty: serializers.IntegerField,
    ndb.FloatProperty: serializers.FloatField,
    ndb.BooleanProperty: serializers.BooleanField,
    ndb.StringProperty: serializers.CharField,
    ndb.TextProperty: serializers.CharField,
    ndb.BlobProperty: serializers.CharField,
    ndb.DateProperty: serializers.DateField,
    ndb.TimeProperty: serializers.TimeField,
    ndb.DateTimeProperty: serializers.DateTimeField,
    ndb.GeoPtProperty: serializers.CharField,
    ndb.KeyProperty: serializers.CharField,
    ndb.BlobKeyProperty: serializers.CharField,
    ndb.UserProperty: serializers.CharField,
    ndb.StructuredProperty: serializers.CharField,
    ndb.LocalStructuredProperty: serializers.CharField,
    ndb.JsonProperty: serializers.CharField,
    ndb.PickleProperty: serializers.CharField,
    ndb.GenericProperty: serializers.CharField,
    ndb.ComputedProperty: serializers.CharField,
}

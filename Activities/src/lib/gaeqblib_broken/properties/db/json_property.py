# python libraries
import json

# app engine libraries
from google.appengine.ext import db
from google.appengine.api import datastore_types

# Django libraries
from django.core.serializers.json import DjangoJSONEncoder


class JSONProperty(db.Property):
    """
    JsonProperty saves and loads data as json
    @see: https://cloud.google.com/appengine/articles/extending_models
    @see: https://cloud.google.com/appengine/docs/python/ndb/subclassprop
    """
    def get_value_for_datastore(self, model_instance):
        """
        Extract the value from a model instance and convert it to one the type that goes in the datastore
        """
        value = super(JSONProperty, self).get_value_for_datastore(model_instance)
        return db.Text(self._deflate(value))

    def validate(self, value):
        """
        Called when an assignment is made to a property to make sure that it is compatible with your assigned attributes
        """
        return self._inflate(value)

    def make_value_from_datastore(self, value):
        """
        Convert a value as found in the datastore to your new user type
        """
        return self._inflate(value)

    def _inflate(self, value):
        """
        Convert value to JSON
        """
        if value is None:
            return {}
        if isinstance(value, unicode) or isinstance(value, str):
            return json.loads(value)
        return value

    def _deflate(self, value):
        """
        Convert JSON to string
        """
        # ref: http://stackoverflow.com/a/4702277/94354
        return json.dumps(value, cls=DjangoJSONEncoder)

    data_type = datastore_types.Text

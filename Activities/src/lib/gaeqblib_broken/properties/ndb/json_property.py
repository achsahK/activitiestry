# python libraries
import json


# app engine libraries
from google.appengine.ext import ndb


class JSONProperty(ndb.StringProperty):
    """
    JsonProperty saves and loads data as json
    @see: https://cloud.google.com/appengine/articles/extending_models
    @see: https://cloud.google.com/appengine/docs/python/ndb/subclassprop
    """
    def _to_base_type(self, value):
        """
        Functionality same as that of `get_value_for_datastore`.
        Difference is that this function only applies to `ndb` models.
        """
        return ndb.Text(json.dumps(value))

    def _from_base_type(self, value):
        """
        Functionality same as that of `make_value_from_datastore`.
        Difference is that this function only applies to `ndb` models.
        """
        if value:
            return json.loads(value)
        else:
            return {}

    def _validate(self, value):
        """
        Functionality same as that of `validate`.
        Difference is that this function only applies to `ndb` models.
        """
        if value and (type(value) is unicode or type(value) is str):
            return json.loads(value)
        else:
            return value

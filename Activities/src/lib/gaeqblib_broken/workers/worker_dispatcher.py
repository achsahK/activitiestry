# python libraries
import logging
from importlib import import_module

# django libraries
from django.http import HttpResponse
from django.views.generic import View
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt


class WorkerDispatcher(View):
    """
    This is a dispatcher class used to dispatch class based views dynamically
    """
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):

        # identify worker class
        worker_class = kwargs.get('worker_class')

        # load worker class
        if worker_class:
            try:
                module_name, class_name = worker_class.rsplit('.', 1)
                module = import_module(module_name)
                worker_class = getattr(module, class_name)
                return worker_class.as_view()(request)
            except Exception as ex:
                logging.exception(ex)
                return HttpResponse(ex.message, status=500)

        return HttpResponse('Unable to find the requested worker class', status=404)

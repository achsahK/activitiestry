# django libraries
from django.http import HttpResponse

# user defined libraries
from workers import Worker


class CronJob(Worker):
    """
    Cron job worker, used to create cron jobs. May be extended in future.
    """
    subject = 'Cron Job Notification'

    def dispatch(self, request, *args, **kwargs):
        """
        Block external access to cron jobs
        """
        if request.META.get('REMOTE_ADDR') != '0.1.0.1':
            return HttpResponse('Cron jobs can only be invoked by the app engine servers', status=403)
        else:
            return super(CronJob, self).dispatch(request, *args, **kwargs)

    def run(self, request, *args, **kwargs):
        """
        What to do?
        """
        raise NotImplementedError()

    def error_handler(self, request, *args, **kwargs):
        """
        Error handler
        """
        raise NotImplementedError()

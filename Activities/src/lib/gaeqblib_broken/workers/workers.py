# django libraries
from django.conf import settings
from django.http import HttpResponse
from django.views.generic import View
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

# user defined libraries
from gaeqblib_broken.utilities import custom_logging as logging
from gaeqblib_broken.email import SendMail


class Worker(View):
    """
    Worker class is used for easy implementation of cron jobs and tasks
    """
    subject = 'Worker Notification'
    logging = logging
    to_address = settings.ADMINS
    from_address = settings.DEFAULT_FROM_EMAIL
    email_context_data = {}
    email_template = 'email_templates/common.html'
    notify_on_success = True
    notify_on_errors = True
    http_method_names = ['get', 'post']

    def __init__(self, *args, **kwargs):
        """
        Initialize messages as empty list
        """
        self.logging.messages = []
        super(Worker, self).__init__(*args, **kwargs)

    def get_worker_name(self):
        """
        Return class name
        """
        return str(self.__class__.__name__)

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        """
        Dispatch handler
        """
        return super(Worker, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        """
        GET HTTP handler
        """
        return self.run_worker(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        """
        POST HTTP handler
        """
        return self.run_worker(request, *args, **kwargs)

    def run_worker(self, request, *args, **kwargs):
        """
        Worker logic
        """
        try:

            # execute run
            self.run(request, *args, **kwargs)

            # send notifications if configured
            if self.notify_on_success:

                # update subject
                self.subject = "%s Script run complete" % self.get_worker_name()

                # update email context data
                self.email_context_data.update({
                    'message': '%s completed successfully :) \n\n Logs: \n\n %s' % (self.__class__.__name__,
                                                                                    self.logging.get_messages()),
                })

                # send mail
                self.notify_admins()

            # response
            return HttpResponse('Worker completed successfully :)', status=200)
        except Exception as ex:

            # log exception
            self.logging.exception(ex)

            # execute error handler
            self.error_handler(request, *args, **kwargs)

            # send notifications if configured
            if self.notify_on_errors:

                # update subject
                self.subject = "Error while running script %s" % self.get_worker_name()

                # update email context data
                self.email_context_data.update({
                    'message': '%s failed :( \n\n Logs: \n\n %s' % (self.__class__.__name__,
                                                                    self.logging.get_messages()),
                })

                # send mail
                self.notify_admins()

            # response
            return HttpResponse('Worker failed :(', status=500)

    def run(self, request, *args, **kwargs):
        """
        Logic or brain of the worker
        """
        raise NotImplementedError()

    def error_handler(self, request, *args, **kwargs):
        """
        Error handler
        """
        raise NotImplementedError()

    def notify_admins(self):
        """
        Send notification mail to admins
        """
        SendMail(
            subject=self.subject,
            template=self.email_template,
            to_address=self.to_address,
            from_address=self.from_address,
            context_data=self.email_context_data
        ).send()

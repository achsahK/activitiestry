# app engine libraries
from google.appengine.ext import deferred

# user defined libraries
from workers import Worker


def deferred_decorator(func):
    """
    Decorator used to call function as deferred.
    @see: https://cloud.google.com/appengine/articles/deferred
    """
    def inner(*args, **kwargs):
        deferred.defer(func, *args, **kwargs)
    return inner


class Task(Worker):
    """
    Task worker, used to run tasks in task queues. May be extended in future.
    """
    subject = 'Task Worker Notification'

    def run(self, request, *args, **kwargs):
        """
        What to do?
        """
        raise NotImplementedError()

    def error_handler(self, request, *args, **kwargs):
        """
        Error handler
        """
        raise NotImplementedError()

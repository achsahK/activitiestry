# python libraries
import httplib2
import datetime

# app engine libraries
from google.appengine.ext import db

# user defined libraries
from gaeqblib_broken.models import Model
from gaeqblib_broken.utilities.functions import get_future_datetime

# 3rd party libraries
from oauth2client.contrib.appengine import CredentialsProperty

# constants
SERVER_LATENCY = 60


class Oauth2Credentials(Model, db.Model):
    """
    Model for saving Oauth2.0 tokens of a user
    """
    user = db.UserProperty(required=True)
    refresh_token = db.StringProperty(required=True)
    access_token = db.StringProperty(required=True)
    expiry_in = db.DateTimeProperty(required=True)  # UTC
    credentials = CredentialsProperty()

    @property
    def is_expired(self):
        """
        A boolean representing whether the token has expired or not
        """
        # an offset is used to compensate for any server latency
        current_time = get_future_datetime(current_datetime=datetime.datetime.utcnow(), seconds=SERVER_LATENCY)
        if self.expiry_in < current_time:
            return True  # token invalid
        else:
            return False  # token valid

    def refresh(self):
        """
        This function fetch new access_token using the refresh_token
        """
        self.credentials.refresh(httplib2.Http())
        self.expiry_in = self.credentials.token_expiry
        self.access_token = self.credentials.access_token
        self.refresh_token = self.credentials.refresh_token
        self.save()

    def life_in_seconds(self):
        """
        This function returns the life span of access token in seconds
        """
        self.get_valid_access_token()
        # an offset is used to compensate for any server latency
        current_time = get_future_datetime(current_datetime=datetime.datetime.utcnow(), seconds=SERVER_LATENCY)
        seconds = (self.expiry_in - current_time).total_seconds()
        return int(seconds)

    def get_valid_access_token(self):
        """
        This function checks the validity of token, refreshes token if expired and return the valid token
        """
        if self.is_expired:
            self.refresh()
        else:
            pass
        return self.access_token

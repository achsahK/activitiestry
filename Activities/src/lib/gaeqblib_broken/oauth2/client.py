# python libraries
from oauth2client.client import OAuth2WebServerFlow

# app engine libraries
from google.appengine.api import users

# django libraries
from django.conf import settings
from django.http import HttpResponseRedirect

# django models
from models import Oauth2Credentials


#  OAuth2.0 dance or workflow
#
#                Consumer                           Service provider
#
#
#                   |                                      |
#                   |   -------- Request Token ------->    |
#                   |   Call webservice of the service     |
#                   |   provider using a `Request token`.  |
#                   |                                      |
#                   |                                      |
#                   |   <----- Authorization Code -----    |
#                   |   Service provider performs user     |
#                   |   authentication and respond with    |
#                   |   a `Authorization code`.            |
#                   |                                      |
#                   |                                      |
#                   |   ----- Authorization Code ----->    |
#                   |   Exchange `Authorization code` for  |
#                   |   `Access Token`.                    |
#                   |                                      |
#                   |                                      |
#                   |   <-------- Access Token --------    |
#                   |                                      |
#                   |                                      |
#                   |   -------- Access Token -------->    |
#                   |   Using the `Access Token` data can  |
#                   |   be obtained from Service providers |
#                   |   APIs.                              |
#                   |                                      |
#


try:
    oauth2 = OAuth2WebServerFlow(
        client_id=settings.OAUTH2_PARAMS['client_id'],
        client_secret=settings.OAUTH2_PARAMS['client_secret'],
        scope=settings.OAUTH2_PARAMS['scopes'],
        redirect_uri=settings.OAUTH2_PARAMS['redirect_url'],
        access_type='offline',
        approval_prompt='force',
        include_granted_scopes='true',
    )
except Exception as e:
    raise Exception('Improperly configured OAUTH2_PARAMS. Please check the settings.')


def step1():
    """
    This function initiates the Oauth2.0 process.
    @see: https://developers.google.com/accounts/docs/OAuth2
    @aee: https://developers.google.com/api-client-library/python/guide/aaa_oauth
    """
    auth_uri = oauth2.step1_get_authorize_url()
    return HttpResponseRedirect(auth_uri)


def step2(code=None):
    """
    This function exchange the authorization code for access code with the Oauth2.0 server.
    @see: https://developers.google.com/accounts/docs/OAuth2
    @see: https://developers.google.com/api-client-library/python/guide/aaa_oauth
    @see: https://developers.google.com/api-client-library/python/guide/google_app_engine
    """
    if code:

        # obtain credentials from oauth2client
        credentials = oauth2.step2_exchange(code)

        # fetch the user credentials from data store
        user = users.User(email=credentials.id_token['email'])
        user_credentials = Oauth2Credentials.all().filter('user =', user).get()

        # update record, if user credentials exists
        if user_credentials:
            user_credentials.expiry_in = credentials.token_expiry
            user_credentials.access_token = credentials.access_token
            user_credentials.refresh_token = credentials.refresh_token
            user_credentials.credentials = credentials
            user_credentials.save()

        # create new record, if user credentials do not exists
        else:
            Oauth2Credentials(
                user=user, refresh_token=credentials.refresh_token, access_token=credentials.access_token,
                expiry_in=credentials.token_expiry, credentials=credentials
            ).save()

        # return access token
        return credentials.access_token
    else:
        raise Exception('Authorization code cannot be empty.')

# python libraries
import re

# django libraries
from django.conf import settings
from django.conf.urls import url

# django views
import views


try:
    callback_url = settings.OAUTH2_PARAMS['redirect_url']
    uri_regex = r'(?:[^\/]*)\/\/(?:[^\/]*)\/(.*)'
    callback_uri = re.match(uri_regex, callback_url).group(1)
except:
    raise Exception('Improperly configured OAUTH2_PARAMS. Please check the settings.')


# url patterns used by oauth2. This needs to be added with the application urls.
oauth2_urlpatterns = [
    url(r'^oauth2request$', views.Oauth2RequestView.as_view(), name='oauth2_request'),
    url(r'^' + callback_uri, views.Oauth2CallbackView.as_view(), name='oauth2_callback'),
    url(r'^oauth2credentials$', views.Oauth2CredentialsView.as_view(), name='oauth2_credentials'),
]

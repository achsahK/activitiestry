# python libraries
import json
import datetime

# app engine libraries
from google.appengine.api import users

# django libraries
from django.conf import settings
from django.views.generic import View
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect

# user defined libraries
from gaeqblib_broken.oauth2.client import step1, step2

# models
from gaeqblib_broken.oauth2.models import Oauth2Credentials


try:
    hand_over_url = settings.OAUTH2_PARAMS['hand_over_url']
except:
    raise Exception('Improper configuration OAUTH2_PARAMS. Please check settings')


class Oauth2RequestView(View):
    """
    This view initiates the OAuth2.0 process
    """
    def get(self, request, *args, **kwargs):
        return step1()


class Oauth2CallbackView(View):
    """
    This view extract authorization code, save the credentials and redirect to the hand_over_url
    """
    def get(self, request, *args, **kwargs):
        error = request.GET.get('error', None)
        code = request.GET.get('code', None)
        if error is None:
            access_token = step2(code)
        else:
            raise Exception('OAuth2.0 server denied access. Error : %s' % error)

        # Prepare response object and set access_token as cookie
        response = HttpResponseRedirect(hand_over_url)
        response.set_cookie('token', access_token)
        return response


class Oauth2CredentialsView(View):
    """
    This view returns oauth2 credentials of a logged in user
    """
    def get(self, request, *args, **kwargs):

        # retrieve user credentials
        oauth2_credentials = Oauth2Credentials.all().filter('user =', users.User(email=request.user.email)).get()

        # perform OAuth dance if credentials are not available
        if oauth2_credentials is None:
            return HttpResponseRedirect('%s/oauth2request' % settings.HOST_URL)

        # perform OAuth dance if credentials are invalid or corrupt
        try:
            oauth2_credentials.get_valid_access_token()
        except:
            return HttpResponseRedirect('%s/oauth2request' % settings.HOST_URL)

        # generate response
        response = {
            'user': request.user.email,
            'access_token': oauth2_credentials.access_token,
            'refresh_token': oauth2_credentials.refresh_token,
            'expiry_in': oauth2_credentials.expiry_in.strftime('%d/%m/%Y %H:%M:%S'),
            'current_server_time': datetime.datetime.utcnow().strftime('%d/%m/%Y %H:%M:%S'),
            'is_expired': oauth2_credentials.is_expired,
            'remaining_life': oauth2_credentials.life_in_seconds()
        }
        return HttpResponse(json.dumps(response, indent=4), content_type='application/json')

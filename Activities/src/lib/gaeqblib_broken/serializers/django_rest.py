# python libraries
import six
import copy
from collections import OrderedDict

# django rest libraries
from rest_framework import serializers

# user defined libraries
from gaeqblib_broken.properties.django_rest import ConditionalPropertiedSerializerMeta


@six.add_metaclass(ConditionalPropertiedSerializerMeta)
class RestSerializer(serializers.Serializer):
    """
    Extends `rest_framework.serializers.Serializer`, to add features like exclude and depth control.
    """
    class Meta:
        exclude = ()  # fields to be excluded in the output

    def get_fields(self):
        """
        Return the dict of field names -> field instances that should be used.
        """
        declared_fields = copy.deepcopy(self._declared_fields)
        exclude_fields = getattr(self.Meta, 'exclude', ())

        # remove excluded fields
        for field in exclude_fields:
            del declared_fields[field]

        return declared_fields

    @property
    def validated_data(self):
        """
        Hack to handle `source` in Reference field
        """
        modified_data = OrderedDict()
        data = super(RestSerializer, self).validated_data
        for key, value in data.iteritems():
            if '_obj' in key:
                modified_data[key.replace('_obj', '')] = value
            modified_data[key] = value
        return modified_data

# python libraries
import json
import unittest
import logging


# user defnied libraries
from gaeqblib_broken.serializers import JsonSerializer
from gaeqblib_broken.unittests.utilities import Environment  # utilities
from gaeqblib_broken.unittests.models.entities import DistrictDb, StateDb, PersonDb, DistrictNdb, StateNdb, PersonNdb  # models


class SerializerDbTestCase(Environment, unittest.TestCase):
    """
    Test cases for `serializer` using `db` models
    """
    # test for json_serializer using db
    def test_json_serializer_db(self):
        district = DistrictDb(name="Thrissur").save()
        state = StateDb(name="Kerala").save()
        person = PersonDb(name="Akhil Lawrence", district=district, state=state).save()
        data = JsonSerializer().serializer(objs=[person], excludes={'district': {'name': {}}})
        # logging.getLogger("Log").debug(data)
        data = json.loads(data)
        # logging.getLogger("Log").debug(data)
        self.assertNotIn("name", data[0]["district"].keys())


class SerializerNdbTestCase(Environment, unittest.TestCase):
    """
    Test cases for `serializer` using `db` models
    """
    # test for json_serializer using ndb
    def test_json_serializer_db(self):
        district = DistrictNdb(name="Thrissur").save()
        state = StateNdb(name="Kerala").save()
        person = PersonNdb(name="Akhil Lawrence", district=district, state=state).save()
        data = JsonSerializer().serializer(objs=[person], excludes={'state': {'name': {}}})
        # logging.getLogger("Log").debug(data)
        data = json.loads(data)
        # logging.getLogger("Log").debug(data)
        self.assertNotIn("name", data[0]["state"].keys())

# app engine libraries
from google.appengine.ext import db
from google.appengine.ext import ndb

# gaeqblib_broken libraries
from gaeqblib_broken.models import Model
from gaeqblib_broken.properties import db as db_p
from gaeqblib_broken.properties import ndb as ndb_p


class TasksDb(Model, db.Model):
    """
    Test `db` model used for test purposes of JSONProperty
    """
    day = db.StringProperty()
    tasks = db_p.JSONProperty()


class TasksNdb(Model, ndb.Model):
    """
    Test `ndb` model used for test purposes of JSONProperty
    """
    day = ndb.StringProperty()
    tasks = ndb_p.JSONProperty()


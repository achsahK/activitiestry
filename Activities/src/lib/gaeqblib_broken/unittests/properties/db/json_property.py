# python libraries
import json
import unittest


# user defnied libraries
from gaeqblib_broken.unittests.utilities import Environment  # utilities
from gaeqblib_broken.unittests.properties.entities import TasksDb  # models


class JsonpropertyDbTestCase(Environment, unittest.TestCase):
    """
    Test cases for `JSONDbProperty` in `db`
    """
    def testReadWrite(self):
        data = {"Morning": "Sleep well", "Noon": "Eat well"}
        obj_save = TasksDb(day="monday", tasks=data).save()
        obj_get = TasksDb.get(obj_save.safe_key)
        self.assertEqual(obj_get.tasks, json.loads(json.dumps(data)))


# python libraries
import json
import unittest


# user defnied libraries
from gaeqblib_broken.unittests.utilities import Environment  # utilities
from gaeqblib_broken.unittests.properties.entities import TasksNdb  # models


class JsonpropertyNdbTestCase(Environment, unittest.TestCase):
    """
    Test cases for `JSONNdbProperty`
    """
    def testReadWrite(self):
        data = {"Morning": "Sleep well", "Noon": "Eat well"}
        obj_save = TasksNdb(day="monday", tasks=data).save()
        obj_get = TasksNdb.get(obj_save.safe_key)
        self.assertEqual(obj_get.tasks, json.loads(json.dumps(data)))

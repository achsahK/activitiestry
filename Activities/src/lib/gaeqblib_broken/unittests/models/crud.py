# python libraries
import copy
import unittest


# user defnied libraries
from gaeqblib_broken.unittests.utilities import Environment  # utilities
from gaeqblib_broken.unittests.models.entities import PhoneDirectoryDb, PhoneDirectoryNdb  # models

class CrudDbTestCase(Environment, unittest.TestCase):
    """
    Test cases for `CRUD` in `db`
    """
    # test for `get`
    def testGet(self):
        obj_save = PhoneDirectoryDb(name="Akhil Lawrence", number="+919995510538").save()
        obj_get = PhoneDirectoryDb.get(obj_save.safe_key)
        self.assertEqual(obj_get.name, obj_save.name)
        self.assertEqual(obj_get.number, obj_save.number)

    # test for `get_multi`
    def testGetMulti(self):
        obj1 = PhoneDirectoryDb(name="Akhil Lawrence", number="+919995510538").save()
        obj2 = PhoneDirectoryDb(name="Lawrence P J", number="+919388150080").save()
        obj3 = PhoneDirectoryDb(name="Fr. Xavi Puthiti", number="+919446763190").save()
        PhoneDirectoryDb.save_multi([obj1, obj2, obj3])
        objs = PhoneDirectoryDb.get_multi([obj1.safe_key, obj2.safe_key, obj3.safe_key])
        self.assertEqual(objs[0].name, obj1.name)
        self.assertEqual(objs[0].number, obj1.number)
        self.assertEqual(objs[1].name, obj2.name)
        self.assertEqual(objs[1].number, obj2.number)
        self.assertEqual(objs[2].name, obj3.name)
        self.assertEqual(objs[2].number, obj3.number)

    # test for `save`
    def testSave(self):
        PhoneDirectoryDb(name="Akhil Lawrence", number="+919995510538").save()
        self.assertEqual(1, len(PhoneDirectoryDb.all().fetch(1000)))

    # test for `save_multi`
    def testSaveMulti(self):
        obj1 = PhoneDirectoryDb(name="Akhil Lawrence", number="+919995510538").save()
        obj2 = PhoneDirectoryDb(name="Lawrence P J", number="+919388150080").save()
        obj3 = PhoneDirectoryDb(name="Fr. Xavi Puthiti", number="+919446763190").save()
        PhoneDirectoryDb.save_multi([obj1, obj2, obj3])
        self.assertEqual(3, len(PhoneDirectoryDb.all().fetch(1000)))

    # test for `edit`
    def testEdit(self):
        obj = PhoneDirectoryDb(name="Akhil Lawrence", number="+919995510538").save()
        old_obj = copy.deepcopy(obj)
        obj.name = "updated"
        obj.save()
        new_obj = PhoneDirectoryDb.get(obj.safe_key)
        self.assertNotEquals(old_obj.name, new_obj.name)
        self.assertEqual(old_obj.number, new_obj.number)
        self.assertEqual(1, len(PhoneDirectoryDb.all().fetch(1000)))

    # test for `delete`
    def testDelete(self):
        obj = PhoneDirectoryDb(name="Akhil Lawrence", number="+919995510538").save()
        self.assertEqual(1, len(PhoneDirectoryDb.all().fetch(1000)))
        obj.delete()
        self.assertEqual(0, len(PhoneDirectoryDb.all().fetch(1000)))


class CrudNdbTestCase(Environment, unittest.TestCase):
    """
    Test cases for `CRUD` in `ndb`
    """
    # test for `get`
    def testGet(self):
        obj_save = PhoneDirectoryNdb(name="Akhil Lawrence", number="+919995510538").save()
        obj_get = PhoneDirectoryNdb.get(obj_save.safe_key)
        self.assertEqual(obj_get.name, obj_save.name)
        self.assertEqual(obj_get.number, obj_save.number)

    # test for `get_multi`
    def testGetMulti(self):
        obj1 = PhoneDirectoryNdb(name="Akhil Lawrence", number="+919995510538").save()
        obj2 = PhoneDirectoryNdb(name="Lawrence P J", number="+919388150080").save()
        obj3 = PhoneDirectoryNdb(name="Fr. Xavi Puthiri", number="+919446763190").save()
        PhoneDirectoryNdb.save_multi([obj1, obj2, obj3])
        objs = PhoneDirectoryNdb.get_multi([obj1.safe_key, obj2.safe_key, obj3.safe_key])
        self.assertEqual(objs[0].name, obj1.name)
        self.assertEqual(objs[0].number, obj1.number)
        self.assertEqual(objs[1].name, obj2.name)
        self.assertEqual(objs[1].number, obj2.number)
        self.assertEqual(objs[2].name, obj3.name)
        self.assertEqual(objs[2].number, obj3.number)

    # test for `save`
    def testSave(self):
        PhoneDirectoryNdb(name="Akhil Lawrence", number="+919995510538").save()
        self.assertEqual(1, len(PhoneDirectoryNdb.query().fetch(1000)))

    # test for `save_multi`
    def testSaveMulti(self):
        obj1 = PhoneDirectoryNdb(name="Akhil Lawrence", number="+919995510538").save()
        obj2 = PhoneDirectoryNdb(name="Lawrence P J", number="+919388150080").save()
        obj3 = PhoneDirectoryNdb(name="Fr. Xavi Puthiri", number="+919446763190").save()
        PhoneDirectoryDb.save_multi([obj1, obj2, obj3])
        self.assertEqual(3, len(PhoneDirectoryNdb.query().fetch(1000)))

    # test for `edit`
    def testEdit(self):
        obj = PhoneDirectoryNdb(name="Akhil Lawrence", number="+919995510538").save()
        old_obj = copy.deepcopy(obj)
        obj.name = "updated"
        obj.save()
        new_obj = PhoneDirectoryNdb.get(obj.safe_key)
        self.assertNotEquals(old_obj.name, new_obj.name)
        self.assertEqual(old_obj.number, new_obj.number)

    # test for `delete`
    def testDelete(self):
        obj = PhoneDirectoryNdb(name="Akhil Lawrence", number="+919995510538").save()
        self.assertEqual(1, len(PhoneDirectoryNdb.query().fetch(1000)))
        obj.delete()
        self.assertEqual(0, len(PhoneDirectoryNdb.query().fetch(1000)))


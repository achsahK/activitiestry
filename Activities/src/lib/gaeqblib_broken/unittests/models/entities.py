# app engine libraries
from google.appengine.ext import db
from google.appengine.ext import ndb

# gaeqblib_broken libraries
from gaeqblib_broken.models import Model


class PhoneDirectoryDb(Model, db.Model):
    """
    Test `db` model used in Crud, advances_queries
    """
    name = db.StringProperty()
    number = db.StringProperty()
    _serializable_fields = ['name', 'number']


class PhoneDirectoryNdb(Model, ndb.Model):
    """
    Test `ndb` model used in Crud, advances_queries
    """
    name = ndb.StringProperty()
    number = ndb.StringProperty()
    _serializable_fields = ['name', 'number']


class DistrictDb(Model, db.Model):
    """
    Test `db` model used in prefetch
    """
    name = db.StringProperty()
    _serializable_fields = ['name']


class StateDb(Model, db.Model):
    """
    Test `db` model used in prefetch
    """
    name = db.StringProperty()
    _serializable_fields = ['name']


class PersonDb(Model, db.Model):
    """
    Test `db` model used in prefetch
    """
    name = db.StringProperty()
    district = db.ReferenceProperty(DistrictDb)
    state = db.ReferenceProperty(StateDb)
    _serializable_fields = ['name', 'district', 'state']


class DistrictNdb(Model, db.Model):
    """
    Test `db` model used in prefetch
    """
    name = db.StringProperty()
    _serializable_fields = ['name']


class StateNdb(Model, db.Model):
    """
    Test `db` model used in prefetch
    """
    name = db.StringProperty()
    _serializable_fields = ['name']


class PersonNdb(Model, db.Model):
    """
    Test `db` model used in prefetch
    """
    name = db.StringProperty()
    district = db.ReferenceProperty(DistrictNdb)
    state = db.ReferenceProperty(StateNdb)
    _serializable_fields = ['name', 'district', 'state']


# python libraries
import unittest


# user defnied libraries
from gaeqblib_broken.unittests.utilities import Environment  # utilities
from gaeqblib_broken.unittests.models.entities import DistrictDb, StateDb, PersonDb, DistrictNdb, StateNdb, PersonNdb  # models


class PrefetchDbTestCase(Environment, unittest.TestCase):
    """
    Test cases for `prefetch` in `db`
    """
    # test for `prefetch_ref_props`
    def test_prefetch(self):
        district_1 = DistrictDb(name='Thrissur').save()
        state_1 = StateDb(name='Kerala').save()
        PersonDb(name='Akhil Lawrence', district=district_1.key(), state=state_1.key()).save()
        district_2 = DistrictDb(name='Wayanad').save()
        state_2 = StateDb(name='Kerala').save()
        PersonDb(name='Lawrence P J', district=district_2.key(), state=state_2.key()).save()
        entities = PersonDb.make_query(order_by=['name'])['results']
        entities = PersonDb.prefetch_ref_props([entity for entity in entities], ['district', 'state'])
        self.assertEqual(entities[0].district_obj.name, 'Thrissur')
        self.assertEqual(entities[1].district_obj.name, 'Wayanad')
        self.assertEqual(entities[0].state_obj.name, 'Kerala')
        self.assertEqual(entities[1].state_obj.name, 'Kerala')


class PrefetchNdbTestCase(Environment, unittest.TestCase):
    """
    Test cases for `prefetch` in `db`
    """
    # test for `prefetch_ref_props`
    def test_prefetch(self):
        district_1 = DistrictNdb(name='Thrissur').save()
        state_1 = StateNdb(name='Kerala').save()
        PersonNdb(name='Akhil Lawrence', district=district_1.key(), state=state_1.key()).save()
        district_2 = DistrictNdb(name='Wayanad').save()
        state_2 = StateNdb(name='Kerala').save()
        PersonNdb(name='Lawrence P J', district=district_2.key(), state=state_2.key()).save()
        entities = PersonNdb.make_query(order_by=['name'])['results']
        entities = PersonNdb.prefetch_ref_props([entity for entity in entities], ['district', 'state'])
        self.assertEqual(entities[0].district_obj.name, 'Thrissur')
        self.assertEqual(entities[1].district_obj.name, 'Wayanad')
        self.assertEqual(entities[0].state_obj.name, 'Kerala')
        self.assertEqual(entities[1].state_obj.name, 'Kerala')

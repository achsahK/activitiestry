# python libraries
import unittest
import logging


# user defnied libraries
from gaeqblib_broken.unittests.utilities import Environment  # utilities
from gaeqblib_broken.unittests.models.entities import PhoneDirectoryDb, PhoneDirectoryNdb  # models
from gaeqblib_broken.models.api import Api


class PhoneDirectoryApi(Api):
    model = PhoneDirectoryDb


class ApiDBTestCase(Environment, unittest.TestCase):
    def testGet(self):
        obj_save = PhoneDirectoryDb(name="Suja Varghese", number="+916787878798").save()
        logging.getLogger("Log").debug("obj_save: %s" % obj_save)
        obj_get = PhoneDirectoryApi().get(key=obj_save.safe_key)
        logging.getLogger("Log").debug("obj_get: %s" % obj_get)
        self.assertEqual(obj_get.name, obj_save.name)
        self.assertEqual(obj_get.number, obj_save.number)

# python libraries
import unittest
import logging


# user defnied libraries
from gaeqblib_broken.unittests.utilities import Environment  # utilities
from gaeqblib_broken.unittests.models.entities import PhoneDirectoryDb, PhoneDirectoryNdb  # models


class AdvancedQueriesDbTestCase(Environment, unittest.TestCase):
    """
    Test cases for `AdvancedQueries` in `db`
    """
    # test for `make_query`
    def test_make_query(self):
        PhoneDirectoryDb(name="Akhil Lawrence", number="+919995510538").save()
        PhoneDirectoryDb(name="Lawrence P J", number="+919388150080").save()
        result = PhoneDirectoryDb.make_query(return_query=True).count()
        self.assertEqual(2, result)

    # test for `make_query` with filters
    def test_make_query_with_filters(self):
        PhoneDirectoryDb(name="Akhil Lawrence", number="+919995510538").save()
        PhoneDirectoryDb(name="Lawrence P J", number="+919388150080").save()
        PhoneDirectoryDb(name="Lawrence P J", number="+919388150080").save()
        filter_1 = {'property': 'number', 'operator': '=', 'value': '+919995510538'}
        result_1 = PhoneDirectoryDb.make_query(filters=[filter_1], return_query=True).count()
        self.assertEqual(1, result_1)
        filter_2 = {'property': 'number', 'operator': '=', 'value': '+919388150080'}
        result_2 = PhoneDirectoryDb.make_query(filters=[filter_2], return_query=True).count()
        self.assertEqual(2, result_2)

    # test for `make_query` with order by
    def test_make_query_with_order_by(self):
        PhoneDirectoryDb(name="Akhil Lawrence", number="+919995510538").save()
        PhoneDirectoryDb(name="Lawrence P J", number="+919388150080").save()
        PhoneDirectoryDb(name="Fr. Xavi Puthiti", number="+919446763190").save()
        result_1 = PhoneDirectoryDb.make_query(order_by=['name'])['results']
        for i in result_1:
            break
        result_2 = PhoneDirectoryDb.make_query(order_by=['-name'])['results']
        for j in result_2:
            break
        self.assertEqual('Akhil Lawrence', i.name)
        self.assertEqual('Lawrence P J', j.name)

    # test for `make_query` with filters and order by
    def test_make_query_with_filter_and_order_by(self):
        PhoneDirectoryDb(name="Akhil Lawrence", number="+919995510538").save()
        PhoneDirectoryDb(name="Duplicate", number="+919995510538").save()
        PhoneDirectoryDb(name="Fr. Xavi Puthiti", number="+919446763190").save()
        filter_1 = {'property': 'number', 'operator': '=', 'value': '+919995510538'}
        result_1 = PhoneDirectoryDb.make_query(filters=[filter_1], order_by=['name'])['results']
        for i in result_1:
            break
        result_2 = PhoneDirectoryDb.make_query(filters=[filter_1], order_by=['-name'])['results']
        for j in result_2:
            break
        self.assertEqual('Akhil Lawrence', i.name)
        self.assertEqual('Duplicate', j.name)


class AdvancedQueriesNdbTestCase(Environment, unittest.TestCase):
    """
    Test cases for `AdvancedQueries` in `ndb`
    """
    # test for `make_query`
    def test_make_query(self):
        PhoneDirectoryNdb(name="Akhil Lawrence", number="+919995510538").save()
        PhoneDirectoryNdb(name="Lawrence P J", number="+919388150080").save()
        result = PhoneDirectoryNdb.make_query(return_query=True).count()
        self.assertEqual(2, result)

    # test for `make_query` with filters
    def test_make_query_with_filters(self):
        PhoneDirectoryNdb(name="Akhil Lawrence", number="+919995510538").save()
        PhoneDirectoryNdb(name="Lawrence P J", number="+919388150080").save()
        PhoneDirectoryNdb(name="Lawrence P J", number="+919388150080").save()
        filter_1 = {'property': 'number', 'operator': '==', 'value': '+919995510538'}
        result_1 = PhoneDirectoryNdb.make_query(filters=[filter_1], return_query=True).count()
        self.assertEqual(1, result_1)
        filter_2 = {'property': 'number', 'operator': '==', 'value': '+919388150080'}
        result_2 = PhoneDirectoryNdb.make_query(filters=[filter_2], return_query=True).count()
        self.assertEqual(2, result_2)

    # test for `make_query` with order by
    def test_make_query_with_order_by(self):
        PhoneDirectoryNdb(name="Akhil Lawrence", number="+919995510538").save()
        PhoneDirectoryNdb(name="Lawrence P J", number="+919388150080").save()
        PhoneDirectoryNdb(name="Fr. Xavi Puthiri", number="+919446763190").save()
        result_1 = PhoneDirectoryNdb.make_query(order_by=['name'])['results']
        i = result_1[0]
        result_2 = PhoneDirectoryNdb.make_query(order_by=['-name'])['results']
        j = result_2[0]
        self.assertEqual('Akhil Lawrence', i.name)
        self.assertEqual('Lawrence P J', j.name)

    # test for `make_query` with filters and order by
    def test_make_query_with_filter_and_order_by(self):
        PhoneDirectoryNdb(name="Akhil Lawrence", number="+919995510538").save()
        PhoneDirectoryNdb(name="Duplicate", number="+919995510538").save()
        PhoneDirectoryNdb(name="Fr. Xavi Puthiri", number="+919446763190").save()
        filter_1 = {'property': 'number', 'operator': '==', 'value': '+919995510538'}
        result_1 = PhoneDirectoryNdb.make_query(filters=[filter_1], order_by=['name'])['results']
        i = result_1[0]
        result_2 = PhoneDirectoryNdb.make_query(filters=[filter_1], order_by=['-name'])['results']
        j = result_2[0]
        self.assertEqual('Akhil Lawrence', i.name)
        self.assertEqual('Duplicate', j.name)

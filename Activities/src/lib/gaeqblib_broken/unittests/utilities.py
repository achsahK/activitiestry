# app engine libraries
from google.appengine.ext import testbed


class Environment():
    """
    This class configure the testing environment by initialising required services
    """
    def setUp(self):
        self.testbed = testbed.Testbed()
        self.testbed.activate()
        self.testbed.init_datastore_v3_stub()
        self.testbed.init_memcache_stub()

    def tearDown(self):
        self.testbed.deactivate()

#! /usr/bin/python


import os
import sys
import unittest
import logging


# configure sys path
BASE_DIR = os.path.realpath(__file__)
temp = BASE_DIR.split('/')
APPLICATION_DIR = '/'.join(temp[:len(temp)-3])
sys.path.append(APPLICATION_DIR)


# import tests
from models import *
from properties import *
from serializers import *


# run tests
if __name__ == '__main__':
    logging.basicConfig(stream=sys.stderr)
    logging.getLogger("Log").setLevel(logging.DEBUG)
    unittest.main()

# python libraries
import re
import logging

# app engine libraries
import endpoints
from google.appengine.api import users
from google.appengine.api import oauth
from google.appengine.api import memcache

# django libraries
from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse

# gaeqblib_broken libraries
from gaeqblib_broken.utilities.functions import get_from_url


class Blank():
    """
    Blank class used to initialize empty object
    """
    pass


def update_request_object(request=None, user=None, is_admin=False, skip_employee=False):
    """
    This function updates the request object
    """
    if request:

        # update user object
        if user and user.email() != "example@example.com":
            request.user = Blank()
            request.user.email = user.email()
            request.user.is_admin = is_admin
            request.user.user_id = user.user_id()
            request.user.nick_name = user.nickname()
        else:
            raise Exception('Invalid User Object')

        # populate employee (space)
        if not skip_employee:

            # import models
            from space.employee.models import Employee, EmployeeDetails

            # update employee details
            key = 'Employee_' + user.email()

            # serve employee from memcache if available
            employee_data = memcache.get(key)
            if employee_data is None:

                # serve employee from datastore if not available in memcache
                email_filter = {'property': 'companyEmail', 'operator': '=', 'value': user.email()}
                employee_data = Employee.make_query(filters=[email_filter], prefetch_props=['role'])['results'][0]
                if memcache.add(key, request.employee, 600):
                    logging.info('Memcache set success [key - %s]' % key)
                else:
                    logging.error('Memcache set fail [key - %s]' % key)
            else:
                pass
            request.employee = employee_data

            key = 'EmployeeDetails_' + user.email()

            # serve employee details from memcache if available
            employee_details_data = memcache.get(key)
            if employee_details_data is None:

                # serve employee details from datastore if not available in memcache
                employee_details_data = EmployeeDetails.all().filter('employee =', request.employee).get()
                if memcache.add(key, request.employee_details, 600):
                    logging.info('Memcache set success [key - %s]' % key)
                else:
                    logging.error('Memcache set fail [key - %s]' % key)
            else:
                pass
            request.employee_details = employee_details_data

            # update privileges
            request.privileges = request.employee.role_obj.privileges

        # update privileges (activities)
        else:

            # serve privileges from memcache if available
            key = 'Privileges_' + user.email()
            privileges = memcache.get(key)
            if privileges is None:

                # serve privileges from datastore if not available in memcache
                privileges = get_from_url(
                    url=settings.SPACE_APIS.get('privileges'),
                    user=request.user.email
                )
                if memcache.add(key, privileges):
                    logging.info('Memcache set success [key - %s]' % key)
                else:
                    logging.error('Memcache set fail [key - %s]' % key)
            else:
                pass
            request.privileges = privileges

            # update employee
            request.employee = privileges.get('loggedInEmployee')

        # update logout url
        request.logout_url = users.create_logout_url('/')
    else:
        raise Exception('Invalid Request Object')


def api_authentication(request, response=HttpResponse('Unauthorized access!', status=403)):
    """
    API authentication (supports oauth 2.0 and endpoints api)
    """
    # oauth 2.0 authentication
    try:

        # identify user and update user details in request
        user = oauth.get_current_user(settings.OAUTH2_PARAMS['scopes'])
        is_admin = oauth.is_current_user_admin(settings.OAUTH2_PARAMS['scopes'])
        update_request_object(request, user=user, is_admin=is_admin, skip_employee=True)
        return None
    except:
        pass

    # endpoints api authentication
    try:

        # identify user and update user details in request
        user = endpoints.get_current_user(settings.OAUTH2_PARAMS['scopes'])
        is_admin = endpoints.is_current_user_admin(settings.OAUTH2_PARAMS['scopes'])
        update_request_object(request, user=user, is_admin=is_admin, skip_employee=True)
        return None
    except:
        pass

    # code for testing purposes, comment in production environment
    # user = users.User(email="akhill+employee@qburst.com")
    # is_admin = True
    # update_request_object(request, user=user, is_admin=is_admin, skip_employee=True)
    # return None

    # default
    return response


class Authentication(object):
    """
    This class implements authentication middleware. At the time of requests, this middleware
    verifies whether the user is logged in or not. If user is not logged in, user is redirected
    to the login page.
    """
    @staticmethod
    def process_request(request):

        # flag used to customize privilege checking in views
        request.require_auth = True

        # holding page
        if settings.HOLDING_PAGE['up']:
            return HttpResponseRedirect(reverse('holding_page'))

        # partial holding page
        if settings.PARTIAL_HOLDING_PAGE['up']:
            for uri_pattern in settings.PARTIAL_HOLDING_PAGE['uri_patterns']:
                if re.match(uri_pattern, str(request.path)):
                    return HttpResponseRedirect(reverse('partial_holding_page'))

        # skip authentication for preflight requests
        if request.method.lower() == 'options':
            return None

        # skip authentication for specified uri patterns
        if settings.IGNORE_URI_PATTERNS:
            for uri_pattern in settings.IGNORE_URI_PATTERNS:
                if re.match(uri_pattern, str(request.path)):
                    return None

        # oauth 2.0 authentication
        if settings.AUTHENTICATION_METHOD is 'OAUTH_2_0':
            try:

                # identify user and update user details in request
                user = oauth.get_current_user(settings.OAUTH2_PARAMS['scopes'])
                is_admin = oauth.is_current_user_admin(settings.OAUTH2_PARAMS['scopes'])
                update_request_object(request, user=user, is_admin=is_admin, skip_employee=True)
                return None
            except oauth.OAuthRequestError:

                # perform api authentication. If it fails, return 403
                return api_authentication(request)

        # users API authentication
        elif settings.AUTHENTICATION_METHOD is 'USERS_API':

            # identify user and update user details in request
            if users.get_current_user():
                user = users.get_current_user()
                is_admin = users.is_current_user_admin()
                update_request_object(request, user=user, is_admin=is_admin, skip_employee=True)
                return None
            else:

                # perform api authentication. If it fails, redirect to users API login page
                return api_authentication(request, HttpResponseRedirect(users.create_login_url(request.get_full_path())))

        # default
        else:
            return HttpResponse('Unauthorized access!', status=403)

# python libraries
import logging


class HandleExceptions(object):

    @staticmethod
    def process_exception(request, exception):
        logging.exception(exception)
        return None

# django libraries
from django.conf import settings
from django.http import HttpResponse

# django rest libraries
from rest_framework import generics
from rest_framework.response import Response


class RestView(generics.GenericAPIView):
    """
    This extends the generic API view available in django rest framework
    """
    key = None  # key passed in URL
    cursor = None  # cursor passed in URL
    ordering = []  # ordering to be used
    direction = 'f'  # pagination direction
    initial_values = None  # flag to determine initial_values should be served or not
    paginate_by = 20  # record limit to be used with pagination
    http_method_names = ['get', 'post', 'put', 'delete', 'options']  # allowed http methods
    permissions_required = {'get': None, 'post': None, 'put': None, 'delete': None}  # permissions

    def parse_from_request(self, param):
        """
        parse requested param from kwargs/request.GET
        """
        if self.kwargs.get(param):
            setattr(self, param, self.kwargs.get(param))
        elif self.request.GET.get(param):
            setattr(self, param, self.request.GET.get(param))
        else:
            pass

    def get_data_dict(self):
        """
        returns data dict after removing empty items
        """
        # return {k: v for k, v in self.request.data.items() if v}
        return {k: v for k, v in self.request.data.items()}

    def get_headers(self):
        response = {
            'Access-Control-Allow-Methods': 'GET, POST',
            'Access-Control-Allow-Headers': 'Content-Type, X-CSRFToken, Authorization'
        }
        origin = self.request.META.get('HTTP_ORIGIN')

        if origin in settings.JAVASCRIPT_ORIGINS:
            response.update({'Access-Control-Allow-Origin': origin})
        return response

    def dispatch(self, request, *args, **kwargs):
        """
        The default implementation will inspect the HTTP method and attempt to delegate to a method that matches
        the HTTP method
        """
        # identify request method
        request_method = request.method.lower()

        permissions = self.permissions_required[request_method] if request_method in self.permissions_required else []
        if not isinstance(permissions, list):
            permissions = [permissions]

        # allow pre-flight requests
        if request_method == 'options' or request.user.is_admin or not request.require_auth:
            pass

        # allow requests with valid permissions
        elif permissions and set(permissions).intersection(request.privileges):
            pass

        # block requests without valid permissions
        else:
            response = HttpResponse('Access denied!', status=403)
            headers = self.get_headers()
            for key, value in headers.iteritems():
                response[key] = value
            return response

        # parse params
        self.parse_from_request('key')
        self.parse_from_request('cursor')
        self.parse_from_request('direction')
        self.parse_from_request('initial_values')

        # handover to the super class
        return super(RestView, self).dispatch(request, *args, **kwargs)

    def options(self, request, *args, **kwargs):
        """
        OPTIONS HTTP method handler
        """
        return Response(status=200, headers=self.get_headers())

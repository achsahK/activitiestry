# python libraries
import logging

# app engine libraries
from google.appengine.api import memcache

# django rest libraries
from rest_framework.response import Response

# user defined libraries
from rest_form_view import RestFormView


class RestCrudView(RestFormView):
    """
    This extends the RestFormView
    """
    model = None  # model class to be used
    prefetch = []  # list of properties to be prefetched
    http_method_names = ['get', 'post', 'put', 'delete', 'options']  # allowed http methods

    def get_object(self):
        """
        Fetch single entity/record identified by the safe_key
        """
        # get object from memcache
        key = self.model.__name__ + '_object_' + self.key
        obj = memcache.get(key)
        if obj is None:

            # get object from datastore
            obj = self.model.get(self.key)  # get object
            obj = self.model.prefetch_ref_props(obj, obj._reference_fields)[0]  # prefetch references

            # update memcache
            if memcache.add(key, obj):
                logging.info('Memcache set success [key - %s]' % key)
            else:
                logging.error('Memcache set fail [key - %s]' % key)
        else:
            pass

        return obj

    def get_queryset(self):
        """
        Fetch list of entities/records that is available
        """
        object_list = self.model.make_query(
            limit=self.paginate_by,
            cursor=self.cursor,
            order_by=self.ordering,
            direction=self.direction,
            prefetch_props=self.prefetch
        )
        return object_list

    def get_success_headers(self, data):
        """
        Headers to be used on successful creation of an object
        """
        try:
            if not self.request.is_ajax:
                return {'Location': self.request.path + '/' + data.safe_key}
            else:
                return {}
        except (TypeError, KeyError):
            return {}

    def create_object(self, validated_data, should_persist=True):
        """
        Create an object using data from validated_data
        """
        if not should_persist:
            return self.model(**validated_data)

        obj = self.model(**validated_data).save()  # create object
        obj = self.model.prefetch_ref_props(obj, obj._reference_fields)[0]  # prefetch references

        # update memcached
        key = self.model.__name__ + '_object_' + obj.safe_key
        if memcache.add(key, obj):
            logging.info('Memcache add failed [key - %s]' % key)
        else:
            logging.error('Memcache add success [key - %s]' % key)

        return obj

    def update_object(self, instance, validated_data):
        """
        Replace values in instance with new values from validated_data
        """
        for field, value in validated_data.items():
            setattr(instance, field, value)
        instance.save()  # update object
        instance = self.model.prefetch_ref_props(instance, instance._reference_fields)[0]  # prefetch references

        # update memcached
        key = self.model.__name__ + '_object_' + self.key
        if not memcache.replace(key, instance):
            logging.error('Memcache replace success [key - %s]' % key)
        else:
            logging.info('Memcache replace failed [key - %s]' % key)

        # update memcached (key-object-api)
        key = self.model.__name__ + '_serialized_object_' + self.key
        if not memcache.replace(key, instance):
            logging.error('Memcache replace success [key - %s]' % key)
        else:
            logging.info('Memcache replace failed [key - %s]' % key)

        return instance

    def delete_object(self):
        """
        Delete instance
        """
        # Fix stupid "feature"/bug in gaeqblib
        raise NotImplementedError
        # self.get_object().delete()  # delete object

        # update memcached
        key = self.model.__name__ + '_object_' + self.key
        if not memcache.delete(key):
            logging.error('Memcache delete success [key - %s]' % key)
        else:
            logging.info('Memcache delete failed [key - %s]' % key)

        # update memcached (key-object-api)
        key = self.model.__name__ + '_serialized_object_' + self.key
        if not memcache.delete(key):
            logging.error('Memcache delete success [key - %s]' % key)
        else:
            logging.info('Memcache delete failed [key - %s]' % key)

    def get(self, request, *args, **kwargs):
        """
        GET HTTP method handler
        """
        # return initial values
        if self.initial_values:
            data = self.get_initial_values()
            return Response(data, headers=self.get_headers())

        # return object
        elif self.key:
            data = self.get_object()
            if data:
                data = self.serializer_class(data).data
                return Response(data, headers=self.get_headers())
            else:
                return Response(status=404, headers=self.get_headers())

        # return object list
        else:
            data = self.get_queryset()
            data['results'] = self.serializer_class(data['results'], many=True).data
            return Response(data, headers=self.get_headers())

    def post(self, request, *args, **kwargs):
        """
        POST HTTP method handler
        """
        # remove empty data
        data = self.get_data_dict()

        # build serializer
        serializer = self.serializer_class(data=data)

        # save valid data
        if serializer.is_valid():
            obj = self.create_object(serializer.validated_data)
            headers = self.get_success_headers(obj)
            headers.update(self.get_headers())
            data = self.serializer_class(obj).data
            return Response(data, status=201, headers=headers)

        # return errors
        else:
            return Response(serializer.errors, status=400, headers=self.get_headers())

    def put(self, request, *args, **kwargs):
        """
        PUT HTTP method handler
        """
        # remove empty data
        data = self.get_data_dict()

        # update record associated with safe_key
        if self.key:
            instance = self.get_object()
            serializer = self.serializer_class(instance, data=data, partial=True)

            # update valid data
            if serializer.is_valid():
                obj = self.update_object(instance, serializer.validated_data)
                data = self.serializer_class(obj).data
                return Response(data, status=200, headers=self.get_headers())

            # return errors
            else:
                return Response(serializer.errors, status=400, headers=self.get_headers())

        # no content
        else:
            return Response('No content', status=204, headers=self.get_headers())

    def delete(self, request, *args, **kwargs):
        """
        DELETE HTTP method handler
        """
        # Fix stupid "feature"/bug in gaeqblib
        raise NotImplementedError
        # save valid data
        if self.key:
            self.delete_object()
            return Response(status=204, headers=self.get_headers())

        # return errors
        else:
            return Response('Invalid safe_key', status=400, headers=self.get_headers())

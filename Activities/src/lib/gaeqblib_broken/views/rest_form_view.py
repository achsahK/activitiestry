# python libraries
import json

# django libraries
from django.http import HttpResponse
from django.middleware.csrf import get_token

# django rest libraries
from rest_framework.response import Response
from rest_framework.serializers import ChoiceField

# user defined libraries
from rest_view import RestView
from gaeqblib_broken.properties.django_rest import ReferenceField, ListField


class RestFormView(RestView):
    """
    This extends the RestView
    """
    initial_values = None  # flag to determine whether initial_values should be served or not
    serializer_class = None  # serializer class to be used
    http_method_names = ['get', 'post', 'options']  # allowed http methods
    permissions_required = {'get': None, 'post': None}  # permissions

    def get_initial_values(self):
        """
        Fetch the initial values and choices to be used with the form/serializer
        """
        # prepare initial values if not present in memcache
        choices = dict()  # dict to save choices
        initial = dict()  # dict to save initial values
        fields = self.serializer_class().fields  # form/serializer fields

        # iterate through fields and get initial and choices
        for field_name, field_type in fields.items():
            initial[field_name] = fields[field_name].initial
            if isinstance(fields[field_name], ChoiceField) or \
                    isinstance(fields[field_name], ReferenceField) or \
                    isinstance(fields[field_name], ListField):
                choices[field_name] = fields[field_name].choices

        # generate response
        return {
            'csrf': get_token(self.request),  # csrf token
            'choices': choices,  # choices to be used in form/serializer
            'initial': initial,  # initial values to be used in form/serializer
        }

    def get(self, request, *args, **kwargs):
        """
        GET HTTP method handler
        """
        # return initial values
        if self.initial_values:
            response = self.get_initial_values()
            response = HttpResponse(json.dumps(response, indent=4), content_type='application/json')
            headers = self.get_headers()
            for key, value in headers.iteritems():
                response[key] = value
            return response

        # blank response
        else:
            return Response(headers=self.get_headers())

    def post(self, request, *args, **kwargs):
        """
        POST HTTP method handler
        """
        # remove empty data
        data = self.get_data_dict()

        # build serializer
        request_serializer = self.serializer_class(data=data)

        # handle valid serializer
        if request_serializer.is_valid():
            return self.serializer_valid(request_serializer.validated_data)

        # handle invalid serializer
        else:
            return self.serializer_invalid(request_serializer.errors)

    def serializer_valid(self, validated_data):
        """
        Process valid data
        """
        return Response(validated_data, headers=self.get_headers())

    def serializer_invalid(self, errors):
        """
        Process invalid data
        """
        return Response(errors, status=400, headers=self.get_headers())

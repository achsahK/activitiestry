# django libraries
from django.shortcuts import render


def page_not_found(request):
    """
    404 - Not Found error handler
    """
    return render(request, 'smart_admin/404.html', status=404)


def internal_server_error(request):
    """
    500 - Internal Server error handler
    """
    return render(request, 'smart_admin/500.html', status=500)


def maintenance_window(request):
    """
    Maintenance window
    """
    return render(request, 'smart_admin/maintenance_window.html')


def holding_page(request):
    """
    Holding page
    """
    return render(request, 'smart_admin/holding_page.html')


def partial_holding_page(request):
    """
    Partial holding page
    """
    return render(request, 'smart_admin/partial_holding_page.html')

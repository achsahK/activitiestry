# python libraries
import logging

# django libraries
from django.http import HttpResponse

# app engine libraries
from google.appengine.api import memcache

# user defined libraries
from rest_view import RestView


class ClearMemcacheApi(RestView):
    """
    View which takes a memcache key and removes it from memcache server
    """
    http_method_names = ['get']
    permissions_required = {'get': 'CLEAR_MEMCACHE'}

    def get(self, request, *args, **kwargs):
        """
        GET HTTP method handler
        """
        key = kwargs.get('key', None)
        if memcache.delete(key):
            logging.info('Memcache deletion success [key - %s]' % key)
        else:
            logging.error('Memcache deletion failed [key - %s]' % key)
        return HttpResponse()

# python libraries
import os


def get_build():
    """
    This function checks the environment variable and determine whether
    the application is running in development or production server.
    """
    if 'Development' in os.getenv('SERVER_SOFTWARE'):
        return 'DEVELOPMENT'
    else:
        return 'PRODUCTION'


def get_version():
    """
    Returns current version id set by app engine
    """
    return os.getenv('CURRENT_VERSION_ID')


def get_build_version_details():
    """
    Return build and version details
    """
    return {
        'BUILD': get_build(),
        'CURRENT_VERSION_ID': get_version()
    }


def is_dev():
    """
    This function checks the environment variable and determine whether
    the application is running in development server.
    """
    if 'Development' in os.getenv('SERVER_SOFTWARE'):
        return True
    else:
        return False


def is_prod():
    """
    This function checks the environment variable and determine whether
    the application is running in production server.
    """
    if 'Development' not in os.getenv('SERVER_SOFTWARE'):
        return True
    else:
        return False

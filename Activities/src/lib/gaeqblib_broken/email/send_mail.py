# django libraries
from django.conf import settings
from django.template import loader, Context

# app engine libraries
from google.appengine.api import mail


class SendMail:
    """
    Class for sending emails. Supports both text mail and html mail.
    All email addresses are expected in the following format,
    [(name-1, email-1), ....., (name-n, email-n)] or [email-1, ....., email-n]
    """
    subject = 'Notification mail'
    to_address = []
    cc_address = []
    bcc_address = []
    from_address = []
    context_data = {}  # for html mail
    mail_content = None  # for text mail
    template = 'email_templates/common.html'  # for html mail

    def __init__(self, *args, **kwargs):
        """
        Initialize data
        """
        self.from_address = settings.DEFAULT_FROM_EMAIL
        for key, value in kwargs.iteritems():
            setattr(self, key, value)

    def get_context(self):
        """
        return context data to be used in templates
        """
        self.context_data.update({'HOST_URL': settings.HOST_URL})
        return self.context_data

    def get_mail_content(self):
        """
        Combine email template and context data and return the result
        """
        tem = loader.get_template(self.template)
        con = Context(self.get_context())
        return tem.render(con)

    @staticmethod
    def format_address(address_tuple=None):
        """
        format `('Name', 'Email')` to the form `Name <Email>`
        """
        if address_tuple:
            return '%s <%s>' % address_tuple
        else:
            return ''

    def validate(self):
        """
        This validates the from, to, cc, bcc addresses and email template
        """
        # validate to address
        if not self.to_address:
            raise Exception('Invalid `to` address')
        else:
            formatted_to_address = []
            for address in self.to_address:
                if isinstance(address, tuple):
                    formatted_to_address.append(self.format_address(address))
                else:
                    formatted_to_address.append(address)
            self.to_address = ','.join(formatted_to_address)

        # validate from address
        if not self.from_address:
            raise Exception('Invalid `from` address')
        else:
            if isinstance(self.from_address, tuple):
                self.from_address = self.format_address(self.from_address)

        # validate cc address
        if self.cc_address:
            formatted_cc_address = []
            for address in self.cc_address:
                if isinstance(address, tuple):
                    formatted_cc_address.append(self.format_address(address))
                else:
                    formatted_cc_address.append(address)
            self.cc_address = ','.join(formatted_cc_address)

        # validate bcc address
        if self.bcc_address:
            formatted_bcc_address = []
            for address in self.bcc_address:
                if isinstance(address, tuple):
                    formatted_bcc_address.append(self.format_address(address))
                else:
                    formatted_bcc_address.append(address)
            self.bcc_address = ','.join(formatted_bcc_address)

    def send(self):
        """
        Send email
        """
        self.validate()

        # initialise message
        message = mail.EmailMessage(sender=self.from_address, to=self.to_address, subject=self.subject)

        # add cc if exists
        if self.cc_address:
            message.cc = self.cc_address

        # add bcc if exists
        if self.bcc_address:
            message.bcc = self.bcc_address

        # text only mail
        if self.mail_content:
            message.body = self.mail_content

        # html mail
        else:
            message.html = self.get_mail_content()

        # send mail
        message.send()

# django libraries
from django.conf import settings
from django.core.mail.backends.base import BaseEmailBackend

from google.appengine.api import mail


class AdminBackend(BaseEmailBackend):

    def send_messages(self, email_messages):
        """
        Sends emails out
        """
        if not email_messages:
            return
        else:
            for message in email_messages:
                mail.send_mail_to_admins(
                    sender=settings.DEFAULT_FROM_EMAIL,
                    subject=message.subject,
                    body=message.body
                )
            return len(email_messages)

# rest framework libraries
from rest_framework.renderers import HTMLFormRenderer

# django libraries
from django.http import HttpResponse


class HTMLFormRenderMixin(object):
    """
    Renders data returned by a serializer into an HTML form and return the HTML form
    """
    template_pack = 'templates/rest_framework/inline'

    def get(self, request, *args, **kwargs):
        serializer = self.serializer_class(**kwargs)
        renderer = HTMLFormRenderer()
        renderer.template_pack = self.template_pack
        form_html = renderer.render(serializer.data, renderer_context={
            'request': request
        })

        return HttpResponse(form_html)

# import django libraries
from django.conf import settings as django_settings


def settings(request):
    """
    Context processor for making settings available in template
    """
    return {'SETTINGS_%s' % value: getattr(django_settings, value) for value in dir(django_settings)}

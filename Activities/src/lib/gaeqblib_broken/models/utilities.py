# app engine libraries
from google.appengine.ext import db, ndb


class Utilities(object):
    """
    This class provides several utility functions
    """

    @classmethod
    def get_key(cls, safe_key=None):
        """
        This function takes url safe representation of key as input and returns the db.key/ndb.key instance
        """
        if safe_key:

            # handle db
            if issubclass(cls, db.Model):
                return db.Key(safe_key)

            # handle ndb
            elif issubclass(cls, ndb.Model):
                return ndb.Key(urlsafe=safe_key)

            # default
            else:
                return None

        else:
            return None

    def get_key_instance(self):
        """
        This function returns key instance
        """
        # handle db
        if isinstance(self, db.Model):
            return self.key()

        # handle ndb
        elif isinstance(self, ndb.Model):
            return self.key

        # default
        else:
            return None

    @staticmethod
    def replace_ref_obj_with_key(obj, attr):
        """
        Get key of reference object
        """
        try:

            # handle db
            if isinstance(obj, db.Model):
                return str(getattr(obj.__class__, attr).get_value_for_datastore(obj))

            # handle ndb
            elif isinstance(obj, ndb.Model):
                return getattr(obj, attr)

            # others
            else:
                return getattr(obj, attr)
        except:
            return None

    def to_dict(self, exclude=[]):
        """
        Convert instance to a dictionary
        """
        # handle db
        if isinstance(self, db.Model):
            d = db.to_dict(self)
            try:
                for e in exclude:
                    del d[e]
            except:
                pass
            return d
            # props = self.properties().keys()
            # return {prop: str(getattr(self, prop)) if prop not in exclude else '' for prop in props}

        # handle ndb
        elif isinstance(self, ndb.Model):
            return super(Utilities, self).to_dict(exclude=exclude)

        # default
        else:
            return {}

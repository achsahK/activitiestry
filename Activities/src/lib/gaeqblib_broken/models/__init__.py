# app engine libraries
from google.appengine.ext import db, ndb

# user defined libraries
from crud import Crud
from advanced_queries import AdvancedQueries
from prefetch import Prefetch
from utilities import Utilities
from memcache import Memcache


class Model(Memcache, Utilities, Prefetch, AdvancedQueries, Crud):
    """
    Generic class which combines different features like CRUD, Prefetch etc.
    """
    _reference_fields = []  # used by json_crud_view to replace key string with key instances
    _serializable_fields = []  # used by the serializer

    @property
    def safe_key(self):
        """
        URL safe representation of key.
        """
        # db instance
        if isinstance(self, db.Model):
            try:
                return str(self.key())
            except Exception as e:
                return None

        # ndb instance
        elif isinstance(self, ndb.Model):
            try:
                return self.key.urlsafe()
            except Exception as e:
                return None

        # other instances
        else:
            return None


class DbModel(Model, db.Model):
    """
    Model for db
    """
    pass


class NdbModel(Model, ndb.Model):
    """
    Model for ndb
    """
    pass

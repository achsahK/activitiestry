# app engine libraries
from google.appengine.ext import db
from google.appengine.ext import ndb


def get_db_key_instance(x=None, prop=None):
    prop = getattr(x.__class__, prop)
    if isinstance(prop, db.ListProperty):
        return None
    else:
        return prop.get_value_for_datastore(x)


class Prefetch(object):
    """
    This class contain functions for prefetching referenced entities.
    Support both `db` and `ndb`.
    """
    @classmethod
    def filter_and_track_empty(cls, items):
        """
        This function removes empty items from a list passed and keeps a record of positions
        from where the item has been removed.
        @param items: list of items from which the empty/none element to be removed.
        @return: list of items with out empty/none elements and another list with positions of none/empty elements.
        """
        position = 0
        filtered_items = []
        filtered_items_positions = []
        for item in items:
            if item:
                filtered_items.append(item)
            else:
                filtered_items_positions.append(position)
            position += 1
        return filtered_items, filtered_items_positions

    @classmethod
    def prefetch_ref_props(cls, entities, props=None):
        """
        This function identifies keys of all referenced properties of an entity/entities and load/prefetch
        the whole objects in a single db/ndb hit. The prefetched object will be inserted into the entity in the
        format `referenced_property_obj`. eg: say you have an referenced property called state, after prefetching
        the entity will be having a property called state_obj with the prefetched data.
        @param entities: list of entity objects whose reference properties must be prefetched.
        @param props: list of referenced properties to be prefetched.
        @warning: dont pass query iterator as the @param entities, it will raise exception. Only entity objects
            are handled by the function. If you have an query iterator, perform some expression like
            [entity for entity in entities] to convert it into entity objects.
        """
        # raise exception if props is none
        if props is None:
            raise Exception('`props` cannot be none')
        # handle single entity
        if type(entities) is not list:
            entities = [entities]
        fields = [(entity, prop) for entity in entities for prop in props]
        if issubclass(cls, db.Model):
            # identify keys of referenced objects - `db` models
            ref_keys = [get_db_key_instance(x, prop) for x, prop in fields]
            # filter out all empty references and keep track of their position - `db` models
            ref_keys, empty_keys_position = cls.filter_and_track_empty(ref_keys)
            # fetch references based on keys - `db` models
            ref_entities = dict((x.key(), x) for x in db.get(set(ref_keys)))
        elif issubclass(cls, ndb.Model):
            # identify keys of referenced objects - `ndb` models
            ref_keys = [getattr(x, prop) for x, prop in fields]
            # filter out all empty references and keep track of their position - `ndb` models
            ref_keys, empty_keys_position = cls.filter_and_track_empty(ref_keys)
            # fetch references based on keys - `ndb` models
            ref_entities = dict((x.key, x) for x in ndb.get_multi(set(ref_keys)))
        else:
            raise Exception('Object must be an instance of `db.Model` or `ndb.Model`')
        # reinsert filtered out empty keys in order to properly pair `ref_entities` to `entities`
        if len(empty_keys_position) > 0:
            for position in empty_keys_position:
                ref_keys.insert(position, None)
        for (entity, prop), ref_key in zip(fields, ref_keys):
            if ref_key is not None:
                setattr(entity, prop + '_obj', ref_entities[ref_key])
            else:
                setattr(entity, prop + '_obj', None)
        return entities

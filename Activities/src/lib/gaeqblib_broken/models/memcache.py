# python libraries
import pickle
import logging

# app engine libraries
from google.appengine.api import memcache


class Memcache(object):

    @classmethod
    def get_choices(cls, label='name', *args, **kwargs):

        # build memcache key
        key = cls.__name__ + '_choices'

        data = memcache.get(key)
        if data is None:

            # serve data from data store
            data = cls.all(*args, **kwargs)
            data = [(x.safe_key, getattr(x, label)) for x in data]  # ('', '-- Select --')
            if memcache.add(key, data, 600):
                logging.info('Memcache set success [key - %s]' % key)
            else:
                logging.error('Memcache set fail [key - %s]' % key)
        else:
            pass

        return data

    @staticmethod
    def add_to_memcache(key, value, chunk_size=950000):
        """
        This helps us to save large amount of data in memcache.
        @see: http://stackoverflow.com/questions/9127982/avoiding-memcache-1m-limit-of-values
        """
        serialized = pickle.dumps(value, 2)
        values = {}
        for i in xrange(0, len(serialized), chunk_size):
            values['%s.%s' % (key, i//chunk_size)] = serialized[i: i + chunk_size]
        return memcache.set_multi(values)

    @staticmethod
    def retrieve_from_memcache(key):
        """
        This helps to retrieve large amount of data we stored in memcache.
        @see: http://stackoverflow.com/questions/9127982/avoiding-memcache-1m-limit-of-values
        """
        result = memcache.get_multi(['%s.%s' % (key, i) for i in xrange(32)])  # maximum 32 MB can be retrieved
        serialized = ''.join([v for v in result.values() if v is not None])
        return pickle.loads(serialized)
# python libraries
import gc

# app engine libraries
from google.appengine.ext import db
from google.appengine.ext import ndb
from google.appengine.datastore.datastore_query import Cursor

# user defined libraries
from prefetch import Prefetch


class QueryIterator(object):
    """
    What is an iterator?
    Iterator is an class which supports the concept of iteration over the containers.
    @see: https://docs.python.org/2/library/stdtypes.html#iterator-types,
    @see: http://anandology.com/python-practice-book/iterators.html#iterators

    Why we need an iterator?
    If you are dealing with thousands of rows of data, fetching them all into memory at once can be very wasteful.
    Even worse, huge querysets can lock up server processes, causing your entire web application to grind to a
    halt. To avoid populating the queryset, but to still iterate over all your results, we use the iterator
    method to fetch the data in chunks.
    @see: http://blog.etianen.com/blog/2013/06/08/django-querysets/
    In GAE, to avoid server problems due to the above mentioned issue, datastore queries can only live for
    30 seconds. So while fetching large querysets GAE may cause a `BadRequestError: query not found` error.
    @see: https://code.google.com/p/googleappengine/issues/detail?id=4432
    In order to overcome these issues we use this QueryIterator.
    """
    results = None

    def __init__(self, query=None, props=None, batch_size=20, keys_only=False, limit=None, offset=None,
                 projections=None, cursor=None):
        self.query = query
        self.props = props
        self.batch_size = batch_size
        self.keys_only = keys_only
        self.limit = limit
        self.offset = offset
        self.projections = projections
        self.cursor = cursor

    def __iter__(self):
        return self

    def next(self):
        """
        On receiving call, this function tries to serve the next result. If next result is not found,
        function loads the next chunk/batch of results and tries to serve the result again.
        @see: http://anandology.com/python-practice-book/iterators.html#the-iteraton-protocol
        """
        try:
            return self.results.next()
        except:  # StopIteration
            self.reset()
            self.populate_data()
            return self.results.next()

    def __getitem__(self, key):
        """
        Called to implement evaluation of self[key].
        @see: https://docs.python.org/2/reference/datamodel.html#object.__getitem__
        """
        if not isinstance(key, slice):
            raise NotImplementedError('Only slices work for now.')
        result = []
        count = key.start
        for item in self:
            count += 1
            result.append(item)
            if count == key.stop:
                break
        return result

    def populate_data(self):
        """
        This function executes the query and store results.
        """
        if isinstance(self.query, db.Query):
            results = self.query.fetch(
                batch_size=self.batch_size, keys_only=self.keys_only, limit=self.limit, offset=self.offset,
                projection=self.projections, start_cursor=Cursor(urlsafe=self.cursor)
            )
            if self.props:
                self.results = Prefetch.prefetch_ref_props(results, self.props)
            self.results = iter(results)
            self.cursor = self.query.cursor()
        elif isinstance(self.query, ndb.Query):
            options = ndb.QueryOptions(
                batch_size=self.batch_size, keys_only=self.keys_only, limit=self.limit, offset=self.offset,
                projection=self.projections, produce_cursors=True, start_cursor=Cursor(urlsafe=self.cursor)
            )
            results, cursor, more = self.query.fetch_page(options=options)
            if self.props:
                self.results = Prefetch.prefetch_ref_props(results, self.props)
            self.results = iter(results)
            self.cursor = cursor.urlsafe()
        else:
            raise Exception('`query` must be an instance of `db` or `ndb`')

    def reset(self):
        self.results = None
        gc.collect()

    def get_cursor(self):
        """
        @return: cursor
        """
        if self.cursor:
            return self.cursor
        else:
            return None

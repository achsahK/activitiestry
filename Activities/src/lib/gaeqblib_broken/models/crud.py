# app engine libraries
from google.appengine.ext import db, ndb


class Crud(object):
    """
    This class contain basic CRUD operations.
    Support both `db` and `ndb`.
    """

    def save(self):
        """
        This function writes an object to the db/ndb, and returns the saved object.
        @return: saved object.
        @see: https://cloud.google.com/appengine/docs/python/datastore/modelclass#Model_put
        @see: https://cloud.google.com/appengine/docs/python/ndb/modelclass#Model_put
        """
        if isinstance(self, db.Model):
            db.put(self)
        elif isinstance(self, ndb.Model):
            self.put()
        else:
            raise Exception('Object must be an instance of `db.Model` or `ndb.Model`')
        return self

    @classmethod
    def save_multi(cls, objs=()):
        """
        This function writes multiple objects to the db/ndb, and returns the saved objects.
        @return: list of saved objects.
        """
        db_obj_count = 0
        ndb_obj_count = 0
        if len(objs) > 0:
            for obj in objs:
                if isinstance(obj, db.Model):
                    db_obj_count += 1
                elif isinstance(obj, ndb.Model):
                    ndb_obj_count += 1
            if db_obj_count > 0 and db_obj_count == len(objs):
                return db.put(objs)
            elif ndb_obj_count > 0 and ndb_obj_count == len(objs):
                return ndb.put_multi(objs)
            else:
                raise Exception('All objects must be instances of same class ( `db.Model` or `ndb.Model` )')
        else:
            return []

    @classmethod
    def get(cls, key=None):
        """
        This function reads an object from the db/ndb based on the key passed.
        @param key: url safe representation or key instance of entity to be retrieved.
        @return: retrieved entity.
        @see: https://cloud.google.com/appengine/docs/python/datastore/modelclass#Model_get
        @see: https://cloud.google.com/appengine/docs/python/ndb/keyclass#Key_get
        """
        if key is not None:

            # db.Key
            if isinstance(key, db.Key):
                try:
                    obj = db.get(key)
                except:
                    raise Exception('Entity with given id does not exists')

            # ndb.Key
            elif isinstance(key, ndb.Key):
                try:
                    obj = key.get()
                except:
                    raise Exception('Entity with given id does not exists')

            # db.Model
            elif issubclass(cls, db.Model):
                try:
                    key = db.Key(key)
                    obj = db.get(key)
                except:
                    raise Exception('Entity with given id does not exists')

            # ndb.Model
            elif issubclass(cls, ndb.Model):
                try:
                    key = ndb.Key(urlsafe=key)
                    obj = key.get()
                except:
                    raise Exception('Entity with given id does not exists')

            # default
            else:
                raise Exception('Object must be an instance of `db.Model` or `ndb.Model`')
            return obj
        else:
            return None

    # read operation for multiple objects
    @classmethod
    def get_multi(cls, keys=()):
        """
        This function reads multiple objects from the db/ndb based on the list of keys passed.
        @param safe_keys: list of url safe representation of entities to be retrieved.
        @return: list of retrieved objects.
        """
        if len(keys) > 0:

            # db.model
            if issubclass(cls, db.Model):
                # build keys from url safe keys
                proper_keys = []
                for key in keys:
                    if not isinstance(key, db.Key):
                        proper_keys.append(db.Key(key))
                    else:
                        proper_keys.append(key)
                # retrieve data based on keys
                return db.get(proper_keys)

            # ndb.model
            elif issubclass(cls, ndb.Model):
                # build keys from url safe keys
                proper_keys = []
                for key in keys:
                    if not isinstance(key, ndb.Key):
                        proper_keys.append(ndb.Key(urlsafe=key))
                    else:
                        proper_keys.append(key)
                # retrieve data based on keys
                return ndb.get_multi(proper_keys)

            # default
            else:
                raise Exception('This method should only be invoked from a subclass of `db.Model` or `ndb.Model`')
        else:
            return []

    # get all
    @classmethod
    def get_all(cls):
        """
        This function returns all entries in the model
        """
        if issubclass(cls, db.Model):
            return cls.all().fetch(1000)
        elif issubclass(cls, ndb.Model):
            return cls.query().fetch(1000)
        else:
            raise Exception('Object must be an instance of `db.Model` or `ndb.Model`')

    # delete operation
    def delete(self):
        """
        This function delete the entity from db/ndb
        @see: https://cloud.google.com/appengine/docs/python/datastore/modelclass#Model_delete
        """
        if isinstance(self, db.Model):
            db.delete(self)
        elif isinstance(self, ndb.Model):
            self.key.delete()
        else:
            raise Exception('Object must be an instance of `db.Model` or `ndb.Model`')

    @classmethod
    def delete_multi(cls, keys=()):
        """
        This function delete list of entities from db/ndb
        @see: https://cloud.google.com/appengine/docs/python/ndb/functions
        @see: https://cloud.google.com/appengine/docs/python/datastore/functions#delete
        """
        if keys:
            if issubclass(cls, db.Model):
                # build keys from url safe keys
                proper_keys = []
                for key in keys:
                    if not isinstance(key, db.Key):
                        proper_keys.append(db.Key(key))
                    else:
                        proper_keys.append(key)
                # delete data based on keys
                db.delete(proper_keys)
            elif issubclass(cls, ndb.Model):
                # build keys from url safe keys
                proper_keys = []
                for key in keys:
                    if not isinstance(key, ndb.Key):
                        proper_keys.append(ndb.Key(urlsafe=key))
                    else:
                        proper_keys.append(key)
                ndb.delete_multi(proper_keys)
            else:
                raise Exception('Object must be an instance of `db.Model` or `ndb.Model`')
        else:
            pass

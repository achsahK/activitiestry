__author__ = 'suja'
# app engine libraries
from google.appengine.ext import db

from gaeqblib_broken.models import Model

class Api(object):

    model = None
    instance = None
    key = None

    def __init__(self, key=None, instance=None, props=None, shouldFetch=True):
        """
        Instantiate the module API instance, populate the model instance

        @param key: The key
        @param instance: The model instance
        @param props: The properties to prefetch
        @param shouldFetch: boolean flag to indicate whether the instance have to be fetched using the key, defaults to True
        """
        from google.appengine.ext.db import NotSavedError

        self.set_instance(instance)
        if key:
            self.key = self.get_key(key)

        if key is not None and self.get_instance() is None and shouldFetch:
            # Populate the module APIs model instance if we have the key
            self.set_instance(self.get(key, props))

        self.getProperty = getattr(self, '_getProperty', self.getProperty)

    def get_instance(self):
        '''
        Getter for the model instance used in this API method. (self.instance)

        @return: The model instance
        '''

        return self.instance

    def set_instance(self, instance):
        '''
        Setter for the model instance.

        @param instance: The model instance
        '''

        self.instance = instance

        if instance and instance.is_saved():
            # We are setting an unsaved model instance, do not try to save the key.
            self.key = instance.key()

    def get_key(self):
        """ Method to get db.Key instance of key
        """
        if isinstance(self.key, db.Model):
            # Check if its an actual model instance, if so just call key()
            # Ideally this should not happen, but this method should be able to handle this conditions as well.
            key = self.key.key()
        elif not isinstance(self.key, db.Key):
            key = db.Key(self.key)
        return key

    def _get_property(self, prop):
        """
        Override getProperty to change behaviour when called against an instance.
        Used to get the property from @property methods for propnameProperty
        """
        p = self.getProperty(prop)
        if isinstance(p, basestring):
            p = getattr(self, '%sProperty' % prop, None)
        return p

    @classmethod
    def getProperty(cls, prop):
        property = cls.model.properties().get(prop, None)
        # property = cls.model._properties.get(prop, None)
        if not property:
            if hasattr(cls, "Property %s" % prop):
                property = prop
            else:
                raise Exception("Class has not property %s" % prop)
        return property

    def get(self, key, props=None):
        '''
        Common api method to return instance of the key passed.

        @param key: Key, either a string key or an instance db.Key
        @param props: Properties to pre-fetch

        @return: The model instance
        '''
        return self.model().prefetch_ref_props(entities=self.model.get(key), props=props)

    def getValueForDatastore(self, prop, model=None):
        '''
        Get the value for datastore for the property
        Returns key of the reference property passed in the model

        @param prop: The property
        @param model: The model instance to get the value from
                      If the model passed is None, then set the self.instance as the model

        @return: The value for datastore from the model instance for the property
        '''

        if not model:
            model = self.get_instance()
        print "model ==>", model
        property = self.getProperty(prop)
        print "property ==>", property
        pt = getattr(model, property.name)

        if property.__dict__.get('reference_class', None):
            return pt.key()
        else:
            return pt

    def getModelProperty(self, prop):
        """
        Get the property definition from model

        @param prop: The name of the property

        @return: The Property definition
        """
        return self.model().getProperty(prop)

    def get_all(self, **kwargs):
        return self.model().make_query(**kwargs)

    def is_model_instance(self, obj, model=None):
        if model:
            return isinstance(obj, model)
        return isinstance(obj, self.model().model)

    def insert(self, should_persist=False, **kwargs):
        from space.common.models import persist

        instance = None
        if kwargs:
            model_object = self.model(**kwargs)
            if should_persist:
                instance = model_object.save()
            else:
                instance = model_object

        # Update the API instance object
        self.set_instance(instance)

        # Call any 'on update' triggers
        modelsToPersist = self.update_trigger(original=None, updated=instance, should_persist=should_persist)
        # If there are models updated as part of the trigger, the trigger should return a list of models to persist
        if not modelsToPersist:
            modelsToPersist = []
        # If there is such a list, add the current model instance to the list to get persisted
        modelsToPersist.append(self.get_instance())

        if should_persist:
            persist(should_persist)
            # If we do persist stuff, we do not need to pass the list along to the caller!
            return self.get_instance()

        return modelsToPersist

    def update(self, should_persist=False, **kwargs):
        from copy import copy
        from space.common.models import persist

        instance = self.get_instance()
        # Make a copy of the model instance, which will have the original values
        orgInstance = copy(instance)

        # Iterate over all the the keyword arguments and set the model attributes/properties
        for k, v in kwargs.iteritems():
            setattr(instance, k, v)

        # Update the API instance object
        self.set_instance(instance)

        # Call any 'on update' triggers
        modelsToPersist = self.update_trigger(original=orgInstance, updated=instance, should_persist=False)
        # If there are models updated as part of the trigger, the trigger should return a list of models to persist
        if not modelsToPersist:
            modelsToPersist = []
        # If there is such a list, add the current model instance to the list to get persisted
        modelsToPersist.append(self.get_instance())

        if should_persist:
            persist(modelsToPersist)
            # If we do persist stuff, we do not need to pass the list along to the caller!
            return self.get_instance()

        return modelsToPersist

    def update_trigger(self, original=None, updated=None, should_persist=True):
        pass
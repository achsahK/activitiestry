# python libraries
import logging
import datetime

# user defined libraries
from query_iterator import QueryIterator

# app engine libraries
from google.appengine.ext import db
from google.appengine.ext import ndb
from google.appengine.datastore.datastore_query import Cursor


class AdvancedQueries(object):
    """
    This class provides functions for performing advanced queries in db/ndb
    support `db` and `ndb`
    """
    @classmethod
    def add_filters(cls, query=None, filters=[]):
        """
        This function apply different filters on the query passed.
        @param query: query object on which the filter has to be applied.
        @param filters: list of filters to be applied on the query. filter is expected to be a dictionary holding
            three values, 1. property, 2. operator and 3. value. `property` is the property of objects on which the
            filtering must be done. `operator` is the comparison/logical operation to be performed on the property
            based on the `value`.
            eg: {'property': 'name', 'operator': '=', 'value': 'Akhil'}
        @return: query with filters applied
        @see: https://cloud.google.com/appengine/docs/python/datastore/queries
        @see: https://cloud.google.com/appengine/docs/python/ndb/queries
        @see: https://cloud.google.com/appengine/docs/python/datastore/queries#Python_Filters
        @see: https://cloud.google.com/appengine/docs/python/ndb/queries#filter_by_prop
        """
        # apply filters one by one
        for filter_item in filters:
            # check filter is valid and raise exception if invalid filter is found
            if 'property' in filter_item.keys() and 'operator' in filter_item.keys() and \
                    'value' in filter_item.keys():
                if issubclass(cls, db.Model):
                    criteria = filter_item['property'] + " " + filter_item['operator']
                    query = query.filter(criteria,  filter_item['value'])
                elif issubclass(cls, ndb.Model):
                    value = filter_item['value']
                    if isinstance(value, datetime.date):
                        # Hack/workaround because FilterNode doesn't like date's
                        # If we had used proper NDB query filtering, this would have been handled for us
                        # since gaeqblib_broken was poorly written it is not handled properly - ASP
                        value = datetime.datetime(
                            year=value.year, month=value.month, day=value.day,
                            hour=0, minute=0, second=0, microsecond=0)
                    query = query.filter(ndb.query.FilterNode(filter_item['property'], filter_item['operator'], value))
                else:
                    raise Exception('Class must be a subclass of `db.Model` or `ndb.Model`')
            else:
                # raise Exception('Improperly configured filter.')
                logging.info('Ignoring improper filter `%s`' % filter_item)
        return query

    @classmethod
    def add_order_by(cls, query=None, order_by=[], direction='f'):
        """
        This function apply different sort orders on the query passed.
        @param query: query object on which the filter has to be applied.
        @param order_by: list of different sort orders to be applied on the query.
        @return: query with sort order applied.
        @see: https://cloud.google.com/appengine/docs/python/datastore/queries
        @see: https://cloud.google.com/appengine/docs/python/ndb/queries
        @see: https://cloud.google.com/appengine/docs/python/ndb/queries#order
        @see: https://cloud.google.com/appengine/docs/python/datastore/queries#Python_Sort_orders
        """
        # apply order by one by one
        for order_by_item in order_by:
            # check order is valid and raise exception if invalid order_by is found
            if order_by_item:
                # handle backward pagination
                if direction == 'b':
                    if '-' in order_by_item:
                        order_by_item = order_by_item.replace('-', '')
                    else:
                        order_by_item = '-%s' % order_by_item
                # apply ordering
                if issubclass(cls, db.Model):
                    query = query.order(order_by_item)
                elif issubclass(cls, ndb.Model):
                    # handle ndb descending order
                    if '-' in order_by_item:
                        order_by_item = getattr(cls, order_by_item.lstrip('-'))
                        query = query.order(-order_by_item)
                    else:
                        query = query.order(getattr(cls, order_by_item))
                else:
                    raise Exception('Class must be a subclass of `db.Model` or `ndb.Model`')
            else:
                raise Exception('Improperly configured order by.')
        return query

    @classmethod
    def make_query(cls, keys_only=False, filters=None, projections=None, order_by=None, cursor=None, direction='f',
                   offset=0, limit=1000, batch_size=20, return_query=False, iterator_only=False, prefetch_props=None):
        """
        This function builds the query and apply filters, sort orders and projections (fields to be fetched)
        @param keys_only: if this is `True` only the keys of the result entities will be fetched instead of the
            entities themselves.
        @param filters: list of filters to be applied on the query.
        @param projections: list of fields/properties to be fetched.
        @param order_by: list of sort orders to be applied on the query.
        @param cursor: a base64-encoded cursor string denoting the position in the query's result set.
        @param direction: order in which cursor must be applied. `f` for forward and `b` for backward
        @param offset: number of results to skip before returning the first one.
        @param limit: maximum number of results to return.
        @param batch_size: number of results to attempt to retrieve per batch.
        @param return_query: flag for returning query object.
        @param prefetch_props: list of referenced properties to be prefetched.
        @return: a dictionary holding result sets, forward cursor and backward cursor
            eg: {'results': [...], 'forward_cursor': '...', 'backward_cursor': '...'}
        @see: https://cloud.google.com/appengine/docs/python/datastore/queries
        @see: https://cloud.google.com/appengine/docs/python/ndb/queries
        @see: https://cloud.google.com/appengine/docs/python/datastore/queries#Python_Filters
        @see: https://cloud.google.com/appengine/docs/python/ndb/queries#filter_by_prop
        @see: https://cloud.google.com/appengine/docs/python/datastore/projectionqueries
        @see: https://cloud.google.com/appengine/docs/python/ndb/projectionqueries
        @see: https://cloud.google.com/appengine/docs/python/ndb/queries#order
        @see: https://cloud.google.com/appengine/docs/python/datastore/queries#Python_Sort_orders
        """
        if issubclass(cls, db.Model):
            query = db.Query(model_class=cls)
        elif issubclass(cls, ndb.Model):
            query = ndb.Query(kind=cls.__name__)
        else:
            raise Exception('Object must be an instance of `db.Model` or `ndb.Model`')
        # apply filters, if exists
        if filters:
            query = cls.add_filters(query, filters)
        # apply order by, if exists
        if order_by:
            query = cls.add_order_by(query, order_by, direction)
        # return query object if requested
        if return_query:
            return query
        # return iterator if requested
        elif iterator_only:
            return QueryIterator(query=query, props=prefetch_props, batch_size=batch_size, keys_only=keys_only,
                                 limit=limit, offset=offset, projections=projections, cursor=cursor)
        # if `return_query` is false, then run the query and return the response
        else:
            pagination_cursor = None
            if issubclass(cls, db.Model):
                results = query.fetch(
                    batch_size=batch_size, keys_only=keys_only, limit=limit, offset=offset,
                    projection=projections, start_cursor=Cursor(urlsafe=cursor)
                )
                pagination_cursor = query.cursor()
            elif issubclass(cls, ndb.Model):
                options = ndb.QueryOptions(
                    batch_size=batch_size, keys_only=keys_only, limit=limit, offset=offset,
                    produce_cursors=True, projection=projections, start_cursor=Cursor(urlsafe=cursor)
                )
                results, pagination_cursor, more = query.fetch_page(limit, options=options)
                pagination_cursor = pagination_cursor.urlsafe() if pagination_cursor else ''
            else:
                pass
            # perform prefetch id props is present
            if prefetch_props:
                results = cls.prefetch_ref_props(results, prefetch_props)
            # generate response
            return {'results': results, 'cursor': pagination_cursor}

import logging
from rest_framework import permissions
from rest_framework import exceptions

class IsPrivileged(permissions.BasePermission):
    """
    Custom permission class to check for valid privileges
    """

    def has_permission(self, request, view):

        # identify request method
        request_method = request.method

        logging.info('request_method: %s' % request_method)
        logging.info('permissions_required: %s' % str(view.permissions_required))

        permissions = view.permissions_required[request_method] if request_method in view.permissions_required else []
        if not isinstance(permissions, list):
            permissions = [permissions]

        # allow pre flight requests
        if request_method == 'options' or request.user.is_admin:
            pass

        # allow requests with valid permissions
        elif permissions and set(permissions).intersection(request.privileges):
            pass

        # block requests without valid permissions
        else:
            raise exceptions.PermissionDenied

        return True

# django libraries
from django.utils.datastructures import SortedDict


SORTED_DICT_ATTRIBUTES = ['keyOrder']


class Enum(SortedDict):
    """
    Enumeration class. Which inherits from django.utils.datastructures.SortedDict to maintain the order of the attributes.

    Note: At the time of writing this django.utils.datastructures.SortedDict only had one attribute 'keyOrder',
    which is added to the SORTED_DICT_ATTRIBUTES list above, this is due to the fact that we override __setattr__ and __getattr__
    and need list of attributes to exclude from setting and getting!
    If in future if the SortedDict is modified to have other attributes, we need to add those to the list above.

    To use this declare a child class like;

    class AnEnum(Enum):
        def __init__(self, *args, **kwargs):
            super(AnEnum, self).__init__(*args, **kwargs)
            self.first = 0
            self.asp = 1
            self.pas = 2

    Then create an instance of the class, this is useful for accessing attributes like An.first and so on. We also don't need to initialize the class every time we use it.
    An = AnEnum()
    """
    def __init__(self, data=None, uncde=False, **kwargs):
        """
        Override the init to set values which are set as keywords.
        @param data:
        """
        super(Enum, self).__init__(data=data)
        for k, v in kwargs.items():
            self[k] = v if not uncde else unicode(v)

    def __setattr__(self, k, v):
        """
        Override the default set attribute to handle Enum.key = value
        @param k: The attribute to set
        @param v: The value to be set to the attribute
        """
        if k in SORTED_DICT_ATTRIBUTES : self.__dict__[k] = v
        self[k] = v

    def __getattr__(self, k):
        """
        Override the default get attributes to handle Enum.key
        @param k: The attribute to be accessed
        """
        if k in SORTED_DICT_ATTRIBUTES: return getattr(self, k)
        return self[k]

    def asDict(self):
        """
        @deprecated: Use the Enum instance it self
        """
        return self

    def getKeys(self):
        """
        @deprecated: Use .keys() instead
        """
        return self.keys()

    def getValues(self):
        """
        @deprecated: Use .values()
        """
        return self.values()

    def getItems(self):
        """
        @deprecated: Use .items()
        """
        return self.items()

    def getValuesAsTuple(self):
        """
        Get the values as a tuple, useful for combo boxes in form and all.

        @return: The values as a list of tuples.
        """
        return [(i, i) for i in self.getValues()]

    def valuesExcept(self, value):
        """
        Return the list all values, with the values mentioned in 'value' removed.
        """
        from types import ListType

        vs = self.values()
        if type(value) is ListType:
            for i in value:
                vs.remove(i)
        else:
            vs.remove(value)
        return vs


class UnknownApplicationExecption(Exception):
    def __init__(self, appId, *args, **kwargs):
        msg = '%s is not in the list of recognized application ids. If this is a new app please add it to'\
                'space.common.appConfig.SpaceAppEnum.' % appId
        super(UnknownApplicationExecption, self).__init__(msg, *args, **kwargs)


class SpaceAppEnum(Enum):
    """
    Enum to hold various app names, for different installations.
    """
    def __init__(self, *args, **kwargs):
        super(SpaceAppEnum, self).__init__(*args, **kwargs)
        self.QBURST = 'qburst-activities'
        self.QBURST_DEMO = 'qburst-activities-demo'
        self.PALADION = 'activities-paladion'
        self.QBURST_STAGING = 'qburst-activities-staging'


SpaceApp = SpaceAppEnum()


class AppConfigUtil(object):
    """
    App config utility class.
    """
    @staticmethod
    def getCurrentAppId():
        """
        Get the current app's id.
        @see: https://developers.google.com/appengine/docs/python/appidentity/functions#get_application_id
        @see: https://developers.google.com/appengine/docs/python/runtime#Requests_and_Domains

        @return string
        """
        from google.appengine.api.app_identity import get_application_id
        return get_application_id()

    @staticmethod
    def getCurrentApp():
        """
        Get the current deployed app. Raises an UnknownAppExecption, if the current application is unknown.

        @return string
        """
        appId = AppConfigUtil.getCurrentAppId()
        if appId not in SpaceApp.values():
            raise UnknownApplicationExecption(appId)
        return appId

    @staticmethod
    def isCurrentApp(app):
        """
        Check if the app is the current serving app.

        @param app: The app name to check for, ideally this should use SpaceApp enum, but any string would work

        @return boolean
        """
        return AppConfigUtil.getCurrentApp() == app

    @staticmethod
    def getCurrentEnabledModules():
        """
        Get all the enabled modules for the current deployed application.

        @return list of enabled modules
        """
        return AppConfigUtil.getEnabledModules(AppConfigUtil.getCurrentApp())

    @staticmethod
    def getEnabledModules(app):
        """
        Get all the enabled modules for the given application.

        @param app: The name of the app to check for

        @return list of enabled modules
        """
        # Temp, need to replicate the multi-tenancy logic in Activities
        return  # MODULES_ENABLED_BY_APP[app]

# The variables defined below are defined as a module variables to leverage app caching in App Engine Python.
# These are all wrappers for/stores result of AppConfigUtil method calls

# The current app that is being served.
SPACE_CURRENT_APP = AppConfigUtil.getCurrentApp()

# Check if the current app being served is QBurst Space?
IS_CURRENT_APP_QBURST = AppConfigUtil.isCurrentApp(SpaceApp.QBURST)

# Check if the current app being served is QBurst Space Demo?
IS_CURRENT_APP_QBURST_DEMO = AppConfigUtil.isCurrentApp(SpaceApp.QBURST_DEMO)

# Check if the current app being served is QBurst Space Staging?
IS_CURRENT_APP_QBURST_STAGING = AppConfigUtil.isCurrentApp(SpaceApp.QBURST_STAGING)

# Check if the current app being served is Paladion Space?
IS_CURRENT_APP_PALADION = AppConfigUtil.isCurrentApp(SpaceApp.PALADION)

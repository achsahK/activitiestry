from protorpc.messages import Message, StringField
from space.common.models import Enum


class ErrorMessage(Message):
    code = StringField(1, required=True)
    message = StringField(2, required=True)
    reference = StringField(3, required=True)


# ENUMs for error codes
class EndpointsErrorCodeEnum(Enum):
    def __init__(self, *args, **kwargs):
        super(EndpointsErrorCodeEnum, self).__init__(*args, **kwargs)
        self.SUCCESS = '00'
        self.FORM_ERROR = '01'


EndpointsErrorCode = EndpointsErrorCodeEnum()

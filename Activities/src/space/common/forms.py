from django import forms

import re

url_re = re.compile(
        r'^https?://' # http:// or https://
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+[A-Z]{2,6}\.?|' #domain...
        r'localhost|' #localhost...
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})' # ...or ip
        r'(?::\d+)?' # optional port
        r'(?:/?|[/?]\S+)$', re.IGNORECASE)

class StrippedCharField(forms.CharField):
    def clean(self, value):
        if value is not None:
            value = value.strip()

        return super(StrippedCharField, self).clean(value)

class DateDropDownWidget(forms.MultiWidget):
    sh = [True, True, True]
    show_label = True

    def __init__(self, attrs = None, year_range = None, month_range = None, day_range = None, \
                  year_range_count = 1, sh = [True, True, True], show_label = True):
        from datetime import date
        from activity.settings.default import APP_INSTALL_YEAR, MONTH_NAMES_DICT_SHORT as m_names

        curr_year = date.today().year
        YEARS = year_range or range(APP_INSTALL_YEAR, curr_year + year_range_count)
        MONTHS = month_range or range(1, 13)
        DAYS = day_range or range(1, 32)

        self.sh = sh
        self.show_label = show_label

        years = map(lambda x: (x, x), YEARS)
        months = map(lambda x: (x, m_names[x]), MONTHS)
        days = map(lambda x: (x, x), DAYS)

        widgets = (forms.Select(attrs = attrs, choices = [('', '----')] + years),
                   forms.Select(attrs = attrs, choices = [('', '----')] + months),
                   forms.Select(attrs = attrs, choices = [('', '----')] + days))

        super(DateDropDownWidget, self).__init__(widgets, attrs)

    def format_output(self, widgets):
        format = ''
        widget_list = []
        if self.sh[0]:
            if self.show_label:
                format += "Year".decode('utf-8')
            format += "%s".decode('utf-8')
            widget_list.append(widgets[0])
        if self.sh[1]:
            if self.show_label:
                format += "&nbsp;Month".decode('utf-8')
            format += "&nbsp;%s".decode('utf-8')
            widget_list.append(widgets[1])
        if self.sh[2]:
            if self.show_label:
                format += "&nbsp;Day".decode('utf-8')
            format += "&nbsp;%s".decode('utf-8')
            widget_list.append(widgets[2])

        return format % tuple(widget_list)

    def decompress(self, value):
        if value:
            return [value.year, value.month, value.day]

        return [None, None, None]

def getCleanData(data_list, show, required):
    data_list_new = []
    EMPTY_VALUES = [None, '']
    i = -1
    for item in data_list:
        i += 1
        if item in EMPTY_VALUES:
            if show[i] and not required[i]:
                data_list_new.append(1)
        else:
            data_list_new.append(item)

    return data_list_new

class DateDropDownField(forms.MultiValueField):
    widget = DateDropDownWidget

    def __init__(self, *args, **kwargs):
        self.required = kwargs.get('required', [True, True, True])
        self.sh = self.widget.sh

        fields = (
                forms.IntegerField(required = self.required[0]),
                forms.IntegerField(required = self.required[1]),
                forms.IntegerField(required = self.required[2]),
                )
        super(DateDropDownField, self).__init__(fields, *args, **kwargs)

    def clean(self, value):
        from django.core.exceptions import ValidationError
        from django.forms.utils import ErrorList

        """
        Validates every value in the given list. A value is validated against
        the corresponding Field in self.fields.

        For example, if this MultiValueField was instantiated with
        fields=(DateField(), TimeField()), clean() would call
        DateField.clean(value[0]) and TimeField.clean(value[1]).
        """
        cleaned_data = []
        EMPTY_VALUES = [None, '']
        errors = ErrorList()
        if self.required and not value:
            raise ValidationError(u'This field is required.')
        elif not self.required and not value:
            return self.compress([])
        if not isinstance(value, (list, tuple)):
            raise ValidationError(u'Enter a list of values.')
        for i, field in enumerate(self.fields):
            try:
                field_value = value[i]
            except KeyError:
                field_value = None
#            if self.required and field_value in EMPTY_VALUES:
            '''Changed to handle the individual required property, 
            since MultiValueField assigns all fields as required false.
            Should create a new class to inherit from MultiValueField 
            and change the init in that and inherit that new class here.'''
            if self.required[i] and field_value in EMPTY_VALUES:
                raise ValidationError(u'This field is required.')
            try:
                cleaned_data.append(field.clean(field_value))
            except ValidationError, e:
                # Collect all validation errors in a single list, which we'll
                # raise at the end of clean(), rather than raising a single
                # exception for the first error we encounter.
                errors.extend(e.messages)
        if errors:
            raise ValidationError(errors)
        return self.compress(cleaned_data)

    def compress(self, data_list):
        from datetime import date

        data_list = getCleanData(data_list, self.sh, self.required)
        EMPTY_VALUES = [None, '']
        ERROR_EMPTY = "This field is required."
        ERROR_INVALID = "Enter a valid date."
        if data_list:
            if filter(lambda x: x in EMPTY_VALUES, data_list) and self.required:
#            if filterDateDropDownItems(self.sh, data_list) and self.required:
                raise forms.ValidationError(ERROR_EMPTY)

            try:
                return date(*map(lambda x:int(x), data_list))
            except:# ValueError:
#                if self.required:
                raise forms.ValidationError(ERROR_INVALID)

        return None

class FormFieldTypes(object):
    TYPED_CHOICE = 'TYPED_CHOICE'
    CHOICE = 'CHOICE'
    INTEGER = 'INTEGER'
    FLOAT = 'FLOAT'
    STRING = 'STRING'
    TEXT = 'TEXT'
    DATE = 'DATE'
    DATETIME = 'DATETIME'

class FormFieldFactory(object):

    @staticmethod
    def getTextField(**kwargs):
        return FIELD_TYPES[FormFieldTypes.STRING](
            widget = forms.Textarea(attrs = kwargs.get('attrs', None)), **kwargs
        )

    @staticmethod
    def getField(typ, **kwargs):
        return FIELD_TYPES[typ](**kwargs)

## Defines all the field types.
## Ideally this should have been a member variable in FormFieldFactory, but due to the limitations on the interpreted language this needs to be global
## NOTE: This should always be define after FormFieldFactory
FIELD_TYPES = { FormFieldTypes.TYPED_CHOICE : forms.TypedChoiceField,
                FormFieldTypes.CHOICE       : forms.ChoiceField,
                FormFieldTypes.INTEGER      : forms.IntegerField,
                FormFieldTypes.FLOAT        : forms.FloatField,
                FormFieldTypes.DATE         : forms.DateTimeField,
                FormFieldTypes.DATETIME     : forms.DateTimeField,
                FormFieldTypes.STRING       : StrippedCharField,
                FormFieldTypes.TEXT         : FormFieldFactory.getTextField
              }

######## Custom Property/Field Definitions for Dynamic forms and so on ########

class DynamicFormFields():
    '''
    Parent class for all dynamic forms.
    Can be used similar to any forms.Form, except we need a flds argument

    example:
    flds = [{ 'name' : 'ASP',
              'type' : FormFieldTypes.DATETIME,
              'args' : {'required' : True, choices = []}
              # in otherwords you know the form field you are creating
              # give me all the arguments for that field under 'args' as a dict
            }]
    ## To get the dynamic form fields
    dff = DynamicFormFields(flds)
    fields = dff.getFields()
    '''

    __flds = None
    fields = {}

    def __init__(self, flds):
        self.fields = {}
        self.__flds = None
        self._setProps(flds)
        self._setFields()

    def _setProps(self, flds):
        self.__flds = flds

    def _getFieldType(self, typ, **kwargs):
        return FormFieldFactory.getField(typ, **kwargs)

    def _setFields(self):
        for p in self.__flds:
            self.fields[p.get('name')] = self._getFieldType(p.get('type'), **p.get('args'))

    def getFields(self):
        return self.fields

class DynamicForm(forms.Form):
    '''
    Parent class for all dynamic forms.
    Can be used similar to any forms.Form, except we need a flds argument

    example:
    flds = What you get from DynamicFormFields().getFields() Stored in the DB in some cases
    df = DynamicForm(flds, initials = {}, data= {})
    or
    df = DynamicForm(flds)
    '''

    def __init__(self, flds, *args, **kwargs):
        super(DynamicForm, self).__init__(*args, **kwargs)
        self._setFields(flds)

    def _setFields(self, flds):
        #choices = [], required = False, coerce = int
        self.fields.update(flds)

#class DynamicForm(forms.Form):
#    '''
#    Parent class for all dynamic forms.
#    Can be used similar to any forms.Form, except we need a flds argument
#
#    example:
#    flds = [{ 'name' : 'ASP',
#              'type' : FormFieldTypes.DATETIME,
#              'args' : {'required' : True, choices = []}
#              # in otherwords you know the form field you are creating
#              # give me all the arguments for that field under 'args' as a dict
#            }]
#    df = DynamicForm(flds, initials = {}, data= {})
#    or
#    df = DynamicForm(flds)
#    '''
#
#    __flds = None
#
#    def __init__(self, flds, *args, **kwargs):
#        self._setProps(flds)
#        super(DynamicForm, self).__init__(*args, **kwargs)
#        self._setFields()
#
#    def _setProps(self, flds):
#        self.__flds = flds
#
#    def _getFieldType(self, typ, **kwargs):
#        return FormFieldFactory.getField(typ, **kwargs)
#
#    def _setFields(self):
#        for p in self.__flds:
#            #choices = [], required = False, coerce = int
#            self.fields[p.get('name')] = self._getFieldType(p.get('type'), **p.get('args'))

###############################################################################

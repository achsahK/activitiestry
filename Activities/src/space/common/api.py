from activity.settings.default import QUERY_MAX_LIMIT, QUERY_BATCH_SIZE

class ModuleAPI(object):

    model = None
    instance = None
    key = None

    def __init__(self, key=None, instance=None, props=None, shouldFetch=True):
        """
        Instantiate the module API instance, populate the model instance

        @param key: The key
        @param instance: The model instance
        @param props: The properties to prefetch
        @param shouldFetch: boolean flag to indicate whether the instance have to be fetched using the key, defaults to True
        """
        from space.utils.utils import getAsKey

        self.setInstance(instance)
        if key:
            self.key = getAsKey(key)

        if key is not None and self.getInstance() is None and shouldFetch:
            # Populate the module APIs model instance if we have the key
            self.setInstance(self.get(key, props))

    def getInstance(self):
        '''
        Getter for the model instance used in this API method. (self.instance)

        @return: The model instance
        '''

        return self.instance

    def setInstance(self, instance):
        '''
        Setter for the model instance.

        @param instance: The model instance
        '''

        self.instance = instance

    def get(self, key, props=None):
        '''
        Common api method to return instance of the key passed.

        @param key: Key, either a string key or an instance db.Key
        @param props: Properties to pre-fetch

        @return: The model instance
        '''

        return self.model().get(key, props)

    def getValueForDatastore(self, prop, model=None):
        '''
        Get the value for datastore for the property
        Returns key of the reference property passed in the model

        @param prop: The property
        @param model: The model instance to get the value from
                      If the model passed is None, then set the self.instance as the model

        @return: The value for datastore from the model instance for the property
        '''
        if not model:
            model = self.getInstance()
        return self.model().getValueForDatastore(prop, model)

    def getModelProperty(self, prop):
        '''
        Get the property definition from model

        @param prop: The name of the property

        @return: The Property definition
        '''
        return self.model().getProperty(prop)

    def getAll(self, limit=QUERY_MAX_LIMIT, props=None, queryOnly=False, batchSize=QUERY_BATCH_SIZE, iteratorOnly=False, **kwargs):
        '''
        Method to fetch all instances

        @param limit: @see: space.common.model.Model.query()
        @param props: @see: space.common.model.Model.query()
        @param queryOnly: @see: space.common.model.Model.query()
        @param batchSize: @see: space.common.model.Model.query()
        @param iteratorOnly: @see: space.common.model.Model.query()
        @param **kwargs: Any keyword arguments accepted by space.common.model.Model.query()

        @return: A list of models matching the filters
        '''

        return self.model().query(limit=limit, props=props, query_only=queryOnly, batch_size=batchSize, iterator_only=iteratorOnly, **kwargs)

    def isModelInstance(self, obj, model=None):
        """
        Method to check whether the obj passed is the instance of model that we have

        @param obj: object, which we have to check as the instance of model
        @param model: model class object for which we have to check obj is an instance

        @return: boolean True: if the object is instance of model
                 boolean False: if the object is not the instance of model

        @author: Ajith Kumar
        """
        if model:
            return isinstance(obj, model)
        return isinstance(obj, self.model().model)

    def insert(self, shouldPersist=False, **kwargs):
        '''
        Insert a new instance the model instance tied to the API, with the attributes passed in as keyword arguments.
        IMPORTANT: This always returns a list, the caller has to handle that properly.

        @param shouldPersist: Should we persist the model instance or not?
        @param **kwargs: Any number of keyword arguments, these MUST be the properties of the model instance

        @return: Model instance - If shouldPersist is True
                 List of models to put - If shouldPersist is False. It will be the responsibility of the caller to put these
        '''
        from space.common.models import persist

        instance = self.model().insert(shouldPersist=False, data=kwargs)

        # Update the API instance object
        self.setInstance(instance)

        # Call any 'on update' triggers
        modelsToPersist = self.updateTrigger(original=None, updated=instance, shouldPersist=shouldPersist)
        # If there are models updated as part of the trigger, the trigger should return a list of models to persist
        if not modelsToPersist:
            modelsToPersist = []
        # If there is such a list, add the current model instance to the list to get persisted
        modelsToPersist.append(self.getInstance())

        if shouldPersist:
            persist(modelsToPersist)
            # If we do persist stuff, we do not need to pass the list along to the caller!
            return self.getInstance()

        return modelsToPersist

    def update(self, shouldPersist=False, **kwargs):
        '''
        Update the model instance tied to the API, with the attributes passed in as keyword arguments.

        @param shouldPersist: Should we persist the model instance or not?
        @param **kwargs: Any number of keyword arguments, these MUST be the properties of the model instance

        @return: Model instance - If shouldPersist is True
                 List of models to put - If shouldPersist is False. It will be the responsibility of the caller to put these
        '''
        from copy import copy
        from space.common.models import persist

        instance = self.getInstance()
        # Make a copy of the model instance, which will have the original values
        orgInstance = copy(instance)

        # Iterate over all the the keyword arguments and set the model attributes/properties
        for k, v in kwargs.iteritems():
            setattr(instance, k, v)

        # Update the API instance object
        self.setInstance(instance)

        # Call any 'on update' triggers
        modelsToPersist = self.updateTrigger(original=orgInstance, updated=instance, shouldPersist=False)
        # If there are models updated as part of the trigger, the trigger should return a list of models to persist
        if not modelsToPersist:
            modelsToPersist = []
        # If there is such a list, add the current model instance to the list to get persisted
        modelsToPersist.append(self.getInstance())

        if shouldPersist:
            persist(modelsToPersist)
            # If we do persist stuff, we do not need to pass the list along to the caller!
            return self.getInstance()

        return modelsToPersist

    def updateTrigger(self, original=None, updated=None, shouldPersist=True):
        '''
        On Update Trigger event to be defined here.
        This will be called whenever a model instance is updated.
        This method is a stub or an abstract, the child classes should implement this.

        @param original: The original instance before updation
        @param updated: The updated instance
        @param shouldPersist: Should we persist the model instance or not?

        @return: void, ideally we should not return anything here, unless we are dealing with batch insertions or something
                 List, if its batch insertion this method should return a list of objects to put.
                 NOTE: It should always return a list, even if there is only one object to be putted!
        '''
        pass

class ModuleJsonAPI(object):

    def respond(self, func, **kwargs):
        """ Method to return invoked function's result as JSON object.
        The exceptions are handled here and appropriate messages are passed
        in the JSON object. The response format is as below :
                {
                    response : '',
                    error    : ''
                }
        """

        import logging
        import json

        response = ''
        try:
            func = getattr(self, func, None)
            if not func:
                return json.dumps({'response' : response, 'error' : 'Invalid function.'})
            responseDict = func(**kwargs)
            response = responseDict.get('response', '')
            error = responseDict.get('error', '')
        except Exception, e:
            logging.exception(e.message)
            error = e.message
        return json.dumps({'response' : response, 'error' : error})

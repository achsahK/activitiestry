from google.appengine.ext import db, search
from activity.settings.default import QUERY_BATCH_SIZE

NO_RETRY_QUEUE = 'noRetryQueue'
NO_RETRY_BACKEND_QUEUE = 'noRetryBackendQueue'
SINGLE_TASK_NO_RETRY_BACKEND_QUEUE = 'singleTaskNoRetryBackendQueue'

# JSON parsing traverse depth
MAX_TRAVERSE_DEPTH = 1

class PropertyType(object):
    import datetime
    from google.appengine.api import users

    STRING = 'STRING'
    BLOB = 'BLOB'
    INT = 'INT'
    FLOAT = 'FLOAT'
    # @deprecated: Use BOOL instead
    BOOLEAN = 'BOOLEAN'
    # This is to properly convert a boolean into JS boolean
    BOOL = 'BOOL'
    REFERENCE = 'REFERENCE'
    DATE = 'DATE'
    DATETIME = 'DATETIME'
    TIME = 'TIME'
    USER = 'USER'
    TEXT = 'TEXT'
    CATEGORY = 'CATEGORY'
    EMAIL = 'EMAIL'
    URL = 'URL'
    PHONE_NUMBER = 'PHONE_NUMBER'
    POSTAL_ADDRESS = 'POSTAL_ADDRESS'
    RATING = 'RATING'
    LIST = 'LIST'
    LIST_STRING = 'LIST_STRING'
    LIST_KEY = 'LIST_KEY'
    LIST_DATE = 'LIST_DATE'
    LIST_FLOAT = 'LIST_FLOAT'
    LIST_TEXT = 'LIST_TEXT'

    HANDLERS = {str : STRING,
                unicode : STRING,
                bool : BOOLEAN,
                int : INT,
                long : INT,
                float : FLOAT,
                datetime.datetime : DATETIME,
                datetime.date : DATE,
                datetime.time : TIME,
                list : LIST,
                db.Key : REFERENCE,
                users.User : USER,
                db.Blob : BLOB,
                db.Text : TEXT,
                db.Category : CATEGORY,
                db.Link : URL,
                db.Email : EMAIL,
                db.PhoneNumber : PHONE_NUMBER,
                db.PostalAddress : POSTAL_ADDRESS,
                db.Rating : RATING,
               }

#### Custom Model Classes for JSON handling, all models will inherit from this. ####

class CustomBaseModel(object):
    # Property types of custom Model
    props = None
    _props = None

    toStringField = 'key'

    _maxDepth = MAX_TRAVERSE_DEPTH
    # Mark a list of model properties as safe. @see: setSafeProperties
    _safeProperties = None
    convert = True
    convertBlob = False
    _additional_attributes = ''
    _view_url = ''
    _key_attr = None

    __dynamicHiddenProperties = None
    __dynamicallyDefinedProps = None

    __dynamicNonPersistedProps = None

    __projectedProps = None

    def __init__(self):
        self.setDynamicHiddenProperties(None)
        self.setDynamicallyDefinedProps(None)
        self.setDynamicNonPersistedProps(None)

        self._maxDepth = MAX_TRAVERSE_DEPTH
        self.convert = True
        self.convertBlob = False
        # Hyperlink stuff
        self._view_url = ''
        self._additional_attributes = ''

        # Really hacky way to change the behaviour of getProperty when called against an instance vs a class.
        # If we have an _getProperty defined, use that instead of getProperty.
        # @see CustomExpandoModel._getProperty
        self.getProperty = getattr(self, '_getProperty', self.getProperty)

    def __str__(self):
        return str(getattr(self, self.toStringField))

    def setMaxDepth(self, maxDepth):
        self._maxDepth = maxDepth

    def setSafeProperties(self, safeProperties):
        '''
        Mark a list of model properties as safe.
        Ideally we should pre-fetch these properties to avoid DB load.
        The list should be a string list of the property names.
        '''
        self._safeProperties = safeProperties

    def toJSON(self, depth, appendString='', convert=True, convertBlob=False, hideHyperLinkAttributes=False):
        '''
        Convert the custom base model object to JSON format.

        @param depth: Depth to which the method should parse to. Used from the serializer in utils to denote how deep it has dug to
        @param appendString: Appends this string to the resultant string, only if convert is TRUE (Used for memcaching)
        @param convert: TRUE (default) : Convert the result to JSON String
        @param convertBlob: FALSE (default) : Convert blob object to base64 encoded String
        @param hideHyperLinkAttributes: FALSE (default) : Do not include the hyperlink attributes in the result

        @example:
        result = { 'asp'               : value
                   'asp1'              : value
                   'dynamicProperties' : {  'asp1' : value
                                         }
                 }

        <b>Note: Dynamic properties will be available as both dict keys as well as in 'dynamicProperties'</b>
        '''
        import json

        if depth > self._maxDepth:
            return None

        if self.getProps() is None:
            raise NotImplementedError

        self.convert = convert
        self.convertBlob = convertBlob
        self.hideHyperLinkAttributes = hideHyperLinkAttributes

        res = self.modelToJSON(depth)
        if self.convert:
            return appendString + json.dumps(res)
        else:
            return res

    def modelToJSON(self, depth):
        val = {}

        # Add hardcoded stuff first
        val['key'] = str(self.key())
        if not self.hideHyperLinkAttributes:
            val['hyperlink'] = unicode(self.hyperlink)
            val['hyperlink_label'] = unicode(self.hyperlink_label)
            val['model_view_url'] = unicode(self.model_view_url)

        val.update(self._getDefinedProperties(depth))
        val.update(self._getDynamicProperties(depth))
        val.update(self._getPropertiesDefinedAsPropertyFunctions(depth))
        # Get properties that are projected onto the model, this is a pretty hacky code, used mainly for reducing memory consumption
        val.update(self._getProjectedProperties(depth))
        # Get dynamically defined and non persisted properties, i.e one assigned to the instance on the fly
        val.update(self._getDynamicNonPersistedProps(depth))

        return val

    def _getDefinedProperties(self, depth):
        import logging

        val = {}

        props = self.getProps()
        if self.getDynamicallyDefinedProps():
            props.update(self.getDynamicallyDefinedProps())

        for k, v in props.items():
            value = self.getValueForDatastore(k)
            if self._safeProperties is not None and k in self._safeProperties:
                value = getattr(self, k)
                if v == PropertyType.LIST_KEY:
                    try:
                        value = getattr(self, k + '_list')
                    except:
                        pass

            logging.debug('Inside modelToJSON::_getDefinedProperties - Checking -  ' + k)
            val[k] = self._parse(value, v, depth, k)

        return val

    def _getDynamicProperties(self, depth):
        import logging
        val = {}
        val['dynamicProperties'] = {}

        for k, v in self.getDynamicProps().items():
            value = getattr(self, k)

            logging.debug('Inside modelToJSON::_getDynamicProperties - Checking -  ' + k)
            parsedValue = self._parse(value, v, depth, k)
            val[k] = parsedValue
            val['dynamicProperties'][k] = parsedValue

        return val

    def _getPropertiesDefinedAsPropertyFunctions(self, depth):
        val = {}

        for k, v in self.getPropertiesDefinedAsPropertyFunctions().items():
            value = getattr(self, k)
            val[k] = self._parse(value, v, depth, k)

        return val

    def _getDynamicNonPersistedProps(self, depth):
        '''
        Get dynamically defined and non persisted properties, i.e one assigned to the instance on the fly.

        @param depth: The depth to we need to scan
        '''

        val = {}

        if self.getDynamicNonPersistedProps():
            for k, v in self.getDynamicNonPersistedProps().items():
                value = getattr(self, k)
                val[k] = self._parse(value, v, depth, k)

        return val

    def _getProjectedProperties(self, depth):
        '''
        Get projected properties, the dynamically defined properties to conserve memory.
        !NOTE: This ignores attribute errors, since that is what we needed it to do at the time of writing this. Need to revisit when that situation changes.

        @param depth: The depth to we need to scan
        '''

        val = {}
        if self.getProjectedProps():
            for k, v in self.getProjectedProps().items():
                try:
                    value = getattr(self, k)
                    val[k] = self._parse(value, v, depth, k)
                except AttributeError:
                    pass
        return val

    def _parse(self, value, typ, depth, prop=None):
        import logging
        import types
        import base64
        from space.utils.utils import getStringDate, handleDateList, safeunicode, handleStringList

        handles = {PropertyType.STRING    : safeunicode , PropertyType.EMAIL : safeunicode,
                   PropertyType.BLOB      : None, PropertyType.INT   : int,
                   PropertyType.BOOLEAN   : None, PropertyType.FLOAT : None,
                   PropertyType.REFERENCE : None, PropertyType.DATE  : None,
                   PropertyType.URL       : safeunicode , PropertyType.LIST_KEY : None,
                   PropertyType.LIST_DATE : handleDateList, PropertyType.BOOL : lambda x: x,
                   PropertyType.LIST_STRING : handleStringList
                  }
        if value is None:
            return value

        # TODO: Find a better way to handle this, this works fine if we are dealing with only one model or a list of model
        # But when expand reference and go deeper this becomes complicated, and the props checking does not make sense
        # For example, if we are iterating over a list of employees, this check would hide all the unwanted properties,
        # but what if I expand the references and go into the designation model maybe, these props makes no sense there!
        if (prop is not None and self.getDynamicHiddenProperties() is not None and prop in self.getDynamicHiddenProperties()):
            return None

        if typ == PropertyType.REFERENCE:
            if isinstance(value, CustomBaseModel):
                logging.debug('Safe calling another toJSON')
                return value.toJSON(depth + 1, convert=self.convert, convertBlob=self.convertBlob)
            elif isinstance(value, db.Key) and not (depth + 1) > self._maxDepth:
                logging.debug('Hitting Db while converting. To fetch ' + str(value))
                return db.get(value).toJSON(depth + 1, convert=self.convert, convertBlob=self.convertBlob)
            else:
                return str(value)
        elif typ == PropertyType.DATE:
            return getStringDate(value)
        elif typ == PropertyType.BLOB:
            if self.convertBlob:
                return base64.b64encode(value)
            return True
        elif typ == PropertyType.LIST_KEY:
            # TODO: Maybe handle this better?
            # Right now I cannot think of a way without increasing the complexity of this method
            return map(lambda it: self._parse(it, PropertyType.REFERENCE, 0, prop), value)
        elif handles.get(typ, None) is not None:
            try:
                return handles[typ](value)
            except Exception, e:
                logging.info('Unable to parse value: %s for model: %s' % (value, self.key()))
                raise e
        else:
            try:
                # TODO: handle boolean
                return str(value)
            except:
                return value

    def getAllProperties(self):
        '''
        Get all properties defined for this model. This combines all the properties defined against the models and those defined as @property functions.
        TODO: Make this a class method, once all models have been updated to move _props outside of init
        IMPORTANT: This includes hidden properties as well, use with care.
        '''
        allProps = {}
        # Note this is needed to avoid overwriting the _props defenitions, since its no longer in init!
        allProps.update(self.getProps())
        allProps.update(self.getHiddenProps())
        allProps.update(self.getPropertiesDefinedAsPropertyFunctions())
        return allProps

    def getProps(self):
        '''
        TODO: Change this so that all models use _props instead of props
        TODO: Make this a class method, once all models have been updated to move _props outside of init
        '''
        props = getattr(self, "props", None)
        if not props:
            props = getattr(self, "_props", None)
        return props

    def setProps(self, props):
        self.props = props
        self._props = props

    def getProjectedProps(self):
        return self.__projectedProps

    def setProjectedProps(self, props):
        self.__projectedProps = props

    def getDynamicProps(self):
        '''
        TODO: Make this a class method, once all models have been updated to move _props outside of init
        '''
        return {}

    def getPropertiesDefinedAsPropertyFunctions(self):
        '''
        Get properties defined as @property functions
        TODO: Make this a class method, once all models have been updated to move _props outside of init
        '''
        return getattr(self, "_propertiesDefinedAsPropertyFunctions", {})

    def getDynamicallyDefinedProps(self):
        '''
        Get properties that were dynamically added, like when trying to serialize the model.
        '''
        return self.__dynamicallyDefinedProps

    def setDynamicallyDefinedProps(self, props):
        '''
        Set properties that were dynamically added, like when trying to serialize the model.
        Add dynamically hidden properties to the model, so that they can be parsed and set on to the instance.
        Useful when a property is hidden from pros, but is needed when serializing.

        @param props: The dynamically defined dict of properties
        '''
        self.__dynamicallyDefinedProps = props

    def getDynamicNonPersistedProps(self):
        '''
        Get dynamically defined and non persisted properties, i.e one assigned to the instance on the fly.
        '''
        return self.__dynamicNonPersistedProps

    def setDynamicNonPersistedProps(self, props):
        '''
        Set dynamically defined and non persisted properties, i.e one assigned to the instance on the fly.

        @param props: The props to set
        '''
        self.__dynamicNonPersistedProps = props

    @classmethod
    def getProperty(cls, prop):
        '''
        Get an instance of the Property class for the attribute/property.

        NOTE: We have a provision to get the property definition from a defined @property method.
        For example; if we want to get the property instance for an @property defenition called 'propname' which returns a reference,
        then define another @property like 'propnameProperty' which will return the instance of the actual property
        then you can use propname when prefetching or when serializing, etc.
        This is currently used in employee for employeeLocation, for reference take a look at that.
        If you are still not confused, take a look at space.utils.manager.Manager.prefetch_refprops()

        @param prop: (string) The name of the property

        @return an object of type db.Property instance
        '''
        p = cls.properties().get(prop, None)
        if not p:
            if getattr(cls, "%sProperty" % prop, None):
                p = str(prop)
            else:
                raise KeyError("There is no '%s' property for this model" % prop)
        return p

    def getValueForDatastore(self, prop):
        '''
        Get the value for datastore for the property from the model instance.

        @param prop: (string) The name of the property

        @return an object representing the datastore value for the property
        '''
        return self.getProperty(prop).get_value_for_datastore(self)

    @property
    def hyperlink_label(self):
        '''
        Needed for hyperlink property, children who wish to use hyperlink property must override this method.
        IMPORTANT: Must return a string.
        '''
        return ''

    @property
    def model_view_url(self):
        '''
        Build a view URL for the model instance.
        Can be accessed by calling model_instance.model_view_url

        Needed for hyperlink property.
        !IMPORTANT: Model should define _url, which should be the space.settings.UrlPattern definition.

        Note: Should only be called against a valid model instance. For safety, this method will always catch all errors and return.
        Usual errors are, called against an instance, which is not persisted, or not a model instance, and so on.
        The errors caught are logged, so check there for more information.
        '''
        import logging
        from space.utils.utils import URLCreator

        try:
            key = self.key() if not self._key_attr else getattr(self, self._key_attr)
            return str(URLCreator(self._view_url, str(key)))
        except Exception, e:
            # Toggle the comment below to turn exception tracing on, as of going live, there are only a few places where the necessary properties are defined.
            # Hence disabling logging.exception, since it will flood the error logs!
#            logging.exception("An error occurred while trying to generate model_view_url for a model: ")
#            logging.debug("An error occurred while trying to generate model_view_url for a model: " + str(e.message))
            pass

        return ''

    @property
    def model_view_absolute_url(self):
        '''
        Build the absolute model view URL, i.e it contains the protocol and host info (http://space.qburst.com/)
        '''
        from activity.settings.default import HOST_URL

        try:
            return '%s%s' % (HOST_URL, self.model_view_url)
        except Exception, e:
            # Toggle the comment below to turn exception tracing on, as of going live, there are only a few places where the necessary properties are defined.
            # Hence disabling logging.exception, since it will flood the error logs!
            # logging.exception("An error occurred while trying to generate model_view_absolute_url for a model: ")
            # logging.debug("An error occurred while trying to generate model_view_absolute_url for a model: "  str(e.message))
            pass

        return ''

    @property
    def hyperlink_with_absolute_url(self):
        '''
        A property to access the models hyperlink, will be added to the serialized model.
        @see: elf.getHyperlink
        '''
        return self.getHyperlink(viewUrl=self.model_view_absolute_url)

    @property
    def hyperlink(self):
        '''
        A property to access the models hyperlink, will be added to the serialized model.
        @see: elf.getHyperlink
        '''
        return self.getHyperlink()

    def getHyperlink(self, queryString="", viewUrl=None):
        '''
        Build a view hyperlink for the model instance.
        Can be accessed by calling model_instance.hyperlink.

        !IMPORTANT: Model should implement hyperlink_label property or else the resultant hyperlink WILL NOT be correct,
        and define _url, which should be the space.settings.UrlPattern definition.

        Note: Should only be called against a valid model instance. For safety, this method will always catch all errors and return.
        Usual errors are, called against an instance, which is not persisted, or not a model instance, and so on.
        The errors caught are logged, so check there for more information.

        @param queryString: The query string to append to the URL
        '''
        import logging
        from django.utils.safestring import mark_safe

        try:
            if not viewUrl:
                viewUrl = self.model_view_url
            if viewUrl:
                key = self.key() if not self._key_attr else getattr(self, self._key_attr)
                return mark_safe(
                                 unicode('<a href="' + viewUrl + queryString + '"' \
                                         + self._additional_attributes + ' ' + getattr(self, 'dynamic_attributes', "") \
                                         + ' entityKey="' + str(key) + '" >' \
                                         + self.hyperlink_label + '</a>'
                                         )
                                 )
            else:
                raise Exception('Unable to generate hyperlink, property url is not definedself.')
        except Exception, e:
            # Toggle the comment below to turn exception tracing on, as of going live, there are only a few places where the necessary properties are defined.
            # Hence disabling logging.exception, since it will flood the error logs!
#            logging.exception("An error occurred while trying to generate hyperlink for a model: ")
#            logging.debug("An error occurred while trying to generate hyperlink for a model: " + str(e.message))
            pass

        return ''

    def getHiddenProps(self):
        """
        Method to get the hidden properties of the model.
        TODO: Make this a class method, once all models have been updated to move _props outside of init
        """
        return getattr(self, "_hidden", {})

    def setHiddenProps(self, hiddenProps):
        """
        Method to set the hidden properties of the model.
        TODO: Make this a class method, once all models have been updated to move _props outside of init
        """
        self._hidden = hiddenProps

    def getSafeProperties(self):
        """ Method to fetch list of safe properties in the model.
        #TODO: The safe properties should be defined in self._props instead of
                removing self._hidden properties.
        """
        props = self.getProps()
        hidden = self.getHiddenProps()

        return filter(lambda propKey: propKey not in hidden, props.keys())

    def setDynamicHiddenProperties(self, dynamicHiddenProperties):
        '''
        Hacky way of hiding properties during serialisation.
        # TODO: Think of a better way to do this!
        '''
        self.__dynamicHiddenProperties = dynamicHiddenProperties

    def getDynamicHiddenProperties(self):
        return self.__dynamicHiddenProperties

class CustomSearchableModel(search.SearchableModel, CustomBaseModel):
    pass
class CustomModel(db.Model, CustomBaseModel):
    pass
class CustomExpandoModel(db.Expando, CustomBaseModel):
    shouldHideDynamicProps = False

    def setProps(self, props):
        self._props = props

    def setShouldHideDynamicProps(self, shouldHideDynamicProps):
        self.shouldHideDynamicProps = shouldHideDynamicProps

    def _getGaeDynamicProperties(self):
        return self._dynamic_properties

    def getDynamicProps(self):
        ret = {}
        if not self.shouldHideDynamicProps:
            for k, v in self._getGaeDynamicProperties().items():
                ret[k] = PropertyType.HANDLERS.get(type(v), str)
        return ret

    def _getProperty(self, prop):
        '''
        Hacky function to enable different behaviour for getProperty depending on how its called.
        When called against an instance this function will be triggered, the difference here is that this method is aware of the dynamic properties.
        When called against a class/type the @classmethod getProperty will be triggered.
        <b>The need for the hack</b> is due to difference in behaviour of a normal model and an expando model, the dynamically defined properties
        have no existance at the class level, i.e at class level we can only access properties that are already defined in the method.
        But once instantiated, all the dynamic properties are also available. We need the getProperty to have access to them both!
        i.e when called against the class level, maybe for pre-fetching and all, it will only scan the pre-defined properties,
        when called against an instance like when serialzing the instance, it will scan all properties, defined + dynamic.

        Overriding the parent behaviour to handle dynamic expando properties.
        @see CustomBaseModel.getProperty
        '''
        try:
            ret = super(CustomExpandoModel, self).getProperty(prop)
        except KeyError:
            ret = False
        if not ret:
            ret = self._getGaeDynamicProperties()[prop]
        return ret

    pass

#### End custom model classes. ####

#### Serializale Object instances and ENUM ####
#### Use toDict() for JSON serialization for now, hopefully this would be fixed in a future release ####

class BaseSerializableObject(object):
    '''
    Base class of a JSON serializable object.
    Can be used for ENUMs and all
    '''
    attrs = {}

    def __init__(self, attrs=None, **kwargs):

        if attrs == None: attrs = {}
        self.attrs = kwargs
        self.attrs.update(attrs)

    def __setattr__(self, name, value):
        if name != 'attrs':
            self.attrs[name] = value
        else:
            self.__dict__['attrs'] = value

    def __getattr__(self, name):
        if name != 'attrs':
            return self.attrs.get(name, None)
        else:
            return self.attrs

    def __str__(self):
        return str(self.attrs)

    def clear(self):
        self.attrs.clear()

    def asDict(self):
        return self.attrs

    def getKeys(self):
        return self.asDict().keys()

    def getValues(self):
        return self.asDict().values()

    def getItems(self):
        return self.asDict().items()

    def getValuesAsTuple(self):
        return [(i, i) for i in self.getValues()]

    def __getstate__(self): return self.attrs
    def __setstate__(self, d): self.attrs.update(d)

    def __reduce_ex__(self, protocol): return (BaseSerializableObject, (), self.attrs, None, None)

#### Custom model which inherits from db.model for now, we need to change this,,####

class DummyModel(BaseSerializableObject):
    '''
    @TODO: This is what we need to use when returning values from APIs.
    Currently only used for memcached results
    '''

    def __str__(self):
        return self.key()

    def __reduce_ex__(self, protocol): return (DummyModel, (), self.attrs, None, None)

    def key(self):
        key = self.attrs.get('key', None)
        if key is not None:
            key = db.Key(key)
        return key


################################################################
####                Enumeration class                       ####
################################################################

from django.utils.datastructures import SortedDict

SORTED_DICT_ATTRIBUTES = ['keyOrder']
class Enum(SortedDict):
    '''
    Enumeration class. Which inherits from django.utils.datastructures.SortedDict to maintain the order of the attributes.

    Note: At the time of writing this django.utils.datastructures.SortedDict only had one attribute 'keyOrder',
    which is added to the SORTED_DICT_ATTRIBUTES list above, this is due to the fact that we override __setattr__ and __getattr__
    and need list of attributes to exclude from setting and getting!
    If in future if the SortedDict is modified to have other attributes, we need to add those to the list above.

    To use this declare a child class like;

    class AnEnum(Enum):
        def __init__(self, *args, **kwargs):
            super(AnEnum, self).__init__(*args, **kwargs)
            self.first = 0
            self.asp = 1
            self.pas = 2

    Then create an instance of the class, this is useful for accessing attributes like An.first and so on. We also don't need to initialize the class every time we use it.
    An = AnEnum()
    '''
    def __init__(self, data=None, uncde=False, **kwargs):
        '''
        Override the init to set values which are set as keywords.
        @param data:
        '''
        super(Enum, self).__init__(data=data)
        for k, v in kwargs.items():
            self[k] = v if not uncde else unicode(v)

    def __setattr__(self, k, v):
        '''
        Override the default set attribute to handle Enum.key = value
        @param k: The attribute to set
        @param v: The value to be set to the attribute
        '''
        if k in SORTED_DICT_ATTRIBUTES : self.__dict__[k] = v
        self[k] = v

    def __getattr__(self, k):
        '''
        Override the default get attributes to handle Enum.key
        @param k: The attribute to be accessed
        '''
        if k in SORTED_DICT_ATTRIBUTES: return getattr(self, k)
        return self[k]

    def asDict(self):
        '''
        @deprecated: Use the Enum instance it self
        '''
        return self

    def getKeys(self):
        '''
        @deprecated: Use .keys() instead
        '''
        return self.keys()

    def getValues(self):
        '''
        @deprecated: Use .values()
        '''
        return self.values()

    def getItems(self):
        '''
        @deprecated: Use .items()
        '''
        return self.items()

    def getValuesAsTuple(self):
        '''
        Get the values as a tuple, useful for combo boxes in form and all.

        @return: The values as a list of tuples.
        '''
        return [(i, i) for i in self.getValues()]

    def valuesExcept(self, value):
        '''
        Return the list all values, with the values mentioned in 'value' removed.
        '''
        from types import ListType

        vs = self.values()
        if type(value) is ListType:
            for i in value:
                vs.remove(i)
        else:
            vs.remove(value)
        return vs

#### End ####

class DumbObject(object):
    '''
    A dumb object which returns none if its not able to find an attribute
    '''

    def key(self):
        return

    def __getattr__(self, k):
        try:
            return getattr(self, k)
        except:
            return None

########    Dict which never raises key errors!    ########

class DictWithDefault(dict):
    '''
    A dict that ignores key errors when looking up a value, and always returns a dumb object.
    '''
    def __init__(self, *args, **kwargs):
        self.__default = kwargs.get('default', None)
        self.__defaultFor = kwargs.get('defaultFor', None)
        try:
            del kwargs['default']
        except:
            pass
        try:
            del kwargs['defaultFor']
        except:
            pass
        dict.__init__(self, *args, **kwargs)
    def __getitem__(self, *args, **kwargs):
        try:
            ret = dict.__getitem__(self, *args, **kwargs)
            if not ret and args[0] in self.__defaultFor:
                ret = self.__default
            return ret
        except:
            return self.__default

class Model(object):

    model = None
    isSearchableModel = False

    def prepareModel(self, modelObject, data={}):
        '''
            This method is to set the attributes for the model
            @param modelObject: object of the model, whose modelObject has to be created
            @param data: dict, in which the attributes to set in the model
            @return: the modelObject, after the attributes have set
            @author: Ajith Kumar
        '''
        for attr in data:
            setattr(modelObject, attr, data[attr])
        return modelObject

    def getNewInstance(self, data={}):
        '''
        Get a new Django ORM model instance

        @param data: A dict of attributes of the model

        @return Object, an instance of the model
        '''
        return self.model(**data)

    def insert(self, data={}, shouldPersist=False):
        '''
        Insert a new instance of the model.

        @param data: A dict of attributes of the model
        @param shouldPersist: A flag to denote if the instance should be persisted or not

        @return the model instance
        '''
        if data:
            modelObject = self.getNewInstance(data)
            if shouldPersist:
                return modelObject.put()
            else:
                return modelObject

    def put(self, models=[]):
        if models:
            return db.put(models)

    def putMulti(self, data=[]):
        '''@deprecated: Use putMultiple'''

        models = []
        for model in data:
            modelObject = self.model()
            modelObject = self.prepareModel(modelObject, model)
            models.append(modelObject)
        return self.put(models)

    def putMultiple(self, data={}):
        '''
            Insert multiple records simultaneously.
            Similar to putMulti, but handles different model types.
            @param data: dict, in which the key defines the model class and the value is a list of models
                        example: {
                                   'projectModel' : [model1, model2]
                                 }
            !NOTE: the key SHOULD be the class name to get the model instance.
            @return: the list of models inserted
        '''
        models = []
        for method, models in data:
            modelObject = getattr(self, method)
            for model in models:
                modelObject = self.prepareModel(modelObject, model)
                models.append(modelObject)

        if len(models) > 0:
            return self.put(models)
        else:
            return None

    def update(self, modelObject, data={}):
        for attr in data:
            setattr(modelObject, attr, data[attr])
        return modelObject.put()

    def get(self, key=None, props=None):
        if key is not None:
            return self.read(key, props=props)

        return self.query()

    def getProperty(self, prop):
        '''
        Get an instance of the Property class for the attribute/property.
        A wrapper for CustomBaseModel.getProperty.

        @param  prop: (string) The name of the property

        @return an object of type db.Property instance
        '''
        return self.model.getProperty(prop)

    def getValueForDatastore(self, prop, model):
        '''
        Get the value for datastore for the property from the model instance.
        A wrapper for CustomBaseModel.getValueForDatastore.

        @param  prop: (string) The name of the property
        @param model: (CustomBaseModel instance) The model instance from which you need the value for datastore

        @return an object representing the datastore value for the property
        '''
        return model.getValueForDatastore(prop)

    def read(self, keys=[], filter={}, props=None):
        "Fetch the data and return. If the key is passed then read based on that alone. Filter is available then based on that."
        from activity.utils.manager import Manager

        if keys:
            res = self.model.get(keys)
            if res and props:
                props = [self.getProperty(prop) for prop in props]
                res = Manager().prefetch_refprops(res, *props)
            return res
        elif filter:
            return self.query(filters=[filter])

    def delete(self, key):
        try:
            db.delete(key)
            return True
        except:
            pass

        return False

    def query(self, filters=[], orderBy=[], limit=1, offset=0, keys_only=False,
              query_only=False, batch_size=QUERY_BATCH_SIZE,
              iterator_only=False,
              projection=None,
              get_query_object=False,
              props=None,
              cursor=None):
        '''
        Handler for the query. API to hide the model.

        @example Activities.query([{'propertyOperator' : 'user', 'value' : 'userKey'}], [user], 1, 0)
        @param filters: List of filters to be applied. Eg: [{'propertyOperator' : 'user', 'value' : 'userKey'}]
        @param orderBy: List of properties to order the result by.
        @param limit: Number of rows to be returned. Defaults to 1.
        @param offset: Offset for the query. Defaults to 0.
        @param keys_only: Return only the keys of the result. Refer: Datastore Keys Only operations.
        @param query_only: Should we fetch the data? Used for heavy list iterations.
        @param batch_size: Specify the batch size, to be used with query_only to specify the batch size to be fetched while iterating
        @param iterator_only: Returns the custom iterator implementation. @see: space.common.models.SpaceDataIterator
                              This is implemented to overcome the limitations mentioned in this issue https://code.google.com/p/googleappengine/issues/detail?id=4432
                              And as mentioned in comment #3, the SpaceDataIterator uses cursors to fetch results in batches.
                              This can be used along with batch_size and even props unlike query_only.
                              IMPORTANT: Try to avoid using this as much as possible. Some of the reasons to avoid this are;
                              * It is very expensive!
                              * It also uses query cursors are limited by their limitations. https://developers.google.com/appengine/docs/python/datastore/queryclass#Query_cursor
        @param projection: Projection queries. @see: https://developers.google.com/appengine/docs/python/datastore/queries#Query_Projection
        @param get_query_object: Returns the actual Django query object. It is highly discouraged to use this, only needed for certain paginations
                                    NOTE: TODO: We need to remove this option once we implement our own custom pagination.
        @param props: list of model properties, to be prefetched from the result. Defaults to None.
            A combination of query_only and batch_size is a very powerful tool to fetch ALL records of a particular model if you do not know the size.
            Also large entities like EmployeeDetails can be fetched in batches, hence overcoming the memory limitations.
            @see google.appengine.ext.db.Query().run()
            NOTE: using this is only advised in rare conditions which match the above mentioned criteria, since using this method is
            highly inefficient, always fetch using limit.
            NOTE: When using a projection query, remember to include the props to be prefetched in the project properties list as well
        @param cursor: To be used in combination with iterator_only, this can be used to set the cursor for the iterated query, useful for paginating over results
        '''
        from activity.utils.manager import Manager

        query = self.getQuery(filters=filters, orderBy=orderBy, keys_only=keys_only, projection=projection)

        if get_query_object:
            res = query
        elif query_only:
            res = query.run(batch_size=batch_size)
        elif iterator_only:
            # TODO: Remove this code duplication, not sure if fetching the props every time
            if props:
                props = [self.getProperty(prop) for prop in props]
            res = SpaceDataIterator(query=query, batchSize=batch_size, props=props, cursor=cursor)
        else:
            if limit == 1:
                res = query.get()
            else:
                res = query.fetch(limit, offset, batch_size=limit)
            if res and props:
                props = [self.getProperty(prop) for prop in props]
                res = Manager().prefetch_refprops(res, *props)

        return res

    def queryKeyOnly(self, filters=[], limit=1, offset=0):
        '''
        Handler for the query on key_only. API to hide the model.

        @example Activities.query([{'propertyOperator' : 'user', 'value' : 'userKey'}], [user], 1, 0)
        @param filters: List of filters to be applied. Eg: [{'propertyOperator' : 'user', 'value' : 'userKey'}]
        @param limit: Number of rows to be returned. Defaults to 1.
        @param offset: Offset for the query. Defaults to 0.
        '''

        from google.appengine.ext.db import GqlQuery

        queryString = 'SELECT __key__ FROM ' + self.modelName
        args = []

        if filters:
            queryString += ' WHERE'
            count = 0
            for filter in filters:
                queryString += ' ' + str(filter['propertyOperator']) + ' :' + str(count + 1)
                args.append(filter['value'])
                if count < (len(filters) - 1):
                    queryString += ' AND'
                count += 1

        query = GqlQuery(queryString, *args)

        if limit == 1:
            return query.get()
        else:
            return query.fetch(limit, offset)

    def createQuery(self, keys_only=False, projection=None):
        '''
        Handler for making a query statement for non searchable models.
        @param keys_only: Return keys only
        @param projection: project query
        '''

        return self.model.all(keys_only=keys_only, projection=projection)

    def applyFilter(self, query, filters):
        '''
        Adds filters to the passed query.
        '''

        for filter in filters:
            query.filter(filter['propertyOperator'], filter['value'])
        return query

    def applyOrderBy(self, query, orderBy):
        '''
        Adds orderBy to the passed query.
        '''

        for property in orderBy:
            query.order(property)
        return query

    def getQuery(self, filters=[], orderBy=[], keys_only=False, projection=None):
        '''
        Handler for making a query statement. API to hide the model.
        @example Activities.query([{'propertyOperator' : 'user', 'value' : 'userKey'}], [user], False)
        @param filters: List of filters to be applied. Eg: [{'propertyOperator' : 'user', 'value' : 'userKey'}]
        @param orderBy: List of properties to order the result by.
        @param keys_only: for keys only queries. Defaults to False.
        @param projection: Projection queries. @see: https://developers.google.com/appengine/docs/python/datastore/queries#Query_Projection
        '''

        query = self.createQuery(keys_only, projection)

        query = self.applyFilter(query, filters)

        query = self.applyOrderBy(query, orderBy)

        return query

    def getPropertyList(self):
        """ Method to return list of properties of the model.
        """

        return (self.model._properties).keys()

    def createInstance(self, **kwargs):
        '''
        Generate the models Django ORM model instance, this is to be used in places where the persisting is handled outside of the module API.
        IMPORTANT: Should try to avoid using this method at all times, and should only be used in dire situations, where there are no other options.
        For example, batch puts, where we call db.put() and actually need the model instance and so on.

        @param **kwargs: All the keyword arguments needed to create the model instance

        @return: The model instance
        '''

        return self.model(**kwargs)

class SearchableModel(Model):

    def getQuery(self, filters=[], orderBy=[], searchText=None, keys_only=False, projection=None):
        '''
        Handler for making a query statement.
        @param filters: List of filters to be applied. Eg: [{'propertyOperator' : 'user', 'value' : 'userKey'}]
        @param orderBy: List of properties to order the result by.
        @param searchText: Search text with which the search is to be made.
        @param keys_only: Fetch only keys
        @param projection: Projection queries. @see: https://developers.google.com/appengine/docs/python/datastore/queries#Query_Projection
        @return: Query statement.
        '''
        query = self.createQuery(keys_only=keys_only, projection=projection)

        query = self.applySearch(query, searchText)

        query = self.applyFilter(query, filters)

        query = self.applyOrderBy(query, orderBy)

        return query

    def createQuery(self, keys_only=False, projection=None):
        '''
        Handler for making a query statement for searchable models.
        @param projection: Projection queries. @see: https://developers.google.com/appengine/docs/python/datastore/queries#Query_Projection
        '''
        from google.appengine.ext.db import Query

        if projection or keys_only:
            return Query(self.model, projection=projection, keys_only=keys_only)

        return self.model.all()

    def applySearch(self, query, searchText=None):
        '''
        Handles search for searchable models.
        @return: query statement after applying search.
        '''

        if searchText:
            query.search(searchText)
        return query

    def search(self, filters=[], orderBy=[], limit=1, offset=0, searchText=None):
        '''
        Handler for the search. API to hide the model.

        @example Activities.query([{'propertyOperator' : 'user', 'value' : 'userKey'}], [user], 1, 0)
        @param filters: List of filters to be applied. Eg: [{'propertyOperator' : 'user', 'value' : 'userKey'}]
        @param orderBy: List of properties to order the result by.
        @param limit: Number of rows to be returned. Defaults to 1.
        @param offset: Offset for the query. Defaults to 0.
        @param searchText: search text for searchable models search.
        '''

        query = self.getQuery(filters, orderBy, searchText)

        if limit == 1:
            return query.get()
        else:
            return query.fetch(limit, offset)

    def getSearchQuery(self, filters=[], orderBy=[], searchText=None):
        '''
        Returns search query statement as per the passed parameters.

        @param filters: List of filters to be applied. Eg: [{'propertyOperator' : 'user', 'value' : 'userKey'}]
        @param orderBy: List of properties to order the result by.
        @param searchText: search text for searchable models search.
        '''

        query = self.getQuery(filters, orderBy, searchText)

        return query

########################################################################################
################    Custom iterator for iterating over large results    ################
########################################################################################

class SpaceDataIterator(object):
    '''
    Custom data iterator class. An iterable class.
    Due to the issue mentioned here https://code.google.com/p/googleappengine/issues/detail?id=4432,
    the default query iterator is not mature enough for Space :-)
    This iterator is especially useful when iterating over huge datasets, like daily activity stuff, where potentially we maybe iterating the same query for more than 10 minutes!
    Whether that is a good thing or not, well theoretically it is NOT, but we need it so here it is..
    IMPORTANT: Take special care when using this class, which should never be directly accessed, and should only be accessed using the iterator_only option in query()
    This class uses query cursors so read up on the limitations of that here;
    https://developers.google.com/appengine/docs/python/datastore/queryclass#Query_cursor
    '''
    __cursor = None
    __items = None

    _query = None
    _batchSize = None
    _props = None
    _count = 0

    def __init__(self, query, batchSize=QUERY_BATCH_SIZE, props=None, cursor=None):
        '''
        Initialize the data iterator

        @param query: The query instance
        @param batchSize: The batch size, defaults to space.settings.QUERY_BATCH_SIZE, which is 100 at the time of writing this
        @param props: The props to prefetch
        @param cursor: The cursor to use for the query
        '''
        self._query = query
        self._batchSize = batchSize
        self._props = props
        self.__cursor = cursor
        self._setCount()

        # Initialized the instance, prepare the initial batch, reset instance variables and populate the first batch
        self._reset()
        self._populateBatch()

    def __iter__(self):
        '''
        Return the iterator, which is self in this case.

        @return: The iterator instance
        '''
        return self

    def next(self):  # @ReservedAssignment
        '''
        Iterate over the iterable. The for loop knows to call this method.

        @return: The next item in the list
        '''
        try:
            return self.__items.next()
        except StopIteration:
            # Finished processing the current batch, reset instance variables and populate the next batch
            self._reset()
            self._mark()
            self._populateBatch()
            return self.__items.next()

    def count(self):
        '''
        Get the query count. This is useful for passing the iterator object to a paginator.
        '''
        return self._count

    def cursor(self):
        '''
        Get the query cursor. This is useful for passing the cursor to a template and starting from the cursor.
        '''
        return self.__cursor

    def reverseCursor(self):
        '''
        Get the reverse query cursor. This is useful for passing the cursor to a template and starting from the cursor.
        '''
        from google.appengine.datastore.datastore_query import Cursor

        return Cursor(urlsafe=self.__cursor).reversed().urlsafe()

    def __getitem__(self, key):
        '''
        List get item to override default list getitem behaviour, makes it easy to access this as a list in paginator.
        NOTE: Only slices are supported and steps for slices are just ignored, only start and stop is considered.

        @param key: The list lookup key, currently only slices are supported.

        @return: List of sliced values
        '''
        if not isinstance(key, slice):
            raise NotImplementedError('Only slices work for now.')

        ret = []
        count = key.start
        for v in self:
            count += 1
            ret.append(v)
            if count == key.stop:
                break

        return ret

    def _setCount(self):
        '''
        Set the query count. This is to set the count before the query is set with the cursor.
        '''
        self._count = self._query.count(10000)

    def _mark(self):
        '''
        Mark the current position. Basically gets and sets the query cursor.
        '''
        self.__cursor = self._query.cursor()

    def _populateBatch(self):
        '''
        Populate data for the current batch. Will fetch the results using query cursor if present, and will prefetch reference props for the batch.
        '''

        self._preFetchReferenceProps(self._query.with_cursor(start_cursor=self.__cursor).fetch(limit=self._batchSize, batch_size=self._batchSize))
        self._mark()

    def _preFetchReferenceProps(self, items):
        '''
        Prefetch reference props for the batch

        @param items: The list of items
        '''
        from activity.utils.manager import Manager

        if self._props:
            items = Manager().prefetch_refprops(items, *self._props)

        self._setItems(items)

    def _setItems(self, items):
        '''
        Set the batch items to the instance.
        @param items: The items in this batch
        '''
        import itertools

        self.__items = itertools.chain(items)

    def _resetCursor(self):
        '''
        Reset the cursor
        '''
        self.__cursor = None

    def _resetItems(self):
        '''
        Reset items batch
        '''
        self.__items = None

    def _reset(self):
        '''
        Reset the instance, resets the cursor and items
        '''
        import gc

#        self._resetCursor()
        self._resetItems()
        # Try and free up some memory
        gc.collect()

########################################################################################


#### JSON Property ####

class JSONProperty(db.Property):
    from google.appengine.api import datastore_types

    def get_value_for_datastore(self, model_instance):
        value = super(JSONProperty, self).get_value_for_datastore(model_instance)
        return db.Text(self._deflate(value))

    def validate(self, value):
        return self._inflate(value)

    def make_value_from_datastore(self, value):
        return self._inflate(value)

    def _inflate(self, value):
        import json

        if value is None:
            return {}
        if isinstance(value, unicode) or isinstance(value, str):
            return json.loads(value)
        return value

    def _deflate(self, value):
        import json

        return json.dumps(value)

    data_type = datastore_types.Text

class PickledProperty(db.Property):
    from google.appengine.api import datastore_types

    def get_value_for_datastore(self, model_instance):
        value = super(PickledProperty, self).get_value_for_datastore(model_instance)
        return db.Blob(self._deflate(value))

    def validate(self, value):
        return self._inflate(value)

    def make_value_from_datastore(self, value):
        return self._inflate(value)

    def _inflate(self, value):
        import pickle
        from google.appengine.api import datastore_types

        if isinstance(value, datastore_types.Blob):
            return pickle.loads(value)
        return value

    def _deflate(self, value):
        import pickle

        return pickle.dumps(value)

    data_type = datastore_types.Blob

class SetProperty(db.ListProperty):
    """
    SetProperty, similar to the datastore's ListProperty, but we accept sets and convert them to List.
    Since BigTable only handles lists and not sets, we inherit ListProperty and override methods to convert the set to list
    so that the checks done in ListProperty does not fail.
    """

    def validate(self, value):
        value = db.Property.validate(self, value)
        if value is not None:
            if not isinstance(value, (set, frozenset)):
                raise db.BadValueError('Property %s must be a set' % self.name)

            value = self.validate_list_contents(value)
        return value

    def default_value(self):
        return self._deflate(db.Property.default_value(self))

    def get_value_for_datastore(self, model_instance):
        value = super(SetProperty, self).get_value_for_datastore(model_instance)
        return self._inflate(value)

    def make_value_from_datastore(self, value):
        if value is not None:
            value = super(SetProperty, self).make_value_from_datastore(value)
            return self._deflate(value)

    def _inflate(self, value):

        if isinstance(value, set):
            return list(value)
        return value

    def _deflate(self, value):

        if isinstance(value, list):
            return set(value)
        return value

class ReferenceProperty(db.ReferenceProperty):
    '''
    Override the default App Engine ReferenceProperty to implement a fix for the cyclic dependency.
    You can specify the 'referenceClass' as the first argument when initializin the class to set the enforce the reference kind.
    The 'referenceClass' should specify the fully qualified name of the class as a string, for example 'space.employee.models.Employee'
    '''
    __reference_class = None

    def __init__(self, referenceClass, *args, **kwargs):
        from space.utils.utils import getClass

        self.__reference_class = getClass(referenceClass)
        super(ReferenceProperty, self).__init__(*args, **kwargs)

    def validate(self, value):
        value = super(ReferenceProperty, self).validate(value)

        if value is not None and not isinstance(value, db.Key) and not isinstance(value, self.__reference_class):
            raise db.KindError('Property %s must be an instance of %s' %
                          (self.name, self.__reference_class.kind()))

        return value

##################################################################################

########        Space Dynamic propertied meta-class implementation        ########

from google.appengine.ext.db import PropertiedClass

class SpaceDynamicPropertiedClass(PropertiedClass):
    '''
    Dynamic propertied meta-class for models.
    This is usually used to plug in module specific properties to a model. Though it can be used in other scenarios as well.
    The class which defines this met-class should define a class attribute '_metaClassPropsGenerator', which should point to a function
    that returns a tuple with two dicts
    1. A dict of properties and values to be assigned to the class.
    2. A dict to be added to the CustomModels _props

    NOTE: All classes using this meta-class should have the _props in the proper manner, i.e _props as class attributes.

    @example:
    For Example:
    def asp():
        from space.common.models import ReferenceProperty
        from space.common.appConfig import IS_DIVISION_MODULE_ENABLED, IS_BUSINESS_UNIT_MODULE_ENABLED

        properties = {}
        props = {}

        if IS_DIVISION_MODULE_ENABLED:
            properties.update({
                'division' : ReferenceProperty(referenceClass = 'space.employee.models.Division', collection_name = 'employee_division')
            })
            props.update({
                'division' : PropertyType.STRING
            })
        if IS_BUSINESS_UNIT_MODULE_ENABLED:
            properties.update({
                'division' : ReferenceProperty(referenceClass = 'space.employee.models.Division', collection_name = 'employee_division')
            })
            props.update({
                'division' : PropertyType.STRING
            })
        return (properties, props)

    class Asp(CustomModel):
        __metaclass__ = SpaceDynamicPropertiedClass
        _metaClassPropsGenerator = asp

        name = StringProperty()

    a = Asp(name = 'ASP', division = 'div')

    print a.name, a.division
    '''
    def __new__(cls, name, bases, attrs):
        '''
        Implement the customized class.

        @see: Referer the following page to understand about metaclasses.
        http://stackoverflow.com/questions/100003/what-is-a-metaclass-in-python/6581949#6581949
        http://eli.thegreenplace.net/2011/08/14/python-metaclasses-by-example/

        @param cls: The class object
        @param name: The name of the class to be created
        @param bases: Base classes, for inheritance
        @param attrs: A dict of all attributes and their values/definitions
        '''
        properties, props = attrs.pop('_metaClassPropsGenerator')()
        attrs.update(properties)
        clas = super(SpaceDynamicPropertiedClass, cls).__new__(cls, name, bases, attrs)
        clas._props.update(props)
        return clas

class AttributeCustomizerMetaClass(type):
    '''
    Dynamic attribute customizer meta-class for classes.
    '''
    def __new__(cls, name, bases, attrs):
        '''
        Implement the customized class.

        @see: Referer the following page to understand about metaclasses.
        http://stackoverflow.com/questions/100003/what-is-a-metaclass-in-python/6581949#6581949
        http://eli.thegreenplace.net/2011/08/14/python-metaclasses-by-example/

        @param cls: The class object
        @param name: The name of the class to be created
        @param bases: Base classes, for inheritance
        @param attrs: A dict of all attributes and their values/definitions
        '''
        attrs.update(attrs.pop('_metaClassPropsGenerator')(attrs))
        return super(AttributeCustomizerMetaClass, cls).__new__(cls, name, bases, attrs)

# from google.appengine.ext.db.djangoforms import ModelFormMetaclass

class SpaceModelFormMetaclass(AttributeCustomizerMetaClass):  # , ModelFormMetaclass):
    '''
    Meta class to add dynamic fields and stuff to the model form.
    @see: space.employee.forms.EmployeeForm for a sample implementation.
    '''
    pass

from django.forms.forms import DeclarativeFieldsMetaclass

class SpaceFormMetaclass(AttributeCustomizerMetaClass, DeclarativeFieldsMetaclass):
    '''
    Meta class to add dynamic fields and stuff to the normal form.
    @see: space.report.forms.EmployeeListReportsForm for a sample implementation.
    '''
    pass

################################################################################################

def persist(models):
    """
    Method to group insertion of multiple objects of same/different models.
    @param models: List of model objects to be inserted.
    """

    return db.put(models)

def get(keys):
    """
    Method to return instances of multiple models
    @param keys: Key list of instances to be fetched from db
    """

    return db.get(keys)

def delete(models):
    """
    Method to delete instances of multiple models
    @param models: Key/Model instance list to be removed from db
    """

    return db.delete(models)

'''
Cron script classes
IMPORTANT NOTE:
This is a common module, any changes made here should also be applied to Activities and any other subsystems.
This only applies until we split common and util functions to a common module, just like OAuth.
'''

import logging

class SpaceWorkerFactory(object):
    '''
    Well not really a factory! It uses introspection to load the classes.
    The purpose of this class is to remove the need to write loads of cron URLs in app.yaml.
    We will have a single entry point for Cron jobs '/cron/job/run/([a-zA-Z-.]+)'
    The last bit in the URL will contain the cron script class name,
    either as the class name or the whole path including modules.
    1. If you only specify the class name you need to define your imports in the CronScriptFactory.get()
    2. If not you can specify the whole path, like space.employee.BirthdayGadgetCronScript
    '''

    @staticmethod
    def get(className, instance):
        from space.utils.utils import getClass
        from space.utils.customException import IllegalArgumentException
        # Import your scripts here, needed only if module is not specified in the class name

        if className.find('.') < 0:
            # No module specified
            # getClass may not work unless its imported here
            cls = eval(className)
        else:
            cls = getClass(className)

        if not issubclass(cls, instance):
            raise IllegalArgumentException("%s is not an instance of %s" % (cls, instance))

        return cls

class CronScriptFactory(object):
    '''
    Well not really a factory! It uses introspection to load the classes.
    The purpose of this class is to remove the need to write loads of cron URLs in app.yaml.
    We will have a single entry point for Cron jobs '/cron/job/run/([a-zA-Z-.]+)'
    The last bit in the URL will contain the cron script class name,
    either as the class name or the whole path including modules.
    1. If you only specify the class name you need to define your imports in the SpaceWorkerFactory.get()
    2. If not you can specify the whole path, like space.employee.BirthdayGadgetCronScript
    '''
    @staticmethod
    def get(className):
        return SpaceWorkerFactory.get(className, CronScript)

class TaskWorkerFactory(object):
    '''
    Well not really a factory! It uses introspection to load the classes.
    The purpose of this class is to remove the need to write loads of task URLs in app.yaml.
    We will have a single entry point for Tasks '/task/run/([a-zA-Z-.]+)'
    The last bit in the URL will contain the task class name,
    either as the class name or the whole path including modules.
    1. If you only specify the class name you need to define your imports in the SpaceWorkerFactory.get()
    2. If not you can specify the whole path, like space.employee.BirthdayGadgetTask
    '''
    @staticmethod
    def get(className):
        return SpaceWorkerFactory.get(className, TaskWorker)

class Loggable(object):
    _logMessage = None

    def __init__(self):
        # Clear the log message variable
        self._logMessage = None

    def log(self, msg, level=logging.DEBUG):
        '''
        Log a message.

        @param msg: The message to log
        @param level: The logging level to use, defaults to logging.DEBUG
        '''

        logging.log(level, msg)

        # Append the message to the member variable,
        # this will be used to send the message via email
        self._appendLoggedMessage(msg + "\n")

    def logInfo(self, msg):
        '''
        Log a message with INFO level, internally calls self.log, so this method is same as calling log('the messge', logging.INFO).

        @param msg: The message to log
        '''

        self.log(msg=msg, level=logging.INFO)

    def logDebug(self, msg):
        '''
        Log a message with DEBUG level, use this to just debug stuff, it will not be added to the log that is emailed.

        @param msg: The message to log
        '''

        logging.debug(msg)

    def logException(self, msg):
        '''
        Log an exception, internally calls the logging.exception to log an exception stacktrace.

        Note: Since it uses logging.exception, it may not log traceback if the method is called from outside an except (catch) block.

        @param msg: The message to log
        '''

        logging.exception(msg)

        # Append the message to the member variable,
        # this will be used to send the message via email
        self._appendLoggedMessage("An exception was logged (check server logs for more information) with the message %s" % msg)

    def _appendLoggedMessage(self, msg):
        '''
        Append the message to the member variable, this will be used to send the message via email

        @param msg: The message
        '''
        from datetime import datetime
        from StringIO import StringIO

        if not self._logMessage:
            # Initialize if empty
            self._logMessage = StringIO()

        self._logMessage.write(datetime.now().strftime("%d-%m-%Y %H:%M:%S ") + msg.encode('utf8', 'ignore'))

    def _getLoggedMessage(self):
        '''
        Get the messages logged till now, which is stored in the member variable, this will be used to send the message via email

        @return: The message string logged till now
        '''
        return self._logMessage.getvalue() if self._logMessage and self._logMessage.getvalue() else 'No messages logged!'

class SpaceWorker(Loggable):
    '''
    Cron script parent class.
    All cron scripts should inherit from this class and override the 'doRun' method to handle actions.

    Note: <b> You have access to Keyword arguments as well as normal arguments </b>

    The purpose of this script is to remove the need for writing exception handlers and common cron script tasks.
    Example:

    class CronScriptChild(CronScript):
        def doRun(self):
            # Get the params
            arg1 = self.getArg(0)
            arg2 = self.getArg(1)

            theKey = self.get('theKey')
            self.log("Log something")
            anotherParam = self.get('anotherParam')
            if self.isDebug:
                # Do something debuggy. Useful when we want to run the script without actually modifying anything.

            # Do something

        def onError(self):
            # Do something on error
            # Note that sending emails and stuff are already handled

    def viewHandler(request):
        from space.utils.utils import getValueFromRequestByKey

        arg1 = getValueFromRequestByKey('arg1')
        arg2 = getValueFromRequestByKey('arg2')

        theKey = getValueFromRequestByKey('theKey')
        anotherParam = getValueFromRequestByKey('anotherParam')

        cs = CronScriptChild(arg1, arg2, theKey = theKey, anotherParam = anotherParam, isDebug = True)
        return cs.run()

    def genericCronScriptViewHandler(request, scriptName):
        from space.utils.utils import getAllParametersFromRequest
        from space.common import CronScriptFactory

        params = getValueFromRequestByKey('anotherParam')

        cs = CronScriptFactory.get(scriptName)

        cs = CronScriptChild(**params)
        return cs.run()
    '''

    _subjectPrependTxt = ''
    _httpRequest = None
    _httpResponse = None
    isDebug = False
    shouldSendEmailOnSuccess = True

    _kwargs = {}
    _args = []

    def __init__(self, isDebug=False, shouldSendEmailOnSuccess=True, subjectPrependTxt='', *args, **kwargs):
        """
        Initialize the instance

        @param isDebug: Denote if the script/task is running in debug mode or not
        @param shouldSendEmailOnSuccess: Denote if the script/task should send an email to notify admins of successful completion
        @param *args: Any number of arguments
        @param **kwargs: Any number of keyword arguments
        """

        super(SpaceWorker, self).__init__()

        self._subjectPrependTxt = subjectPrependTxt

        # Denote if the script/task is running in debug mode or not
        self.isDebug = isDebug
        # Denote if the script/task should send an email to notify admins of successful completion
        self.shouldSendEmailOnSuccess = shouldSendEmailOnSuccess
        # Clear the HTTP request/response objects
        self._httpRequest = None
        self._httpResponse = None

        # Save all the arguments
        self._args = args
        self._kwargs = kwargs

    def run(self, request=None):
        '''
        The main worker method.
        Note: <b>This method should NOT be overridden</b>

        @return: HTTP response object (http.HttpResponse(), always returns 200 response)
        '''
        from django import http

        # Ideally this should return a 500 incase of an exception
        # But this is a cron script, we don't really care about the HTTP response,
        # all the details of the error should have been emailed to admins and logged
        self._httpResponse = http.HttpResponse()
        self._httpResponse.status_code = 200

        try:
            self._httpRequest = request

            self.doRun()

            # All done, email about script completion
            self._notifiySuccess()
        except Exception, e:
            logging.exception(self._getErrorHeader())
            self._sendEmailToAppAdminsOnError(e)
            self.onError(e)

        return self._httpResponse

    def get(self, k):
        '''
        Get the keyword argument by keyword

        @param k: The keyword of the keyword argument
        '''
        return self._kwargs.get(k)

    def getArg(self, i):
        '''
        Get the non-keyword argument by index

        @param i: The index of the non-keyword argument
        '''
        return self._args[i]

    def getHttpRequestObject(self):
        '''
        Get the HTTP request object
        '''
        return self._httpRequest

    def getHttpResponseObject(self):
        '''
        Get the HTTP response object
        NOTE: We default the response with status code 200, it is not recommended to alter this!
        '''
        return self._httpResponse

    def doRun(self):
        '''
        Does the actual work. Any keyword argument passed to the run() method is passed on to this method.
        Every child SHOULD override this method.

        '''
        raise NotImplementedError

    def onError(self, exp):
        '''
        On error handler.
        Children may override this, if they want a custom implementation.
        Has access to all the keyword arguments passed to run() and the exception object.
        Note that sending emails and stuff are already handled.

        @param exp: The exception object
        '''
        return

    def getScriptName(self):
        """
        Get the script name.
        Can be overridden to provide custom script name, defaults to class name.

        @return string script name
        """
        return str(self.__class__)

    def _sendEmailToAppAdminsOnError(self, exp):
        '''
        Send email to application admins on error

        @param exp: The exception object
        '''

        msg = '''
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
    An error occurred while running this cron script, the exception has been logged.
     <br />  %s
     <br /><br />
     Messages appended till now are attached (please refer to the logs to get a more clear picture).
</body>
</html>
     ''' % exp

        self._sendEmailToAdmins(self._getErrorHeader(), msg)

    def _sendEmailToAdmins(self, subject, msg):
        """
        Send emails to app admins

        @param subject: The subject
        @param msg: The message
        """
        import logging
        from space.utils.utils import sendMailsToAdmins, sendGenericMails
        from django.conf import settings

        # Who send the email to if we fail to send mail to admins
        MAIL_TO_ON_ERROR = [settings.APP_ADMIN_EMAIL_INFO]

        subject = self._subjectPrependTxt + '  ' + subject

        if self.isDebug:
            subject = "DEBUG MODE: " + subject

        try:
            sendMailsToAdmins(subject, msg, attachments=[('debug.log', self._getLoggedMessage())])
        except:
            logging.exception(self._getErrorHeader() + ", while trying to send email to admins. Going to try sending using Mailer.")
            try:
                sendGenericMails(MAIL_TO_ON_ERROR, subject + ' - Failed to send to admins', msg)
            except:
                logging.exception(self._getErrorHeader() + ", while trying to send email to admins, using Mailer.")

    def _notifiySuccess(self):
        msg = '''
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
    Script run complete. The output has been logged.
     <br />
     Script Status: %s
     <br /><br />
     Messages appended till now are attached (please refer to the logs to get a more clear picture).
</body>
</html>
     ''' % self._getSuccessHeader()

        # Log everything
        self.log(msg)
        if self.shouldSendEmailOnSuccess:
            # Notify Admins via email about script's/task's completions
            self._sendEmailToAdmins(self._getSuccessHeader(), msg)

    def _getErrorHeader(self):
        return "Error while running cron script %s" % self.getScriptName()

    def _getSuccessHeader(self):
        return "%s Script run complete" % self.getScriptName()

# Child classes inherting SpaceWorker class for Cron and Task
class CronScript(SpaceWorker):

    def onError(self, exp):
        '''
        On error handler.
        Cron scripts will return a 500 response on error, since it does not re-run automatically on error.
        You can override this, but it is not recommended, since it won't show up in the Admin Console, and we will not know if no email is sent.
        IMPORTANT: If this method is overridden, it SHOULD call super().onError as well.

        @see: SpaceWorker.onError
        '''
        self.getHttpResponseObject().status_code = 500

class TaskWorker(SpaceWorker):
    pass

####    This code needs to stay put for the DeferredTaskWorker to work    ####

'''
Pickle does not work with instance methods, it just does not! Googled for a bit and found the below solution.
The below code is to help with pickling and unpickling instance methods.
@see: http://bytes.com/topic/python/answers/552476-why-cant-you-pickle-instancemethods
The answer by 'Steven Bethard'
Also the one with class support in SO
@see: http://stackoverflow.com/questions/5429584/handling-the-classmethod-pickling-issue-with-copy-reg
'''

def _pickle_method(method):
    func_name = method.im_func.__name__
    obj = method.im_self
    cls = method.im_class
    return _unpickle_method, (func_name, obj, cls)

def _unpickle_method(func_name, obj, cls):
    for cls in cls.mro():
        try:
            func = cls.__dict__[func_name]
        except KeyError:
            pass
        else:
            break
    return func.__get__(obj, cls)

def makeInstanceMethodsPicklable():
    import copy_reg
    import types

    copy_reg.pickle(types.MethodType, _pickle_method, _unpickle_method)

####    This code needs to stay put for the DeferredTaskWorker to work    ####

from functools import wraps

class DeferredTaskWorker(TaskWorker):
    '''
    Deferred task worker class, which is a re-structured SpaceWorker child.
    A decorator class that can be used to make any function into a deferred task. Inherits the exception and success handling features from TaskWorker,
    this helps avoid code duplication and what not.

    IMPORTANT NOTE
    1. This currently only works with instance methods.
    2. Any type of function that cannot be used with Deferred task probably cannot be used with this as well.
    3. Only Keyword argument seems to work, normal parameters does not seem to work!

    @example: To use this, just add the following decorator to your method, for example;
    def onE(e):
        # This is an on exception handler function, this will be called when ever there is an error processing the script.
        print e.message

    @DeferredTaskWorker(onE)
    def doSomeWork(param):
        # This is the actual function that does some work!
        # To explain that we catch all exceptions this particular function is behaving badly and raises an exception.
        raise Exception("Im bad!!")

    The optional 'onE' argument above is the onError method that will be called if there is an error.
    NOTE: The onError should ideally be not an instance method, this is code is not tested against an instance method and may behave badly, or may just work.

    To learn more about deferred tasks read;
    @see: https://developers.google.com/appengine/articles/deferred?hl=en
    and
    @see: https://developers.google.com/appengine/docs/python/taskqueue/overview-push?hl=en#Deferred_Tasks
    '''

    _queue = None
    _target = None

    def __init__(self, onErrorFunc=None, subjectPrependTxt='', queue=None, target=None):
        '''
        Obviously the contructor!

        @param onErrorFunc: A function to be called when there is an error, this is similar to the @see space.common.SpaceWorker.onError
        @param subjectPrependTxt: The text to prepend to the success and error mails
        @param queue: The queue to use for the task
        '''
        if onErrorFunc:
            self.onError = onErrorFunc

        self._subjectPrependTxt = subjectPrependTxt
        self._queue = queue
        self._target = target

    def __call__(self, func):
        '''
        Override the __call__ function to enable wrapping and all, make it a proper decorator function.
        @return the wrapped function
        '''

        @wraps(func)
        def wrapper(instance, dtw_depth=0, *args, **kwargs):
            '''
            The wrapper function, I do the wrapping.

            @param instance: Since we are only dealing with instance methods that are deferred, the first argument will always be the instance or self
            @param dtw_depth: The depth unto which we have reached. Used for recursion handling.
                              IMPORTANT: This paramter should not be there in the function keyword args.
            @param _subjectPrependTxt: Special parameter to alter the subject of the success mail
            @param args: Any number of arguments
            @param kwargs: Any number of keyword arguments
            '''
            from google.appengine.ext import deferred

            if dtw_depth > 0:
                logging.info('Going to call the actual function(%s)! with arguments (%s, %s)' % (func.__name__, args, kwargs))
                # If we have reached the max depth, in this case the depth is to avoid recursion, so a depth > 0 would be a recursive call.
                # This is a bit hacky to handle depth/recursion like this, but I am not able find another/better solution

                # Adding logging functions to the instance, so that the logs are added to the debug.log attachment
                instance.loggerInfo = self.logInfo
                instance.loggerDebug = self.logDebug
                return func(instance, *args, **kwargs)

            # Do magic to make instance methods picklable
            makeInstanceMethodsPicklable()

            logging.info('Going to call the run wrapper')
            if self._queue:
                # Set the queue if present
                kwargs['_queue'] = self._queue
            if self._target:
                # Set the target if present
                kwargs['_target'] = self._target
            if kwargs.get('_subjectPrependTxt'):
                self._subjectPrependTxt = kwargs.get('_subjectPrependTxt')
                del kwargs['_subjectPrependTxt']
            # The function that actually does the work will be passed as a parameter to the deferred task
            deferred.defer(self.run, getattr(instance, func.__name__), dtw_depth=1, *args, **kwargs)

        return wrapper

    def run(self, obj, dtw_depth, *args, **kwargs):
        '''
        The main worker method. The note on the parent class mentions that this method should never be overridden,
        but that note is for child classes that implement logic using the workers. Its okay for me to do what I want!

        @param obj: The function/class init to be called
        @param dtw_depth: The depth unto which we have reached. Used for recursion handling.
        @param args: Any number of arguments
        @param kwargs: Any number of keyword arguments

        @return: anything the decorated function returns
        '''

        ret = None

        try:
            self.logInfo('Going to run the deferred task')
            ret = obj(dtw_depth=dtw_depth, *args, **kwargs)

            self.logInfo('All done, going to email about deferred task completion')
            # All done, email about script completion
            self._notifiySuccess()
        except Exception, e:
            logging.exception(self._getErrorHeader())
            self._sendEmailToAppAdminsOnError(e)
            self.onError(e)

        return ret

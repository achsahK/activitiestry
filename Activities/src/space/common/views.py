from django.shortcuts import render_to_response

from google.appengine.api import users

from activity.utils.manager import Manager

def direct_to_template_with_login(request, template):
    params = Manager().checkLogin(module = 'HR')

    template_values = {
                        'user': users.get_current_user(),
                        'params' : params
                      }

    template_values.update(request.GET)
    return render_to_response(template, template_values)

def cronJobRun(request, cronClassName):
    """
    Generic handler for all cron jobs

    @param cronClassName: The class name of the cron task

    @return: HttpResponse
    """
    import logging
    from space.utils.utils import getAllParametersFromRequest
    from space.common import CronScriptFactory

    logging.debug("Looking up class for class name %s" % cronClassName)

    return CronScriptFactory.get(cronClassName)(**getAllParametersFromRequest(request)).run(request)


def taskRun(request, taskClassName):
    """
    Generic handler for all tasks

    @param cronClassName: The class name of the task

    @return: HttpResponse
    """
    import logging
    from space.utils.utils import getAllParametersFromRequest
    from space.common import TaskWorkerFactory

    logging.debug("Looking up class for class name %s" % taskClassName)

    return TaskWorkerFactory.get(taskClassName)(**getAllParametersFromRequest(request)).run(request)

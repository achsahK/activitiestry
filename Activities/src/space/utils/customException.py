class UnauthorizedAccessException(Exception):
    def __init__(self, message = 'Access Violation'):
        super(UnauthorizedAccessException, self).__init__(message)

class LoginRequiredException(Exception):
    def __init__(self, message, url):
        self.message = message
        self.url = url
        super(LoginRequiredException, self).__init__(message)

class ExternalAccessException(Exception):
    pass

class ReferentialIntegrityViolation(Exception):
    pass

class InvalidPayrollStatus(Exception):
    pass

class IllegalArgumentException(Exception):
    pass

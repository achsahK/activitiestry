from google.appengine.ext import db
from space.common.forms import url_re
from activity.settings.default import DEFAULT_DATE_FORMAT, DEFAULT_TIME_ZONE, EMAIL_SENDER_NAME, EMAIL_USERNAME
import datetime
import time
import types
import re

class UTCTZ(datetime.tzinfo):
    def utcoffset(self, dt):
        return datetime.timedelta(hours=0, minutes=0)
    def dst(self, dt):
        return datetime.timedelta(0)

class ISTTZ(datetime.tzinfo):
    def utcoffset(self, dt): return datetime.timedelta(hours=5, minutes=30)
    def dst(self, dt): return datetime.timedelta(0)
    def tzname(self, dt): return 'IST'
    def olsen_name(self): return 'Asia/Calcutta'

def getLocalTime(dt=datetime.datetime.utcnow(), tz=DEFAULT_TIME_ZONE, comp=True):
    tzs = { 'UTC' : UTCTZ,
            'IST' : ISTTZ,
          }

    dt = dt.replace(tzinfo=tzs['UTC']())

    if comp:
        return dt.astimezone(tzs[tz]()).replace(tzinfo=None)
    else:
        return dt.astimezone(tzs[tz]())

def incrDay(dtm=datetime.datetime.now(), n=1):
    delta = datetime.timedelta(days=n)
    return dtm + delta

def getMonthStartAndEnd(dt=datetime.date.today()):
    import calendar

    rnge = calendar.monthrange(dt.year, dt.month)
    start = dt.replace(day=1)
    end = dt.replace(day=rnge[1])

    return {'start' : start,
            'end' : end
           }

def incrMonth(dtm=datetime.date.today(), n=1):
    return dtm + datetime.timedelta(n * 30)
#    return datetime.date(dtm.year, dtm.month + n, dtm.day)

def getStringDate(date, format=DEFAULT_DATE_FORMAT):
    if date:
        return date.strftime(format)

def isDateInRange(chkDate, startDate, endDate):
    try:
        if chkDate >= startDate and chkDate <= endDate:
            return True
    except:
        pass

    return False

def isValidDate(date, required=False):
    if required or date:
        try:
            datetime.datetime.strptime(date, DEFAULT_DATE_FORMAT)
        except:
            return False

    return True

def getDateFromString(strDate, format=DEFAULT_DATE_FORMAT):
    try:
        date = datetime.datetime.strptime(strDate, format)
        return date
    except:
        return strDate

def getDateRangeString(dateFrom, dateTo, isAfternoon=False, isForenoon=False, format=DEFAULT_DATE_FORMAT):
    if dateTo > dateFrom:
        string = dateFrom.strftime(format) + ''
        if isAfternoon:
            string += ' (Afternoon)'

        string += ' to ' + dateTo.strftime(format)
        if isForenoon:
            string += ' (Forenoon)'
    else:
        string = dateFrom.strftime(format)
        if isAfternoon:
            string += ' (Afternoon)'
        elif isForenoon:
            string += ' (Forenoon)'

    return string

def requestHasKey(req, key):
    return req.GET.has_key(key) or req.POST.has_key(key)

def getValueFromRequestByKey(req, key):
    return req.GET.get(key) or req.POST.get(key)

def getAllParametersFromRequest(req):
    '''
    Get all parameters from request, combines POST and GET parameters
    @param req: The request
    '''
    params = {}
    for k, v in req.GET.items():
        params[str(k)] = v
    for k, v in req.POST.items():
        params[str(k)] = v
    return params

def linebreaksbr(value):
    "Converts newlines into <br />s"
    return value.replace('\n', '<br />')

def sortSkillBasedDict(countDict, skillDict, n=None):
    items = countDict.items()
    decorated = [ (val[1], val[0]) for val in items ]
    decorated.sort()
    decorated.reverse()
    a = []
    i = 0
    for value in decorated:
        if n and i == n:
            break
        l = skillDict.get(value[1])
        a.append((value[1], l))
        i = i + 1

    return a
#    return [(adict.get(key), key) for key in keys]
#    return map(adict.get, keys)

def caseInSensitiveStringListComp(string, compare):
    if compare:
        for elem in compare:
            if elem.strip().lower() == string.strip().lower():
                return True

    return False

def compareAndReplaceFromList(string, replaceStr, list):
    for elem in list:
        if elem.strip().lower() == string.strip().lower():
            list.remove(elem)
            if replaceStr:
                list.append(replaceStr)

    return list

def isValidUrl(url):
    if url:
        validUrl = url_re.search(url.strip())
        if validUrl:
            return True
        else:
            return False

def calculateExp(dt1, dt2=None, shouldAddNow=True):
    exp = 0.0
    if dt2:
        dt2 = datetime.datetime.fromtimestamp(time.mktime(dt2.timetuple()))
    else:
        if shouldAddNow:
            dt2 = datetime.datetime.now()

    if dt1 and dt2:
        dt1 = datetime.datetime.fromtimestamp(time.mktime(dt1.timetuple()))
        diff = dt2 - dt1
        exp = float(diff.days / 365.0)
    return round(exp, 1)

def convertJSONToDate(dtStr):
    ret = None

    try:
        ret = datetime.datetime.strptime(dtStr, "%Y-%m-%d %H:%M:%S")
    except:
        pass

    return ret

def getClass(cls):
    '''
    Get the class type instance from string class name

    @param cls: Class name

    @return: The class type
    '''
    parts = cls.split('.')

    # No module specified
    if len(parts) < 2:
        return eval(cls)

    module = ".".join(parts[:-1])
    m = __import__(module)
    for comp in parts[1:]:
        m = getattr(m, comp)
    return m

def getBillingPeriodDtStr(start, end):
    dtStr = ''
    if start and end:
        stFrmt = '%B %d, %Y'
        enFrmt = '%B %d, %Y'
        if start.year == end.year:
            stFrmt = '%B %d'
        dtStr = start.strftime(stFrmt) + " to " + end.strftime(enFrmt)

    return dtStr

def getInvoiceMailSubject(invoice, isFlName=False):
    subject = 'QBurst Invoice ' + invoice.invoiceNumber + ' - ' + invoice.invoiceTo.name
    if invoice.billingPeriod:
        subject += ' - ' + invoice.billingPeriod
#    if invoice.billingPeriodStart and invoice.billingPeriodEnd:
#        subject += ' - ' + getBillingPeriodDtStr(invoice.billingPeriodStart, invoice.billingPeriodEnd)

    if isFlName:
        subject = subject.replace(' ', '_')
        subject = subject.replace('/', '-')

    return subject

def getCurrencyHTML(currency):
    from activity.settings.default import CURRENCY_HTML_CODES

    if currency.htmlCode:
        htmlCode = currency.htmlCode
    else:
        htmlCode = CURRENCY_HTML_CODES.get(currency.currency, currency.currency)

    return htmlCode

def getFormattedImage(content):
    '''
    Returns a dict with formated image details as follows:
    thumbnail : Thumbnail of the uploaded image.
    image : The image content that is ready to be put to db as blob.
    '''

    from google.appengine.api import images

    img = images.Image(content)
    img.im_feeling_lucky()
    thumbnail = img.execute_transforms(output_encoding=images.JPEG)
    image = db.Blob(thumbnail)

    imageInfo = {}
    imageInfo['thumbnail'] = thumbnail
    imageInfo['image'] = image

    return imageInfo

def addMonths(sourceDate=datetime.date.today(), n=1):
    import  calendar
    month = sourceDate.month - 1 + n
    year = sourceDate.year + month / 12
    month = month % 12 + 1
    day = min(sourceDate.day, calendar.monthrange(year, month)[1])
    return datetime.date(year, month, day)

def sendGenericMails(toField, subject, template, senderEmail=EMAIL_USERNAME, senderName=EMAIL_SENDER_NAME, templateValues={}, ccField=[]):
    '''Send mails according to the mail_fields passed.'''

    from space.utils.sendMail import SendMail
    from django import http

    response = http.HttpResponse()
    for key in templateValues:
        template = template.replace('#' + key + '#', templateValues[key])
    mail_fields = {
               'to' : toField,
               'sender' : senderEmail,
               'sender_name' : senderName,
               'cc' : ccField,
               'subject' : subject,
               'body' : template,
               'html' : True,
              }
    temp = [mail_fields]
    mail = SendMail(temp)
    if mail.is_initialized():
        mail.send()
    return response

def sendMailsToAdmins(subject, template, senderEmail=EMAIL_USERNAME, templateValues={}, **kwargs):
    """
    Creates and sends a single email message addressed to all administrators of the application.
    sender, subject, and body are required fields of the message.
    Additional fields can be specified as keyword arguments.
    """
    from google.appengine.api.mail import send_mail_to_admins
    from space.common.appConfig import SPACE_CURRENT_APP

    for key in templateValues:
        template = template.replace('#' + key + '#', templateValues[key])

    subject = SPACE_CURRENT_APP + " " + subject

    send_mail_to_admins(senderEmail, subject, template, html=template, **kwargs)

def calculateNoOfDaysWorked(actualStartDate, actualEndDate, holidays):
    '''
    Returns the number of days worked based on the values passed.
    @param actualStartDate: Start date of period of employee actually worked.
    @param actualEndDate: End date of period of employee actually worked.
    @param holidays: Ascending sorted list of holidays falling in the range startDate to endDate.
    '''

    applicableHolidays = 0
    for holiday in holidays:
        if holiday.date > actualEndDate:
            break
        elif holiday.date >= actualStartDate:
            applicableHolidays += 1

    return ((actualEndDate - actualStartDate).days + 1) - applicableHolidays

class SerializerForCustomModel(object):
    '''
    Util class to handle json conversion for custom base models.
    '''

    value = None
    convert = True
    convertBlob = False
    hideHyperLinkAttributes = False
    dynamicHiddenProperties = None
    __dynamicallyDefinedProps = None
    __dynamicNonPersistedProps = None
    __projectedProps = None
    __shouldHideDynamicProps = None

    def __init__(self, value, convert=True, convertBlob=False, hideHyperLinkAttributes=False, dynamicHiddenProperties=None, dynamicallyDefinedProps=None, dynamicNonPersistedProps=None, projectedProps=None, shouldHideDynamicProps=False):
        '''
        Initialize the serializer.

        @param value: The value to be serializes
        @param convert: Should convert to JSON or not
        @param convertBlob: Should we convert blobs to base64 encoded strings?
        @param hideHyperLinkAttributes: Should we hide hyperlink attributes?
        @param dynamicHiddenProperties: List of props that are to be hidden from the result
        @param dynamicallyDefinedProps: List of dynamically defined model properties
        @param dynamicNonPersistedProps: List of dynamically defined and non-persisted model properties
        @param projectedProps: Ability to override the models props, similar to projection queries, but this is used more to save memory rather than performance
                              !NOTE: Use this only if all the instances in the list to be serialized belongs to the same model
        @param shouldHideDynamicProps: Should we hide the dynamic properties for expandos
                                      !NOTE: Only use this with expando models.
        '''
        self.value = value
        self.convert = convert
        self.convertBlob = convertBlob
        self.hideHyperLinkAttributes = hideHyperLinkAttributes
        self.dynamicHiddenProperties = dynamicHiddenProperties
        self.__dynamicallyDefinedProps = dynamicallyDefinedProps
        self.__dynamicNonPersistedProps = dynamicNonPersistedProps
        self.__projectedProps = projectedProps
        self.__shouldHideDynamicProps = shouldHideDynamicProps

    def _any(self, value, props=None, safeProps=None, setMaxDepth=None):
        from google.appengine.ext.db import Key
        from space.common.models import CustomBaseModel, SpaceDataIterator

        if props is not None:
            return self._entityList(value, props)
        elif type(value) is types.ListType or isinstance(value, SpaceDataIterator):
            return self._list(value, safeProps=safeProps, setMaxDepth=setMaxDepth)
        elif type(value) is types.DictType:
            return self._dict(value, safeProps=safeProps, setMaxDepth=setMaxDepth)
        elif isinstance(value, CustomBaseModel):
            if setMaxDepth is not None:
                value.setMaxDepth(setMaxDepth)
            if safeProps is not None:
                value.setSafeProperties(safeProps)
            # TODO: This is a temporary hack to hide total experience and other _propertiesDefinedAsPropertyFunctions.
            # We need to fix this once the safe/props thingy goes live.
            if self.dynamicHiddenProperties:
                value.setDynamicHiddenProperties(self.dynamicHiddenProperties)
            # Add dynamically hidden properties to the model, so that they can be parsed and set on to the instance.
            # Useful when a property is hidden from pros, but is needed when serializing.
            if self.__dynamicallyDefinedProps:
                value.setDynamicallyDefinedProps(self.__dynamicallyDefinedProps)
            # Set dynamically defined and non persisted properties, i.e one assigned to the instance on the fly.
            if self.__dynamicNonPersistedProps:
                value.setDynamicNonPersistedProps(self.__dynamicNonPersistedProps)
            # Override the model's props, useful when you only want to return a specific set of properties
            if self.__projectedProps:
                value.setProjectedProps(self.__projectedProps)
            # Hide dynamic properties for expando model
            if self.__shouldHideDynamicProps:
                value.setShouldHideDynamicProps(self.__shouldHideDynamicProps)
            return value.toJSON(0, convert=self.convert, convertBlob=self.convertBlob, hideHyperLinkAttributes=self.hideHyperLinkAttributes)
        elif isinstance(value, Key):
            return str(value)

        # @todo: write a handle for when the object passed in not recognized..
        return value

    def _entityList(self, value, props):
        '''
        Parse an entity list, i.e list of entities.

        @param value: The value to be parsed
        @param props: The properties that is to be pre-fetched
                      @see space.utils.manager.Manager.prefetch_refprops

        @return parse the entity list and return a list of JSON serialiable objects, usually a dict
        '''
        from activity.utils.manager import Manager

        value = Manager().prefetch_refprops(value, *props)
        safeProps = map(
            lambda p: p if isinstance(p, str) else p.name,
            props
        )
        return self._any(value, safeProps=safeProps, setMaxDepth=0)

    def _list(self, value, safeProps=None, setMaxDepth=None):
        ret = []
        for el in value:
            ret.append(self._any(el, safeProps=safeProps, setMaxDepth=setMaxDepth))

        return ret

    def _dict(self, value, safeProps=None, setMaxDepth=None):
        ret = {}
        for k, v in value.items():
            ret[k] = self._any(v, safeProps=safeProps, setMaxDepth=setMaxDepth)

        return ret

    def get(self, props=None, maxDepth=None, safeProps=None):
        '''
        Get the serializer safe mapping for the given object.
        Usually objects are converted to dict, then properties of the object that is not serializable is converted to another format and so on.
        Basically it just does stuff, don't ask how or why :-)

        @param props: The properties to be pre-fetched, these need to be actual Property instances.
        @param maxDepth: The maximum depth unto which you want me to crawl to! This should be set to 0, higher value means
                         that we will resolve references and make them serializable too.
        @param safeProps: The properties that are safe to be resolved. Usually used when the model list is pre-fetched in another source,
                          using SpaceDataIterator or query() for example.
                          If you pass the props argument then this is not needed.
                          The data type also differs from props, you need to pass a list of strings with the property names.

        @return: A serialzer safe mapping of the object given
        '''
        return self._any(self.value, props=props, setMaxDepth=maxDepth, safeProps=safeProps)

FINANCIAL_YEAR_START_MONTH = 4

def getFinancialYear(sourceDate=datetime.date.today()):
    '''
        This is to get the Financial year for the current date or the passed date.

        @todo: This should be taken from a Financial Year field in Company model in future
    '''
    if sourceDate.month < FINANCIAL_YEAR_START_MONTH:
        return (sourceDate.year - 1)
    return sourceDate.year

def checkIfAnyOfTheFieldsHasChanged(values, fieldsToCheck):
    '''
        This method is to check whether any fields in the passed values are changed
        @param values: dict of lists with previous and current values
        @type values: dict
        @param fieldsToCheck: fields that have to be checked for any change
        @param fieldsToCheck: dict

    '''
    from activity.common.models import CustomBaseModel

    for key in fieldsToCheck:
        previous = values[key][0]
        current = values[key][1]
        for field in fieldsToCheck[key]:
            previousAttribute = previous.__getattribute__(field)
            currentAttribute = current.__getattribute__(field)
            if isinstance(previousAttribute, CustomBaseModel):
                previousAttribute = previous.getValueForDatastore(field)
            if isinstance(currentAttribute, CustomBaseModel):
                currentAttribute = current.getValueForDatastore(field)

            if not (previousAttribute == currentAttribute):
                return True

def getRecordSize(recordSz=None):
    recordSize = 50
    try:
        if int(recordSz) > 0:
            recordSize = int(recordSz)
    except(Exception):
        pass
    return recordSize

def isANumber(number):

    if number and not number.isnumeric():
        return False
    return True

def isValidMobileNumber(number):
    '''
    Validate if the given number is of a valid mobile number format
    '''

#    if number and not (re.search('.....-.....', number)):
    if number and (re.search('^[0-9\-]{9,14}$', number)):
        return True
    return False

def isValidLandLineNumber(number):

#    if number and not (re.search('0...-.......', number)):
    if number and (re.search('^[0-9\-]{9,14}$', number)):
        return True
    return False

def getRandomCharFromString(string):
    """
    Method to return a random character from the string passed
    @param string:String from which random character is to be fetched
    @return:  A random letter from string passed

    @author:  Jayakrishnan Damodaran
    """
    import random

    if string != '' and string != None:
        stringLength = len(string)
        randomNumber = random.randint(0, stringLength - 1)
        return string[randomNumber : randomNumber + 1]

def getRandomNumber(start=1, limit=100):
    """
    Method to return a random number
    @param  start: from where random number generation should begin
    @param limit: limit in which, random number should belong
                if both not specified selects a value between 1 and 100
    @return: a random number between start and limit values specified

    @author:  Jayakrishnan Damodaran
    """
    import random

    return random.randint(start, limit)

def getPageParamDict(paginatorObj):
    '''
        Method to get the pagination parameters to be passed on to JS.
        @param paginatorObj: The paginator Object whose parameters are used for pagination in JS.
        @return: A dict containing the pagination params.
    '''

    paginationParams = {
                            'prevPgNo'      : paginatorObj.previous_page_number(),
                            'nextPgNo'      : paginatorObj.next_page_number(),
                            'page_number'   : paginatorObj.number,
                            'rec_size'      : paginatorObj.paginator.per_page,
                            'tot_pages'     : paginatorObj.paginator.num_pages,
                            'hits'          : paginatorObj.paginator.count
                        }
    return paginationParams

def getMonthDiff(current_date=datetime.date.today(), dateToChk=None):
    """Method to get month difference between two dates passed.
    Checks the difference of a date with current date by default.
    @param  current_date: date object, by default it is set to today
    @param  dateToChk: date object for whose month difference is required
    @return:  difference between the months of two dates if both of them are given
                else returns None
    """
    import math

    if dateToChk and current_date:
        if dateToChk.year == current_date.year:
            diff = math.fabs(dateToChk.month - current_date.month)
        else:
            yearDiff = math.fabs(dateToChk.year - current_date.year)
            if current_date.month > dateToChk.month:
                diff = (12 * yearDiff) - (current_date.month - dateToChk.month)
            else:
                diff = (12 * yearDiff) + (dateToChk.month - current_date.month)
        return diff
    return None

def handleDateList(dateList):
    """This method is a request handler for the propertyType LIST_DATE
    It iterates over the dateList passed and creates a list with corresponding string values of dates
    Only one parameter should be passed to this method.
    This method either returns a string list of dates or None
    @param dateList: list of dates to be parsed, of type LIST
    @return: list of string dates
    @author: Ajith Kumar

    """

    if dateList:
        v = []
        for item in dateList:
            v.append(getStringDate(item))
        return v
    return None

def updateObject(obj, data):
    """Method to update passed Object with data.

        @param obj: The object to be updated
        @param data: Data to be inserted into the object
    """

    for key, value in data.items():
        setattr(obj, key, value)

    return obj

def getJsonResponse(result):
    """ Method to format the result as a Http JSON response
    @param result:  Dict containing the data to be converted as JSON
    """
    from django import http

    response = http.HttpResponse(content_type="application/json")
    response.status_code = 200
    response.write(result)
    return response

def isKeyOfModel(key, modelName):
    """ Method to check if the key passed is matches the model passed.
    @param key: Key to be checked
    @param modelName: Name of model which is to be matched against the key
    """
    try:
        if type(key) != db.Key:
            key = db.Key(key)
        if key.kind() == modelName:
            return True
    except:
        pass

    return False

def getAsKey(key):
    """ Method to get db.Key instance of key
    """
    if not isinstance(key, db.Key):
        key = db.Key(key)
    return key

def appendQueryStringToAnchorHref(anchor, queryString):
    '''
    A helper function to modify an anchor '<a>' tags href or link.
    Created with the intend to help places where we use the model.hyperlink attribute, this method can be used if we need to add
    query strings to the hyperlink.

    @param      anchor: The anchor tag, either the HTML or an DOM element
    @param queryString: The querystring to be appended to the href, besure to add the '?' if not already present in the achor tag

    @return: String: The same anchor tag passed, but with the query string appended
    '''

    return re.sub(re.compile(r'^(<a.* href=")(.*?)(".*)$'), r"\1\2" + queryString + r"\3", anchor)


def clearTime(dt=datetime.datetime.now()):
    '''
    Clear all time attributes of the date time object.
    Clears the hour, minute, second, microsecond and tzinfo
    @param dt: The datetime instance
    '''
    return dt.replace(hour=0, minute=0, second=0, microsecond=0, tzinfo=None)

def createFilter(fldName, operator, val):
    """ Method to create filter for the field name passed with the operator and the value passed
        @param fldName: name of the field with which do filtering. Should be of type string
        @param operator: operator to relate the field and value. Should be of type string
        @param val: value to which the fldName is compared
        @return: the filter dict in which the fldName as the property operator and val as the value
    """

    propOperator = fldName + ' ' + operator
    filterDict = {'propertyOperator' : propOperator, 'value' : val}
    return filterDict

def safeunicode(s):
    try:
        unicode(s, errors='ignore')
    except TypeError:
        return s


def safestringfy(s):
    try:
        s = str(s.decode('utf8', 'ignore').encode('utf8', 'ignore'))
    except UnicodeEncodeError:
        s = str(s.encode('utf8', 'ignore'))
    return s


def URLCreator(urlPattern, *keys):
    """A utility method to convert custom defined URLs(probably URLs in regex) to real time URLs for redirection.
    Pass a url pattern with or without key values that you want to substitute, it will return the desired URL for redirection.
    NOTE : The order of passing key values must be the same in which they appear in the URL. For example;
        custom URL = '^abcd/(key 1)/efgh/(key 2)' then,
        method invocation pattern : URLCreator('^abcd/(key 1)/efgh/(key 2)', value to substitute for key 1, value to substitute for key 2)

    @param urlPattern : a URL in regex pattern, example : r'^abcd/([a-zA-Z0-9-_]+)/view/([a-zA-Z]+)$'
    @param keys : values that you want to substitute for each key in the urlPattern (BEWARE of their order, refer docs above)
    @return:  A real time URL for redirection

    @author: Jayakrishnan Damodaran
    """
    if urlPattern:
        urlPattern = urlPattern.replace('^', '')
        urlPattern = urlPattern.replace('$', '')
        urlSplit = urlPattern.split('/')
        i = 0
        for uri in urlSplit:
            if not keys or len(keys) == 0:
                break
            if i < len(keys) and re.match(uri, keys[i]):
                urlSplit[urlSplit.index(uri)] = keys[i]
                i += 1
        return '/' + '/'.join(urlSplit)
    return None


def render_to_response_with_context(request, template, *args, **kwargs):
    """
    Wrapper for django.shortcuts.render_to_response to always pass the request context instance.

    @param request: The HTTP request object
    @param template: The template to use for generating response
    @param args: Any number of arguments for django.shortcuts.render_to_response
    @param kwargs:Any number of keyword arguments for django.shortcuts.render_to_response
    @param responseCode: Although not defined as a parameter, 'responseCode' parameter has a special meaning, it will set the HTTP response status code.
    """
    from django.template import RequestContext
    from django.shortcuts import render_to_response

    responseCode = kwargs.pop('responseCode', None)
    response = render_to_response(template, *args, context_instance=RequestContext(request), **kwargs)

    if responseCode:
        # Hack due to the way Django's response code setting works. We basically cannot set any response codes if we use render_to_response
        # This issue seems to be fixed in Django 1.3 and above, so can be removed once we move over to that..
        response.status_code = responseCode

    return response


def handleStringList(stringList):
    try:
        returnList = [str(item) for item in stringList]
    except:
        returnList = stringList

    return returnList


def chunks(l, n):
    """
    Yield successive n-sized chunks from l.
    """
    for i in xrange(0, len(l), n):
        yield l[i:i+n]
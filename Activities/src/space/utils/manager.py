class Manager(object):

    @staticmethod
    def checkEndPointLogin():
        '''
        Check and return the user object if the user is authenticated via an endpoint service
        '''
        import endpoints

        user = None
        try:
            user = endpoints.get_current_user()
        except endpoints.InvalidGetUserCall:
            pass

        return user

    def EndpointCallPrivileges(self, func, requireAuth=True):
        """
        Check if the current user has the privileges necessary for the Endpoint API call.
        """
        import endpoints
        from activity.settings.default import ENDPOINT_CALL_PERMISSIONS
        from space.role import RoleModuleAPI

        # Get the privileges dict for the current logged in employee
        # Will return a partially empty dict if the employee is not in DB
        try:
            privileges = RoleModuleAPI().getLoggedInEmployeePrivileges()
        except:
            raise endpoints.UnauthorizedException('Access violation.')

        is_admin = privileges['is_admin']

        if is_admin:
            requireAuth = False

        if requireAuth:
            permissions = privileges.get('permissions', [])
            if ENDPOINT_CALL_PERMISSIONS.get(func) not in permissions:
                raise endpoints.UnauthorizedException('Access violation.')

        return privileges

import urllib

from google.appengine.api import urlfetch

from activity.settings.default import EMAIL_ACCESS_KEY, MAILER_URL

import json

class SendMail:
    """ Call the external mailer web service and sends the Message contents in JSON format.
    The data will be sent via an HTTP Post method.
    Get would return an Ok (200) status for every request, without actually processing the request.
    External hosted mailer will put the mails in a thread queue and process it.
    Mailer will return an Ok (200) status if the request contains the valid access key.
    Mailer will return an Forbidden (403) status if the request contains an invalid access key.

    @param mail_fields_list: Contains a list of dicts containing the mail fields (TO, FROM, etc.)

            Mail fields are,
            'to': List of recipients. The list must of the format [[Personal Name + Email], [Personal Name + Email]]
            'sender': Set the from mail address. Should use the user name used to login.
            'sender_name': Set the From address Personal Name header.
            'cc': List of recipients to be carbon copied. Similar to 'To' field.
            'bcc': List of recipients to be blank carbon copied. Similar to 'To' field.
            'reply_to': self.reply_to,
            'subject': self.subject,
            'body': self.body,
            'html': self.html,

    @return: True if the messages were sent to the Mailer and the Mailer returned an Ok (200) status.
                Does not specify the status of the actual mail.
             False if any exception is raised or if the mailer returns a Forbidden (403) status.

    @author: Arun Shanker Prasad.
    """

    def __init__(self, mail_fields_list=[]):
        self.mail_fields_list = mail_fields_list

    def is_initialized(self):
        is_initialized = False
        for mail_fields in self.mail_fields_list:
            if mail_fields['to'] and mail_fields['sender'] and mail_fields['subject'] and mail_fields['body']:
                is_initialized = True
            else:
                return False

        return is_initialized

    def generate_mail_fields(self):
        from activity.utils.manager import Manager, SpaceEnvironment

        isDevEnv = Manager().getBuildVersionDetails().get('BUILD') == SpaceEnvironment.DEVELOPMENT
        for mail_fields in self.mail_fields_list:
            if isDevEnv:
                mail_fields['subject'] = 'TEST: ' + mail_fields['subject']

            mail_fields['access_key'] = EMAIL_ACCESS_KEY

    def send(self):
        import logging

        try:
#            mail_fields = {
#                            'access_key' : EMAIL_ACCESS_KEY,
#                            'to' : self.to,
#                            'sender' : self.sender,
#                            'sender_name' : self.sender_name,
#                            'cc' : self.cc,
#                            'bcc' : self.bcc,
#                            'reply_to' : self.reply_to,
#                            'subject' : self.subject,
#                            'body' : self.body,
#                            'html' : self.html,
#                           }
#
#            mail_fields_arr = []
#            mail_fields_arr.append(mail_fields)
#            mail_fields_arr.append(mail_fields)

            self.generate_mail_fields()

            form_fields = {
                            'JSONContent' : json.dumps(self.mail_fields_list),
                           }

            form_data = urllib.urlencode(form_fields)

            response = urlfetch.fetch(url=MAILER_URL,
                                        payload=form_data,
                                        method=urlfetch.POST,
                                        headers={'Content-Type': 'application/x-www-form-urlencoded'})

            if response.status_code == '200':
                return True

        except Exception, e:
            try:
                # We don't want to it throw an exception while debugging!!
                logging.exception("Could not Send Email : ")
            except:
                pass
            pass

        return False

import endpoints
from space.utils.manager import Manager
import time


def endpoint_user_required(function):
    def wrapper(self, *args):
        if Manager.checkEndPointLogin():
            return function(self, *args)
        raise endpoints.UnauthorizedException('Invalid token.')
    return wrapper


class retry(object):
    defaultexceptions = (Exception,)
    def __init__(self, tries=1, exceptions=None, delay=0):
        """
        Decorator for retrying a function if an exception occurs
        @source: http://peter-hoffmann.com/2010/retry-decorator-python.html (updated to dynamically override tries)

        Usage:
        @retry(tries=4, exceptions=DownloadError, delay=1)
        def asp():

        # OR if you want to dynamically set the tries, add a keyword arg to your function like;
        @retry(tries=1, exceptions=DownloadError, delay=1)
        def asp(a, b):
            pass

        # When calling the function
        asp(_tries=4)

        @param: tries -- num tries
        @param: exceptions -- exceptions to catch
        @param: delay -- wait between retries
        """
        self.tries = tries
        if exceptions is None:
            exceptions = retry.defaultexceptions
        self.exceptions = exceptions
        self.delay = delay

    def __call__(self, f):
        def fn(*args, **kwargs):
            # Dynamically override tries
            if '_tries' in kwargs:
                self.tries = kwargs.pop('_tries')
            exception = None
            for i in range(self.tries):
                try:
                    return f(*args, **kwargs)
                except self.exceptions, e:
                    print "Retry, exception: " + str(e)
                    time.sleep(self.delay)
                    exception = e
            # If no success after defined tries, raise the last exception
            raise exception
        return fn

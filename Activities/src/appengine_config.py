"""
Google App Engine provides a mechanism allowing users to specify their own values for constants and hook functions
for use by some App Engine modules. Specifying your own values can change the default behavior of those modules based
on the application's needs. The file where you specify these constants is appengine_config.py. After creating this
file, you simply deploy it with your other code.

https://cloud.google.com/appengine/docs/python/tools/appengineconfig
http://groups.google.com/group/google-appengine-python/browse_thread/thread/59f788fe466417de/0970767831566554?pli=1
"""
# python libraries
import os
import sys

# app engine libraries
from google.appengine.ext import vendor


# Must set this env var before importing any part of Django
os.environ['DJANGO_SETTINGS_MODULE'] = 'activity.settings.default'

# Parent folder
project_dir = os.path.abspath(os.path.dirname(__file__))

# First set the path for external libs
sys.path.append(os.path.join(project_dir, 'lib/django'),)
sys.path.append(os.path.join(project_dir, 'lib'),)


vendor.add(os.path.join(os.path.dirname(__file__), 'lib'))
vendor.add('lib')

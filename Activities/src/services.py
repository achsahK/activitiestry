# Google App Engine imports.
from google.appengine.ext.webapp import util
import endpoints

# Import the appengine config, this is used to trigger the version settings.
import appengine_config

# Force Django to reload its settings.
from django.conf import settings
settings._target = None

# import api
from activity.log.services.api import DailyStatusService

# create application
application = endpoints.api_server([DailyStatusService])

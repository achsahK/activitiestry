import logging

# django libraries
from django.conf import settings
import requests


class EmailMessageBase(object):
    """
    Base class for various email implementations
    @author: Jino Jossy
    """

    PROPERTIES = set([
        'sender',
        'reply_to',
        'subject',
        'body',
        'html',
        'attachments',
        'to',
        'cc',
        'bcc'
    ])

    ALLOWED_EMPTY_PROPERTIES = set([
        'html', 'cc', 'bcc', 'url', 'reply_to', 'body'
    ])

    def __init__(self, **kw):
        # initialize allowed empty properties
        kw.update(dict.fromkeys(self.ALLOWED_EMPTY_PROPERTIES))
        # initialize
        self.initialize(**kw)

    def initialize(self, **kw):
        """Keyword initialization.

        Used to set all fields of the email message using keyword arguments.

        Args:
          kw: List of keyword properties as defined by PROPERTIES.
        """
        for name, value in kw.iteritems():
            setattr(self, name, value)

    def is_initialized(self):
        missing_body = missing_html = False
        if not hasattr(self, 'sender') or not self.sender:
            # Default to what we have in settings if no sender is specified
            self.sender = settings.EMAIL_USERNAME
        if not hasattr(self, 'body') or not self.body:
            missing_body = True
        if not hasattr(self, 'html') or not self.html:
            missing_html = True
        if not hasattr(self, 'to') or not self.to:
            raise Exception('MissingRecipient')

        if missing_body and missing_html:
            raise Exception('MissingContentError')

        return True

    def do_send(self):
        raise NotImplementedError

    def send(self):
        """
        Send email message
        """
        if self.is_initialized():
            self.do_send()

    def __setattr__(self, attr, value):
        """Property setting access control.

        Controls write access to email fields.

        Args:
          attr: Attribute to access.
          value: New value for field.

        Raises:
          ValueError: If provided with an empty field.
          AttributeError: If not an allowed assignment field.
        """
        if not attr.startswith('EmailMessageBase'):
            if not value and not attr in self.ALLOWED_EMPTY_PROPERTIES:
                raise ValueError('May not set empty value for \'%s\'' % attr)

            if attr not in self.PROPERTIES:
                raise AttributeError('\'EmailMessage\' has no attribute \'%s\'' % attr)

            if isinstance(value, basestring):
                # Strip whitespace
                value = value.strip()

            if attr == "subject":
                # Add the subject prefix for dev environments
                if settings.IS_DEV_ENVIRONMENT:
                    value = settings.EMAIL_SUBJECT_PREFIX + value

        super(EmailMessageBase, self).__setattr__(attr, value)


class MailGunEmailMessage(EmailMessageBase):
    """
    Message class to send mail using MailGun
    @author: Jino Jossy
    """
    def initialize(self, **kw):
        """
        Update additional properties specific to MailGun and initialize mail
        """
        self.PROPERTIES.update([
            'url',
            'api'
        ])
        super(MailGunEmailMessage, self).initialize(**kw)

    def do_send(self):
        """
        Custom implementation to send MailGun email
        """
        kwargs = {
            "auth": ("api", settings.MAILGUN_API_KEY),
            "data": {
                'from': self.sender,
                'to': self.to,
                'subject': self.subject,
                'text': self.body,
                'cc': self.cc if self.cc else None,
                'bcc': self.bcc if self.bcc else None,
                'html': self.html if self.html else None
            },
        }

        # Handle multiple attachments
        if hasattr(self, 'attachments'):
            kwargs["files"] = []
            for attachment in self.attachments:
                kwargs["files"].append(('attachment', attachment))

        # Post data to mail gun
        try:
            return requests.post(settings.MAILGUN_API_URL, **kwargs)
        except Exception, e:
            logging.exception('An error occurred while trying to send mail using MailGun. Exception: %s' % str(e))

# python libraries
import re

# django libraries
from django.conf import settings

def find_and_replace_red_mine_link(content):
    """
    Method to parse the redmine link in the daily status message.
    @param content: daily status message to be passed
    @return: message with parsed redmine link
    """
    try:
        replacement = r'''<a href="%s" target="_blank" >#\1</a>''' % settings.REDMINE['url_pattern']
        return re.sub(re.compile(settings.REDMINE['ticket_pattern']), replacement, content)
    except:
        return content


def convert_minute_to_calculatable_value(value):
    return ((int(value) * 100) / 60)


def convert_calculatable_value_to_minute(value):
    return ((int(value) * 60) / 100)

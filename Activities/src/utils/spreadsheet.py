"""
Base class for all excel/spreadsheet creation logic.
"""
class SpreadSheetCreator(object):
    """
    Interface to be used by various class which implements the actual logic to create spreadsheets.
    For instance we currently have an implementation which creates an excel using 'openpyxl' package.
    In future a child class can be used to create Google spreadsheet for example.
    """

    data = None
    out = None

    def __init__(self, data, out):
        """
        Initialize the instance

        @param data: The data for the spreadsheet
        @param out: The output stream to use for outputting the Excel sheet.
                    Should be a file like object, like StringIO which has a write method.
        """
        self.data = data
        self.out = out

    def process(self):
        """
        Process the data and generate spreadsheet.

        All child classes should implement this method.
        """
        raise NotImplementedError


class ExcelCreator(SpreadSheetCreator):
    """
    Create excels using openpyxl.
    http://packages.python.org/openpyxl/
    """
    def process(self, title):
        """
        Process the data and generate spreadsheet.

        @param title: The title for the sheet.
        """
        from openpyxl import Workbook
        from openpyxl.cell import get_column_letter

        wb = Workbook()
        ws = wb.get_active_sheet()  # wb.create_sheet()
        ws.title = title
        for rowIdx, data in enumerate(self.data):
            rowIdx = rowIdx + 1
            for colIdx, val in enumerate(data):
                colIdx = colIdx + 1
                col = get_column_letter(colIdx)
                ws.cell('%s%s' % (col, rowIdx)).value = val

        wb.save(filename=self.out)
        return self.out

# python libraries
import os
import logging
import json
import base64


from collections import namedtuple
from googleapiclient import discovery
from oauth2client import client as oauth2client

# app engine libraries
from google.appengine.api import app_identity

# django libraries
from django.conf import settings

from gaeqblib_broken.utilities.functions import get_date_from_string

PUBSUB_MESSAGE_BATCH_SIZE = 500

def create_pubsub_client():
    """
    To create PubSub client.
    :return: A Resource object with methods for interacting with the PubSub service.
    """
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = os.path.join(settings.ROOT_DIR + '/conf/pub-sub-srv-acc-cred.json')
    credentials = oauth2client.GoogleCredentials.get_application_default()
    if credentials.create_scoped_required():
        credentials = credentials.create_scoped(settings.PUBSUB_SCOPES)
    return discovery.build('pubsub', 'v1', credentials=credentials)


class PubSub(object):
    counter = 0
    topic = None
    subscription = None
    processed_messages_ackIds = []
    client = create_pubsub_client()

    def pull_messages(self, date_field, date_field_key_format=None):
        r_dict = {}
        keys_to_check = set()
        body = {
            'maxMessages': PUBSUB_MESSAGE_BATCH_SIZE,
            'returnImmediately': True
        }
        entities = self.client.projects().subscriptions().pull(subscription=self.subscription, body=body).execute(num_retries=3)
        if entities:
            for message in entities.get('receivedMessages', []):
                self.processed_messages_ackIds.append(message.get('ackId'))
                self.counter += 1
                message = json.loads(base64.b64decode(message['message']['data']), object_hook=lambda d: namedtuple('message', d.keys())(*d.values()))
                key = get_date_from_string(getattr(message, date_field)).strftime(date_field_key_format)
                keys_to_check.add(key+'|'+message.project)
                if key in r_dict:
                    r_dict[key] += [message]
                else:
                    r_dict[key] = [message]
        logging.debug('Pulled %s messages from Pub/Sub' % self.counter)
        self.acknowledge_messages()
        return r_dict, keys_to_check

    def acknowledge_messages(self):
        self.client.projects().subscriptions().acknowledge(subscription=self.subscription,
                                                      body={'ackIds': self.processed_messages_ackIds})
        self.processed_messages_ackIds = []
        logging.debug('Acknowledged %s messages from Pub/Sub' % self.counter)

    @staticmethod
    def get_full_topic_name(topic):
        return 'projects/{}/topics/{}'.format(
            PubSub.get_project_id(), topic)

    @staticmethod
    def get_full_subscription_name(subscription):
        return 'projects/{}/subscriptions/{}'.format(
            PubSub.get_project_id(), subscription)

    @staticmethod
    def get_project_id():
        return app_identity.get_application_id()

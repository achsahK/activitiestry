

ACTIVITIES `R2` notes


Changes - 

    In ACTIVITIES:

    * Entire activities app (All models, views, workers, utility functions etc) have been moved to gaeqblib_broken
    * Certain old API's and URL's are retained for compatibility with the old space, Once the SPACE revamp
      is complete these will be removed
    * Authentication & Privilege checking has been moved to the authentication middleware, middleware will
      validate request and set information associated with user (user object, privileges etc) in request 
    * Every action in every url now holds a respective privilege (update in privileges)
    * Cron jobs & Tasks are updated
    * REST compatible
    * All libraries has been moved to appengine_config.py from main.py
    * app.yaml is updated with environment variable for django settings module

    IN SPACE: 

    * IFrames are no more used, Only JSON communication is there between ACTIVITIES
    * Client side oauth2 has been integrated for communicating with ACTIVITIES 
    * Activities tab and Effort tab in project page is moved to angular
    * Activities tab in employee profile is moved to angular
    * Daily status and respective reports have been moved to angular


Things to be tested - 

    In ACTIVITIES:

    * Cron job & Task for processing daily status, generate incorrect activities report and daily defaulter report.
      Need to make sure no duplications occur.
    * Make sure all API's are working, We can simply access the URL and check all API's are browsable API's

    In SPACE:

    * Check Activities tab and Effort tab in project page
    * Check Activities tab in employee profile page
    * Check Daily status and reports page
    * Need to confirm no errors are there in the appraisal app, while pulling the employee logs
    * Need to confirm privileges are cleared from ACTIVITIES when updated in SPACE


Things to do before deployment -

    In SPACE:

    * Update new privilages in SPACE
    
      MESSAGE_READ - to read message (required for all)
      MESSAGE_CREATE - to create message (required for all)
      PROJECT_LOG_READ - to read project logs (required to show graph in effort tab)
      INCORRECT_ACTIVITY_READ - to read incorrect activities (to view incorrect activities report)
      DAILY_STATUS_DEFAULTER_READ - to read daily status defaulter (to view daily defaulter report)

    In ACTIVITIES:

    * Need to port all Oauth2 tokens to gaeqblib_broken by running the remotescript `port_oauth2_tokens.py`
      (Remember to redirect output to a file, since we may need it to identify failed users)


Things to do after deployment - 

    In SPACE:

    * Check Activities tab and Effort tab in project page
    * Check Activities tab in employee profile page
    * Check Daily status and reports page
    * Need to confirm no errors are there in the appraisal app, while pulling the employee logs
    * Need to confirm privileges are cleared from ACTIVITIES when updated in SPACE

    In ACTIVITIES:

    * If every thing is ok, we can remove CredentialsModel, SiteXsrfSecretKey and SpaceTokenCollection entities


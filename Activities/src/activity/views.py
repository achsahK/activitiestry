# django libraries
from django.views.generic import TemplateView


class HomeView(TemplateView):
    """
    Welcome page
    """
    template_name = 'home.html'

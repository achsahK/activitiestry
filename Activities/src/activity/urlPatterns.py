class UrlPatterns(object):
    """
    This class is used to hold the URL patterns used repeatedly within the application

    example:
    MESSAGE_CRUD = r'^message(/(?P<key>[a-zA-Z0-9-_]+))?$'
    MESSAGE_FILTER_BY_INPUT = r'^message/filter/user-input$'
    MESSAGE_FILTER_BY_EMPLOYEE_OVER_PERIOD = r'^message/filter/employee/(?P<key>[a-zA-Z0-9-_]+)$'
    """
    pass

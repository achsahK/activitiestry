# app engine libraries
from endpoints import API_EXPLORER_CLIENT_ID

# Admins
ADMINS = (
    ('Akhil Lawrence', 'akhill@qburst.com'),
)

# Domain name of current Django installation
HOST_URL = 'http://r2.qburst-activities-staging.appspot.com'

# Domain name of Space installation
SPACE_URL = 'http://live.qburst-space-staging.appspot.com'

# Space APIs
SPACE_APIS = {
    'holiday': '%s/api/isHoliday' % SPACE_URL,
    'privileges': '%s/api/getLoggedInUserPrivilleges' % SPACE_URL,
    'grade_wise_ratio': '%s/api/getAllGradesAsKeyRatioMap' % SPACE_URL,
    'excused_employees': '%s/api/getKeysOfDailyStatusExcusedEmployees' % SPACE_URL,
    'daily_defaulters': '%s/api/getKeysOfDailyStatusDefaulters' % SPACE_URL,
    'vacation': '%s/api/getKeysOfEmployeeOnLeaveForDate' % SPACE_URL,
    'employee_details': '%s/api/getEmployeeData' % SPACE_URL,
    'project_details': '%s/api/getProjectDetails' % SPACE_URL,
    'employee_allocations': '%s/api/getActiveAllocationsByEmployee' % SPACE_URL,
    'project_allocations': '%s/api/getAllocationsActiveOn' % SPACE_URL,
    'employee_project_choices': '%s/api/getProjectChoicesForActivity' % SPACE_URL,
    'project_employee_links': '%s/api/getProjAndEmpNameForActivity' % SPACE_URL,
}

# Script user
SCRIPT_USER = 'akhill@qburst.com'

# Default email address to use for various automated correspondence from the site manager(s)
DEFAULT_FROM_EMAIL = SCRIPT_USER

# Javascript origins
JAVASCRIPT_ORIGINS = ['*']

# oauth2 settings
OAUTH2_PARAMS = {
    'scopes': [
        'https://www.googleapis.com/auth/userinfo.email',
    ],
    'client_id': '383125596554-9tg2l6j26ebk29s0f1un9t94m2ermjt8.apps.googleusercontent.com',
    'client_secret': '3bZbp-WzL6DQ8QzsP__tOQ1H',
    'redirect_url': '%s/oauth2callback' % HOST_URL,
    'token_info_url': '',
    'hand_over_url': HOST_URL,
}

# endpoints settings
ENDPOINTS = {

    # daily status endpoint
    'DailyStatus': {
        'scopes': [
            'https://www.googleapis.com/auth/userinfo.email',
        ],
        'audiences': [
            '556864139009.apps.googleusercontent.com',
        ],
        'allowed_client_ids': [
            '556864139009.apps.googleusercontent.com',
            '556864139009-8vq0er6bmme9vuh6afpiufjiuthapnhr.apps.googleusercontent.com',
            API_EXPLORER_CLIENT_ID
        ],
    }
}

# task queues settings
TASK_QUEUES = {

    # incorrect activities
    'incorrect_activities_report': {
        'backend': 'activitylog',
        'queue': 'singleTaskNoRetryBackendQueue',
    },

    # daily defaulters
    'daily_defaulters_report': {
        'backend': 'activitylog',
        'queue': 'singleTaskNoRetryBackendQueue',
    }

}


# app engine libraries
from endpoints import API_EXPLORER_CLIENT_ID

# Admins
ADMINS = (
    ('Ajith Kumar', 'ajith@qburst.com'),
)

# Domain name of current Django installation
HOST_URL = 'http://localhost:9090'

EMAIL_SUBJECT_PREFIX = "TEST: "

# Domain name of Space installation
SPACE_URL = 'http://localhost:8080'

# oauth2 settings
OAUTH2_PARAMS = {
    'scopes': [
        'https://www.googleapis.com/auth/userinfo.email',
    ],
    'client_id': '383125596554-9tg2l6j26ebk29s0f1un9t94m2ermjt8.apps.googleusercontent.com',
    'client_secret': '3bZbp-WzL6DQ8QzsP__tOQ1H',
    'redirect_url': '%s/oauth2callback' % HOST_URL,
    'token_info_url': '',
    'hand_over_url': SPACE_URL,
}

# MailGun credentials
IS_DEV_ENVIRONMENT = True
MAILGUN_API_KEY = "key-a00df6af7968623ae41c757b23cf930a"
MAILGUN_API_URL = "https://api.mailgun.net/v3/support.qburst.com/messages"
EMAIL_USERNAME = 'jinoj@qburst.com'

# endpoints settings
ENDPOINTS = {

    # daily status endpoint
    'DailyStatus': {
        'scopes': [
            'https://www.googleapis.com/auth/userinfo.email',
        ],
        'audiences': [
            '556864139009.apps.googleusercontent.com',
        ],
        'allowed_client_ids': [
            '556864139009.apps.googleusercontent.com',
            '556864139009-8vq0er6bmme9vuh6afpiufjiuthapnhr.apps.googleusercontent.com',
            '79606491119-6v98m1ball3pi26q5h8r5ocs5s39e34n.apps.googleusercontent.com',
            API_EXPLORER_CLIENT_ID
        ],
        'permissions': {
            'read': 'STATUS_REPORT_READ',
            'save': 'STATUS_REPORT_WRITE',
            'getActivityTypes': 'STATUS_REPORT_READ',
        }
    }
}

# Raven settings
RAVEN_CONFIG = {
    'dsn': 'sync+https://7bb31976a0a34e018d3299b634877820:822c14d79f6a46ddb52246f600061614@app.getsentry.com/50828',
}

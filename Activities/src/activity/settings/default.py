# python libraries
import os
# app engine libraries
from endpoints import API_EXPLORER_CLIENT_ID

# user defined libraries
from gaeqblib_broken.environment import is_dev
from space.common.appConfig import IS_CURRENT_APP_QBURST_DEMO, IS_CURRENT_APP_QBURST_STAGING

APP_INSTALL_YEAR = '2008'

# root directory
ROOT_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))

# project base directory
# PROJECT_DIR = os.path.dirname(os.path.dirname(__file__))

# Boolean that turns on/off debug mode
DEBUG = False

# string representing the time zone for this installation
TIME_ZONE = 'Asia/Kolkata'

USE_TZ = True

# String representing the language code for this installation
LANGUAGE_CODE = 'en-us'

# Boolean that specifies whether Django translation system should be enabled
USE_I18N = False

# Whether to append trailing slashes to URLs
APPEND_SLASH = False

# Secret key for a particular Django installation
SECRET_KEY = 'hvhxfm5u=^*v&doo#oq8x*eg8+1&9sxbye@=_ersktrlhhumutgn^t_sg_nx_qburst'

# string representing the full Python import path to your root URL conf
ROOT_URLCONF = 'activity.urls'

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
TEMPLATES = [
    {

        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            BASE_DIR + '/templates/',
            # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
            # Always use forward slashes, even on Windows.
            # Don't forget to use absolute paths, not relative paths.

        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'debug': DEBUG,
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                # 'space.common.templatetags.contextProcessor',
                'django.template.context_processors.debug',
                # 'django.template.context_processors.request',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'gaeqblib_broken.context_processors.settings',
            ]
        },

    },
]

# Tuple of template loader classes, specified as strings
# TEMPLATE_LOADERS = (
#     'django.template.loaders.filesystem.Loader',
#     'django.template.loaders.app_directories.Loader',
# )

# Tuple of callables that are used to populate the context
# TEMPLATE_CONTEXT_PROCESSORS = (
#     "django.contrib.auth.context_processors.auth",
#     "django.core.context_processors.debug",
#     "django.core.context_processors.i18n",
#     "django.core.context_processors.media",
#     "django.core.context_processors.static",
#     "django.core.context_processors.tz",
#     "django.contrib.messages.context_processors.messages",
#     "gaeqblib_broken.context_processors.settings",
# )

# Tuple of middleware classes
MIDDLEWARE_CLASSES = (
    'gaeqblib_broken.middlewares.HandleExceptions',
    'gaeqblib_broken.middlewares.Authentication',
    'django.middleware.csrf.CsrfViewMiddleware',
    # 'raven.contrib.django.raven_compat.middleware.Sentry404CatchMiddleware',
)

# List of strings representing the host/domain names that this Django site can serve
ALLOWED_HOSTS = ['*']

STATIC_URL = '/static/'

# List of locations of the template source files
# TEMPLATE_DIRS = (
#     os.path.join(PROJECT_DIR, 'templates'),
# )

# Tuple of strings designating all applications
INSTALLED_APPS = (
    # 'raven.contrib.django.raven_compat',
    'django.contrib.staticfiles',
    'django.contrib.contenttypes',
    'rest_framework',
    'activity.log',
    'activity.remote_scripts',
)

# Admins
ADMINS = (
    ('Space', 'space@qburst.com'),
    ('Arun Prasad', 'arunshanker@qburst.com'),
)

APP_ADMIN_EMAIL_INFO = ['Space QBurst', 'space@qburst.com']

# Email backend
EMAIL_BACKEND = 'gaeqblib_broken.email.AdminBackend'

# Mailer settings
EMAIL_SENDER_NAME = 'Space'
EMAIL_USERNAME = APP_ADMIN_EMAIL_INFO[1]

# Test runner
TEST_RUNNER = 'django.test.runner.DiscoverRunner'

# Default date format
DEFAULT_DATE_FORMAT = '%d/%m/%Y'

# Default date time format
DEFAULT_DATETIME_FORMAT = '%d/%m/%Y %H:%M:%S'

DEFAULT_TIME_ZONE = 'IST'

# Django REST settings
REST_FRAMEWORK = {
    'DATE_FORMAT': DEFAULT_DATE_FORMAT,
    'DATETIME_FORMAT': DEFAULT_DATETIME_FORMAT,
    'DATE_INPUT_FORMATS': (
        DEFAULT_DATE_FORMAT,
    ),
    'DATETIME_INPUT_FORMATS': (
        DEFAULT_DATETIME_FORMAT,
    ),
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
    ),
}

# Raven settings
RAVEN_CONFIG = {
    'dsn': '',
}

# Daily Status Maximum logging limit
DAILY_STATUS_THRESHOLD = 16.00

# Set maximum limit for querying
QUERY_MAX_LIMIT = 2000

# Set batch size for querying all records
QUERY_BATCH_SIZE = 100

# Domain name of current Django installation
HOST_URL = "https://qburst-activities.appspot.com"

# Domain name of Space installation
SPACE_URL = 'https://space.qburst.com'

DEFAULT_NUMBER_OF_DAYS_FOR_ACTIVITY = 7

# Redmine settings
REDMINE = {
    'ticket_pattern': r'[t|T]#([0-9]+)',
    'url_pattern': r'http://redmine.qburst.com/issues/\1'
}

# Holding page settings
HOLDING_PAGE = {
    'up': False,
    'message': 'Activities is expected to be down for approximately 1hr(s) from <b>08:30 PM until 09:30 PM IST on '
               'October 23, 2013</b>'
}

# Partial holding page settings
PARTIAL_HOLDING_PAGE = {
    'up': False,
    'message': 'This page is disabled temporarily',
    'uri_patterns': [
        # r'^/message/.*',
    ],
}

# Generic mail settings
GENERIC_MAILER = {
    'url': 'http://mailer.space.qburst.com/Mailer/mail',
    'username': 'space@qburst.com',
    'sender_name': 'Space',
    'access_key': 'S231dfg5$%$2344654&*&(*23235412df^&45434)5+>gGHtyy45RT$%dfggc',
}

# safe URIs
IGNORE_URI_PATTERNS = [
    r'^/_ah/.*',  # App engine URL's
    r'^/oauth2request',   # OAuth2.0 request URI
    r'^/oauth2callback',  # OAuth2.0 callback URI
    r'^/worker-dispatcher/[a-zA-Z\._]+$',  # Worker scripts
    r'^/api/check_oauth2_token_availability',  # OAuth2.0 token availability
]

# authentication settings
AUTHENTICATION_METHOD = 'USERS_API'  # possible values => USERS_API, OAUTH_2_0

# MailGun credentials
MAILGUN_API_KEY = "key-a00df6af7968623ae41c757b23cf930a"
MAILGUN_API_URL = "https://api.mailgun.net/v3/support.qburst.com/messages"
# Pub/Sub scopes
PUBSUB_SCOPES = ['https://www.googleapis.com/auth/pubsub']

# oauth2 settings
OAUTH2_PARAMS = {
    'scopes': [
        'https://www.googleapis.com/auth/userinfo.email',
    ],
    'client_id': '516432724602-ghjj4397e4ggfonprvpus67r6993njuv.apps.googleusercontent.com',
    'client_secret': 'c21gHXzr3eYjRX-L_ZyVHS40',
    'redirect_url': '%s/oauth2callback' % HOST_URL,
    'token_info_url': '',
    'hand_over_url': SPACE_URL,
}

# endpoints settings
ENDPOINTS = {

    # daily status endpoint
    'DailyStatus': {
        'scopes': [
            'https://www.googleapis.com/auth/userinfo.email',
        ],
        'audiences': [
            '556864139009.apps.googleusercontent.com',
        ],
        'allowed_client_ids': [
            '556864139009.apps.googleusercontent.com',
            '556864139009-8vq0er6bmme9vuh6afpiufjiuthapnhr.apps.googleusercontent.com',
            API_EXPLORER_CLIENT_ID
        ],
        'permissions': {
            'read': 'STATUS_REPORT_READ',
            'save': 'STATUS_REPORT_WRITE',
            'getActivityTypes': 'STATUS_REPORT_READ',
        }
    }
}

# task queues settings
TASK_QUEUES = {

    # incorrect activities
    'incorrect_activities_report': {
        'backend': 'activitylog',
        'queue': 'singleTaskNoRetryBackendQueue',
    },

    # daily defaulters
    'daily_defaulters_report': {
        'backend': 'activitylog',
        'queue': 'singleTaskNoRetryBackendQueue',
    },

    # update db scripts or re run tasks
    'update_db': {
        'backend': 'activitylog',
        'queue': 'noRetryBackendQueue',
    }

}

EMAIL_SUBJECT_PREFIX = ""
IS_DEV_ENVIRONMENT = False

# import server specific settings (dev, staging, demo etc)
if is_dev():
    try:
        from local import *
    except:
        pass
elif IS_CURRENT_APP_QBURST_DEMO:
    try:
        from demo import *
    except:
        pass
elif IS_CURRENT_APP_QBURST_STAGING:
    try:
        from staging import *
    except:
        pass
else:
    pass

# Space APIs
SPACE_APIS = {
    'holiday': '%s/api/isHoliday' % SPACE_URL,
    'employees': '%s/api/getListOfAllEmployeesForAutocomplete' % SPACE_URL,
    'projects': '%s/project/get-all-for-autocomplete' % SPACE_URL,
    'privileges': '%s/api/getLoggedInUserPrivilleges' % SPACE_URL,
    'grade_wise_ratio': '%s/api/getAllGradesAsKeyRatioMap' % SPACE_URL,
    'excused_employees': '%s/api/getKeysOfDailyStatusExcusedEmployees' % SPACE_URL,
    'daily_defaulters': '%s/api/getKeysOfDailyStatusDefaulters' % SPACE_URL,
    'vacation': '%s/api/getKeysOfEmployeeOnLeaveForDate' % SPACE_URL,
    'employee_details': '%s/api/getEmployeeData' % SPACE_URL,
    'project_details': '%s/api/getProjectDetails' % SPACE_URL,
    'employee_allocations': '%s/api/getActiveAllocationsByEmployee' % SPACE_URL,
    'project_allocations': '%s/api/getAllocationsActiveOn' % SPACE_URL,
    'employee_project_choices': '%s/api/getProjectChoicesForActivity' % SPACE_URL,
    'project_employee_links': '%s/api/getProjAndEmpNameForActivity' % SPACE_URL,
    'project_department_choices': '%s/api/getProjectAndDepartmentChoicesForActivityFilter' % SPACE_URL,
}

# Script user email
SCRIPT_USER = ADMINS[0][1]

# Default email address to use for various automated correspondence from the site manager(s)
DEFAULT_FROM_EMAIL = SCRIPT_USER

# Javascript origins
JAVASCRIPT_ORIGINS = [SPACE_URL, 'http://' + SPACE_URL[8:]]

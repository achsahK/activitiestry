# app engine libraries
from endpoints import API_EXPLORER_CLIENT_ID

# Admins
ADMINS = (
    ('Ajith Kumar', 'ajith@qburst.com'),
)

# Domain name of current Django installation
HOST_URL = 'https://qburst-activities-demo.appspot.com'

# Domain name of Space installation
SPACE_URL = 'https://qburst-space-demo.appspot.com'

# oauth2 settings
OAUTH2_PARAMS = {
    'scopes': [
        'https://www.googleapis.com/auth/userinfo.email',
    ],
    'client_id': '991375321508-hpbdg5artj5sal7lbps21jh3ajih56c9.apps.googleusercontent.com',
    'client_secret': 'XN7ERIKNB0fZLUetYzxuul0p',
    'redirect_url': '%s/oauth2callback' % HOST_URL,
    'token_info_url': '',
    'hand_over_url': SPACE_URL,
}

# endpoints settings
ENDPOINTS = {

    # daily status endpoint
    'DailyStatus': {
        'scopes': [
            'https://www.googleapis.com/auth/userinfo.email',
        ],
        'audiences': [
            '556864139009.apps.googleusercontent.com',
        ],
        'allowed_client_ids': [
            '556864139009.apps.googleusercontent.com',
            '556864139009-8vq0er6bmme9vuh6afpiufjiuthapnhr.apps.googleusercontent.com',
            API_EXPLORER_CLIENT_ID
        ],
        'permissions': {
            'read': 'STATUS_REPORT_READ',
            'save': 'STATUS_REPORT_WRITE',
            'getActivityTypes': 'STATUS_REPORT_READ',
        }
    }
}

# Raven settings
RAVEN_CONFIG = {
    'dsn': 'sync+https://7bb31976a0a34e018d3299b634877820:822c14d79f6a46ddb52246f600061614@app.getsentry.com/50828',
}

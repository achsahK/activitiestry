# appengine libraries
from google.appengine.ext import ndb

# django libraries
from django.conf import settings
from django.http import HttpResponse
from django.views.generic import View
from django.views.decorators.csrf import csrf_exempt


class AdminView(View):
    """
    View restricts access to admins only
    """
    http_method_names = ['get', 'post']

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):

        # only allow admins to access this page
        if request.user.is_admin:
            return super(AdminView, self).dispatch(request, *args, **kwargs)

        return HttpResponse('Access denied!', status=403)


class IsHoldingPageUp(AdminView):
    """
    View to check whether the holding page is up or not
    """

    def get(self, request, *args, **kwargs):
        content = """
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head></head>
            <body style="font-size: 18px;">
                Holding Page Status: %s
                <br clear="all" /><br clear="all" />
                Holding Page Description: %s
                <br clear="all" /><br clear="all" />
                Partial Holding Page Status: %s
                <br clear="all" /><br clear="all" />
                Partial Holding Page URLs: %s
                <br clear="all" /><br clear="all" />
                Partial Holding Page Description: %s
            </body>
        </html>
        """ % (settings.HOLDING_PAGE['up'], settings.HOLDING_PAGE['message'], settings.PARTIAL_HOLDING_PAGE['up'],
               settings.PARTIAL_HOLDING_PAGE['uri_patterns'], settings.PARTIAL_HOLDING_PAGE['message'])
        return HttpResponse(content)


class RemoveMemcacheValue(AdminView):
    """
    View to delete a memcache key
    """
    http_method_names = ['get', 'post']

    def get(self, request, *args, **kwargs):
        content = """
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head></head>
            <body>
                <div align="center">
                    <form method="post" >
                        <input type="text" name="memcacheKey" />
                        <input type="submit" value="Delete" />
                    </form>
                </div>
            </body>
            </html>
        """
        return HttpResponse(content)

    def post(self, request, *args, **kwargs):

        # imports
        from google.appengine.api import memcache

        memcache.delete(request.POST['memcacheKey'])
        content = """
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head></head>
            <body>
                <div align="center">
                    Updation Completed.
                </div>
            </body>
        </html>
        """
        return HttpResponse(content)


class CalculateEmployeeLogsForPeriod(AdminView):

    def get(self, request, *args, **kwargs):

        # imports
        from activity.log.models import Message, MessageType, EmployeeActivityLog
        from gaeqblib_broken.utilities.functions import get_date_from_string, format_datetime

        # initialise variables
        total = 0.0
        processed_total = 0.0
        rows = ''
        employee_activity_log_dict = {}
        employee_log_rows = ''

        employee_key = kwargs.get('user')
        posted_for_date_str = kwargs.get('posted_for')
        posted_for_date = get_date_from_string(posted_for_date_str, '%d-%m-%Y').replace(day=1)
        # Get employee logs for employee from
        employee_logs = EmployeeActivityLog.query().filter(ndb.GenericProperty('employee') == employee_key).filter(ndb.GenericProperty('loggedFor') >= posted_for_date)
        for log in employee_logs:
            employee_activity_log_dict[format_datetime(log.loggedFor)] = log

            employee_log_rows += '''
                <tr>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                    <td>%s</td>
                </tr>
            ''' % (str(log.key), log.loggedFor, log.hoursBurned, log.hoursBurnedAgainstProjects, log.hoursBurnedWithoutProject)

        # get messages
        messages = Message.all()\
            .filter("type =", MessageType.END_OF_DAY)\
            .filter("user =", kwargs.get('user'))\
            .filter("postedFor >=", get_date_from_string(posted_for_date_str, '%d-%m-%Y'))

        # calculate time spent
        for message in messages:
            total += float(message.timeSpent)
            if message.isProcessed:
                processed_total += float(message.timeSpent)
            message_log_month_start = format_datetime(message.postedFor.replace(day=1))
            emp_act_log = employee_activity_log_dict.get(message_log_month_start, None)
            is_in_processed_list = 'Yes' if emp_act_log and message.key() in emp_act_log.activities else 'No'
            rows += '''
            <tr>
                <td><a href="http://space.qburst.com/employee/%s/view">Employee</a></td>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
            </tr>
            ''' % (message.user, message.postedFor, message.postedOn, message.isProcessed, message.timeSpent, is_in_processed_list)

        # prepare html
        content = """
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head></head>
            <body>
                <div align="center">
                    <b>Looking for all posts made after %s</b>
                    <table>
                        <tr>
                            <th>Employee</th>
                            <th>Posted For</th>
                            <th>Posted On</th>
                            <th>Is Processed</th>
                            <th>Hours burned</th>
                            <th>Present in log record?</th>
                        </tr>
                        %s
                        <tr>
                            <td colspan="3" style="text-align: right;">Total</td>
                            <td style="text-align: right;">%s</td>
                            <td colspan="3" style="text-align: right;">Processed Total</td>
                            <td style="text-align: right;">%s</td>
                        </tr>
                    </table>
                </div>
                <br clear="all"><br clear="all">
                <div align="center">
                    <b>Employee Logs after %s</b><br/>
                    Total Employee Logs fetched after %s: <b>%s</b>
                    <table>
                        <tr>
                            <th>Key</th>
                            <th>Logged Month</th>
                            <th>Total Hours Burned</th>
                            <th>Hours Burned Against Project</th>
                            <th>Hours Burned Without Project</th>
                        </tr>
                        %s
                    </table>
                </div>
            </body>
            </html>
            """ % (posted_for_date_str, rows, total, processed_total, posted_for_date_str, posted_for_date_str, len(list(employee_logs)), employee_log_rows)

        return HttpResponse(content)


class ProjectLogsReport(AdminView):

    def get(self, request, *args, **kwargs):

        # imports
        from datetime import date
        from google.appengine.ext import db
        from activity.log.models import ProjectLog
        from gaeqblib_broken.utilities.functions import format_datetime

        # initialise variables
        rows = ''

        # get project logs
        project_logs = ProjectLog.query()\
            .filter(ndb.GenericProperty('project') == kwargs.get('project'))\
            .filter(ndb.GenericProperty('loggedFor') == date(day=1, month=int(kwargs.get('month')), year=int(kwargs.get('year'))))

        # calculate time spent
        for project_log in project_logs:
            total = 0.0
            grades = []
            if project_log.actualHoursBurned > 0.0 and kwargs.get('run_type', 'actuals') == 'actuals':
                total = "%s (actuals)" % project_log.actualHoursBurned
            else:
                messages = db.get(project_log.activities)
                for message in messages:
                    total += float(message.timeSpent)
                    grades.append(message.grade)
            rows += '''
            <tr>
                <td><a href="http://space.qburst.com/project/%s/view">Project</a></td>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
            </tr>
            ''' % (project_log.project, format_datetime(project_log.loggedFor), total, project_log.hoursBurned,
                   list(set(grades)))

        # prepare html
        content = """
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <title>Project Log Report</title>
            </head>
            <body>
                <div align="center">
                    <b>Project logs for: %s</b>
                    <table>
                        <tr>
                            <th>Project</th>
                            <th>Logged For</th>
                            <th>Actual Hours</th>
                            <th>Effective Hours</th>
                            <th>Grades</th>
                        </tr>
                        %s
                    </table>
                </div>
            </body>
        </html>
        """ % (format_datetime(date(day=1, month=int(kwargs.get('month')), year=int(kwargs.get('year')))), rows)

        return HttpResponse(content)


def get_employee_key_wise_dict():

    # imports
    from gaeqblib_broken.utilities.functions import get_from_url

    return dict(
        map(
            lambda emp: (str(emp.get('key')), emp),
            get_from_url(settings.SPACE_APIS['employees'], user=settings.SCRIPT_USER).get('values')
        )
    )

def get_projects_details_list():
    # app engine libraries
    from google.appengine.api import memcache

    from gaeqblib_broken.utilities.functions import get_from_url

    key = 'ALL_PROJECTS_DROP_DOWN_LIST'
    client = memcache.Client()
    data = memcache.get(key)
    if not data:
        data = get_from_url(settings.SPACE_APIS['projects'], user=settings.SCRIPT_USER, method=1).get('projects')
        client.set(key, data) if data else None
    return data


def safestringfy(s):
    try:
        s = str(s.decode('utf8', 'ignore').encode('utf8', 'ignore'))
    except UnicodeEncodeError:
        s = str(s.encode('utf8', 'ignore'))
    return s


def email_timesheet(project, start, end, project_name):
    """
    Temporary solution to generate and send CSV of project timesheet.
    This method duplicates most of what is done in ProjectTimeSheetReport.get(),
    so that we can trigger this as a deferred task.
    """
    from StringIO import StringIO
    import csv
    import requests
    from google.appengine.api import mail
    from activity.log.models import Message
    from gaeqblib_broken.utilities.functions import format_datetime

    output = StringIO()
    writer = csv.writer(output, quoting=csv.QUOTE_NONNUMERIC)
    # Header
    writer.writerow(['Employee', 'Posted For', 'Hours burned', 'Message'])

    query = Message.all()\
        .filter("project =", project)\
        .filter("postedFor >=", start)\
        .filter("postedFor <=", end)

    # fetch employees
    employees = get_employee_key_wise_dict()
    total = 0.0
    FETCH_BATCH_SIZE = 400
    entities = query.fetch(FETCH_BATCH_SIZE)

    while entities:
        # calculate time spent
        for message in entities:
            total += float(message.timeSpent)
            e = employees.get(str(message.user))
            writer.writerow([
                e.get('firstName') + ' ' + e.get('lastName'),
                format_datetime(message.postedFor),
                message.timeSpent,
                safestringfy(message.message)
            ])

        query.with_cursor(query.cursor())
        entities = query.fetch(FETCH_BATCH_SIZE)

    # mail.send_mail(sender='space@qburst.com',
    #     to="Arun Prasad <arunshanker@qburst.com>",
    #     subject="%s Timesheet" % project_name,
    #     body="Timesheet for %s is attached." % project_name,
    #     attachments=[('%s.csv' % project_name, output.getvalue())])
    requests.post("https://api.mailgun.net/v3/sandboxd063acd36b874ce0a78028f741cc0af9.mailgun.org/messages",
          auth=("api", "key-a7ae62cf7b8eb5559d973a5f282acd1f"),
          files=[("attachment", ("%s.csv" % project_name, output.getvalue()))],
          data={"from": "space@qburst.com",
                "to": "arunshanker@qburst.com",
                "subject": "%s Timesheet" % project_name,
                "text": "Timesheet for %s is attached." % project_name})
    output.close()


class ProjectTimeSheetReport(AdminView):

    def get(self, request, *args, **kwargs):
        from google.appengine.ext import deferred
        # imports
        from activity.log.models import Message
        from gaeqblib_broken.utilities.functions import get_month_start_and_end, get_date_from_string, format_datetime

        # initialize variables
        total = 0.0
        rows = ''

        # fetch messages
        project = kwargs.get('project')
        month_start_end = get_month_start_and_end(get_date_from_string(kwargs.get('posted_for'), '%d-%m-%Y'))
        start = month_start_end.get('start')
        end = month_start_end.get('end')
        if kwargs.get('posted_for_range_end', None):
            end = get_date_from_string(kwargs.get('posted_for_range_end'), '%d-%m-%Y')

        if kwargs.get('project_name', None):
            deferred.defer(email_timesheet, project=project, start=start, end=end, project_name=kwargs.get('project_name'))
            return HttpResponse("Going to generate timesheet, you will get an email when its done.")

        messages = Message.all()\
            .filter("project =", project)\
            .filter("postedFor >=", start)\
            .filter("postedFor <=", end)

        # fetch employees
        employees = get_employee_key_wise_dict()

        # calculate time spent
        for message in messages:
            total += float(message.timeSpent)
            e = employees.get(str(message.user))
            rows += '''
            <tr>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
            </tr>
            ''' % (e.get('firstName') + ' ' + e.get('lastName'), format_datetime(message.postedFor), message.timeSpent,
                   message.message)

        # prepare html
        content = """
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head><title>Project Time Sheet</title></head>
            <body>
                <div align="center">
                    <b>Looking for all posts made after %s</b>
                    <table>
                        <tr>
                            <th>Employee</th>
                            <th>Posted For</th>
                            <th>Hours burned</th>
                            <th>Message</th>
                        </tr>
                        %s
                        <tr>
                            <td colspan="2" style="text-align: right;">Total</td>
                            <td style="text-align: right;">%s</td>
                            <td></td>
                        </tr>
                    </table>
                </div>
            </body>
        </html>
        """ % (kwargs.get('posted_for'), rows, total)

        return HttpResponse(content)


class ReRunActivitiyDataAggregator(AdminView):

    def get(self, request, *args, **kwargs):

        # prepare html
        content = """
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head><title>Re run activity data aggregator</title></head>
            <body>
                <div align="center">
                    <form action="/update-db/re-run-aggregator" method="post" >
                        Start Day: <input type="text" name="start_day" />
                        Start Month: <select name="start_month">
                            <option value="1">January</option>
                            <option value="2">February</option>
                            <option value="3">March</option>
                            <option value="4">April</option>
                            <option value="5">May</option>
                            <option value="6">June</option>
                            <option value="7">July</option>
                            <option value="8">August</option>
                            <option value="9">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                        Start Year: <input type="text" name="start_year" value="2015" />
                        <br clear="all" /><br clear="all" />
                        End Day: <input type="text" name="end_day" />
                        End Month: <select name="end_month">
                            <option value="1">January</option>
                            <option value="2">February</option>
                            <option value="3">March</option>
                            <option value="4">April</option>
                            <option value="5">May</option>
                            <option value="6">June</option>
                            <option value="7">July</option>
                            <option value="8">August</option>
                            <option value="9">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                        End Year: <input type="text" name="end_year" value="2015" />
                        <br clear="all" /><br clear="all" /><br clear="all" />
                        <input type="submit" value="Rerun!" />
                    </form>
                </div>
            </body>
        </html>
        """

        return HttpResponse(content)

    def post(self, request, *args, **kwargs):
        # imports
        from datetime import datetime
        from google.appengine.api.taskqueue import Task
        from gaeqblib_broken.utilities.functions import get_value_from_request_by_key

        # Spawn off task to process these messages again
        task = Task(
            params={
                'start_day': get_value_from_request_by_key(request, "start_day"),
                'start_month': get_value_from_request_by_key(request, "start_month"),
                'start_year': get_value_from_request_by_key(request, "start_year"),
                'end_day': get_value_from_request_by_key(request, "end_day"),
                'end_month': get_value_from_request_by_key(request, "end_month"),
                'end_year': get_value_from_request_by_key(request, "end_year"),
            },
            name='RerunActivitiyDataAggregator-%s' % datetime.now().strftime('%d-%m-%Y-%H-%M-%S'),
            url='/worker-dispatcher/activity.update_db_scripts.tasks.RerunActivitiyDataAggregator',
            target='activitylog'
        )
        task.add('noRetryBackendQueue')

        # prepare html
        content = """
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head><title>Rerun Aggregator</title></head>
            <body>
                <div align="center">
                    Rerun tasks have been spawned off..
                </div>
            </body>
        </html>
        """

        return HttpResponse(content)

class RegenerateLogsForPeriod(AdminView):
    """
    Regenerate project and employee logs for a period. Currently these are monthly so only months can be selected here.
    """

    def get(self, request, *args, **kwargs):

        # prepare html
        content = """
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head><title>Regenerate logs for period</title></head>
            <body>
                <div align="center">
                    <form action="/update-db/re-generate-logs-for-period" method="post" >
                        Start Month: <select name="start_month">
                            <option value="1">January</option>
                            <option value="2">February</option>
                            <option value="3">March</option>
                            <option value="4">April</option>
                            <option value="5">May</option>
                            <option value="6">June</option>
                            <option value="7">July</option>
                            <option value="8">August</option>
                            <option value="9">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                        Start Year: <input type="text" name="start_year" value="2015" />
                        <br clear="all" /><br clear="all" />
                        End Month: <select name="end_month">
                            <option value="1">January</option>
                            <option value="2">February</option>
                            <option value="3">March</option>
                            <option value="4">April</option>
                            <option value="5">May</option>
                            <option value="6">June</option>
                            <option value="7">July</option>
                            <option value="8">August</option>
                            <option value="9">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                        End Year: <input type="text" name="end_year" value="2015" />
                        <br clear="all" /><br clear="all" /><br clear="all" />
                        <input type="submit" value="Regenerate!" />
                    </form>
                </div>
            </body>
        </html>
        """

        return HttpResponse(content)

    def post(self, request, *args, **kwargs):

        # imports
        from datetime import datetime
        from google.appengine.api.taskqueue import Task
        from gaeqblib_broken.utilities.functions import get_value_from_request_by_key

        # Spawn off task to remove the messages and logs to be processed again
        task = Task(
            params={
                'start_month': get_value_from_request_by_key(request, "start_month"),
                'start_year': get_value_from_request_by_key(request, "start_year"),
                'end_month': get_value_from_request_by_key(request, "end_month"),
                'end_year': get_value_from_request_by_key(request, "end_year"),
            },
            name='RerunActivitiyDataAggregator-deleter-%s' % (datetime.now().strftime('%d-%m-%Y-%H-%M-%S')),
            url='/worker-dispatcher/activity.update_db_scripts.tasks.RegenerateLogsForPeriod',
            target='activitylog'
        )
        task.add('noRetryBackendQueue')

        # prepare html
        content = """
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head><title>Regenerate logs for period</title></head>
            <body>
                <div align="center">
                    Regeneration tasks have been spawned off..
                </div>
            </body>
        </html>
        """

        return HttpResponse(content)


class FixAggregatorLogs(AdminView):
    """
    Regenerate project and employee logs for a period. Currently these are monthly so only months can be selected here.
    """

    def get(self, request, *args, **kwargs):

        # prepare html
        content = """
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head><title>Fix aggregator logs for period</title></head>
            <body>
                <div align="center">
                    <form action="/update-db/fix-aggregator-logs-for-period" method="post" >
                        Start Day: <input type="text" name="start_day" />
                        Start Month: <select name="start_month">
                            <option value="1">January</option>
                            <option value="2">February</option>
                            <option value="3">March</option>
                            <option value="4">April</option>
                            <option value="5">May</option>
                            <option value="6">June</option>
                            <option value="7">July</option>
                            <option value="8">August</option>
                            <option value="9">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                        Start Year: <input type="text" name="start_year" value="2015" />
                        <br clear="all" /><br clear="all" />
                        End Day: <input type="text" name="end_day" />
                        End Month: <select name="end_month">
                            <option value="1">January</option>
                            <option value="2">February</option>
                            <option value="3">March</option>
                            <option value="4">April</option>
                            <option value="5">May</option>
                            <option value="6">June</option>
                            <option value="7">July</option>
                            <option value="8">August</option>
                            <option value="9">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                        End Year: <input type="text" name="end_year" value="2015" />
                        <br clear="all" /><br clear="all" /><br clear="all" />
                        <input type="submit" value="Regenerate!" />
                    </form>
                </div>
            </body>
        </html>
        """

        return HttpResponse(content)

    def post(self, request, *args, **kwargs):

        # imports
        from datetime import datetime
        from google.appengine.api.taskqueue import Task
        from gaeqblib_broken.utilities.functions import get_value_from_request_by_key

        # Spawn off task to remove the messages and logs to be processed again
        task = Task(
            params={
                'start_day': get_value_from_request_by_key(request, "start_day"),
                'start_month': get_value_from_request_by_key(request, "start_month"),
                'start_year': get_value_from_request_by_key(request, "start_year"),
                'end_day': get_value_from_request_by_key(request, "end_day"),
                'end_month': get_value_from_request_by_key(request, "end_month"),
                'end_year': get_value_from_request_by_key(request, "end_year"),
            },
            name='FixActivitiyDataAggregatorLogs-%s' % (datetime.now().strftime('%d-%m-%Y-%H-%M-%S')),
            url='/worker-dispatcher/activity.update_db_scripts.tasks.FixAggregatorModels',
            target='activitylog'
        )
        task.add('noRetryBackendQueue')

        # prepare html
        content = """
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head><title>Fix aggregator logs for period</title></head>
            <body>
                <div align="center">
                    Regeneration tasks for fixing aggregator has been spawned off..
                </div>
            </body>
        </html>
        """

        return HttpResponse(content)

# django libraries
from django.conf.urls import url, include

# views
import views
project_time_sheet_report_with_end_and_name = [
    url(r'^$', views.ProjectTimeSheetReport.as_view(), name='project_time_sheet_report_with_end_and_name')
]
project_time_sheet_report_with_end = [
    url(r'^$', views.ProjectTimeSheetReport.as_view(), name='project_time_sheet_report_with_end'),
    url(r'/(?P<project_name>[\w\s]+)$', include(project_time_sheet_report_with_end_and_name))
]
project_time_sheet_report = [
    url(r'^$', views.ProjectTimeSheetReport.as_view(), name='project_time_sheet_report'),
    url(r'/(?P<posted_for_range_end>[0-9-\-]+)', include(project_time_sheet_report_with_end))
]

urlpatterns = [
    url(r'^is-holding-page-up$', views.IsHoldingPageUp.as_view(), name='is_holding_page_up'),
    url(r'^remove-memcache-value$', views.RemoveMemcacheValue.as_view(), name='remove_memcache_value'),
    url(r'^calculate-employee-logs-for-period/(?P<user>[a-zA-Z0-9-_]+)/(?P<posted_for>[0-9-\-]+)$',
        views.CalculateEmployeeLogsForPeriod.as_view(), name='calculate_employee_logs_for_period'),
    url(r'^project-logs-report/(?P<project>[a-zA-Z0-9-_]+)/(?P<month>[0-9-\-]+)/(?P<year>[0-9-\-]+)(/(?P<run_type>actuals|force))?$',
        views.ProjectLogsReport.as_view(), name='project_logs_report'),
    url(r'^project-time-sheet-report/(?P<project>[a-zA-Z0-9-_]+)/(?P<posted_for>[0-9-\-]+)',
        include(project_time_sheet_report)),
    url(r'^re-run-aggregator$', views.ReRunActivitiyDataAggregator.as_view(), name='re_run_activity_data_aggregator'),
    url(r'^re-generate-logs-for-period', views.RegenerateLogsForPeriod.as_view(), name='re_generate_logs_for_period'),
    url(r'^fix-aggregator-logs-for-period', views.FixAggregatorLogs.as_view(), name='fix_aggregator_logs_for_period'),
]

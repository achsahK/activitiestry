# python libraries
from datetime import date

# appengine libraries
from google.appengine.ext import ndb, db
from google.appengine.api import mail

from activity.log.crons import ActivitiyDataAggregatorCron
from activity.log.models import Message

# django libraries
from django.conf import settings

# Others
from gaeqblib_broken.workers import Task
from gaeqblib_broken.utilities.functions import get_future_datetime, get_value_from_request_by_key, chunks


class RerunActivitiyDataAggregator(ActivitiyDataAggregatorCron):
    """
    Task to re run activity data aggregator on specified dates
    """
    def __init__(self, *args, **kwargs):
        self.counter = 0
        self.messages_to_be_aggregated = {}
        super(RerunActivitiyDataAggregator, self).__init__(*args, **kwargs)

    def dispatch(self, request, *args, **kwargs):
        # update remote address to that of app engine servers
        if request.META.get('REMOTE_ADDR'):
            request.META['REMOTE_ADDR'] = '0.1.0.1'

        start = date(day=int(request.POST.get('start_day')), month=int(request.POST.get('start_month')), year=int(request.POST.get('start_year')))
        end = date(day=int(request.POST.get('end_day')), month=int(request.POST.get('end_month')), year=int(request.POST.get('end_year')))
        self.logging.info('Going to re-run aggregators for %s to %s' % (start, end))

        start_date_filter = {'property': 'postedFor', 'operator': '>=', 'value': start}
        end_date_filter = {'property': 'postedFor', 'operator': '<=', 'value': end}
        entities = Message.make_query(filters=[start_date_filter, end_date_filter], iterator_only=True)
        for message in entities:
            self.create_message_processed_dict(message)

        return super(RerunActivitiyDataAggregator, self).dispatch(request, *args, **kwargs)


class RegenerateLogsForPeriod(Task):
    """
    Task to regenerate logs for a specified period
    """

    def _get_employee_key_map(self):

        # imports
        from django.conf import settings
        from gaeqblib_broken.utilities.functions import get_from_url

        employee_dict = {}
        employees = get_from_url(url=settings.SPACE_APIS['employees'], user=settings.SCRIPT_USER).get('values', [])
        for employee in employees:
            employee_dict[employee.get('key')] = employee

        return employee_dict

    def run(self, request, *args, **kwargs):

        # imports
        import gc
        import logging
        from datetime import datetime
        from activity.log.models import Message, MessageStatus, MessageType, ProjectLog, EmployeeActivityLog
        from gaeqblib_broken.utilities.functions import get_month_start_and_end

        # initialize variables
        counter = 0
        logs_removed = 0
        to_put = []
        to_delete = []
        start = datetime(day=1, month=int(request.POST.get('start_month')),
                         year=int(request.POST.get('start_year')))  # month_start_end.get('start')
        end = datetime(day=1, month=int(request.POST.get('end_month')),
                       year=int(request.POST.get('end_year')))  # month_start_end.get('end')
        end = get_month_start_and_end(end).get('end')
        logging.debug("Start: %s  End: %s" % (start, end))

        employee_key_map = self._get_employee_key_map()
        messages = Message.make_query(
            filters=[
                {'property': 'postedFor', 'operator': '>=', 'value': start},
                {'property': 'postedFor', 'operator': ' <=', 'value': end},
                {'property': 'type', 'operator': ' =', 'value': MessageType.END_OF_DAY},
                {'property': 'status', 'operator': ' =', 'value': MessageStatus.PENDING}
            ],
            iterator_only=True,
            batch_size=100
        )
        for message in messages:
            counter += 1
            message.isProcessed = False
            employee = employee_key_map.get(message.user)
            message.department = str(employee.get('department'))
            message.designation = str(employee.get('designation'))
            message.grade = str(employee.get('grade'))

            # mark message for saving
            to_put.append(message)

            # interim save
            if (counter % 200) == 0:
                logging.info("Counter reached %s going to persist" % counter)
                db.put(to_put)
                to_put = []
                gc.collect()

        # final save
        if to_put:
            db.put(to_put)
        project_log_query = ProjectLog.query().filter(ProjectLog.loggedFor >= start).filter(
            ProjectLog.loggedFor <= end)
        project_log_entities = project_log_query.iter(keys_only=True)
        for project_log_key in project_log_entities:
            to_delete.append(project_log_key)
            logs_removed += 1
        employee_log_query = EmployeeActivityLog.query().filter(
            EmployeeActivityLog.loggedFor >= start).filter(
            EmployeeActivityLog.loggedFor <= end)
        employee_log_entities = employee_log_query.iter(keys_only=True)
        for employee_log_key in employee_log_entities:
            to_delete.append(employee_log_key)
            logs_removed += 1
        ndb.delete_multi(to_delete)
        logging.info("No of messages updated: " + str(counter))
        logging.info("No of logs deleted: " + str(logs_removed))


class FixAggregatorModels(Task):
    def run(self, request, *args, **kwargs):

        # imports
        import gc
        import logging
        import csv
        from datetime import datetime
        from StringIO import StringIO
        from google.appengine.ext import db
        from activity.log.models import ProjectLog, EmployeeActivityLog

        # initialize variables
        counter = 0
        employee_logs_removed = 0
        project_logs_removed = 0
        messages_to_be_updated = set()
        employee_logs_to_delete = []
        project_logs_to_delete = []
        start = datetime(day=int(request.POST.get('start_day')), month=int(request.POST.get('start_month')),
                         year=int(request.POST.get('start_year')))  # month_start_end.get('start')
        end = datetime(day=int(request.POST.get('end_day')), month=int(request.POST.get('end_month')),
                       year=int(request.POST.get('end_year')))  # month_start_end.get('end')
        self.logging.debug("Start: %s  End: %s" % (start, end))
        project_log_query = ProjectLog.query().filter(ProjectLog.lastModifiedOn >= start).filter(
            ProjectLog.lastModifiedOn <= end)
        project_log_entities = project_log_query.iter()
        for project_log in project_log_entities:
            message_keys = [x.to_old_key() for x in project_log.activities]
            messages_to_be_updated |= set(message_keys)
            project_logs_to_delete.append(project_log.key)
            project_logs_removed += 1
        employee_log_query = EmployeeActivityLog.query().filter(
            EmployeeActivityLog.lastModifiedOn >= start).filter(
            EmployeeActivityLog.lastModifiedOn <= end)
        employee_log_entities = employee_log_query.iter()
        for employee_log in employee_log_entities:
            message_keys = [x.to_old_key() for x in employee_log.activities]
            messages_to_be_updated |= set(message_keys)
            employee_logs_to_delete.append(employee_log.key)
            employee_logs_removed += 1

        # delete project log and employee activity log models
        to_delete = employee_logs_to_delete+project_logs_to_delete
        output1 = StringIO()
        output2 = StringIO()
        writer1 = csv.writer(output1, quoting=csv.QUOTE_NONNUMERIC)
        writer2 = csv.writer(output2, quoting=csv.QUOTE_NONNUMERIC)
        # Header
        writer1.writerow(['Project Logs', 'Employee Logs'])
        messages_to_be_updated = list(messages_to_be_updated)
        if len(employee_logs_to_delete) > len(project_logs_to_delete):
            rows = self._build_rows(employee_logs_to_delete, project_logs_to_delete)
        else:
            rows = self._build_rows(project_logs_to_delete, employee_logs_to_delete)
        for row in rows:
            writer1.writerow(row)
        for messages_list in chunks(messages_to_be_updated, 1500):
            writer2.writerow(messages_list)
        mail.send_mail(
            sender=settings.EMAIL_USERNAME,
            to="arunshanker@qburst.com",
            body="Please find the documents attached.",
            subject="Consolidated details on deletion of aggregator logs",
            attachments=[['Logs deleted.csv', output1.getvalue()], ['Messages to be updated.txt', output2.getvalue()]]
        )
        output1.close()
        output2.close()
        ndb.delete_multi(to_delete)
        gc.collect()
        messages_to_be_updated = list(set(messages_to_be_updated))
        self.logging.info("No of employee logs deleted: " + str(employee_logs_removed))
        self.logging.info("No of project logs deleted: " + str(project_logs_removed))
        self.logging.debug("No of messages to be processed is " + str(len(messages_to_be_updated)))
        for messages in chunks(messages_to_be_updated, 1500):
            activity_aggregator = ActivitiyDataAggregatorCron()
            for message in db.get(messages):
                counter += 1
                activity_aggregator.create_message_processed_dict(message)
            # interim aggregator run to aggregate those messages
            logging.info("Counter reached %s going to persist" % counter)
            activity_aggregator.run(request, *args, **kwargs)
            gc.collect()
        self.logging.info("No of messages updated: " + str(counter))

    def _build_rows(self, list1, list2):
        rows = []
        for list1_key in list1:
            rows.append([list1_key])
        for count, row in enumerate(rows):
            if count <= len(list2) - 1:
                row.append(list2[count])
        return rows

/*
  See https://developers.google.com/api-client-library/javascript/ for details.
  For this to work you need to add google js library just after this script.
  <script src="https://apis.google.com/js/client.js?onload=handleClientLoad"></script>
  Also dont forget to change the apiKey and clientId.
*/

var scopes = 'email';
var apiKey = 'AIzaSyBBZrWIUQ4zgpwFxrK40zjkDykpGHOYEEw';
var clientId = '383125596554-9tg2l6j26ebk29s0f1un9t94m2ermjt8.apps.googleusercontent.com';

/* initialize js client */
function handleClientLoad() {
    gapi.client.setApiKey(apiKey);
    window.setTimeout(checkAuth, 1);
}

/* perform authentication */
function checkAuth() {
   gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: true}, handleAuthResult);
}

/* handle response */
function handleAuthResult(authResult) {
   if (authResult && !authResult.error) {
       /* authentication successful. use gapi.auth.getToken() to get the token */
   } else{
       checkAuth();
   }
}

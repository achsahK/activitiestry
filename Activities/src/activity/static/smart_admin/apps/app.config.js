/*         ______________________________________
  ________|                                      |_______
  \       |           SmartAdmin WebApp          |      /
   \      |      Copyright © 2014 MyOrange       |     /
   /      |______________________________________|     \
  /__________)                                (_________\

 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * =======================================================================
 * SmartAdmin is FULLY owned and LICENSED by MYORANGE INC.
 * This script may NOT be RESOLD or REDISTRUBUTED under any
 * circumstances, and is only to be used with this purchased
 * copy of SmartAdmin Template.
 * =======================================================================
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * =======================================================================
 * original filename: app.config.js
 * filesize: ??
 * author: Sunny (@bootstraphunt)
 * email: info@myorange.ca
 * =======================================================================
 *
 * APP CONFIGURATION (HTML/AJAX/PHP Versions ONLY)
 * Description: Enable / disable certain theme features here
 * GLOBAL: Your left nav in your app will no longer fire ajax calls, set 
 * it to false for HTML version
 */	
	$j.navAsAjax = false; 
/*
 * GLOBAL: Sound Config
 */
	$j.sound_path = "sound/";
	$j.sound_on = true; 
/*
 * Impacts the responce rate of some of the responsive elements (lower 
 * value affects CPU but improves speed)
 */
	var throttle_delay = 350,
/*
 * The rate at which the menu expands revealing child elements on click
 */
	menu_speed = 235,	
/*
 * Turn on JarvisWidget functionality
 * dependency: js/jarviswidget/jarvis.widget.min.js
 */
	enableJarvisWidgets = true,
/*
 * Warning: Enabling mobile widgets could potentially crash your webApp 
 * if you have too many widgets running at once 
 * (must have enableJarvisWidgets = true)
 */
	enableMobileWidgets = false,	
/*
 * Turn on fast click for mobile devices?
 * Enable this to activate fastclick plugin
 * dependency: js/plugin/fastclick/fastclick.js 
 */
	fastClick = false,
/*
 * These elements are ignored during DOM object deletion for ajax version 
 * It will delete all objects during page load with these exceptions:
 */
	ignore_key_elms = ["#header, #left-panel, #main, div.page-footer, #shortcut, #divSmallBoxes, #divMiniIcons, #divbigBoxes, #voiceModal, script"],
/*
 * VOICE COMMAND CONFIG
 * dependency: voicecommand.js
 */
	voice_command = true,
/*
 * Turns on speech without asking
 */	
	voice_command_auto = false,
/*
 * 	Sets the language to the default 'en-US'. (supports over 50 languages 
 * 	by google)
 * 
 *  Afrikaans         ['af-ZA']
 *  Bahasa Indonesia  ['id-ID']
 *  Bahasa Melayu     ['ms-MY']
 *  Català            ['ca-ES']
 *  Čeština           ['cs-CZ']
 *  Deutsch           ['de-DE']
 *  English           ['en-AU', 'Australia']
 *                    ['en-CA', 'Canada']
 *                    ['en-IN', 'India']
 *                    ['en-NZ', 'New Zealand']
 *                    ['en-ZA', 'South Africa']
 *                    ['en-GB', 'United Kingdom']
 *                    ['en-US', 'United States']
 *  Español           ['es-AR', 'Argentina']
 *                    ['es-BO', 'Bolivia']
 *                    ['es-CL', 'Chile']
 *                    ['es-CO', 'Colombia']
 *                    ['es-CR', 'Costa Rica']
 *                    ['es-EC', 'Ecuador']
 *                    ['es-SV', 'El Salvador']
 *                    ['es-ES', 'España']
 *                    ['es-US', 'Estados Unidos']
 *                    ['es-GT', 'Guatemala']
 *                    ['es-HN', 'Honduras']
 *                    ['es-MX', 'México']
 *                    ['es-NI', 'Nicaragua']
 *                    ['es-PA', 'Panamá']
 *                    ['es-PY', 'Paraguay']
 *                    ['es-PE', 'Perú']
 *                    ['es-PR', 'Puerto Rico']
 *                    ['es-DO', 'República Dominicana']
 *                    ['es-UY', 'Uruguay']
 *                    ['es-VE', 'Venezuela']
 *  Euskara           ['eu-ES']
 *  Français          ['fr-FR']
 *  Galego            ['gl-ES']
 *  Hrvatski          ['hr_HR']
 *  IsiZulu           ['zu-ZA']
 *  Íslenska          ['is-IS']
 *  Italiano          ['it-IT', 'Italia']
 *                    ['it-CH', 'Svizzera']
 *  Magyar            ['hu-HU']
 *  Nederlands        ['nl-NL']
 *  Norsk bokmål      ['nb-NO']
 *  Polski            ['pl-PL']
 *  Português         ['pt-BR', 'Brasil']
 *                    ['pt-PT', 'Portugal']
 *  Română            ['ro-RO']
 *  Slovenčina        ['sk-SK']
 *  Suomi             ['fi-FI']
 *  Svenska           ['sv-SE']
 *  Türkçe            ['tr-TR']
 *  български         ['bg-BG']
 *  Pусский           ['ru-RU']
 *  Српски            ['sr-RS']
 *  한국어          ['ko-KR']
 *  中文                            ['cmn-Hans-CN', '普通话 (中国大陆)']
 *                    ['cmn-Hans-HK', '普通话 (香港)']
 *                    ['cmn-Hant-TW', '中文 (台灣)']
 *                    ['yue-Hant-HK', '粵語 (香港)']
 *  日本語                         ['ja-JP']
 *  Lingua latīna     ['la']
 */
	voice_command_lang = 'en-US',
/*
 * 	Use localstorage to remember on/off (best used with HTML Version)
 */	
	voice_localStorage = true;
/*
 * Voice Commands
 * Defines all voice command variables and functions
 */	
 	if (voice_command) {
	 		
		var commands = {
					
			'show dashboard' : function() { $j('nav a[href="dashboard.html"]').trigger("click"); },
			'show inbox' : function() { $j('nav a[href="inbox.html"]').trigger("click"); },
			'show graphs' : function() { $j('nav a[href="flot.html"]').trigger("click"); },
			'show flotchart' : function() { $j('nav a[href="flot.html"]').trigger("click"); },
			'show morris chart' : function() { $j('nav a[href="morris.html"]').trigger("click"); },
			'show inline chart' : function() { $j('nav a[href="inline-charts.html"]').trigger("click"); },
			'show dygraphs' : function() { $j('nav a[href="dygraphs.html"]').trigger("click"); },
			'show tables' : function() { $j('nav a[href="table.html"]').trigger("click"); },
			'show data table' : function() { $j('nav a[href="datatables.html"]').trigger("click"); },
			'show jquery grid' : function() { $j('nav a[href="jqgrid.html"]').trigger("click"); },
			'show form' : function() { $j('nav a[href="form-elements.html"]').trigger("click"); },
			'show form layouts' : function() { $j('nav a[href="form-templates.html"]').trigger("click"); },
			'show form validation' : function() { $j('nav a[href="validation.html"]').trigger("click"); },
			'show form elements' : function() { $j('nav a[href="bootstrap-forms.html"]').trigger("click"); },
			'show form plugins' : function() { $j('nav a[href="plugins.html"]').trigger("click"); },
			'show form wizards' : function() { $j('nav a[href="wizards.html"]').trigger("click"); },
			'show bootstrap editor' : function() { $j('nav a[href="other-editors.html"]').trigger("click"); },
			'show dropzone' : function() { $j('nav a[href="dropzone.html"]').trigger("click"); },
			'show image cropping' : function() { $j('nav a[href="image-editor.html"]').trigger("click"); },
			'show general elements' : function() { $j('nav a[href="general-elements.html"]').trigger("click"); },
			'show buttons' : function() { $j('nav a[href="buttons.html"]').trigger("click"); },
			'show fontawesome' : function() { $j('nav a[href="fa.html"]').trigger("click"); },
			'show glyph icons' : function() { $j('nav a[href="glyph.html"]').trigger("click"); },
			'show flags' : function() { $j('nav a[href="flags.html"]').trigger("click"); },
			'show grid' : function() { $j('nav a[href="grid.html"]').trigger("click"); },
			'show tree view' : function() { $j('nav a[href="treeview.html"]').trigger("click"); },
			'show nestable lists' : function() { $j('nav a[href="nestable-list.html"]').trigger("click"); },
			'show jquery U I' : function() { $j('nav a[href="jqui.html"]').trigger("click"); },
			'show typography' : function() { $j('nav a[href="typography.html"]').trigger("click"); },
			'show calendar' : function() { $j('nav a[href="calendar.html"]').trigger("click"); },
			'show widgets' : function() { $j('nav a[href="widgets.html"]').trigger("click"); },
			'show gallery' : function() { $j('nav a[href="gallery.html"]').trigger("click"); },
			'show maps' : function() { $j('nav a[href="gmap-xml.html"]').trigger("click"); },
			'show pricing tables' : function() { $j('nav a[href="pricing-table.html"]').trigger("click"); },
			'show invoice' : function() { $j('nav a[href="invoice.html"]').trigger("click"); },
			'show search page' : function() { $j('nav a[href="search.html"]').trigger("click"); },
			'go back' :  function() { history.back(1); }, 
			'scroll up' : function () { $j('html, body').animate({ scrollTop: 0 }, 100); },
			'scroll down' : function () { $j('html, body').animate({ scrollTop: $j(document).height() }, 100);},
			'hide navigation' : function() { 
				if ($j.root_.hasClass("container") && !$j.root_.hasClass("menu-on-top")){
					$j('span.minifyme').trigger("click");
				} else {
					$j('#hide-menu > span > a').trigger("click"); 
				}
			},
			'show navigation' : function() { 
				if ($j.root_.hasClass("container") && !$j.root_.hasClass("menu-on-top")){
					$j('span.minifyme').trigger("click");
				} else {
					$j('#hide-menu > span > a').trigger("click"); 
				}
			},
			'mute' : function() {
				$j.sound_on = false;
				$j.smallBox({
					title : "MUTE",
					content : "All sounds have been muted!",
					color : "#a90329",
					timeout: 4000,
					icon : "fa fa-volume-off"
				});
			},
			'sound on' : function() {
				$j.sound_on = true;
				$j.speechApp.playConfirmation();
				$j.smallBox({
					title : "UNMUTE",
					content : "All sounds have been turned on!",
					color : "#40ac2b",
					sound_file: 'voice_alert',
					timeout: 5000,
					icon : "fa fa-volume-up"
				});
			},
			'stop' : function() {
				smartSpeechRecognition.abort();
				$j.root_.removeClass("voice-command-active");
				$j.smallBox({
					title : "VOICE COMMAND OFF",
					content : "Your voice commands has been successfully turned off. Click on the <i class='fa fa-microphone fa-lg fa-fw'></i> icon to turn it back on.",
					color : "#40ac2b",
					sound_file: 'voice_off',
					timeout: 8000,
					icon : "fa fa-microphone-slash"
				});
				if ($j('#speech-btn .popover').is(':visible')) {
					$j('#speech-btn .popover').fadeOut(250);
				}
			},
			'help' : function() {
				$j('#voiceModal').removeData('modal').modal( { remote: "ajax/modal-content/modal-voicecommand.html", show: true } );
				if ($j('#speech-btn .popover').is(':visible')) {
					$j('#speech-btn .popover').fadeOut(250);
				}
			},		
			'got it' : function() {
				$j('#voiceModal').modal('hide');
			},	
			'logout' : function() {
				$j.speechApp.stop();
				window.location = $j('#logout > span > a').attr("href");
			}
		}; 
		
	};
/*
 * END APP.CONFIG
 */

 
 
 
 
 
 
 	
# python libraries
import gc
import json
import random
import logging


from datetime import date, datetime

# django libraries
from django.conf import settings

# app engine libraries
from google.appengine.api.taskqueue import Task
from google.appengine.ext import db, ndb
from google.appengine.api import memcache

# user defined libraries
from gaeqblib_broken.workers import CronJob
from gaeqblib_broken.utilities.functions import format_datetime, get_future_datetime, get_from_url, get_date_from_string, chunks

# from utils.pub_sub import PubSub
# Models
from .models import Message, MessageStatus, MessageType, ProjectLog, EmployeeActivityLog, DailyStatusDefaulter, IncorrectActivity
from .tasks import Utilities

# Constants
MESSAGE_BATCH = 1200
PUBSUB_MESSAGE_BATCH_SIZE = 500


class DataAggregator(object):
    """
    Interface for data aggregator
    """
    def pre_run(self):
        """
        Things to be done before processing
        """
        raise NotImplementedError('This has to be implemented')

    def run(self, key, message):
        """
        Perform processing here
        """
        raise NotImplementedError('This has to be implemented')

    def post_run(self):
        """
        Perform clean up
        """
        raise NotImplementedError('This has to be implemented')


class ProjectLogDataAggregator(DataAggregator):
    """
    Data aggregator for project log
    """
    project_logs_dict = {}
    grade_wise_ratios_dict = {}
    project_logs_to_save = []

    def __init__(self):
        """
        Initialize project log data aggregator
        """
        # Reset the project log cache
        self.project_logs_dict = {}
        self.messages = []

        # populate grade wise ratio
        self.grade_wise_ratios_dict = get_from_url(
            url=settings.SPACE_APIS['grade_wise_ratio'],
            user=settings.SCRIPT_USER
        )

    def run(self, key, message):
        """
        Project log aggregation, we process the current message and update the cached project log dict, which will be used at the end to save to database.
        :param key: YYYY|MM format key
        :param message: Message object to be processed
        """
        # calculate time spent
        grade_wise_correction_factor = self.grade_wise_ratios_dict.get(message.grade, 1.0)
        time_spent = float(message.timeSpent) * grade_wise_correction_factor
        proj_key = key + '|' + message.project
        if proj_key not in self.project_logs_dict:
            # Initialize the ProjectLog if its not present
            self.project_logs_dict[proj_key] = {
                'employees': set(),
                'groupedByGrade': {},
                'groupedByActivityType': {},
                'groupedByEmployee': {},
                'groupedByDepartment': {},
                'hoursBurned': 0.0,
                'actualHoursBurned': 0.0,
                'activities': set(),
                # TODO: awesome-activity-agregator We need use project's ID rather than key here
                'project': message.project,
                'loggedFor': get_date_from_string(message.postedFor).date().replace(day=01) if isinstance(message.postedFor, unicode) else message.postedFor.date().replace(day=01)
            }
        # Update the log
        project_log = self.project_logs_dict[proj_key]
        message_key = message.key if isinstance(message.key, unicode) else message.key()
        if ndb.Key.from_old_key(message_key) not in project_log['activities']:
            self.messages.append(message_key)
            project_log['employees'].add(message.user)
            project_log['activities'].add(ndb.Key.from_old_key(message_key))
            project_log['groupedByGrade'][message.grade] = project_log['groupedByGrade'].get(message.grade,
                                                                                                    0.0) + time_spent
            project_log['groupedByActivityType'][message.activityType] = project_log['groupedByActivityType'].get(
                message.activityType, 0.0) + time_spent
            project_log['groupedByEmployee'][message.user] = project_log['groupedByEmployee'].get(
                message.user, 0.0) + time_spent
            project_log['groupedByDepartment'][message.department] = project_log['groupedByDepartment'].get(
                message.department, 0.0) + time_spent
            project_log['hoursBurned'] += time_spent
            project_log['actualHoursBurned'] += float(message.timeSpent)

            # Populate it to the instancd cache
            self.project_logs_dict[proj_key] = project_log

    def post_run(self):
        """
        Save data and perform clean up
        """
        to_put = []
        message_counter = 0
        old_proj_log_models_to_be_removed = []
        logging.debug('Total ProjectLog fetched using new key format: %s' % len(self.project_logs_to_save))

        # TODO: activity-agregator-cleanup: Fetch the project logs the old fashioned way. This will be needed for a few months.
        # TODO: activity-agregator-cleanup: Remove this later.
        for proj_log_key in find_missing(cache_dict_keys=self.project_logs_dict.keys(), fetched=self.project_logs_to_save):
            # Split the key to get logged for year, month and the project key/id
            year, month, project = proj_log_key.split("|")
            logged_for = datetime.strptime('%s_%s_01' % (year, month), '%Y_%m_%d')
            # TODO: new-gaeqb-lib Until we implement new gaeqblib, we need to stop using the "broken" one
            project_log_query = ProjectLog.query().filter(ndb.GenericProperty('loggedFor') ==  logged_for)
            project_log = project_log_query.filter(ndb.GenericProperty('project') == project).get()
            if project_log:
                # Add it to the list of project logs that we need to remove, as we will be building a shiny new one with a proper "key"
                old_proj_log_models_to_be_removed.append(project_log.key)
                # Create a new project log instance that is the clone of the old one with the new proper key
                proj_log_new_instance = project_log.clone_with_key(key_name=proj_log_key)
                self.project_logs_to_save.append(proj_log_new_instance)
            else:
                p_log = self.project_logs_dict[proj_log_key]
                # Create a new project log instance if this is the first message in corresponding month for project
                to_put.append(ProjectLog(
                                                id = proj_log_key,
                                                project = p_log.get('project'),
                                                employees = p_log.get('employees'),
                                                groupedByGrade = p_log.get('groupedByGrade'),
                                                groupedByEmployee = p_log.get('groupedByEmployee'),
                                                groupedByDepartment = p_log.get('groupedByDepartment'),
                                                actualHoursBurned = p_log.get('actualHoursBurned'),
                                                hoursBurned = p_log.get('hoursBurned'),
                                                loggedFor = p_log.get('loggedFor'),
                                                activities = p_log.get('activities'),
                                                groupedByActivityType = p_log.get('groupedByActivityType'),
                                                lastModifiedOn = p_log.get('lastModifiedOn'),
                                            ))
        for project_log in self.project_logs_to_save:
            if project_log.key.string_id() in self.project_logs_dict:
                p_log = self.project_logs_dict[project_log.key.string_id()]
                # Update the instance
                project_log.activities.extend(p_log['activities'])
                project_log.actualHoursBurned += p_log['actualHoursBurned']
                project_log.hoursBurned += p_log['hoursBurned']
                project_log.employees.extend(p_log['employees'])
                # The following are dicts that needs their values aggregated, a normal dict update will just overwrite the values
                project_log.groupedByGrade = add_dict_aggregates(project_log.groupedByGrade,
                                                                 p_log['groupedByGrade'])
                project_log.groupedByEmployee = add_dict_aggregates(project_log.groupedByEmployee,
                                                                    p_log['groupedByEmployee'])
                project_log.groupedByDepartment = add_dict_aggregates(project_log.groupedByDepartment,
                                                                      p_log['groupedByDepartment'])
                project_log.groupedByActivityType = add_dict_aggregates(project_log.groupedByActivityType,
                                                                        p_log['groupedByActivityType'])
        to_put.extend(self.project_logs_to_save)

        # Try and save some memory, not sure if this is of any use, as the memory is quite fat now
        self.project_logs_dict = {}
        gc.collect()
        # Save all that and more!
        logging.debug('Total ProjectLog models being saved: %s' % len(to_put))
        ndb.put_multi(to_put)
        to_put = []
        self.project_logs_to_save = []
        # TODO: activity-agregator-cleanup: We replaced the old logs with fresh new ones with proper keys, we can remove the old ones now.
        # TODO: activity-agregator-cleanup: Remove this later.
        logging.debug('Total Old ProjectLog models being deleted: %s' % len(old_proj_log_models_to_be_removed))
        ndb.delete_multi(old_proj_log_models_to_be_removed)
        gc.collect()
        # TODO prefetch the old and new models that needs to be updated for first few months....remove this later

        # Update Message isProcessed flag
        messages_to_put = []
        for messages in chunks(self.messages, 1000):
            for message in db.get(messages):
                message_counter += 1
                message.isProcessed = True
                messages_to_put.append(message)
                # interim save
                if (message_counter % 200) == 0:
                    logging.info("Counter reached %s going to persist" % message_counter)
                    db.put(messages_to_put)
                    messages_to_put = []
        if messages_to_put:
            logging.info("Counter reached %s going to persist" % message_counter)
            db.put(messages_to_put)
            messages_to_put = []
        # clear Messages
        self.messages = []


class EmployeeLogDataAggregator(DataAggregator):
    """
    Data aggregator for employee log
    """
    employee_activity_logs_dict = {}

    def __init__(self):
        """
        Initialize project log data aggregator
        """
        # Reset the employee activity log cache
        self.employee_activity_logs_dict = {}
        
    def run(self, key, message):
        """
        Employee Log aggregation, we process current message and then update cached employee activity dict which will be then saved at the end.
        :param key: YYYY|MM format key
        :param message: Message object to be processed
        """
        # calculate time spent
        time_spent = float(message.timeSpent)
        user_key = key + '|' + message.user
        if user_key not in self.employee_activity_logs_dict:
            # Initialize EmployeeActivityLog if not present
            self.employee_activity_logs_dict[user_key] = {
                'projects': set(),
                'activities': set(),
                'groupedByActivityType': {},
                'groupedByProject': {},
                # TODO: awesome-activity-agregator We need use employee's ID rather than key here
                'employee': message.user,
                'hoursBurned': 0.0,
                'hoursBurnedAgainstProjects': 0.0,
                'loggedFor': get_date_from_string(message.postedFor).date().replace(day=01) if isinstance(message.postedFor, unicode) else message.postedFor.date().replace(day=01)
            }
        # Update Employee Activity Log
        employee_activity_log = self.employee_activity_logs_dict[user_key]
        message_key = message.key if isinstance(message.key, unicode) else message.key()
        if ndb.Key.from_old_key(message_key) not in employee_activity_log['activities']:
            employee_activity_log['projects'].add(message.project)
            employee_activity_log['activities'].add(ndb.Key.from_old_key(message_key))
            employee_activity_log['groupedByActivityType'][message.activityType] = employee_activity_log['groupedByActivityType'].get(message.activityType, 0.0) + time_spent
            employee_activity_log['groupedByProject'][message.project] = employee_activity_log['groupedByActivityType'].get(message.project, 0.0) + time_spent
            employee_activity_log['hoursBurned'] += time_spent
            employee_activity_log['hoursBurnedAgainstProjects'] += time_spent if message.project else 0.0

            # Populate it to the instancd cache
            self.employee_activity_logs_dict[user_key] = employee_activity_log

    def post_run(self):
        """
        Save data and perform clean up
        """
        to_put = []
        old_employee_activity_logs_to_be_removed = []

        # Get all ProjectLog entries using the new awesome(r) key format
        ndb_formatted_keys = [ndb.Key('EmployeeActivityLog', key) for key in self.employee_activity_logs_dict.keys()]
        employee_activity_logs_to_save = ndb.get_multi(ndb_formatted_keys)
        employee_activity_logs_to_save = [x for x in employee_activity_logs_to_save if x is not None]
        logging.debug('Total EmployeeActivityLog fetched using new key format: %s' % len(employee_activity_logs_to_save))
        # TODO: activity-agregator-cleanup: Fetch the employee activity logs the old fashioned way. This will be needed for a few months.
        # TODO: activity-agregator-cleanup: Remove this later.
        for employee_activity_log_key in find_missing(cache_dict_keys=self.employee_activity_logs_dict.keys(), fetched=employee_activity_logs_to_save):
            year, month, user = employee_activity_log_key.split("|")
            logged_for = datetime.strptime('%s_%s_01' % (year, month), '%Y_%m_%d')
            # TODO: new-gaeqb-lib Until we implement new gaeqblib, we need to stop using the "broken" one
            employee_activity_log_query = EmployeeActivityLog.query()
            employee_activity_log_query = employee_activity_log_query.filter(ndb.GenericProperty('loggedFor') == logged_for)
            employee_activity_log = employee_activity_log_query.filter(ndb.GenericProperty('employee') == user).get()
            if employee_activity_log:
                # Add it to the list of employee activity logs that we need to remove, as we will be building a shiny new one with a proper "key"
                old_employee_activity_logs_to_be_removed.append(employee_activity_log.key)
                # Create a new employee activity log new instance that is the clone of the old one with the new proper key
                employee_activity_log_new_instance = employee_activity_log.clone_with_key(key_name=employee_activity_log_key)
                employee_activity_logs_to_save.append(employee_activity_log_new_instance)
            else:
                # Create a new project log instance if this is the first message in corresponding month for employee
                e_log = self.employee_activity_logs_dict[employee_activity_log_key]
                to_put.append(EmployeeActivityLog(
                    id=employee_activity_log_key,
                    employee=e_log.get('employee'),
                    projects=e_log.get('projects'),
                    groupedByProject=e_log.get('groupedByProject'),
                    groupedByActivityType=e_log.get('groupedByActivityType'),
                    hoursBurnedAgainstProjects=e_log.get('hoursBurnedAgainstProjects'),
                    hoursBurned=e_log.get('hoursBurned'),
                    loggedFor=e_log.get('loggedFor'),
                    activities=e_log.get('activities'),
                    lastModifiedOn=e_log.get('lastModifiedOn'),
                ))
        for employee_log in employee_activity_logs_to_save:
            if employee_log.key.string_id() in self.employee_activity_logs_dict:
                e_log = self.employee_activity_logs_dict[employee_log.key.string_id()]
                employee_log.projects.extend(e_log['projects'])
                employee_log.activities.extend(e_log['activities'])
                employee_log.groupedByActivityType = add_dict_aggregates(employee_log.groupedByActivityType,
                                                                         e_log['groupedByActivityType'])
                employee_log.groupedByProject = add_dict_aggregates(employee_log.groupedByProject,
                                                                    e_log['groupedByProject'])
                employee_log.hoursBurned += e_log['hoursBurned']
                employee_log.hoursBurnedAgainstProjects += e_log['hoursBurnedAgainstProjects']
        to_put.extend(employee_activity_logs_to_save)
        # Try and save some memory, not sure if this is of any use, as the memory is quite fat now
        self.employee_activity_logs_dict = {}
        gc.collect()
        # Save all that and more!
        logging.debug('Total EmployeeActivityLog models being saved: %s' % len(to_put))
        ndb.put_multi(to_put)
        employee_activity_logs_to_save = to_put = []
        # TODO: activity-agregator-cleanup: We replaced the old logs with fresh new ones with proper keys, we can remove the old ones now.
        # TODO: activity-agregator-cleanup: Remove this later.
        logging.debug('Total Old EmployeeActivityLog models being deleted: %s' % len(old_employee_activity_logs_to_be_removed))
        ndb.delete_multi(old_employee_activity_logs_to_be_removed)


class ActivitiyDataAggregatorCron(CronJob):
    """
    Cron job to aggregate activity messages
    """
    def __init__(self, *args, **kwargs):
        self.messages_to_be_aggregated = {}
        self.project_logs_dict_keys = set()
        super(ActivitiyDataAggregatorCron, self).__init__(*args, **kwargs)

    def run(self, request, *args, **kwargs):
        """
        cron logic
        """
        counter = 0
        message_keys = []
        duplicate_message_keys = []
        # initialize aggregator
        logging.info('Initializing aggregators')
        project_log_aggregator = ProjectLogDataAggregator()
        employee_log_aggregator = EmployeeLogDataAggregator()

        if not self.messages_to_be_aggregated:
            end_date = get_future_datetime(days=-1)
            start_date = get_future_datetime(end_date, days=-90)
            is_processed_filter = {'property': 'isProcessed', 'operator': '=', 'value': False}
            type_filter = {'property': 'type', 'operator': '=', 'value': MessageType.END_OF_DAY}
            status_filter = {'property': 'status', 'operator': '=', 'value': MessageStatus.PENDING}
            start_date_filter = {'property': 'postedFor', 'operator': '>=', 'value': start_date.replace(day=1)}
            end_date_filter = {'property': 'postedFor', 'operator': '<=', 'value': end_date}
            filters = [start_date_filter, end_date_filter, is_processed_filter, type_filter, status_filter]
            entities = Message.make_query(filters=filters, iterator_only=True)
            for count, message in enumerate(entities):
                self.create_message_processed_dict(message)
                if count > MESSAGE_BATCH:
                    break

        # if not self.messages_to_be_aggregated:
        #     # updating rerun flag in aggregators
        #     project_log_aggregator.is_rerun = False
        #     employee_log_aggregator.is_rerun = False
        #     # No messages means it is not Rerun so need to pull messages from PubSub
        #     logging.info('Going to run aggregators for %s.' % (date.today().strftime('%m %d, %Y')))
        #
        #     # run prerequisites
        #     logging.info('Running pre-requisites')
        #
        #     pubsub = PubSub()
        #     pubsub.subscription = pubsub.get_full_subscription_name('daily-status-aggregator')
        #     self.messages_to_be_aggregated, self.project_logs_dict_keys = pubsub.pull_messages(date_field='postedFor', date_field_key_format='%Y|%m')

        # Get ProjectLog entries using the new awesome(r) key format so that we can remove already logged messages
        ndb_formatted_keys = [ndb.Key('ProjectLog', key) for key in self.project_logs_dict_keys]
        # clear proj log dict keys
        self.project_logs_dict_keys = set()
        project_logs_to_save = ndb.get_multi(ndb_formatted_keys)
        project_logs_to_save = [x for x in project_logs_to_save if x is not None]
        for project_log in project_logs_to_save:
            message_keys.extend(project_log.activities)
        project_log_aggregator.project_logs_to_save = project_logs_to_save
        project_logs_to_save = ndb_formatted_keys = []
        gc.collect()
        if self.messages_to_be_aggregated:
            logging.info('Starting employee and project data aggregation...')
            for key, messages in self.messages_to_be_aggregated.iteritems():
                for message in messages:
                    message_key = message.key if isinstance(message.key, unicode) else message.key()
                    if ndb.Key.from_old_key(message_key) in message_keys:
                        duplicate_message_keys.append(message_key)
                        continue
                    counter += 1
                    project_log_aggregator.run(key, message)
                    employee_log_aggregator.run(key, message)

        # Update Message isProcessed flag for duplicate messages
        processed_messages = db.get(duplicate_message_keys)
        for message in processed_messages:
            message.isProcessed = True
        db.put(processed_messages)
        # run clean up
        logging.info('Clean messages from Aggregator Dict')
        self.logging.info('Processed %s messages!' % str(counter))
        self.messages_to_be_aggregated = {}
        message_keys = processed_messages = duplicate_message_keys = []

        logging.info('Going to perform post run...saving models and all that!')
        employee_log_aggregator.post_run()
        project_log_aggregator.post_run()

        logging.debug('Completed processing messages...')
        gc.collect()

    def error_handler(self, request, *args, **kwargs):
        pass
    
    def create_message_processed_dict(self, message):
        key = message.postedFor.strftime('%Y|%m')
        self.project_logs_dict_keys.add(key+'|'+message.project)
        if key in self.messages_to_be_aggregated:                      
            self.messages_to_be_aggregated[key] += [message]           
        else:                                                     
            self.messages_to_be_aggregated[key] = [message]            


class IncorrectActivitiesDataAggregatorCron(CronJob):
    """
    Cron job to generate incorrect activities and defaulters report for yesterdays activity messages
    """
    end_date = get_future_datetime(days=-1)
    start_date = get_future_datetime(end_date, days=-30)

    def __init__(self, *args, **kwargs):
        super(IncorrectActivitiesDataAggregatorCron, self).__init__(*args, **kwargs)

    def run(self, request, *args, **kwargs):
        """
        cron logic
        """
        # initialize
        invalid_activities_keys = []
        messages_to_generate_report = {}

        logging.info('Going to generate incorrect activities report!')

        date_filter = {'property': 'postedOn', 'operator': '>=', 'value': get_future_datetime(days=-7)}
        entities = Message.make_query(iterator_only=True, filters=[date_filter])

        for message in entities:
            message_tuple = (str(message.key()), message.postedFor)
            # non project activities are not required for incorrect activities report
            if message.project:
                logging.info('Processing message: %s for employee: %s' % (str(message.key()), message.user))
                if message.user in messages_to_generate_report:
                    if message.project in messages_to_generate_report[message.user]:
                        messages_to_generate_report[message.user][message.project] += (message_tuple,)
                    else:
                        messages_to_generate_report[message.user][message.project] = (message_tuple,)
                else:
                    messages_to_generate_report[message.user] = {message.project: (message_tuple,)}
        # clear entities
        entities = []

        if messages_to_generate_report:
            # process the fetched messages
            for employee_key, project_wise_status_dict in messages_to_generate_report.iteritems():
                employee_allocations = get_from_url(
                    url=settings.SPACE_APIS['project_allocations'],
                    data={'args': json.dumps([employee_key, format_datetime(self.start_date)])},
                    user=settings.SCRIPT_USER
                )
                logging.debug('Start processing employee allocations for employee: %s!' % employee_key)
                # process allocations one by one
                for employee_allocation in employee_allocations:

                    # get activities associated with allocation/project
                    project_key = employee_allocation.get('project')
                    if project_key in project_wise_status_dict.keys():

                        # if end date is none - activity is valid
                        if not employee_allocation.get('endDate'):
                            project_wise_status_dict.pop(project_key)

                        # if end date is not none check date and validate activity
                        else:

                            # process activities one by one
                            incorrect_activities = []
                            for activity in project_wise_status_dict.get(project_key):

                                # if end date is over - activity is invalid
                                if datetime.strptime(employee_allocation.get('endDate'),
                                                     settings.DEFAULT_DATE_FORMAT).date() < activity[1].date():
                                    incorrect_activities.append(activity)

                            # save incorrect activity
                            if incorrect_activities:
                                project_wise_status_dict[project_key] = incorrect_activities

                            # remove valid activity
                            else:
                                project_wise_status_dict.pop(project_key)

                # if incorrect activities is present
                if project_wise_status_dict:
                    for project_key, activity in project_wise_status_dict.iteritems():
                        invalid_activities_keys.extend([entry[0] for entry in activity])
        # clean messages to generate report
        messages_to_generate_report = {}

        # invoke deferred task if invalid activities is present
        if invalid_activities_keys:
            task_name = 'Generate-Incorrect-Activities-Report-date-%s-random-%s' % \
                        (datetime.now().strftime('%d-%m-%Y'), str(random.random())[2:])
            task_url = '/worker-dispatcher/activity.log.tasks.GenerateIncorrectActivitiesReportTask'
            task_data = {'payload': invalid_activities_keys}
            Task(
                name=task_name, url=task_url, params=task_data,
                target=settings.TASK_QUEUES['incorrect_activities_report']['backend'], countdown=30
            ).add(settings.TASK_QUEUES['incorrect_activities_report']['queue'])

        gc.collect()

        logging.info('Completed generating incorrect activities report ....')

    def error_handler(self, request, *args, **kwargs):
        pass


class GenerateDailyDefaultersReportCron(CronJob):
        """
        Task to generate daily defaulters report
        """
        project_details = {}
        employee_details = {}
        date_being_processed = get_future_datetime(days=-1).date().strftime(format=settings.DEFAULT_DATE_FORMAT)
        daily_status_defaulters = []
        daily_status_defaulters_dict = {}
        employees_posted_for_the_day = []
        employees_not_posted_for_the_day = []

        def is_holiday(self):
            """
            Check whether the day under process is an holiday
            """
            self.logging.info('Going to check whether %s is a holiday' % self.date_being_processed)
            response = get_from_url(
                url=settings.SPACE_APIS['holiday'],
                data={'args': json.dumps([self.date_being_processed])},
                user=settings.SCRIPT_USER
            )
            return response

        def is_employee_on_vacation(self, employee_key):
            """
            Check whether the employee is on leave
            """
            employees_on_vacation = memcache.get('employeeVacationList')
            if employees_on_vacation is None:

                # get employees on vacation
                employees_on_vacation = get_from_url(
                    url=settings.SPACE_APIS['vacation'],
                    data={'args': json.dumps([self.date_being_processed])},
                    user=settings.SCRIPT_USER
                )
                if memcache.add('employeeVacationList', employees_on_vacation):
                    self.logging.info('Memcache set success [key - employeeVacationList]')
                else:
                    self.logging.error('Memcache set failed [key - employeeVacationList]')
            return employee_key in employees_on_vacation

        def set_employees_posted_status_for_the_day(self):
            """
            Get list of employees posted message for the day
            """
            date_filter = {'property': 'postedFor', 'operator': '>=', 'value': self.date_being_processed}
            messages = Message.make_query(filters=[date_filter], iterator_only=True)
            self.employees_posted_for_the_day = [message.user for message in messages]

        def set_employees_not_posted_status_for_the_day(self):
            """
            Get list of employees not posted message for the day (excluding those who are exempted)
            """
            # get excused employees
            excused_employees = get_from_url(
                url=settings.SPACE_APIS['excused_employees'],
                user=settings.SCRIPT_USER
            )
            employees_posted = tuple(set(self.employees_posted_for_the_day) - set(excused_employees))
            self.employees_not_posted_for_the_day = get_from_url(
                url=settings.SPACE_APIS['daily_defaulters'],
                data={'args': json.dumps([employees_posted])},
                user=settings.SCRIPT_USER
            )

        def set_employee_details(self, employee_keys):
            """
            Fetch employee details from space that is not present in employee_details dict
            """
            employees_to_be_fetched = []
            for employee_key in employee_keys:
                if employee_key not in self.employee_details.keys():
                    employees_to_be_fetched.append(employee_key)
            employee_details = Utilities.get_employee_details_from_space(employees_to_be_fetched)
            for employee_detail in employee_details:
                self.employee_details[employee_detail['key']] = employee_detail

        def set_project_details(self, project_keys):
            """
            Fetch project details from space that is not present in project_details dict
            """
            projects_to_be_fetched = []
            for project_key in project_keys:
                if project_key not in self.project_details.keys():
                    projects_to_be_fetched.append(project_key)
            project_details = Utilities.get_project_details_from_space(projects_to_be_fetched)
            for project_detail in project_details:
                self.project_details[project_detail['key']] = project_detail

        def create_or_update_defaulter_instance(self, defaulter_instance=None, employee=None):
            """
            Create or update defaulter instance
            """
            allocations = []

            # get project allocations of employee
            employee_allocations = Utilities.get_employee_allocations_from_space(employee)

            # update project details (from employee allocations)
            self.set_project_details([allocation['project'] for allocation in employee_allocations])

            # update employee details (from employee allocations)
            pm_list = []
            for allocation in employee_allocations:
                if self.project_details.get(allocation['project']):
                    pm_list.extend(self.project_details[allocation['project']]['projectManagerKeyList'])
            self.set_employee_details(pm_list)

            # build allocations property for defaulter instance
            for allocation in employee_allocations:
                allocated_project_details = self.project_details.get(allocation['project'])
                Utilities.get_employee_hyper_links(allocated_project_details['projectManagerKeyList'],
                                                   self.employee_details)
                allocations.append({
                    'projectKey': allocated_project_details['key'],
                    'projectHyperLink': allocated_project_details['hyperlink'],
                    'PMKey': allocated_project_details['projectManagerKeyList'],
                    'PMHyperlink': Utilities.get_employee_hyper_links(
                        allocated_project_details['projectManagerKeyList'],
                        self.employee_details),
                    'allocationPrecentage': allocation['allocation']
                })

            # update defaulter instance if exists
            if defaulter_instance:
                defaulter_instance.employeeKey = employee
                defaulter_instance.employeeHyperLink = self.employee_details[employee]['hyperlink']
                defaulter_instance.allocations = allocations
                defaulter_instance.daysDefaulted += 1

            # create defaulter instance if not exists
            else:
                defaulter_instance = DailyStatusDefaulter(
                    employeeKey=employee, employeeHyperLink=self.employee_details[employee]['hyperlink'],
                    allocations=allocations, daysDefaulted=1
                )

            # update daily status defaulter dict
            self.daily_status_defaulters_dict[employee] = defaulter_instance

        def run(self, request, *args, **kwargs):
            """
            What to do?
            """
            # check whether day under process is a holiday
            if not self.is_holiday():

                # set list of daily defaulters
                self.daily_status_defaulters = DailyStatusDefaulter.make_query(iterator_only=True)

                # set list of employees who has posted status for the day
                self.set_employees_posted_status_for_the_day()

                # set list of employees who has not posted status for the day (excluding those who are exempted)
                self.set_employees_not_posted_status_for_the_day()

                # remove inapplicable defaulter instance
                models_to_delete = []
                self.logging.info('Deleting existing daily status defaulter report')
                for defaulter in self.daily_status_defaulters:
                    if defaulter.employeeKey not in self.employees_not_posted_for_the_day:
                        models_to_delete.append(defaulter.get_key_instance())
                    else:
                        self.daily_status_defaulters_dict[defaulter.employeeKey] = defaulter
                DailyStatusDefaulter.delete_multi(models_to_delete)
                gc.collect()

                # get employee details
                self.set_employee_details(self.employees_not_posted_for_the_day)

                # process employee
                counter = 0
                models_to_save = []
                self.logging.info('Starting to process employee messages')
                for employee in self.employees_not_posted_for_the_day:

                    # ignore employee on leave
                    if self.is_employee_on_vacation(employee):
                        self.daily_status_defaulters_dict.pop(employee, None)

                    # process other employees
                    else:
                        counter += 1
                        defaulter_instance = self.daily_status_defaulters_dict.get(employee, None)
                        if (not defaulter_instance) or \
                                (getattr(defaulter_instance, 'lastModifiedOn', False) != date.today()):

                            # if defaulter_instance exists update
                            # else create new defaulter_instance and update it in daily_status_defaulters_dict
                            self.create_or_update_defaulter_instance(defaulter_instance, employee)

                            # save defaulter instance
                            defaulter_instance = self.daily_status_defaulters_dict.pop(employee, None)
                            if defaulter_instance:
                                models_to_save.append(defaulter_instance)

                        # interim save daily defaulter
                        if len(models_to_save) >= 100:
                            DailyStatusDefaulter.save_multi(models_to_save)
                            models_to_save = []

                # save pending daily defaulter
                if models_to_save:
                    DailyStatusDefaulter.save_multi(models_to_save)
                    models_to_save = []

                # clean up memory
                gc.collect()

            else:
                self.logging.info('Date being processed is a holiday')

        def error_handler(self, request, *args, **kwargs):
            """
            Error handler
            """
            pass


class CleanUpOldIncorrectActivitiesReport(CronJob):
    """
    Task to clean up IncorrectActivitiy records older than 1 week
    """
    def run(self, request, *args):
        """
        Run cron to remove old records
        """
        to_remove = []
        # create date filter
        date_filter = {'property': 'createdOn', 'operator': '<=', 'value': get_future_datetime(days=-7)}
        # fetch older incorrect activity records
        entities = IncorrectActivity.make_query(iterator_only=True, filters=[date_filter])
        for incorrectActivity in entities:
            to_remove.append(incorrectActivity.key())

        # Remove old IncorrectActivity records
        IncorrectActivity.delete_multi(to_remove)

    def error_handler(self, request, *args, **kwargs):
        pass


#### Utility methods


def add_dict_aggregates(dict1, dict2):
    return_dict = dict1.copy() if dict1 else {}
    return_dict.update(dict2)
    for key in dict1.keys():
        if key in dict2:
            return_dict[key] += dict2[key]
        return return_dict

def find_missing(cache_dict_keys, fetched):
    """
    Find the missing logs, i.e the ones that are not there in the new format.
    Eventually we will not need this, as all will be ported over.
    """
    fetched = set([k.key.string_id() for k in fetched])
    return set(cache_dict_keys) - fetched

import logging
import json
import base64


# python libraries
from StringIO import StringIO
from collections import OrderedDict

# django libraries
from django.conf import settings

# django rest libraries
from rest_framework.response import Response
from rest_framework.exceptions import NotAcceptable
from rest_framework import status

# app engine libraries
from google.appengine.api import memcache

# user defined libraries
from gaeqblib_broken.views import RestFormView, RestCrudView
from gaeqblib_broken.utilities.functions import get_from_url

# models
from activity.log.models import Message, ExportFields
from utils.spreadsheet import ExcelCreator
from utils.send_mail import MailGunEmailMessage
# from utils.pub_sub import PubSub
from activity.update_db_scripts.views import get_employee_key_wise_dict
from space.common import DeferredTaskWorker
from space.utils.utils import safestringfy

# serializers
from activity.log.serializers import MessageSerializer, MessageFilterSerializer, MessageFilterOverPeriodSerializer


class MessageCrudView(RestCrudView):
    """
    Performs CRUD on message
    """
    model = Message
    ordering = ['-postedOn']
    serializer_class = MessageSerializer
    permissions_required = {'get': 'STATUS_REPORT_READ', 'post': 'STATUS_REPORT_WRITE'}

    def get_initial_values(self):
        initial_values = super(MessageCrudView, self).get_initial_values()

        # update choices for project
        project_choices = get_from_url(
            url=settings.SPACE_APIS['employee_project_choices'],
            user=self.request.user.email
        )

        initial_values['choices']['allocated_projects'] = OrderedDict(project_choices.get('allocated_projects', []))
        initial_values['choices']['all_projects'] = project_choices.get('project_choices', [])
        initial_values['choices']['project_status'] = project_choices.get('project_status', {})

        return initial_values

    def get_user_daily_status_log_from_memcache(self, key,  user, posted_for):
        """
        Returns the hours logged by the user for a day.
        @param key: memcache key for current user
        @param user: expects the key of logged user.
        @param posted_for: date object for which the status log is expected.
        """
        client = memcache.Client()
        data = memcache.get(key)
        if not data:
            filters = [{'property': 'user', 'operator': '=', 'value': user},
                       {'property': 'postedFor', 'operator': '=', 'value': posted_for}]
            query = Message.make_query(filters=filters, projections=['timeSpent'])['results']
            if query:
                total_time_spent = 0
                for message in query:
                    total_time_spent += message.timeSpent
                data = total_time_spent
                client.set(key, data)
        return data

    def post(self, request, *args, **kwargs):

        data = self.get_data_dict()
        serializer = self.serializer_class(data=data)
        # pub_sub_client = PubSub.client

        if serializer.is_valid():
            client = memcache.Client()
            value = serializer.validated_data['timeSpent']
            limit = settings.DAILY_STATUS_THRESHOLD

            memcache_key = str(serializer.validated_data['postedFor']) + '_' + serializer.validated_data['user']
            data = self.get_user_daily_status_log_from_memcache(memcache_key, serializer.validated_data['user'], serializer.validated_data['postedFor'])
            if data:
                if data + value > limit:
                    return Response({'timeSpent': ['You have exceeded your limit of %s hours for the day.' % limit]}, status=400, headers=self.get_headers())
                else:
                    data += value
            else:
                data = value
            if not client.set(memcache_key, data):
                logging.debug('Failed to set Memcache.')
                memcache.delete(memcache_key)

        return super(MessageCrudView, self).post(request, *args, **kwargs)
        # if response.status_code == status.HTTP_201_CREATED:
        #     message = base64.b64encode(json.dumps(response.data))
        #     body = {
        #         'messages': [{'data': message}]
        #     }
        #     pub_sub_client.projects().topics().publish(
        #         topic=PubSub.get_full_topic_name('daily-status'), body=body).execute()
        # return response

    def delete_object(self):
        # Fix stupid "feature"/bug in gaeqblib
        raise NotImplementedError

        # get object
        message_obj = self.get_object()

        # only delete messages with isProcessed false
        if message_obj.isProcessed:
            raise NotAcceptable(detail='Message already processed by the aggregator')

        super(MessageCrudView, self).delete_object()


class MessagesFilterByInputView(RestFormView):
    """
    Filter messages based on user input
    """
    model = Message
    ordering = ['-postedOn']
    serializer_class = MessageFilterSerializer
    permissions_required = {'get': 'ACTIVITIES_REPORT', 'post': 'ACTIVITIES_REPORT'}

    def get_initial_values(self):
        initial_values = super(MessagesFilterByInputView, self).get_initial_values()

        # get choices for employee
        key = 'employee_choices'
        employee_choices = memcache.get(key)
        if employee_choices is None:
            employee_choices = []
            for item in get_from_url(url=settings.SPACE_APIS['employees'], user=self.request.user.email).get('values'):
                employee_choices.append((item['key'], item['name']))
            memcache.add(key, employee_choices, 600)
        initial_values['choices']['employee'] = OrderedDict(employee_choices)

        # get choices for department and projects
        key = 'project_department_choices'
        project_department_choices = memcache.get(key)
        if project_department_choices is None:
            project_department_choices = get_from_url(
                url=settings.SPACE_APIS['project_department_choices'],
                user=self.request.user.email
            )
            memcache.add(key, project_department_choices, 600)
        initial_values['choices']['project'] = OrderedDict(project_department_choices.get('projects'))
        initial_values['choices']['department'] = OrderedDict(project_department_choices.get('departments'))

        return initial_values

    def serializer_valid(self, validated_data):
        """
        Fetch list of entities/records that is available
        """
        # prepare filter
        type_filter = {'property': 'type', 'operator': '=', 'value': validated_data.get('type')}
        date_filter = {'property': 'postedFor', 'operator': '=', 'value': validated_data.get('date')}
        project_filter = {'property': 'project', 'operator': '=', 'value': validated_data.get('project')}
        employee_filter = {'property': 'user', 'operator': '=', 'value': validated_data.get('employee')}
        department_filter = {'property': 'department', 'operator': '=', 'value': validated_data.get('department')}

        # remove invalid filters
        filters = [type_filter, date_filter, project_filter, employee_filter, department_filter]
        filters = [x for x in filters if x['value']]

        # fetch data
        data = self.model.make_query(limit=self.paginate_by, cursor=self.cursor, order_by=self.ordering,
                                     direction=self.direction, filters=filters)

        # serialize data
        data['results'] = MessageSerializer(data['results'], many=True).data

        # generate response
        return Response(data, headers=self.get_headers())


class MessageFilterByEmployeeOverPeriodView(RestFormView):
    """
    Filter message based on employee over a period
    """
    ordering = ['-postedOn']
    serializer_class = MessageFilterOverPeriodSerializer
    permissions_required = {'get': 'STATUS_REPORT_READ', 'post': 'STATUS_REPORT_READ'}

    def serializer_valid(self, validated_data):
        """
        Fetch list of entities/records that is available
        """
        # prepare filter
        employee_filter = {'property': 'user', 'operator': '=', 'value': self.key}
        start_date_filter = {'property': 'postedFor', 'operator': '>=', 'value': validated_data.get('start_date')}
        end_date_filter = {'property': 'postedFor', 'operator': '<=', 'value': validated_data.get('end_date')}

        # remove invalid filters
        filters = [employee_filter, start_date_filter, end_date_filter]
        filters = [x for x in filters if x['value']]

        # fetch data
        data = Message.make_query(limit=self.paginate_by, cursor=self.cursor, order_by=self.ordering,
                                  direction=self.direction, filters=filters)

        # serialize data
        data['results'] = MessageSerializer(data['results'], many=True).data

        # generate response
        return Response(data, headers=self.get_headers())


class MessageFilterByProjectOverPeriodView(RestFormView):
    """
    Filter message based on project over a period
    """
    ordering = ['-postedFor']
    serializer_class = MessageFilterOverPeriodSerializer
    permissions_required = {'get': 'STATUS_REPORT_READ', 'post': 'STATUS_REPORT_READ'}

    def serializer_valid(self, validated_data):
        """
        Fetch list of entities/records that is available
        """
        # prepare filter
        project_filter = {'property': 'project', 'operator': '=', 'value': self.key}
        start_date_filter = {'property': 'postedFor', 'operator': '>=', 'value': validated_data.get('start_date')}
        end_date_filter = {'property': 'postedFor', 'operator': '<=', 'value': validated_data.get('end_date')}

        # remove invalid filters
        filters = [project_filter, start_date_filter, end_date_filter]
        filters = [x for x in filters if x['value']]

        # fetch data
        data = Message.make_query(limit=self.paginate_by, cursor=self.cursor, order_by=self.ordering,
                                      direction=self.direction, filters=filters)
        # serialize data
        data['results'] = MessageSerializer(data['results'], many=True).data

        # generate response
        return Response(data, headers=self.get_headers())


class SendProjectTimeSheetExportView(RestFormView):
    """
    Handle export project time sheet
    @author: Jino Jossy
    """
    ordering = ['-postedFor']
    serializer_class = MessageFilterOverPeriodSerializer
    permissions_required = {
        'post': ['PROJECT_ACTIVITIES_EXPORT', 'PROJECT_ACTIVITIES_EXPORT_SELF'],
    }

    def serializer_valid(self, validated_data):

        SendProjectTimeSheetExportView().sendProjectTimeSheetExportMail(validated_data=validated_data,project_key=self.key)

        # generate response
        return Response(status=200, data={'isSuccess': True}, headers=self.get_headers(), content_type='json')

    @DeferredTaskWorker(subjectPrependTxt="Send Project Time Sheet Export mail", queue="noRetryBackendQueue")
    def sendProjectTimeSheetExportMail(self, validated_data, project_key):
        excel = StringIO()
        excel_data = []
        excel_header = []
        # prepare filter
        project_filter = {'property': 'project', 'operator': '=', 'value': project_key}
        start_date_filter = {'property': 'postedFor', 'operator': '>=', 'value': validated_data.get('start_date')}
        end_date_filter = {'property': 'postedFor', 'operator': '<=', 'value': validated_data.get('end_date')}

        # remove invalid filters
        filters = [project_filter, start_date_filter, end_date_filter]
        filters = [x for x in filters if x['value']]
        data = Message.make_query(iterator_only=True, cursor=self.cursor, order_by=self.ordering,
                                  direction=self.direction, filters=filters)
        fields_dict = {
            'EMPLOYEE': ('postedBy', 'Name'),
            'HOURS_BURNED': ('timeSpent', 'Hours Burned'),
            'TASKS': ('message', 'Tasks'),
            'ACTIVITY_TYPE': ('activityType', 'Activity Type'),
            'DESIGNATION': ('designation', 'Designation'),
            'DEPARTMENT': ('department', 'Department'),
            'POSTED_FOR': ('postedFor', 'Posted For'),
            'POSTED_ON': ('postedOn', 'Posted On'),
        }
        exportFields = json.loads(validated_data.get('export_fields'))
        for val in exportFields:
            excel_header.append(fields_dict[val][1])
        excel_data.append(excel_header)
        # fetch employees
        employees = get_employee_key_wise_dict()
        to = employees.get(validated_data.get('emp_key'))

        for message in data:
            processed_data = []
            for export_field in exportFields:
                if export_field == ExportFields.TASKS:
                    processed_data.append(safestringfy(getattr(message, fields_dict[export_field][0])))
                elif export_field != ExportFields.EMPLOYEE:
                    processed_data.append(getattr(message, fields_dict[export_field][0]))
                else:
                    employee = employees.get(message.postedBy)
                    if not employee or not to:
                        raise Exception('Employee List not available!')
                    processed_data.append(safestringfy('=HYPERLINK("http://space.qburst.com/viewEmployee.html/%s", "%s")' % (employee.get('key'), employee.get('name'))))
            excel_data.append(processed_data)
        ExcelCreator(data=excel_data, out=excel).process(title="Project Time Sheet")
        sendNotification = MailGunEmailMessage()
        sendNotification.to = to.get('companyEmail')
        sendNotification.attachments = [(validated_data.get('proj_name')+" Time Sheet.xlsx", excel.getvalue())]
        sendNotification.subject = validated_data.get('proj_name') +": Project Time Sheet"
        sendNotification.body = "Project Timesheet from %s to %s is attached." %(validated_data.get('start_date'), validated_data.get('end_date'))
        sendNotification.send()
        excel.close()

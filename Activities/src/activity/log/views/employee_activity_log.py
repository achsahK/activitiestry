# django libraries
from django.http import HttpResponse

# django rest libraries
from rest_framework.response import Response

# user defined libraries
from gaeqblib_broken.views import RestFormView, RestCrudView

# models
from activity.log.models import EmployeeActivityLog

# serializers
from activity.log.serializers import EmployeeActivityLogSerializer, EmployeeActivityFilterOverPeriodSerializer


class EmployeeActivityLogCrudView(RestCrudView):
    """
    Performs CRUD on EmployeeActivityLog
    """
    # TODO: Need-to-fix will not work because of make_query issues
    model = EmployeeActivityLog
    ordering = ['-loggedFor']
    serializer_class = EmployeeActivityLogSerializer
    permissions_required = {'get': 'STATUS_REPORT_EMPLOYEE_READ'}


class EmployeeActivityLogFilterByEmployeeOverPeriodView(RestFormView):
    """
    Return list of employee activities
    """
    ordering = ['-loggedFor']
    serializer_class = EmployeeActivityFilterOverPeriodSerializer
    permissions_required = {'get': ['APPRAISAL_DETAILS_VIEW', 'STATUS_REPORT_EMPLOYEE_READ'],
                            'post': ['APPRAISAL_DETAILS_VIEW', 'STATUS_REPORT_EMPLOYEE_READ']}

    def dispatch(self, request, *args, **kwargs):

        # override privilege checking in view
        request.require_auth = False

        return super(EmployeeActivityLogFilterByEmployeeOverPeriodView, self).dispatch(request, *args, **kwargs)

    def serializer_valid(self, validated_data):
        """
        Fetch list of entities/records that is available
        """

        # custom privilege checking
        request_method = self.request.method.lower()

        if self.request.user.is_admin or \
                (request_method in self.permissions_required and
                     set(list(self.permissions_required[request_method])).intersection(self.request.privileges)) or \
                (self.key == self.request.employee):
            pass

        # block unauthorized access
        else:
            return HttpResponse('Access denied!', status=403)

        # prepare filter
        employee_filter = {'property': 'employee', 'operator': '=', 'value': self.key}
        start_date_filter = {'property': 'loggedFor', 'operator': '>=', 'value': validated_data.get('start_date')}
        end_date_filter = {'property': 'loggedFor', 'operator': '<=', 'value': validated_data.get('end_date')}

        # remove invalid filters
        filters = [employee_filter, start_date_filter, end_date_filter]
        filters = [x for x in filters if x['value']]

        # fetch data
        # TODO: Need-to-fix will not work because of make_query issues
        data = EmployeeActivityLog.make_query(limit=self.paginate_by, cursor=self.cursor, order_by=self.ordering,
                                              direction=self.direction, filters=filters)

        # serialize data
        data['results'] = EmployeeActivityLogSerializer(data['results'], many=True).data

        # generate response
        return Response(data, headers=self.get_headers())

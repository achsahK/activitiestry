# django rest libraries
from rest_framework.response import Response

from google.appengine.ext import ndb

# user defined libraries
from gaeqblib_broken.views import RestFormView, RestCrudView

# models
from activity.log.models import ProjectLog

# serializers
from activity.log.serializers import ProjectLogSerializer, ProjectLogFilterOverPeriodSerializer


class ProjectLogCrudView(RestCrudView):
    """
    Performs CRUD on project log
    """
    # TODO: Need-to-fix will not work because of make_query issues
    model = ProjectLog
    ordering = ['-loggedFor']
    serializer_class = ProjectLogSerializer
    permissions_required = {'get': 'STATUS_REPORT_PROJECT_READ'}


class ProjectLogFilterByProjectOverPeriodView(RestFormView):
    """
    Filter project log based on project
    """
    # ordering = ['-loggedFor']
    serializer_class = ProjectLogFilterOverPeriodSerializer
    permissions_required = {
        'get': ['STATUS_REPORT_PROJECT_READ', 'STATUS_REPORT_PROJECT_READ_SELF'],
        'post': ['STATUS_REPORT_PROJECT_READ', 'STATUS_REPORT_PROJECT_READ_SELF']
    }

    def serializer_valid(self, validated_data):
        """
        Fetch list of entities/records that is available
        """
        # prepare query
        q = ProjectLog.query().order(-ProjectLog.loggedFor)
        # filter query
        q = q.filter(ProjectLog.project == self.key)
        q = q.filter(ProjectLog.loggedFor >= validated_data.get('start_date')) if validated_data.get('start_date') else q
        q = q.filter(ProjectLog.loggedFor <= validated_data.get('end_date')) if validated_data.get('end_date') else q

        # prepare cursor
        cursor = ndb.Cursor(urlsafe=self.cursor)
        # fetch data
        items, next_curs, more = q.fetch_page(self.paginate_by, start_cursor=cursor)
        if more:
            next_c = next_curs.urlsafe()
        else:
            next_c = None

        # serialize data
        data = {'cursor': next_c}
        data['results'] = ProjectLogSerializer(items, many=True).data

        # generate response
        return Response(data, headers=self.get_headers())

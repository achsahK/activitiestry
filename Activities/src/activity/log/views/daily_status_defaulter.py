# user defined libraries
from gaeqblib_broken.views import RestCrudView

# models
from activity.log.models import DailyStatusDefaulter

# serializers
from activity.log.serializers import DailyStatusDefaulterSerializer


class DailyStatusDefaulterCrudView(RestCrudView):
    """
    Performs CRUD on DailyStatusDefaulter
    """
    model = DailyStatusDefaulter
    ordering = ['-lastModifiedOn']
    serializer_class = DailyStatusDefaulterSerializer
    permissions_required = {'get': 'STATUS_REPORT_DEFAULTER_LIST_VIEW'}

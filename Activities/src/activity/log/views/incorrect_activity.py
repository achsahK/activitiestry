# user defined libraries
from gaeqblib_broken.views import RestCrudView

# models
from activity.log.models import IncorrectActivity

# serializers
from activity.log.serializers import IncorrectActivitySerializer


class IncorrectActivityCrudView(RestCrudView):
    """
    Performs CRUD on IncorrectActivity
    """
    model = IncorrectActivity
    ordering = ['stream']
    serializer_class = IncorrectActivitySerializer
    permissions_required = {'get': 'INCORRECT_ACTIVITIES_VIEW'}

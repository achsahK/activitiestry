from space.common.models import Enum


class MessageStatus():
    APPROVED = 'APPROVED'
    PENDING = 'PENDING'
    REJECTED = 'REJECTED'


class DepartmentHandle(object):
    '''
    Department handles, basically hard-coded values we use in code.
    Be very careful while changing the values, these are defined in Space sub-system.
    '''
    DEVELOPMENT = u'DEVELOPMENT'
    QA = u'QA'
    HTML_CSS = u'HTML_CSS'
    GRAPHIC_DESIGN = u'GRAPHIC_DESIGN'
    BUSINESS_DEVELOPMENT = u'BUSINESS_DEVELOPMENT'
    PROJECT_MANAGEMENT = u'PROJECT_MANAGEMENT'
    TRAINING = u'TRAINING'
    SYS_ADMINISTRATION = u'SYS_ADMINISTRATION'
    USER_EXPERIENCE = u'USER_EXPERIENCE'
    HR = u'HR'
    OTHER = u'OTHER'


class ActivityTypeEnum(Enum):
    def __init__(self, *args, **kwargs):
        super(ActivityTypeEnum, self).__init__(*args, **kwargs)
        self.PROJECT_MANAGEMENT = u'Project management'
        self.TRAINING = u'Training'
        self.ARCHITECTING = u'Architecting'
        self.REQUIREMENTS_ANALYSIS = u'Requirements analysis'
        self.SYSTEM_DESIGN = u'System design'
        self.CODING = u'Coding'
        self.GRAPHIC_DESIGN = u'Graphic design'
        self.TESTING = u'Testing'
        self.HTML_CSS = u'HTML/CSS'
        self.PRE_SALES = u'Pre-sales'
        self.TECH_SUPPORT = u'Tech Support'
        self.UX_DESIGN = u'UX design'
        self.MARKETING = u'Marketing'
        self.BUSINESS_ANALYSIS = u'Business Analysis'
        self.RECRUITMENT_HR = 'Recruitment & HR'
        self.OTHER = u'Other'


class MessageType():
    END_OF_DAY = 'end-of-day'


ActivityType = ActivityTypeEnum()


ACTIVITY_TYPES = ActivityType.getValues()


ACTIVITY_TYPES_TUPLE = ActivityType.getValuesAsTuple()


ACTIVITY_TYPE_DEPARTMENT_MAP = {
    DepartmentHandle.DEVELOPMENT: ActivityType.CODING,
    DepartmentHandle.QA: ActivityType.TESTING,
    DepartmentHandle.HTML_CSS: ActivityType.HTML_CSS,
    DepartmentHandle.GRAPHIC_DESIGN: ActivityType.GRAPHIC_DESIGN,
    DepartmentHandle.BUSINESS_DEVELOPMENT: ActivityType.PRE_SALES,
    DepartmentHandle.PROJECT_MANAGEMENT: ActivityType.PROJECT_MANAGEMENT,
    DepartmentHandle.SYS_ADMINISTRATION: ActivityType.TECH_SUPPORT,
    DepartmentHandle.USER_EXPERIENCE: ActivityType.UX_DESIGN,
    DepartmentHandle.TRAINING: ActivityType.TRAINING,
    DepartmentHandle.HR: ActivityType.RECRUITMENT_HR
}

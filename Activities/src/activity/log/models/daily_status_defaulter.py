# app engine libraries
from google.appengine.ext import db

# user defined libraries
from gaeqblib_broken.models import Model
from gaeqblib_broken.properties.db import JSONProperty


class DailyStatusDefaulter(Model, db.Model):
    """
    Model defining daily status defaulter
    """
    employeeKey = db.StringProperty()
    employeeHyperLink = db.StringProperty(indexed=False)
    allocations = JSONProperty(indexed=False)
    daysDefaulted = db.IntegerProperty()
    lastModifiedOn = db.DateProperty(auto_now=True)

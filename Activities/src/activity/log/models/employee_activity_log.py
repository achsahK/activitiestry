# app engine libraries
from google.appengine.ext import ndb, db


class EmployeeActivityLog(ndb.Model):
    """
    Model defining employee activity log
    """
    employee = ndb.StringProperty()
    projects = ndb.StringProperty(repeated=True)
    hoursBurnedAgainstProjects = ndb.FloatProperty(default=0.0)
    groupedByProject = ndb.JsonProperty()
    hoursBurned = ndb.FloatProperty(default=0.0)
    loggedFor = ndb.DateProperty()
    activities = ndb.KeyProperty(repeated=True, indexed=False)
    groupedByActivityType = ndb.JsonProperty()
    lastModifiedOn = ndb.DateProperty(auto_now=True)

    @property
    def hoursBurnedWithoutProject(self):
        """
        Get the number of hours the employee burned against without projects.
        Basically hoursBurned - hoursBurnedAgainstProjects
        @return: float
        """
        return self.hoursBurned - self.hoursBurnedAgainstProjects

    def clone_with_key(self, key_name):
        """
        Create a new project log instance that is the clone of the old one with the new proper key

        :param key_name: The key name to use for the new clone
        """
        return EmployeeActivityLog(
            id = key_name,
            employee = self.employee,
            projects = self.projects,
            groupedByActivityType = self.groupedByActivityType,
            groupedByProject = self.groupedByProject,
            hoursBurnedAgainstProjects = self.hoursBurnedAgainstProjects,
            hoursBurned = self.hoursBurned,
            loggedFor = self.loggedFor,
            activities = [ndb.Key.from_old_key(activity_key) if isinstance(activity_key, db.Key) else activity_key for activity_key in  list(set(self.activities))],
            lastModifiedOn = self.lastModifiedOn,
        )

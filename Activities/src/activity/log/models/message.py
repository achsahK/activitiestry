# app engine libraries
from google.appengine.ext import db

# user defined libraries
from gaeqblib_broken.models import Model


class Message(Model, db.Expando):
    """
    Current Expando Properties for Project Activities
    project
    timeSpent
    displayTimeSpent
    postedFor
    status
    activityType
    department
    designation
    grade
    Note: If you add more fields to this model, it will cause attribute error. In such situation we need to update and
    run the `fix_missing_attributes` remote script to resolve the situation
    """
    user = db.StringProperty()
    postedBy = db.StringProperty()
    postedOn = db.DateTimeProperty(auto_now_add=True)
    message = db.TextProperty()
    type = db.StringProperty()
    isProcessed = db.BooleanProperty(default=False)


class ExportFields(object):
    EMPLOYEE = 'EMPLOYEE'
    TASKS = 'TASKS'

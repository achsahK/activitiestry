# app engine libraries
from google.appengine.ext import db

# user defined libraries
from gaeqblib_broken.models import Model
from gaeqblib_broken.properties.db import JSONProperty


class IncorrectActivity(Model, db.Model):
    """
    Model defining incorrect activity
    """
    employee = db.StringProperty()
    employeeHyperLink = db.StringProperty(indexed=False)
    stream = db.StringProperty()  # stream of the employee
    allocations = JSONProperty(indexed=False)
    projectKeys = db.StringListProperty()  # to which the incorrect entries were made
    projectHyperlinks = db.StringListProperty(indexed=False)
    statusMessages = JSONProperty(indexed=False)
    createdOn = db.DateProperty(auto_now=True)

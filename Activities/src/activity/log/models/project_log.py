# app engine libraries
from google.appengine.ext import ndb, db


class ProjectLog(ndb.Model):
    """
    Model defining project log
    """
    project = ndb.StringProperty()
    employees = ndb.StringProperty(repeated=True)
    groupedByGrade = ndb.JsonProperty()
    groupedByEmployee = ndb.JsonProperty()
    groupedByDepartment = ndb.JsonProperty()
    actualHoursBurned = ndb.FloatProperty(default=0.0)
    hoursBurned = ndb.FloatProperty(default=0.0)
    loggedFor = ndb.DateProperty()
    activities = ndb.KeyProperty(repeated=True, indexed=False)
    groupedByActivityType = ndb.JsonProperty()
    lastModifiedOn = ndb.DateProperty(auto_now=True)

    def clone_with_key(self, key_name):
        """
        Create a new project log instance that is the clone of the old one with the new proper key

        :param key_name: The key name to use for the new clone
        """
        return ProjectLog(
            id = key_name,
            project = self.project,
            employees = self.employees,
            groupedByGrade = self.groupedByGrade,
            groupedByEmployee = self.groupedByEmployee,
            groupedByDepartment = self.groupedByDepartment,
            actualHoursBurned = self.actualHoursBurned,
            hoursBurned = self.hoursBurned,
            loggedFor = self.loggedFor,
            activities = [ndb.Key.from_old_key(activity_key) if isinstance(activity_key, db.Key) else activity_key for activity_key in  list(set(self.activities))],
            groupedByActivityType = self.groupedByActivityType,
            lastModifiedOn = self.lastModifiedOn,
        )

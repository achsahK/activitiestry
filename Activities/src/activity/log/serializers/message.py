# python libraries
import six
from collections import OrderedDict

# django libraries
from django.conf import settings

# django rest libraries
from rest_framework import serializers

# user defined libraries
from gaeqblib_broken.serializers import RestSerializer
from gaeqblib_broken.properties.django_rest import DateTimeField, DateChoiceField
from gaeqblib_broken.utilities.functions import get_current_datetime, get_future_datetime, get_date_range_tuple

# models
from activity.log.models import MessageStatus, MessageType, ActivityType


def get_posted_for_choices():
    """
    Function to generate date choices for postedFor
    """
    end_date = get_current_datetime().date()
    start_date = get_future_datetime(days=-settings.DEFAULT_NUMBER_OF_DAYS_FOR_ACTIVITY).date()
    date_choices = get_date_range_tuple(start_date, end_date)
    return date_choices


class MessageSerializer(RestSerializer):
    """
    Serializer to perform CRUD on Message
    """
    def __init__(self, *args, **kwargs):
        super(MessageSerializer, self).__init__(*args, **kwargs)

        # update initial for postedFor
        self.fields['postedFor'].initial = get_current_datetime().strftime(settings.DEFAULT_DATE_FORMAT)

        # to do: Need to find a proper solution. We tried `source`, but not working as expected
        # update choices for postedFor
        self.fields['postedFor'].choices = OrderedDict([(x, x) for x in get_posted_for_choices()])
        self.fields['postedFor'].choice_strings_to_values = dict([
            (six.text_type(key), key) for key in self.fields['postedFor'].choices.keys()
        ])

    key = serializers.CharField(read_only=True, source='safe_key')
    user = serializers.CharField()
    postedBy = serializers.CharField()
    postedOn = DateTimeField(read_only=True)
    message = serializers.CharField(max_length=180)
    type = serializers.CharField(default=MessageType.END_OF_DAY, initial=MessageType.END_OF_DAY)
    displayTimeSpent = serializers.SerializerMethodField(read_only=True, method_name='get_display_time_spent')
    project = serializers.CharField(allow_blank=True)
    timeSpent = serializers.FloatField()
    postedFor = DateChoiceField(choices=[])
    status = serializers.CharField(default=MessageStatus.PENDING, initial=MessageStatus.PENDING)
    activityType = serializers.ChoiceField(choices=ActivityType.getValues(), initial=ActivityType.CODING)
    department = serializers.CharField()
    designation = serializers.CharField()
    grade = serializers.CharField()

    def validate(self, data):

        # imports
        from utils import find_and_replace_red_mine_link, convert_minute_to_calculatable_value

        # update time spent
        minutes = convert_minute_to_calculatable_value(str(data['timeSpent']).split('.')[1])
        data['timeSpent'] = float('%s.%s' % (str(data['timeSpent']).split('.')[0], minutes))

        # replace redmine tickets with redmine links
        data['message'] = find_and_replace_red_mine_link(data['message'])

        return data

    def get_display_time_spent(self, obj):
        """
        Function to generate display time spent on the fly
        """
        from utils import convert_calculatable_value_to_minute

        ts = float(obj.timeSpent) % 1

        minutes = convert_calculatable_value_to_minute(int(ts * 100))
        display_time_spent = float('%s.%s' % (int(float(obj.timeSpent)), minutes))

        return ':'.join(('%.2f' % display_time_spent).split('.'))


class MessageFilterSerializer(RestSerializer):
    """
    Serializer to filter messages
    """
    type = serializers.CharField(required=False)
    date = serializers.DateField(required=False)
    project = serializers.CharField(required=False)
    employee = serializers.CharField(required=False)
    department = serializers.CharField(required=False)


class MessageFilterOverPeriodSerializer(RestSerializer):
    """
    Serializer to filter project logs over a period
    """
    start_date = serializers.DateField(required=False)
    end_date = serializers.DateField(required=False)
    export_fields = serializers.CharField(required=False)
    emp_key = serializers.CharField(required=False)
    proj_name = serializers.CharField(required=False)

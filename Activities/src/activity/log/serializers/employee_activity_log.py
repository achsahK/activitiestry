# django rest libraries
from rest_framework import serializers

# user defined libraries
from gaeqblib_broken.serializers import RestSerializer
from gaeqblib_broken.properties.django_rest import ListField, JSONField

# models
from activity.log.models import Message

# serializers
from message import MessageSerializer


class EmployeeActivityLogSerializer(RestSerializer):
    """
    Serializer to perform CRUD on EmployeeActivityLog
    """
    key = serializers.CharField(read_only=True, source='safe_key')
    employee = serializers.CharField()
    projects = serializers.ListField(child=serializers.CharField())
    hoursBurnedAgainstProjects = serializers.FloatField(default=0.0)
    hoursBurnedWithoutProject = serializers.FloatField(default=0.0)
    groupedByProject = JSONField()
    hoursBurned = serializers.FloatField(default=0.0)
    loggedFor = serializers.DateField()
    activities = ListField(model=Message, serializer=MessageSerializer, hide_choices=True, write_only=True)
    groupedByActivityType = JSONField()
    lastModifiedOn = serializers.DateField()


class EmployeeActivityFilterOverPeriodSerializer(RestSerializer):
    """
    Serializer to filter employee activity logs over a period
    """
    start_date = serializers.DateField(required=False)
    end_date = serializers.DateField(required=False)

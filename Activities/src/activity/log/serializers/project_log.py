# django rest libraries
from rest_framework import serializers

# user defined libraries
from gaeqblib_broken.serializers import RestSerializer
from gaeqblib_broken.properties.django_rest import ListField, JSONField

# models
from activity.log.models import Message

# serializers
from message import MessageSerializer


class ProjectLogSerializer(RestSerializer):
    """
    Serializer to perform CRUD on project log
    """
    key = serializers.CharField(read_only=True, source='safe_key')
    project = serializers.CharField()
    employees = serializers.ListField(child=serializers.CharField())
    groupedByGrade = JSONField()
    groupedByEmployee = JSONField()
    groupedByDepartment = JSONField()
    actualHoursBurned = serializers.FloatField(default=0.0)
    hoursBurned = serializers.FloatField(default=0.0)
    loggedFor = serializers.DateField()
    activities = ListField(model=Message, serializer=MessageSerializer, label='activities', hide_choices=True,
                           write_only=True)
    groupedByActivityType = JSONField()
    lastModifiedOn = serializers.DateField(required=False)


class ProjectLogFilterOverPeriodSerializer(RestSerializer):
    """
    Serializer to filter project logs over a period
    """
    start_date = serializers.DateField(required=False)
    end_date = serializers.DateField(required=False)

# django rest libraries
from rest_framework import serializers

# user defined libraries
from gaeqblib_broken.serializers import RestSerializer
from gaeqblib_broken.properties.django_rest import JSONField


class IncorrectActivitySerializer(RestSerializer):
    """
    Serializer to perform CRUD on IncorrectActivity
    """
    key = serializers.CharField(read_only=True, source='safe_key')
    employee = serializers.CharField()
    employeeHyperLink = serializers.CharField()
    stream = serializers.CharField()
    allocations = JSONField()
    projectKeys = serializers.ListField(child=serializers.CharField())
    projectHyperlinks = serializers.ListField(child=serializers.URLField())
    statusMessages = JSONField()

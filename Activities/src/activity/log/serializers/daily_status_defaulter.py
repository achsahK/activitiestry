# django rest libraries
from rest_framework import serializers

# user defined libraries
from gaeqblib_broken.serializers import RestSerializer
from gaeqblib_broken.properties.django_rest import JSONField


class DailyStatusDefaulterSerializer(RestSerializer):
    """
    Serializer to perform CRUD on DailyStatusDefaulter
    """
    key = serializers.CharField(read_only=True, source='safe_key')
    employeeKey = serializers.CharField()
    employeeHyperLink = serializers.URLField()
    allocations = JSONField()
    daysDefaulted = serializers.IntegerField(default=0)
    lastModifiedOn = serializers.DateField()

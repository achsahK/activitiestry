# app engine library
from protorpc import messages

# user defined libraries
from space.common.services.messages import ErrorMessage


# messages associated with `save`


class DailyStatusPostRequest(messages.Message):
    message = messages.StringField(1, required=True)
    project = messages.StringField(2, required=True)
    timeSpent_hours = messages.StringField(3, required=True)
    timeSpent_minutes = messages.StringField(4, required=True)
    postedFor = messages.StringField(5, required=True)
    activityType = messages.StringField(6, required=True)
    department = messages.StringField(7, required=True)
    designation = messages.StringField(8, required=True)
    grade = messages.StringField(9, required=True)


class DailyStatusPostResponse(messages.Message):
    isSaved = messages.BooleanField(1)
    error = messages.MessageField(ErrorMessage, 2, required=False, repeated=False)


# messages associated with `read`


class DailyStatus(messages.Message):
    message = messages.StringField(1, required=True)
    project = messages.StringField(2, required=True)
    displayTimeSpent = messages.StringField(3, required=True)
    date = messages.StringField(4, required=True)
    activityType = messages.StringField(5, required=True)
    messageType = messages.StringField(6, required=True)
    postedOn = messages.StringField(7, required=True)


class DailyStatusReadResponse(messages.Message):
    dailyStatuses = messages.MessageField(DailyStatus, 1, repeated=True)
    error = messages.MessageField(ErrorMessage, 2, required=False, repeated=False)


# messages associated with `getActivityTypes`


class ActivityType(messages.Message):
    key = messages.StringField(1, required=True)
    value = messages.StringField(2, required=True)


class ActivityTypeResponse(messages.Message):
    activityTypes = messages.MessageField(ActivityType, 1, repeated=True)
    error = messages.MessageField(ErrorMessage, 2, required=False, repeated=False)

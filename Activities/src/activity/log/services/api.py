# python libraries
import json

# app engine libraries
import endpoints
from protorpc import remote, message_types

# django libraries
from django.conf import settings
from django.http import HttpRequest

# user defined libraries
from gaeqblib_broken.middlewares import Authentication
from gaeqblib_broken.utilities.functions import get_current_datetime, format_datetime
from space.common.services.messages import ErrorMessage, EndpointsErrorCode

# endpoint request/response messages
from messages import DailyStatus, DailyStatusPostRequest, DailyStatusPostResponse, DailyStatusReadResponse,\
    ActivityType, ActivityTypeResponse

# serializers
from activity.log.serializers import MessageSerializer

# models
from activity.log.models import Message, MessageType, MessageStatus, ACTIVITY_TYPES

# utils
from utils import convert_minute_to_calculatable_value


@endpoints.api(
    name='dailyStatus', version='v1', description='API for posting daily Status',
    audiences=settings.ENDPOINTS['DailyStatus']['audiences'],
    allowed_client_ids=settings.ENDPOINTS['DailyStatus']['allowed_client_ids'])
class DailyStatusService(remote.Service):

    @endpoints.method(
        DailyStatusPostRequest, DailyStatusPostResponse, name='save', path='save', http_method='POST',
        scopes=settings.ENDPOINTS['DailyStatus']['scopes'], audiences=settings.ENDPOINTS['DailyStatus']['audiences'],
        allowed_client_ids=settings.ENDPOINTS['DailyStatus']['allowed_client_ids']
    )
    def save(self, request):
        """
        Method to save the daily status posted by logged in employee.
        """
        # validate user and get privileges
        django_request = HttpRequest()
        django_request.path = '/'
        django_request.method = 'GET'
        django_auth_response = Authentication().process_request(django_request)
        if django_auth_response:
            raise endpoints.UnauthorizedException('Login to continue')  # authentication failed

        # check privileges
        if not (django_request.user.is_admin or
                settings.ENDPOINTS['DailyStatus']['permissions']['save'] in django_request.privileges):
            raise endpoints.UnauthorizedException('You do not have privilege to access the content')

        # build and validate serializer
        time_spent = float('%s.%s' % (request.timeSpent_hours, convert_minute_to_calculatable_value(request.timeSpent_minutes)))
        data_dict = {
            'user': django_request.employee['key'],
            'postedBy': django_request.employee['key'],
            'message': request.message,
            'type': MessageType.END_OF_DAY,
            'project': request.project,
            'timeSpent': time_spent,
            'postedFor': request.postedFor,
            'status': MessageStatus.PENDING,
            'activityType': request.activityType,
            'department': request.department,
            'designation': request.designation,
            'grade': request.grade
        }
        serializer = MessageSerializer(data=data_dict)

        # save valid data
        if serializer.is_valid():
            Message(**serializer.validated_data).save()  # save
            return DailyStatusPostResponse(isSaved=True)

        # return errors in case of invalid data
        return DailyStatusPostResponse(
            isSaved=False,
            error=ErrorMessage(
                code=EndpointsErrorCode.FORM_ERROR,
                message=json.dumps(serializer.errors),
                reference=format_datetime(get_current_datetime())
            )
        )

    @endpoints.method(
        message_types.VoidMessage, DailyStatusReadResponse, name='read', path='read', http_method='GET',
        scopes=settings.ENDPOINTS['DailyStatus']['scopes'], audiences=settings.ENDPOINTS['DailyStatus']['audiences'],
        allowed_client_ids=settings.ENDPOINTS['DailyStatus']['allowed_client_ids'],
    )
    def read(self, request):
        """
        Method to fetch the recent daily status updates made by logged in employee.
        """
        # validate user and get privileges
        django_request = HttpRequest()
        django_request.path = '/'
        django_request.method = 'GET'
        django_auth_response = Authentication().process_request(django_request)
        if django_auth_response:
            raise endpoints.UnauthorizedException('Login to continue')  # authentication failed

        # check privileges
        if not (django_request.user.is_admin or
                settings.ENDPOINTS['DailyStatus']['permissions']['read'] in django_request.privileges):
            raise endpoints.UnauthorizedException('You do not have privilege to access the content')

        # fetch messages of logged in user
        user_filter = {'property': 'user', 'operator': '=', 'value': django_request.employee['key']}
        messages = Message.make_query(filters=[user_filter], limit=8)['results']

        # prepare messages
        messages_list = []
        for message in messages:
            messages_list.append(
                DailyStatus(
                    message=message.message,
                    project=message.project,
                    displayTimeSpent=message.displayTimeSpent if hasattr(message, 'displayTimeSpent') else '%.2f' % message.timeSpent,
                    date=format_datetime(message.postedFor),
                    activityType=message.activityType,
                    messageType=message.type,
                    postedOn=format_datetime(message.postedOn)
                )
            )

        # return response
        return DailyStatusReadResponse(dailyStatuses=messages_list)

    @endpoints.method(
        message_types.VoidMessage, ActivityTypeResponse, name='getActivityTypes', path='activityTypes',
        http_method='GET', scopes=settings.ENDPOINTS['DailyStatus']['scopes'],
        audiences=settings.ENDPOINTS['DailyStatus']['audiences'],
        allowed_client_ids=settings.ENDPOINTS['DailyStatus']['allowed_client_ids'],
    )
    def getActivityTypes(self, request):
        """
        Method to fetch activity types from Space.
        """
        # validate user and get privileges
        django_request = HttpRequest()
        django_request.path = '/'
        django_request.method = 'GET'
        django_auth_response = Authentication().process_request(django_request)
        if django_auth_response:
            raise endpoints.UnauthorizedException('Login to continue')  # authentication failed

        # check privileges
        if not (django_request.user.is_admin or
                settings.ENDPOINTS['DailyStatus']['permissions']['getActivityTypes'] in django_request.privileges):
            raise endpoints.UnauthorizedException('You do not have privilege to access the content')

        # return response
        return ActivityTypeResponse(activityTypes=map(lambda x: ActivityType(key=x, value=x), ACTIVITY_TYPES[:]))

# python libraries
import gc
import json


# django libraries
from django.conf import settings

# user defined libraries
from gaeqblib_broken.workers import Task
from gaeqblib_broken.utilities.functions import get_from_url, chunks

# models
from activity.log.models import Message, IncorrectActivity

# constants
CHUNK_SIZE = 50


class Utilities(object):
    @staticmethod
    def get_employee_hyper_links(employee_keys, employee_details):
        """
        Get employee hyperlink from employee details
        """
        return_list = []
        if not isinstance(employee_keys, list):
            employee_keys = [employee_keys]
        for employee_key in employee_keys:
            return_list.append(employee_details[employee_key]['hyperlink'])
        return return_list

    @staticmethod
    def get_project_hyper_links(project_keys, project_details):
        """
        Get project hyperlink from project details
        """
        return_list = []
        if not isinstance(project_keys, list):
            project_keys = [project_keys]
        for project_key in project_keys:
            return_list.append(project_details[project_key]['hyperlink'])
        return return_list

    @staticmethod
    def get_employee_details_from_space(employee_keys):
        """
        fetch employee details from space
        """
        employee_details = []
        if type(employee_keys) is not list:
            employee_keys = [employee_keys]
        for employee_keys_chunk in chunks(employee_keys, CHUNK_SIZE):
            employee_details.extend(
                get_from_url(
                    url=settings.SPACE_APIS['employee_details'],
                    data={'args': json.dumps(employee_keys_chunk)},
                    user=settings.SCRIPT_USER
                )
            )
        return employee_details

    @staticmethod
    def get_project_details_from_space(project_keys):
        """
        fetch project details from space
        """
        project_details = []
        if type(project_keys) is not list:
            project_keys = [project_keys]
        for project_keys_chunk in chunks(project_keys, CHUNK_SIZE):
            project_details.extend(
                get_from_url(
                    url=settings.SPACE_APIS['project_details'],
                    data={'args': json.dumps(project_keys_chunk)},
                    user=settings.SCRIPT_USER
                )
            )
        return project_details

    @staticmethod
    def get_employee_allocations_from_space(employee_keys):
        """
        fetch employee project allocation details from space
        """
        employee_allocation_details = []
        if type(employee_keys) is not list:
            employee_keys = [employee_keys]
        for employee_keys_chunk in chunks(employee_keys, CHUNK_SIZE):
            employee_allocation_details.extend(
                get_from_url(
                    url=settings.SPACE_APIS['employee_allocations'],
                    data={'args': json.dumps(employee_keys_chunk)},
                    user=settings.SCRIPT_USER
                )
            )
        return employee_allocation_details


class GenerateIncorrectActivitiesReportTask(Task):
    """
    Task to generate incorrect activities report
    """
    employee_details = {}
    project_details = {}
    invalid_messages_keys = []
    employee_wise_messages_dict = {}

    def prepare_employee_wise_messages_dict(self, messages):
        """
        Function to prepare employee wise messages dictionary from invalid messages passed
        """
        for message in messages:

            # create if not exists, append if already exists
            if message.user in self.employee_wise_messages_dict.keys():

                # create if not exists, append if already exists
                if message.project in self.employee_wise_messages_dict.get(message.user).keys():
                    self.employee_wise_messages_dict[message.user][message.project] += (message.to_dict(),)
                else:
                    self.employee_wise_messages_dict[message.user][message.project] = (message.to_dict(),)
            else:
                self.employee_wise_messages_dict[message.user] = {message.project: (message.to_dict(),)}

        # structure of self.employee_wise_messages_dict at this point of time
        #
        # {
        #     <employeee-1>: {
        #                      <project-1>: (message-dict-1, message-dict-2, ...),
        #                      <project-2>: (message-dict-1, message-dict-2, ...),
        #                      ..................................................
        #                    },
        #     <employeee-2>: {
        #                      <project-1>: (message-dict-1, message-dict-2, ...),
        #                      <project-2>: (message-dict-1, message-dict-2, ...),
        #                      ..................................................
        #                    },
        #     ...................................................................
        # }

    def chop_employee_wise_messages_dict_to_chunks(self):
        """
        split employee wise message dictionary into chunks in order tp limit memory usage
        """
        temp = {}
        counter = 0
        employee_wise_messages_dict_chunks = []
        for employee, messages in self.employee_wise_messages_dict.iteritems():
            temp[employee] = messages
            counter += 1
            if counter >= 50:
                employee_wise_messages_dict_chunks.append(temp)
                temp = {}
        if temp:
            employee_wise_messages_dict_chunks.append(temp)
        self.employee_wise_messages_dict = employee_wise_messages_dict_chunks

    def set_employee_details(self, employee_keys):
        """
        Fetch employee details from space that is not present in employee_details dict
        """
        employees_to_be_fetched = []
        for employee_key in employee_keys:
            if employee_key not in self.employee_details.keys():
                employees_to_be_fetched.append(employee_key)
        employee_details = Utilities.get_employee_details_from_space(employees_to_be_fetched)
        for employee_detail in employee_details:
            self.employee_details[employee_detail['key']] = employee_detail

    def set_project_details(self, project_keys):
        """
        Fetch project details from space that is not present in project_details dict
        """
        projects_to_be_fetched = []
        for project_key in project_keys:
            if project_key not in self.project_details.keys():
                projects_to_be_fetched.append(project_key)
        project_details = Utilities.get_project_details_from_space(projects_to_be_fetched)
        for project_detail in project_details:
            self.project_details[project_detail['key']] = project_detail

    def run(self, request, *args, **kwargs):
        """
        We have the invalid messages. So we collect associated employee and project details and prepare
        incorrect activity object
        """
        employee_keys = set()
        project_keys = set()

        # get invalid message objects
        self.invalid_messages_keys = request.POST.get('payload', [])
        if type(self.invalid_messages_keys) is not list:
            self.invalid_messages_keys = [self.invalid_messages_keys]
        self.logging.info('Payload size %s' % len(self.invalid_messages_keys))

        # prepare employee wise message dictionary
        self.logging.info('Preparing employee wise message dict')
        self.prepare_employee_wise_messages_dict(Message.get_multi(self.invalid_messages_keys))
        gc.collect()

        # chop employee wise message dictionary into small chunks
        self.logging.info('Chopping employee wise dict into small chunks')
        self.chop_employee_wise_messages_dict_to_chunks()
        gc.collect()

        # process employee messages
        incorrect_activities = []
        self.logging.info('Starting to process employee messages')
        for employee_wise_messages in self.employee_wise_messages_dict:

            employee_keys.update(employee_wise_messages.keys())

            for employee_key, project_wise_messages in employee_wise_messages.iteritems():

                project_keys.update(project_wise_messages.keys())

                # update employee details (from project details)
                pm_list = []
                for project in project_wise_messages.keys():
                    if self.project_details.get(project):
                        pm_list.extend(self.project_details[project]['projectManagerKeyList'])
                employee_keys.update(pm_list)

                # get project allocations of employee
                employee_allocations = Utilities.get_employee_allocations_from_space(employee_key)

                # update project details (from employee allocations)
                project_keys.update([allocation['project'] for allocation in employee_allocations])

                # update project details
                self.set_project_details(project_keys)

                # update employee details (from employee allocations)
                pm_list = []
                for allocation in employee_allocations:
                    if self.project_details.get(allocation['project']):
                        pm_list.extend(self.project_details[allocation['project']]['projectManagerKeyList'])
                employee_keys.update(pm_list)

                # update employee details
                self.set_employee_details(employee_keys)

                # prepare incorrect activity object
                allocations = []
                for allocation in employee_allocations:
                    allocated_project_details = self.project_details.get(allocation['project'])
                    allocations.append({
                        'projectKey': allocated_project_details['key'],
                        'projectHyperLink': allocated_project_details['hyperlink'],
                        'PMKey': allocated_project_details['projectManagerKeyList'],
                        'PMHyperlink': Utilities.get_employee_hyper_links(
                            allocated_project_details['projectManagerKeyList'], self.employee_details),
                        'allocationPrecentage': allocation['allocation']
                    })

                # prepare incorrect activity object
                incorrect_activities.append(
                    IncorrectActivity(
                        employee=str(employee_key),
                        employeeHyperLink=self.employee_details[employee_key]['hyperlink'],
                        stream=self.employee_details[employee_key]['stream'],
                        allocations=allocations,
                        projectKeys=project_wise_messages.keys(),
                        projectHyperlinks=Utilities.get_project_hyper_links(project_wise_messages.keys(),
                                                                            self.project_details),
                        statusMessages=project_wise_messages,
                    )
                )

                # interim save - incorrect activities
                if len(incorrect_activities) >= 100:
                    IncorrectActivity.save_multi(incorrect_activities)
                    incorrect_activities = []

        # save pending incorrect activities
        if incorrect_activities:
            IncorrectActivity.save_multi(incorrect_activities)
            incorrect_activities = []

        # clean up memory
        gc.collect()

    def error_handler(self, request, *args, **kwargs):
        """
        Error handler
        """
        pass

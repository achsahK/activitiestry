# django libraries
from django.conf.urls import url

# views
import views

urlpatterns = [
    # message
    url(r'^message(/(?P<key>[a-zA-Z0-9-_]+))?$', views.MessageCrudView.as_view(), name='message_crud'),
    url(r'^message/filter/user-input$', views.MessagesFilterByInputView.as_view(), name='message_filter_by_input'),
    url(r'^message/filter/employee/(?P<key>[a-zA-Z0-9-_]+)$', views.MessageFilterByEmployeeOverPeriodView.as_view(),
        name='message_filter_by_employee_over_period'),
    url(r'^message/filter/project/(?P<key>[a-zA-Z0-9-_]+)$', views.MessageFilterByProjectOverPeriodView.as_view(),
        name='message_filter_by_project_over_period'),
    url(r'^message/project-time-sheet-data/(?P<key>[a-zA-Z0-9-_]+)$', views.SendProjectTimeSheetExportView.as_view(),
            name='project_time_sheet_data'),

    # project log
    # TODO: Need-to-fix will not work because of make_query issues
    url(r'^project-log(/(?P<key>[a-zA-Z0-9-_]+))?$', views.ProjectLogCrudView.as_view(), name='project_log_crud'),
    url(r'^project-log/filter/project/(?P<key>[a-zA-Z0-9-_]+)$', views.ProjectLogFilterByProjectOverPeriodView.as_view(),
        name='project_log_filter_by_project_over_period'),

    # incorrect activity
    url(r'^incorrect-activity(/(?P<key>[a-zA-Z0-9-_]+))?$', views.IncorrectActivityCrudView.as_view(),
        name='incorrect_activity_crud'),

    # employee activity log
    # TODO: Need-to-fix will not work because of make_query class
    url(r'^employee-activity-log(/(?P<key>[a-zA-Z0-9-_]+))?$', views.EmployeeActivityLogCrudView.as_view(),
        name='employee_activity_log_crud'),
    url(r'^employee-activity-log/filter/employee/(?P<key>[a-zA-Z0-9-_]+)$',
        views.EmployeeActivityLogFilterByEmployeeOverPeriodView.as_view(),
        name='employee_activity_log_filter_by_employee_over_period'),

    # daily status defaulter
    url(r'^daily-status-defaulter(/(?P<key>[a-zA-Z0-9-_]+))?$', views.DailyStatusDefaulterCrudView.as_view(),
        name='daily_status_defaulter_crud'),
]

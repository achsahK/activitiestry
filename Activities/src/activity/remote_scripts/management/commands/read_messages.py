# django libraries
from django.core.management.base import BaseCommand

# user defined libraries
from gaeqblib_broken.remote_script import RemoteScriptMixin

# models
from activity.log.models import Message


class Command(RemoteScriptMixin, BaseCommand):
    """
    Example remote script to read messages from activities
    """
    app_url = 'qburst-activities-staging.appspot.com'
    remote_api_uri = '/remote_api'

    def handle(self, *args, **kwargs):
        """
        Remote script
        """
        for message in Message.all():
            print message.__dict__
            break

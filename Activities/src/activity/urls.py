# django libraries
from django.conf.urls import include, url

# user defined libraries
from gaeqblib_broken.oauth2.urls import oauth2_urlpatterns
# from gaeqblib_broken.views.key_object_api import KeyObjectApi
from gaeqblib_broken.views.clear_memcache import ClearMemcacheApi
from gaeqblib_broken.workers import WorkerDispatcher
from gaeqblib_broken.views import page_not_found, internal_server_error, holding_page, partial_holding_page

# views
from views import HomeView
from activity.old_code_compatibility.api import ApiView

urlpatterns = [
    # Home
    url(r'^$', HomeView.as_view(), name='home'),

    # Log app
    url(r'^', include('activity.log.urls')),

    # Error pages
    url(r'^page-not-found$', page_not_found, name='page_not_found'),
    url(r'^holding-page$', holding_page, name='holding_page'),
    url(r'^partial-holding-page$', partial_holding_page, name='partial_holding_page'),
    url(r'^internal-server-error$', internal_server_error, name='internal_server_error'),

    # Application level APIs
    # url(r'^key-object-api$', KeyObjectApi.as_view(), name='key_object_api'),
    url(r'^clear-memcache/(?P<key>.+)$', ClearMemcacheApi.as_view(), name='clear_memcache'),
    url(r'^worker-dispatcher/(?P<worker_class>[a-zA-z\.]+)$', WorkerDispatcher.as_view(), name='worker_dispatcher'),

    # Old code compatibility
    url(r'^api(/(?P<func>[a-zA-Z0-9_]+))?$', ApiView.as_view(), name='api'),

    # Update DB scripts
    url(r'^update-db/', include('activity.update_db_scripts.urls')),
]

# add oauth2 urls
urlpatterns += oauth2_urlpatterns

# 404 error page handler
handler404 = 'gaeqblib_broken.views.page_not_found'

# 500 error page handler
handler500 = 'gaeqblib_broken.views.internal_server_error'

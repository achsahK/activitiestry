# python libraries
import json
import logging

# django libraries
from django.http import HttpResponse
from django.views.generic import View
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

# app engine libraries
from google.appengine.api import memcache, users
from google.appengine.ext import db, ndb

# user defined libraries
from gaeqblib_broken.oauth2.models import Oauth2Credentials
from gaeqblib_broken.utilities.functions import get_date_from_string

# models
from activity.log.models import ProjectLog, EmployeeActivityLog, Message, MessageType

# serializers
from activity.log.serializers import EmployeeActivityLogSerializer, MessageSerializer


class ApiView(View):
    """
    Base class for old APIs
    """
    func = None
    api_args = None

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ApiView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):

        # parse params
        api_args = request.POST.get('args')
        api_args = json.loads(api_args)
        if kwargs.get('func'):
            func = kwargs.get('func')
        else:
            func, api_args = api_args[0], api_args[1:]

        # logging
        logging.debug('Requested API : %s' % func)
        logging.debug('Payload: %s' % api_args)

        # identify api method
        api_methods = APIMethods()
        func = getattr(api_methods, func, None)

        # reply with 404 if api method is not found
        if not func:
            return HttpResponse(status=404)

        # evaluate function
        result = func(*api_args, request=request)

        # generate response
        return HttpResponse(json.dumps(result), status=200)


class APIMethods(object):

    def clearEmployeePrivilegesInMemcache(self, *args, **kwargs):
        """
        Method to clear the privilege for employees in memcache with a particular role
        """
        request_obj = kwargs.get('request')
        if request_obj.user.is_admin:
            logging.info('Clearing memcached privileges from activities')
            if memcache.delete_multi(keys=args[0], key_prefix='Privileges_'):
                return True
        return False

    def getEmployeeLogsForAPeriod(self, employee_key, start_date, end_date, *args, **kwargs):
        """
        An API handler method to get the activity logs of an employee in a particular period.

        @param employee_key: String value of the key of employee instance
        @param start_date: date from which hours have to be calculated
        @param end_date: date up to which hours have to be calculated
        @return: Employee logs for the period
        @author:  Jayakrishnan Damodaran
        """
        # prepare params
        start_date = get_date_from_string(start_date)
        end_date = get_date_from_string(end_date)

        # fetch logs
        employee_filter = {'property': 'employee', 'operator': '=', 'value': employee_key}
        start_date_filter = {'property': 'loggedFor', 'operator': '>=', 'value': start_date}
        end_date_filter = {'property': 'loggedFor', 'operator': '<=', 'value': end_date}
        logs_query = EmployeeActivityLog.query()
        logs_query = logs_query.filter(ndb.GenericProperty('employee') == employee_key)
        logs_query = logs_query.filter(ndb.GenericProperty('loggedFor') >= start_date)
        logs_query = logs_query.filter(ndb.GenericProperty('loggedFor') <= end_date)
        logs = logs_query.iter()

        # serialize logs
        return EmployeeActivityLogSerializer(logs, many=True).data

    def getMessagesForEmployee(self, employee_key, start_date, end_date, *args, **kwargs):
        """
        An API handler method to get the activity messages of an employee in a particular period.

        @param employee_key: String value of the key of employee instance
        @param start_date: date from which hours have to be calculated
        @param end_date: date up to which hours have to be calculated
        @return: Employee messages for the period
        @author:  Jino Jossy
        """
        # prepare params
        start_date = get_date_from_string(start_date)
        end_date = get_date_from_string(end_date)

        # fetch logs
        messages = Message.all().filter("type =", MessageType.END_OF_DAY).filter("user =", employee_key).filter("postedFor >=", start_date).filter("postedFor <=", end_date)

        hoursBurnedWithoutProject = 0.0
        hoursBurnedAgainstProjects = 0.0
        hoursBurnedGroupedByProject = {}
        for message in messages:
            if not message.project:
                hoursBurnedWithoutProject += message.timeSpent
            else:
                hoursBurnedAgainstProjects += message.timeSpent
                if message.project in hoursBurnedGroupedByProject:
                    hoursBurnedGroupedByProject[message.project] += message.timeSpent
                else:
                    hoursBurnedGroupedByProject[message.project] = message.timeSpent

        return {
            'hoursBurnedWithoutProject': float(hoursBurnedWithoutProject),
            'hoursBurnedAgainstProjects': float(hoursBurnedAgainstProjects),
            'hoursBurnedGroupedByProject': hoursBurnedGroupedByProject
        }

    def getProjectHoursBurnedForPeriod(self, project_key, start_date, end_date, *args, **kwargs):
        """
        An API handler method to get the project logs of a project for a particular period.

        @param project_key: String value of the key of project instance
        @param start_date: date from which hours have to be calculated
        @param end_date: date up to which hours have to be calculated
        @return: float value of the number of hours worked
        """
        # prepare params
        start_date = get_date_from_string(start_date)
        end_date = get_date_from_string(end_date)

        # fetch logs
        logs_query = ProjectLog.query()
        logs_query = logs_query.filter(ndb.GenericProperty('project') == project_key)
        logs_query = logs_query.filter(ndb.GenericProperty('loggedFor') >= start_date)
        logs = logs_query.filter(ndb.GenericProperty('loggedFor') <= end_date).iter()

        # calculate hours burned
        hours = 0.0
        for log in logs:
            hours += log.hoursBurned
        return hours

    def getProjectLogs(self, project_keys, *args, **kwargs):
        """
        An API handler method to get the project logs for the project keys passed.
        @param project_keys: List of project keys
        """

        ndb_formatted_keys = [ndb.Key('ProjectLog', key) for key in project_keys]
        project_logs = ndb.get_multi(ndb_formatted_keys)
        project_logs_data = {}
        for project_log in project_logs:
            if project_log:
                project_logs_data[project_log.project] = project_log.groupedByEmployee

        return project_logs_data

    def check_oauth2_token_availability(self, email, *args, **kwargs):
        """
        API to check oauth2 tokens availability
        """
        # by default, we assume that user have valid credentials
        response = True

        # retrieve user credentials
        oauth2_credentials = Oauth2Credentials.all().filter('user =', users.User(email=email)).get()

        # credentials not available
        if oauth2_credentials is None:
            response = False

        # credentials are invalid or corrupt
        try:
            oauth2_credentials.get_valid_access_token()
        except:
            response = False

        return response

    def delete_oauth2_credential(self):
        """
        API to delete oauth2 credentials for logged in user
        :return: Boolean status of the action
        """
        oauth2_credential = Oauth2Credentials.all().filter('user =', users.get_current_user()).get()
        if oauth2_credential:
            db.delete(oauth2_credential)
            return True

        return False

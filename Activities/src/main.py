# python libraries
import logging

# django libraries
from django.dispatch import receiver
from django.core.wsgi import get_wsgi_application
from django.core.signals import got_request_exception


# receiver/handler for got_request_exception signal
@receiver(got_request_exception)
def log_exception(*args, **kwargs):
    logging.exception('Exception occured in request')

# Create a Django application for WSGI.
application = get_wsgi_application()

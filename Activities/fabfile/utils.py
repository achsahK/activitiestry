import functools
# Import smtplib for the actual sending function
import smtplib
import getpass

# Import the email modules we'll need
from email.mime.text import MIMEText
from fabric.api import run, env, local
from fabric.colors import blue
from google.appengine.api import appinfo

from .settings import frm, to


class App(object):
    SPACE = "space"
    ACTIVITIES = "activities"


def include_appcfg(func):
    """Decorator that ensures the current Fabric env has a GAE app.yaml config
    attached to it."""
    @functools.wraps(func)
    def decorated_func(*args, **kwargs):
        if not hasattr(env, 'appcfg'):
            appcfg = appinfo.LoadSingleAppInfo(open(env.gae_src + 'app.yaml'))
            env.appcfg = appcfg
        return func(*args, **kwargs)
    return decorated_func


def send_deployment_email():
    """
    Send deployment notification emails.
    """
    # Create a text/plain message
    msg = MIMEText("""
%s deployed to %s (version: %s) by %s
    """ % (env.branch, env.app, env.version, getpass.getuser()))

    # me == the sender's email address
    # you == the recipient's email address
    msg['Subject'] = 'Space Deployment Notification - %s Deployed to %s (version: %s)' % (env.branch, env.app, env.version)
    msg['From'] = frm
    msg['To'] = ', '.join(to)

    # Send the message via our own SMTP server, but don't include the
    # envelope header.
    s = smtplib.SMTP(env.mail_host, env.mail_port)
    # For GMail
    # http://stackoverflow.com/a/10147497/94354
    # http://stackoverflow.com/a/19769702/94354
    s.starttls()
    s.login(env.mail_user, env.mail_passwd)
    s.sendmail(
        frm,
        to,
        msg.as_string()
    )
    s.quit()

def setup_env_for_system(deploy_to):
    """
    Set up the env variables based on the system to deploy to, currently Space and Activities.
    :param deploy_to:
    """
    env.src = env.activities_src
    env.conf_dir = env.activities_conf_dir


def switch_system(deploy_to):
    """
    Switch to the current active system
    """
    setup_env_for_system(deploy_to=deploy_to)

    local("cp -r %s%s/root/* src/" % (env.conf_dir, env.app))

def update_project(project):
    """
    Update the project before deploying using gcloud
    :param project: The project to be deployed
    """
    local("gcloud config set project %s" % project)

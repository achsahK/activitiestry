from fabric.api import env

# App Engine admin account
env.gae_email = "someone@org.com"
# MailGun Details
env.mailgun_user = "postmaster@sandbox44f6d73225784b54b2ecf1c33ad5e11d.mailgun.org"
env.mailgun_passwd = "6749b3bc7b61bfa96cabc5040db4e610"

# Now Sent to mailgun mailing list.
# To add new people go to MailGun admin, we can also add people to the list below for temporary use
to = ['space-devs@sandbox44f6d73225784b54b2ecf1c33ad5e11d.mailgun.org']

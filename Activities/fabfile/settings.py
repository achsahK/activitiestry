# Global Settings For Fabric
import os
import sys
from os.path import expanduser

from fabric.api import env

# Paths to various folders
env.user_home = expanduser("~")
dev_home = env.user_home + '/Dev/'
env.app_engine_home = dev_home + 'SDK/google_appengine/'
env.project_home = dev_home + 'Space/'

env.datastore_home = env.project_home + '/datastore/dev.datastore'

env.space_directory = env.project_home + 'Space/'
env.activities_directory = env.project_home + 'Activities/'

env.space_src = env.space_directory + 'src/'
env.activities_src = env.activities_directory + 'src/'

env.space_conf_dir = env.space_directory + 'conf/'
env.activities_conf_dir = env.activities_directory + 'conf/'
env.systems = os.listdir(env.activities_conf_dir)

# Email notification details
frm = 'space+dev@qburst.com'
# MailGun mailing list, to add new people visit mailgun.com
to = ['space-devs@sandbox44f6d73225784b54b2ecf1c33ad5e11d.mailgun.org']


# Fix App Engine paths
# This needs to be done everywhere, hence added in settings
def fix_appengine_path():
    EXTRA_PATHS = [
        env.app_engine_home,
        os.path.join(env.app_engine_home, 'lib', 'yaml', 'lib'),
    ]

    sys.path = EXTRA_PATHS + sys.path

fix_appengine_path()

try:
    from .local_settings import *
except:
    pass

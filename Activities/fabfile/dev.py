from fabric.api import cd, env, execute, hide, prefix, prompt, run, task, local
from fabric.contrib.console import confirm
from fabric.decorators import roles
from fabric.colors import blue


@task
def space(app="qburst-space"):
    run_server(env.space_directory)


@task
def spce(*args, **kwargs):
    space(*args, **kwargs)


@task
def activities(app="qburst-activities"):
    run_server(env.activities_directory, port='8181', admin_port='8001')


@task
def act(*args, **kwargs):
    activities(*args, **kwargs)


def run_server(app_directory, port='8080', admin_port='8000'):
    default_opts = "--host=0.0.0.0 --port=%s --admin_port=%s --show_mail_body --require_indexes --enable_sendmail --log_level=info --automatic_restart" % (port, admin_port)
    local(
        "python %s/dev_appserver.py %s --datastore_path=%s %s/src" % (env.app_engine_home, default_opts, env.datastore_home, app_directory),
        capture=False
    )

from fabric.api import cd, env, execute, hide, prefix, prompt, run, task, local
from fabric.contrib.console import confirm
from fabric.decorators import roles
from fabric.colors import blue, green, red

from .utils import *


@task
def deploy(version, branch=None, app=None, promote='y'):
    """
    Deploy to App Engine  (branch=branch name, app=the system to which to deploy to, bknds=update backends or not,act=the action to perform, defaults to "u" for update).

    Usage: fab remote.deploy:version=<version>,branch=<branch_name>,app=<system_to_deploy_to>,bknds=y,promote=y
    Example:
    Deploy master to Activities
    fab remote.deploy:version=4-0-19,branch=master,app=qburst-activities
    Deploy master to Activities and update backends
    fab remote.deploy:version=4-0-19,branch=master,app=qburst-activities-staging,bknds=y
    Update index to Activities demo
    fab remote.deploy:version=4-0-19,branch=master,app=activities-demo,act=i
    Update cron to Activities demo
    fab remote.deploy:version=4-0-19,branch=master,app=activities-demo,act=c

    @param version: The version to use (required)
    @param branch: The branch to deploy (required)
    @param app: Which app to deploy to
    @param promote: whether or not promote the current version
    """
    if not version:
        version = prompt('Enter a version to deploy: ', validate=r'[0-9\-a-z]+')
    env.version = version

    if not branch:
        branch = prompt('Enter a branch to deploy: ', default='master', validate=r'.+')
    env.branch = branch

    if app not in env.systems:
        syss = '|'.join(env.systems)
        app = prompt('Enter a valid system to deploy to (%s): ' % syss, validate=r'(' + syss + ')')
    env.app = app

    promote = promote.lower()

    with hide('running'):
        success = deploy_onto_remote(version=version, promote=promote)

    if success:
        print green("%s deployed successfully to %s!" % (env.branch, env.app), bold=True)
        # All done send notification emails
        send_deployment_email()


def deploy_onto_remote(version, promote):
    """
    Perform the app engine deployment using appcfg.

    @param promote: whether or not promote the current version
    """
    print blue("Please verify the Git Diff for index.yaml below")
    local("git diff live/master src/index.yaml", capture=False)

    if confirm(red("Are You sure that the INDEX is safe for upload?"), default=False):
        switch_system(deploy_to='space')
        update_project(env.app)
        print green("Going to deploy to %s" % env.app)
        cmd = "gcloud app deploy {0}/app.yaml {0}/cron.yaml {0}/queue.yaml {0}/index.yaml --version={1} {2}"
        if promote == 'n' or promote == 'no':
            local(cmd.format(
                env.src,
                version,
                "" # no promote
            ))
        else:
            local(cmd.format(
                    env.src,
                    version,
                    "--promote" # promote to default/serving version
                ))
        return True
    return False
